FROM qgis/qgis:release-3_28 as pip-env

ARG USER_ID
ARG GROUP_ID
RUN groupadd -g ${GROUP_ID} jenkins \
&& useradd --create-home -u ${USER_ID} -g jenkins jenkins
 
RUN apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y curl python3-venv \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

ENV VIRTUALENV=/opt/venv
RUN python3 -m venv --system-site-packages $VIRTUALENV
ENV PATH="$VIRTUALENV/bin:$PATH"
RUN chown -R jenkins:jenkins $VIRTUALENV

COPY requirements.txt requirements.txt
RUN python -m pip install -r requirements.txt
RUN python -m pip install pytest-cov

CMD ["bash"]
