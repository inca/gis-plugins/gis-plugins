======
 INCA
======

INCA consists of two components:

* A Python package which calculates ecosystem service accounts.  The package can be integrated into other Python
  programs, or it can be used directly with a command line interface.
* A QGIS plugin, which provides a graphical interface to configure and run calculations with the Python package.  When
  the plugin is installed in QGIS, it will attempt to automatically install the INCA Python package and all its
  dependencies.

.. contents:: Overview
   :local:

.. highlight:: shell

Repository structure
====================

docs
   Sphinx documentation sources.
inca_plugin
   Contains all source code for the QGIS plugin.
integration_tests
   Sample configuration files with small input data sets to quickly test complete ecosystem accounting modules from the
   command line.
qgis_tools
   A git submodule containing some generic utilities for QGIS plugins.  The subdirectory ``marvin_qgis_tools`` is
   included in the ``inca_plugin`` directory using a symbolic link.
src/inca
   root directory of the inca Python package, which is used by the inca QGIS plugin to perform actual calculations for
   the ecosystem accounts.  The package consists of a different subpackage for each ecosystem service, as well as

   src/inca/common
      Modules with basic functions shared between multiple services (e.g. geoprocessing, NUTS shapefiles, standard
      ecosystem types, ...).

   src/inca/data
      Data files bundled with the package (default vector data, default tables, ...).

tests
   Unit tests for the Python package.

tools
   Helper scripts for development, which don't have to be included in the package.

Coding conventions
==================

The INCA Python package can be used outside of QGIS.  Therefore, modules in src/inca **must not** depend on components of
QGIS or the plugin.

We follow PEP8 coding conventions, but with a 120 character line length limit.

Docstrings should follow the Sphinx/reST format, described at https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html .

To document function argument types, use type annotations instead of adding types to the docstring.

See the documentation of the :py:mod:`inca` module for more information on the code structure.

Development environment
=======================

* We use `setuptools_scm <https://github.com/pypa/setuptools_scm>`_ for versioning.

* To guarantee a smooth installation of the inca package together with our QGIS plugin, we try to use packages which are
  available in OSGeo4W, or which can be installed easily using pip.  For development, it is recommended to set up a
  virtualenv containing only the packages listed in requirements.txt, so you don't accidentally pick up dependencies
  from e.g. your system Python installation.  If you find you need an extra package (i.e. one that is not currently
  included in requirements.txt or the install_requires section of setup.cfg), check if it can be installed in QGIS.

Release checklist
=================

Update the INCA core package
----------------------------

The following steps are needed to create a new version of the INCA core package, and a plugin which relies on that new
version.  Skip these steps if you only want to update the plugin, but not the core INCA package (e.g. changes to the
GUI).

1. Update the attributes ``_inca_min_version`` and ``_inca_next`` in  ``inca_plugin/install_deps.py`` to match the
   version you want to  release (e.g. to use inca '1.0.0', set the minimum version to 1.0.0 and the "next" version to
   1.1.0).  These attributes tell the plugin which versions of the INCA package would be compatible.

2. Commit this version of the code, and tag the git commit with the new version ('1.0.0' in our example).

3. Build a new wheel file for INCA from a clean git checkout of the new release tag.  This should produce a file
   ``inca-tool-<VERSION>-py3-none-any.whl`` (e.g. ``inca-tool-1.0.0-py3-none-any.whl``).

4. Add the INCA wheel file to the package repository.

Update the plugin
-----------------

Create a plugin zip file using ``tools/make_plugin_release.py`` and publish it.

Update the documentation
------------------------

To generate html documentation for the code, make sure Python packges ``sphinx`` and ``sphinx_rtd_theme`` are installed,
and run ::

  python setup.py build_sphinx

To generate pdf documentation, you will also need a LaTeX installation.  Then, navigate to the `docs` subdirectory and
run ::

  make latexpdf

QGIS plugin development
=======================

Getting started
---------------

The plugin uses the package marvin_qgis_tools from the qgis_tools repository at
https://github.com/VITObelgium/qgis_tools.  Code from the qgis_tools repository is integrated here using a
git submodule.  To properly initialize the submodule after a ``git clone``, run the following command ::

    git submodule update --init

After running ``git pull``, you should also check if the submodule needs updating ::

    git submodule update -r

Note that switching to another branch of the inca repository which uses a different version of the qgis_tools submodule,
will not automatically update the qgis_tools submodule to that version.  To switch the submodule contents as well, run
``git submodule update`` once more.  Alternatively, you can configure git to do this automatically for each checkout ::

  git config --global submodule.recurse True

Note for Windows users
----------------------

The qgis_tools submodule is located at ``<repository root>/qgis_tools``.  We use a symbolic link at
``inca_plugin/marvin_qgis_tools`` to integrate the marvin_qgis_tools package in to the plugin.  After a checkout, the
symbolic link may not work correctly on windows.  Some steps that may be required:

* enable symbolic links during git installation, or run ::

    git config --global core.symlinks true

* enable Windows Developer mode

* in an existing checkout, observe the current value of the setting with ::

      git config --show-scope --show-origin core.symlinks

  If a local setting overrides the system-wide setting, remove it with ::

      git config --unset core.symlinks

If the symbolic link still doesn't function properly after these steps, it may help to remove the file
inca_plugin/marvin_qgis_tools, and restore it using ::

  git checkout inca_plugin/marvin_qgis_tools

or ::

  git restore -- inca_plugin/marvin_qgis_tools

If all alse fails, getting a fresh checkout (with ``core.symlinks`` set to true, and developer mode enabled), of the
repository should solve the issue.

make_plugin_release.py
----------------------

The script ``make_plugin_release.py`` can be used to create a zip file which can be installed in QGIS.  The script takes
care of the following:

* Compile the resources.qrc file using pyrcc5.
* Substitute the current commit id in the plugin metadata.txt.  The commit hash of the current HEAD is used, so run from
  a clean checkout to have the hash match the contents...
* Zip the contents of the plugin, skipping files that should not be distributed.

The script should be run from inside the repository directory, and you should test installing the resulting zip file
before publishing it.

Python package repository
=========================

In the last step of the plugin installation, we install the INCA package itself, as well as any remaining dependencies
which we have not installed using OSGeo4W.  To make this step as easy as possible for plugin users, we have to make
sure pre-built packages (in the python 'Wheel' format) are available for all those dependencies.  For the current
version of INCA, this means we have to provide our own package for `RichDEM <https://github.com/r-barnes/richdem>`_,
which contains C++ Python extensions, and for which no pre-built packages are available from the default PyPI package
repository.  To make the installation process simple and cross-platform, we host the inca-tool Python package, as well
as compiled packages for RichDEM, on the terrascope `Artifactory <https://artifactory.vgt.vito.be>`_.

Building RichDEM
----------------

Because RichDEM uses C++ Python extension modules, a different package file is required for each platform (Windows,
MacOS, Linux) and for each minor (3.8, 3.9, ...) Python version.  Therefore, when QGIS switches to a new minor Python
version, we need to add a package for this Python version and for each supported platform (ideally Windows and MacOS) to
our repository.

Windows
~~~~~~~

1. Install the Visual Studio C++ compilers.  If you do not want to install the complete Visual Studio from
   https://visualstudio.microsoft.com/downloads, you can download `Build Tools for Visual Studio
   <https://aka.ms/vs/17/release/vs_BuildTools.exe>`_ separately.

2. Install Python header files from OSGeo4W: run OSGeo4W Setup, search for the package ``python3-devel`` in the Libs
   section, and install it.

3. Install the Python wheel package.  From the OSGeo4W Shell, run ::

     python -m pip install wheel

4. Build a .whl file for RichDEM.  Still in the OSGeo4W Shell, run ::

     python -m pip wheel richdem --no-deps

   If the build succeeds, you should have a file such as ``richdem-0.3.4-cp39-cp39-win_amd64.whl`` in your current
   directory.

MacOS
~~~~~

You can build a wheel file for the official QGIS MacOS bundle using the following steps:

1. Install Xcode command line tools (see https://developer.apple.com/xcode/resources).

2. Obtain Python header files for the QGIS MacOS bundle.  You can obtain these by building the QGIS MacOS dependencies
   yourself, using scripts and instructions at https://github.com/qgis/QGIS-Mac-Packager/tree/master/qgis_deps, or you
   can download and extract the pre-built dependencies from https://qgis.org/downloads/macos/deps.

3. Install the Python wheel package.  From the terminal, run ::

     /Applications/QGIS.app/Contents/MacOS/bin/python3 -m pip install wheel

4. Build a .whl file for RichDEM, and specify the path to the Python header files.  Still from the terminal, run ::

     /Applications/QGIS.app/Contents/MacOS/bin/python3 -m pip wheel richdem --no-deps \
         --global-option=build_ext --global-option="-I/<PATH_TO_QGIS_DEPS>/include/pythonX.Y"

   where ``PATH_TO_QGIS_DEPS`` is the location where the QGIS dependencies from step 2 can be found, and ``pythonX.Y``
   matches the QGIS python version.  If the build succeeds, you should have file such as
   ``richdem-0.3.4-cp39-cp39-macosx_10_13_x86_64.whl`` in your current directory.

Linux
~~~~~

We could use the `manylinux` approach described at https://github.com/pypa/manylinux to build a wheel file that will run
on most Linux systems.  For now, we expect Linux users to compile RichDEM themselves.  The steps needed will depend on
the Linux distribution involved, but likely involve

1. Install C++ compilers.  On Debian or Ubuntu, the package ``build-essential`` should contain everything that's needed.

2. Install Python header files.  On Debian or Ubuntu, these can be installed from the ``python3-dev`` package.

3. Install RichDEM.  When compilers and Python header files are available, run::

     python3 -m pip install richdem

Building INCA
-------------

1. Set up a development environment for INCA.  Make sure packages ``setuptools``, ``setuptools_scm`` and ``wheel`` are
   installed.

2. Build a wheel by running ::

     python -m pip wheel . --no-deps

   from the INCA git root directory (where the file ``setup.py`` is found).

Updating the package repository
-------------------------------

When all required wheel files for INCA and RichDEM are available, we can upload them to the Artifactory package
repository.  In order to do this, you need to register an account at `Terrascope <https://terrascope.be>`_ and request
write permissions for the Artifactory.

Once you have an account with the necessary permissions, you can deploy new package versions manually.

1. Navigate to https://artifactory.vgt.vito.be to access the web app.

2. Open the Artifact Repository Browser, and look for the repository 'python-packages-public'.

3. Deploy inca-tool and/or RichDEM wheel files to the inca-tool and richdem subdirectories, respectively.

4. Wait a few minutes before trying to install the new package versions from the repository, as it can take a minute for
   the update to get processed.

Command line interface
======================

Installing the ``inca-tool`` package will also instal the ``inca`` command line tool defined in :py:mod:`inca.__main__`.
In order to run ``inca``, make sured the location of the installed executable is on your ``PATH`` environment variable.
You can look up the installation directory of the ``inca-tool`` package as follows ::

  python -m pip show inca-tool

The ``Location:`` key contains the location of the ``inca-tool`` package.  The ``inca`` executable is typically located
in the ``bin`` (Unix) or ``Scripts`` (Windows) sibling directory of the package location.  Run ``inca -h`` for an
overview of the different services, or ``inca <service_name> -h`` for an overview of the command line options for that
service.

.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.0.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
