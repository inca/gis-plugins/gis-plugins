pipeline {
    agent {
        dockerfile {
            filename 'Dockerfile'
            dir '.'
            additionalBuildArgs '--build-arg USER_ID=$(id -u) --build-arg GROUP_ID=$(id -g)'
            args '-v ${HOME}/.ssh:${HOME}/.ssh'
        }
    }
    options {
        disableConcurrentBuilds()
    }
    stages {
        stage('Pip install') {
            steps {
                sh '''#!/bin/bash
                      source /opt/venv/bin/activate
                      python -m pip install -e .
                      '''
            }
        }
        stage('Run unit tests') {
            steps {
                sh '''#!/bin/bash
                      source /opt/venv/bin/activate
                      pytest
                      '''
            }
        }
        stage('Run integration tests') {
            steps {
                sh '''#!/bin/bash
                    EXIT_STATUS=0
                    export PYTHONDEVMODE=1
                    cd integration_tests
                    inca global_climate_regulation globalclimateregulation_default-GL_nomonetary.yaml --verbose || EXIT_STATUS=1
                    inca global_climate_regulation globalclimateregulation_default-GLB_nomonetary.yaml --verbose || EXIT_STATUS=1
                    inca global_climate_regulation globalclimateregulation_default-SD_nomonetary.yaml --verbose || EXIT_STATUS=1
                    inca global_climate_regulation globalclimateregulation_default-GL.yaml --verbose || EXIT_STATUS=1
                    inca global_climate_regulation globalclimateregulation_default-GLB.yaml --verbose || EXIT_STATUS=1
                    inca global_climate_regulation globalclimateregulation_default-SD.yaml --verbose || EXIT_STATUS=1
                    inca wood_provision woodprovision.yaml --verbose || EXIT_STATUS=1
                    inca wood_provision woodprovision_nomonetary.yaml --verbose || EXIT_STATUS=1
                    inca flood_control floodcontrol.yaml --verbose || EXIT_STATUS=1
                    inca flood_control floodcontrol_nomonetary.yaml --verbose || EXIT_STATUS=1
                    inca tourism tourism_default1.yaml --verbose || EXIT_STATUS=1
                    inca tourism tourism_default2.yaml --verbose || EXIT_STATUS=1
                    inca crop_provision cropprovision.yaml --verbose || EXIT_STATUS=1
                    inca crop_provision cropprovision_matflow.yaml --verbose || EXIT_STATUS=1
                    inca crop_provision cropprovision_matflow_noyieldproxy.yaml --verbose || EXIT_STATUS=1
                    inca crop_provision cropprovision_nomonetary.yaml --verbose || EXIT_STATUS=1
                    inca air_filtration airfiltration_LAI.yaml --verbose || EXIT_STATUS=1
                    inca air_filtration airfiltration_LUT.yaml --verbose || EXIT_STATUS=1
                    inca soil_retention soilretention.yaml --verbose || EXIT_STATUS=1
                    inca soil_retention soilretention_nomonetary.yaml --verbose || EXIT_STATUS=1
                    inca local_climate_regulation localclimate.yaml --verbose || EXIT_STATUS=1
                    exit $EXIT_STATUS
                    '''
            }
        }
    }
    post {
        always {
            deleteDir()
        }
    }
}
