# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Check if all dependencies are installed, and try to install them automatically if possible.

On a Windows installation based on OSGeo4W, we run the OSGeo4W installer to install rasterio, geopandas, scipy, etc.
On other systems, we present a warning message and expect the user to take care of this installation.
"""
import importlib
import os
import platform
import subprocess
import sys
import tempfile
from importlib.metadata import (  # pragma: no cover
    PackageNotFoundError,
    distributions,
    version,
)
from site import getusersitepackages

from pkg_resources import parse_version
from PyQt5.QtWidgets import QDialog, QMessageBox
from qgis.core import Qgis, QgsMessageLog

from .marvin_qgis_tools import osgeo4w
from .pip_install_dialog import PipInstallDialog

_inca_min_version = "2.1.1"  # Minimum required inca-tool version for the plugin.
_inca_next = "2.1.2"  # Next inca-tool version which may no longer be compatible with this version of the plugin.
_inca_url = "https://artifactory.vgt.vito.be/api/pypi/python-packages/simple"


def get_python_interpreter():
    """Get a suitable interpreter to run external Python processes, depending on the host system."""
    if os.name == "nt":
        # On Windows, use the interpreter from OSGEO4W_ROOT\bin, *not* the one in PYTHONHOME, because that one fails to
        # import the ssl module!
        osgeo4w_root = os.getenv("OSGEO4W_ROOT")
        if osgeo4w_root is not None:
            return os.path.join(osgeo4w_root, "bin", "python3.exe")
        return "python3"  # On windows, not having OSGEO4W_ROOT is unlikely, but just in case...
    elif os.name == "posix":  # Linux, Mac, ...
        pythonhome = os.getenv("PYTHONHOME")
        if (
            pythonhome is not None
        ):  # Normally on Mac, we have PYTHONHOME=/Applications/QGIS[-LTR].app/Contents/MacOS
            return os.path.join(pythonhome, "bin", "python3")
        return "python3"  # Should be ok on Linux
    else:
        QgsMessageLog.logMessage(
            f'os.name "{os.name}" not recognized', level=Qgis.Warning
        )
        return "python3"


def install_pip_deps():
    """Install remaining dependencies using pip."""
    install_dialog = PipInstallDialog()
    install_dialog.message.setText(
        "INCA plugin must install the inca-tool core package (and possible dependencies) "
        "using pip.  OK to download and install?"
    )
    answer = install_dialog.exec()
    if answer != QDialog.Accepted:  # Installation cancelled.
        return False

    proxy_option = []
    if install_dialog.proxyGroup.isChecked():
        proxy_user = install_dialog.proxyUser.text()
        proxy_pass = install_dialog.proxyPass.text()
        proxy_host = install_dialog.proxyHost.text()
        proxy_port = install_dialog.proxyPort.text()
        if len(proxy_user) and len(proxy_pass):
            auth = f"{proxy_user}:{proxy_pass}@"
        else:
            auth = ""
        proxy_option = ["--proxy", f"http://{auth}{proxy_host}:{proxy_port}"]

    python = get_python_interpreter()

    # Due to distribution rename from 'inca' to 'inca-tool', first try to remove existing 'inca' package intalled under
    # the old name:
    try:
        version(
            "inca"
        )  # if we don't get a PackageNotFoundError, an old 'inca' distribution is still installed
        # first remove any old 'inca' packages, if user is upgrading from a version before we renamed to 'inca-tool':
        subprocess.run(
            [f"{python}", "-m", "pip", "uninstall", "-y", "inca"],
            capture_output=True,
            check=True,
            text=True,
        )
    except PackageNotFoundError:  # no 'inca' distribution found, so nothing to do
        pass
    except subprocess.CalledProcessError as e:
        QMessageBox.warning(
            None,
            "INCA Installation",
            "A previous installation of the core INCA package was found and could not be removed."
            f"Exit status: {e.returncode}, see message log for output.",
        )
        QgsMessageLog.logMessage(
            f"pip uninstall failed, stdout: {e.stdout}, stderr: {e.stderr}.",
            level=Qgis.Critical,
        )

    # Generate a constraints file for pip, to prevent pip from *downgrading* existing packages
    # (except existing inca-tool installations):
    with tempfile.NamedTemporaryFile(
        mode="w+t", delete=False, prefix="inca_constraints"
    ) as cf:
        for dist in distributions():
            dist_name = dist.metadata["name"]
            if dist_name == "inca-tool":
                # inca-tool may be downgraded if the user wants to install an older plugin version
                continue
            cf.write(f"{dist_name}>={dist.version}\n")

    # On windows and mac, force installing our richdem binary package
    only_binary_option = []
    if platform.system() in ("Windows", "Darwin"):
        only_binary_option = ["--only-binary", "richdem"]
    try:
        # now install our package
        subprocess.run(
            [
                f"{python}",
                "-m",
                "pip",
                "install",
                "-U",
                "-c",
                cf.name,  # use constraints file to prevent downgrades
                f"inca-tool>={_inca_min_version},<{_inca_next}",
                "--index-url",
                f"{_inca_url}",
                "--extra-index-url",
                "https://pypi.org/simple",
            ]
            + proxy_option
            + only_binary_option,
            capture_output=True,
            check=True,
            text=True,
        )
    except subprocess.CalledProcessError as e:
        QMessageBox.warning(
            None,
            "INCA Installation",
            f"Installation of core INCA package using pip failed.  "
            "INCA may not work correctly\n"
            f"Exit status: {e.returncode}, see message log for output.",
        )
        QgsMessageLog.logMessage(
            f"pip install failed, stdout: {e.stdout}, stderr: {e.stderr}.",
            level=Qgis.Critical,
        )
        return False
    finally:
        # Clean up the constraints file
        os.remove(cf.name)
    return True


def check_dependencies():
    """Try to import required extra packages, and try to install them if it fails."""
    # First try to install packages available from OSGeo4W
    if not osgeo4w.check_packages(
        {
            "geopandas": "python3-geopandas",
            "matplotlib": "python3-matplotlib",
            "rasterio": "python3-rasterio",
            "rtree": "python3-rtree",
            "sklearn": "python3-scikit-learn",
            "scipy": "python3-scipy",
            "pip": "python3-pip",
            "setuptools": "python3-setuptools",
            "statsmodels": "python3-statsmodels",
        }
    ):
        return False

    # Now check installed inca version; if inca is not available or not the right version, run pip.
    run_pip = True
    try:
        installed_version = version("inca-tool")
        QgsMessageLog.logMessage(
            (f"Current inca-tool version: {installed_version}."), level=Qgis.Info
        )
        if parse_version(installed_version) >= parse_version(_inca_min_version):
            run_pip = False  # We have everything we need, in the right version.
    except PackageNotFoundError:
        QgsMessageLog.logMessage(
            "No version of inca core package is currently installed.", level=Qgis.Info
        )

    if run_pip:
        QgsMessageLog.logMessage(
            f"inca-tool version {_inca_min_version} will be installed using pip.",
            level=Qgis.Info,
        )
        if not install_pip_deps():
            QMessageBox.warning(
                None,
                "INCA Installation",
                "The installation is not complete.  The INCA plugin may not "
                "work correctly.",
            )
            return False

    # Check if we can import the right version: If a previous version of inca was already loaded before installation, we
    # have to restart QGIS in order to import the new version.
    #
    # If inca was not installed before, 'import' might still fail due to the importlib cache.  Invalidate the caches to
    # make sure we can import the newly installed inca:
    importlib.invalidate_caches()
    # If the user's site packages directory (e.g. C:\Users\<username>\AppData\... on windows) directory is empty at
    # QGIS startup, the directory is not added to sys.path.  Therefore, if inca-tool was installed in the user's site
    # packages directory, 'import' will also fail.  Therefore, add it to sys path if needed:
    user_site_packages = getusersitepackages()
    if user_site_packages not in sys.path:
        sys.path.append(user_site_packages)

    import inca

    if parse_version(inca.__version__) < parse_version(_inca_min_version):
        QMessageBox.warning(
            None,
            "Restart QGIS",
            "The INCA package was updated.  Please restart QGIS before using the "
            "INCA plugin.",
        )
        return False

    return True
