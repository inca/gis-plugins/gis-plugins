# -*- coding: utf-8 -*-
# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

import configparser
import ctypes
import importlib.resources
import operator
import os
import threading
from functools import reduce

import geopandas as gpd
import yaml
from qgis.PyQt import QtGui, QtWidgets, QtCore, uic
from qgis.core import Qgis, QgsTask, QgsMessageLog, QgsApplication, QgsProject
from qgis.gui import QgsFileWidget
from qgis.utils import iface

import inca
import inca.airfiltration
import inca.common.nuts
import inca.cropprovision
import inca.floodcontrol
import inca.globalclimate
import inca.localclimate
import inca.tourism
import inca.soilretention
import inca.woodprovision
import inca.croppollination
from inca.croppollination import _RADIATION, _DAYLENGTH, _TEMPERATURE, _SUMMER, _CROPS
from inca.airfiltration import YEARLY, SEASONAL, MONTHLY, PM10, PM2P5, LAI_MODEL, TABLE_MODEL
from inca.common.errors import Error, ConfigError
from inca.cropprovision import AGRI_STATS, MAT_FLOW
from inca.globalclimate import EURO_TON_CO2, EURO_TON_C
from inca.globalclimate import STOCK_DIFFERENCE, GAIN_LOSS, GAIN_LOSS_BASE
from .qt_tools import expand_template, writeWidget, readWidget, ShapeChoice
from .wood_metadata_dialog import WoodMetadataDialog

FORM_CLASS, BASE_CLASS = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'inca_plugin_dockwidget_base.ui'))


_inca_tasks = []


class IncaPluginDockWidget(BASE_CLASS, FORM_CLASS):

    closingPlugin = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)

        self.metadata_editors = {
            inca.woodprovision.WoodRun.service_name: WoodMetadataDialog
        }
        self.metadata = {}

        self.toolbar = QtWidgets.QToolBar()
        self.toolbar.setIconSize(iface.iconSize(dockedToolbar=True))
        self.loadact = QtWidgets.QAction(QtGui.QIcon(":/plugins/inca_plugin/mActionFileOpen.svg"),
                                         'Load Configuration', self)
        self.loadact.triggered.connect(self.loadConfig)
        self.saveact = QtWidgets.QAction(QtGui.QIcon(":/plugins/inca_plugin/mActionFileSaveAs.svg"),
                                         'Save Configuration', self)
        self.saveact.triggered.connect(self.saveConfig)
        self.toolbar.addAction(self.loadact)
        self.toolbar.addAction(self.saveact)
        self.toolbar.addSeparator()

        self.metadataact = QtWidgets.QAction('Edit metadata', self)
        self.metadataact.triggered.connect(self.edit_metadata)
        self.toolbar.addAction(self.metadataact)
        self.toolbar.addSeparator()

        self.runact = QtWidgets.QAction(QtGui.QIcon(":/plugins/inca_plugin/play-button-svgrepo-com.svg"), 'Run', self)
        self.runact.triggered.connect(self.run)
        self.toolbar.addAction(self.runact)
        self.toolbar.widgetForAction(self.runact).setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)

        self.continue_run = QtWidgets.QCheckBox('Continue existing run')
        self.continue_run.setObjectName('continue_run')
        self.toolbar.addWidget(self.continue_run)

        spacer = QtWidgets.QWidget()
        spacer.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        self.toolbar.addWidget(spacer)

        self.aboutact = QtWidgets.QAction(QtGui.QIcon(":/plugins/inca_plugin/inca_icon.svg"), 'About', self)
        self.aboutact.triggered.connect(self.about)
        self.toolbar.addAction(self.aboutact)
        self.toolbar.widgetForAction(self.aboutact).setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)

        self.toolbarLayout.addWidget(self.toolbar)

        self.reporting_choice = ShapeChoice(self.reporting_areas_nuts_level, self.reporting_areas_filewidget,
                                            self.reporting_radio_nuts, self.reporting_radio_custom)
        self.data_choice = ShapeChoice(self.data_areas_nuts_level, self.data_areas_filewidget,
                                       self.data_radio_nuts, self.data_radio_custom)
        # we need to set QObject names in order to load/save settings together with a QGIS project.
        self.reporting_choice.setObjectName('reporting_choice')
        self.data_choice.setObjectName('data_choice')

        self.fill_month_selection()

        self.set_up_config_templates()

        self.current_reporting_shape = None
        self.reporting_file_clicked = False

        data_line = self.data_areas_filewidget.lineEdit()
        data_line.setNullValue('')
        data_line.setValue(None)
        data_line.setShowClearButton(True)

        reporting_line = self.reporting_areas_filewidget.lineEdit()
        reporting_line.setNullValue('')
        reporting_line.setValue(None)
        reporting_line.setShowClearButton(True)

        reclassification_line = self.land_classification.lineEdit()
        reclassification_line.setNullValue('INCA default mapping for CORINE data')
        reclassification_line.setValue(None)
        reclassification_line.setShowClearButton(True)

        typeofareacontribution_line = self.tourism_typeofareacontribution.lineEdit()
        typeofareacontribution_line.setNullValue('INCA default area type contribution factors')
        typeofareacontribution_line.setValue(None)

        ecosystemweights_line = self.tourism_ecosystemweights.lineEdit()
        ecosystemweights_line.setNullValue('INCA default ecosystem weights')
        ecosystemweights_line.setValue(None)

        accessibilityweights_line = self.tourism_accessibilityweights.lineEdit()
        accessibilityweights_line.setNullValue('Use map values as weights.')
        accessibilityweights_line.setValue(None)

        facilitiesweights_line = self.tourism_facilitiesweights.lineEdit()
        facilitiesweights_line.setNullValue('Use map values as weights.')
        facilitiesweights_line.setValue(None)

        landscapeattractivenessweights_line = self.tourism_landscapeattractivenessweights.lineEdit()
        landscapeattractivenessweights_line.setNullValue('Use map values as weights.')
        landscapeattractivenessweights_line.setValue(None)

        self.carbon_retention_operation_mode.addItem('Gain-loss baseline stock', GAIN_LOSS_BASE)
        self.carbon_retention_operation_mode.addItem('Stock-difference', STOCK_DIFFERENCE)
        self.carbon_retention_operation_mode.addItem('Gain-loss', GAIN_LOSS)
        self.carbon_retention_operation_mode.currentTextChanged.connect(self.update_carbon_retention_widgets)
        self.update_carbon_retention_widgets()

        # For carbon sequestration, only one of these should be provided:
        make_mutually_exclusive(self.carbon_unmanaged_forest_map, self.carbon_unmanaged_forest_table)
        make_mutually_exclusive(self.carbon_unmanaged_wetland_map, self.carbon_unmanaged_wetland_table)
        make_mutually_exclusive(self.carbon_unreported_peatland_map, self.carbon_unreported_peatland_table)

        self.crop_operation_mode.addItem('Material flow', MAT_FLOW)
        self.crop_operation_mode.addItem('Agricultural statistics', AGRI_STATS)
        self.crop_operation_mode.currentTextChanged.connect(self.update_crop_yield_widget)
        self.update_crop_yield_widget()
        self.crop_lookup_table.lineEdit().setNullValue('INCA default lookup table')
        self.crop_lookup_table.lineEdit().setValue(None)

        self.air_pollution_type.addItem('PM 2.5', PM2P5)
        self.air_pollution_type.addItem('PM 10', PM10)

        self.lai_model_cbbox.addItem('LAI Model', LAI_MODEL)
        self.lai_model_cbbox.addItem('Table Model', TABLE_MODEL)

        self.air_input_frequency.addItem('Monthly', MONTHLY)
        self.air_input_frequency.addItem('Seasonal', SEASONAL)
        self.air_input_frequency.addItem('Yearly', YEARLY)

        QgsProject.instance().readProject.connect(self.loadSettings)
        QgsProject.instance().writeProject.connect(self.saveSettings)

        self.service.currentTextChanged.connect(self.service_changed)

        # Always initialize UI so the service combobox and the service input stackwidget are on the first page
        # (otherwise, the UI could be inconsistent if the inca_plugin_dockwidget_base.ui file was saved while another
        # page was open):
        self.service.setCurrentIndex(0)
        self.serviceInputs.setCurrentIndex(0)

        self.connect_reporting_shape_choice()

        self.set_optional_filewidgets()

        self.loadSettings()

    def about(self):
        """Display a QMessageBox with the 'about' text from the plugin metadata."""
        # We use the built-in ConfigParser class to read the metadata.txt file for our plugin.
        # We should also be able to access the metadata with
        #   iface.pluginManagerInterface().pluginMetadata('inca_plugin')
        # but this method sometimes returns 'None' for reasons I do not currently understand...
        config = configparser.ConfigParser()
        config.read(importlib.resources.files('inca_plugin').joinpath('metadata.txt'))
        about = config['general']['about']
        QtWidgets.QMessageBox.information(self, 'About INCA', about)

    def update_carbon_retention_widgets(self):
        """Set up signals such that widgets for carbon sequestration are enabled / disabled as required by the chosen
         calculation method."""
        # get operation mode from user_data = template.currentData(QtCore.Qt.UserRole)
        retention_mode = self.carbon_retention_operation_mode.currentData(QtCore.Qt.UserRole)

        widgets_for_mode = {STOCK_DIFFERENCE: [self.carbon_closing_stock_table, self.carbon_closing_stock_proxy],
                            GAIN_LOSS: [self.carbon_opening_stock_table, self.carbon_opening_stock_proxy,
                                        self.carbon_harvest_wood_proxy],
                            GAIN_LOSS_BASE: [self.carbon_stock_table, self.carbon_harvest_wood_proxy]}

        for widget in (self.carbon_closing_stock_table, self.carbon_closing_stock_proxy,
                       self.carbon_opening_stock_table, self.carbon_opening_stock_proxy,
                       self.carbon_stock_table, self.carbon_harvest_wood_proxy):
            widget.setEnabled(widget in widgets_for_mode[retention_mode])

    def update_crop_yield_widget(self):
        """Set up the file filter for the crop_yield widget so it is pre-filtered to 'apro_cpsh1.tsv' for agricultural
        statistics mode, and 'env_ac_mfa.tsv' for material flow mode."""
        crop_mode = self.crop_operation_mode.currentData(QtCore.Qt.UserRole)

        if crop_mode == MAT_FLOW:
            file_filter = "env_ac_mfa (env_ac_mfa.tsv);; TSV (*.tsv);; All Files (*.*)"
        elif crop_mode == AGRI_STATS:
            file_filter = "apro_cpsh1 (apro_cpsh1.tsv);; TSV (*.tsv);; All Files (*.*)"
        else:
            file_filter = "TSV (*.tsv);; All Files (*.*)"

        self.crop_yield.setFilter(file_filter)

    def connect_reporting_shape_choice(self):
        """Signals and slots to update the region selection combobox when a new reporting shapefile is selected."""
        # We want to update the list of regions whenever a new shape file was set by the user, but only when the
        # user has finished editing (i.e. don't attempt to load a new NUTS shape while the user is still typing the
        # filename).  The file can be changed in the following ways:
        # - user changes the radio button selection between custom or built-in NUTS shape
        self.reporting_radio_custom.toggled.connect(self.updateRegions)
        self.reporting_radio_nuts.toggled.connect(self.updateRegions)
        # - user changes the combobox selection
        self.reporting_areas_nuts_level.currentTextChanged.connect(self.updateRegions)
        # - user edits the text using the widget's LineEdit -> use the editingFinished signal for this
        self.reporting_areas_filewidget.lineEdit().editingFinished.connect(self.updateRegions)
        # - user clicks the button and selects a file using the menu -> there's no simple method (e.g. a signal) to
        #   detect this, so  we use a slightly hacky workaround:
        # 1. find the QgsFileWidget button using findChildren(QtWidgets.QAbstractButton) -- findChildren returns a list,
        #    but there will be exactly one child object of type QAbstractButton -- and listen for the 'pressed' signal
        #    to set a flag when the user has clicked the button.
        for x in self.reporting_areas_filewidget.findChildren(QtWidgets.QAbstractButton):
            x.pressed.connect(self.nuts_file_buttonclicked)
        # 2. listen for the LineEdit's textChanged signal to detect updates to the filename, but only update the menu
        #    when the button clicked flag is set. (The LineEdit also sends a textChanged signal whenever the user types
        #    in the lineEdit, but we don't want to update the list of regions while the user is still typing.)
        self.reporting_areas_filewidget.lineEdit().textChanged.connect(self.nuts_file_textchanged)
        # 3. handle special case when the user clicks the button but then clicks cancel, so no textChanged signal is
        #    sent, and then edits the filename by typing.
        QgsApplication.instance().focusChanged.connect(self.focuschanged)

    def nuts_file_textchanged(self):
        """Update NUTS region list after the user has selected a file using the QgsFileWidget button."""
        # We *only* want to handle textChanged if it's the consequence of clicking the QgsFileWidget button.  If the
        # button wasn't clicked, this signal means the user is busy typing a file name in the line edit, and we do not
        # want to update the available regions on every keypress (instead, we listen for editingFinished).
        if self.reporting_file_clicked:
            self.reporting_file_clicked = False
            self.updateRegions()

    def nuts_file_buttonclicked(self):
        """Detect when the user opens the NUTS file selection dialog."""
        self.reporting_file_clicked = True

    def focuschanged(self, old, new):
        # We handle the following special case:
        # 1) user clicks the file button for the NUTS shape
        # 2) user clicks cancel (thus, the file remains the same, but our NUTS_file_clicked flag will be set to True)
        # 3) [... arbitrary number of other user interactions in between...]
        # 4) at a later stage, the user clicks the line edit and changes the text.
        #
        # -> if our NUTS_file_clicked flag is still True, editing the text will trigger an update of the NUTS region
        # list after the first keypress.  Therefore, we set the NUTS_file_clicked to False when the NUTS file lineEdit
        # receives focus.
        if new == self.reporting_areas_filewidget.lineEdit():
            self.reporting_file_clicked = False

    def service_changed(self, service_name):
        # If we have metadata editor for this service, enable the button
        self.metadataact.setEnabled(service_name in self.metadata_editors)

    def fill_month_selection(self):
        """Fill the month selection combobox with month names."""
        self.croppol_param_short_summer_months.clear()
        self.croppol_param_long_summer_months.clear()
        for month in range(1, 13):
            self.croppol_param_short_summer_months.addItem(QtCore.QDate.longMonthName(month), month)
            self.croppol_param_long_summer_months.addItem(QtCore.QDate.longMonthName(month), month)
            self.croppol_param_short_summer_months.setCheckedItems([QtCore.QDate.longMonthName(m) for m in [4, 5, 6, 7, 8, 9]])
            self.croppol_param_long_summer_months.setCheckedItems([QtCore.QDate.longMonthName(m) for m in [4, 5, 6, 7, 8, 9]])

    def updateRegions(self):
        new_reporting_choice = self.reporting_choice.value()
        if self.current_reporting_shape == new_reporting_choice:
            # Do nothing if the NUTS file after editing is the same as the one before.
            return

        nuts_levels = {'NUTS-0': 0, 'NUTS-1': 1, 'NUTS-2': 2}
        level = nuts_levels.get(new_reporting_choice)
        if level is not None:
            regions = list(inca.common.nuts.get_nuts_shape(level).index)
        elif new_reporting_choice:
            try:
                regions = gpd.read_file(new_reporting_choice, ignore_geometry=True)['NUTS_ID']
            except BaseException:
                QgsMessageLog.logMessage(f'Failed to read NUTS_ID\'s from file "{new_reporting_choice}"',
                                         level=Qgis.Critical)
                self.reporting_areas_filewidget.lineEdit().setStyleSheet('border: 2px solid red')
                return
        else:
            regions = []
        self.selected_regions.clear()
        for nuts_id in sorted(regions):
            self.selected_regions.addItem(nuts_id)
        self.current_reporting_shape = new_reporting_choice
        self.selected_regions.selectAllOptions()
        self.selected_regions.repaint()

    def set_optional_filewidgets(self):
        """Set a null value text of 'None' for QgsFileWidgets for optional inputs, to indicate that leaving them empty
        is a valid option."""
        for widget in (self.wood_proxy_weights,
                       self.tourism_data_area_map,
                       self.tourism_data_area_table,
                       self.tourism_accessibilitymap,
                       self.tourism_facilitiesmap,
                       self.tourism_landscapeattractivenessmap,
                       self.carbon_unmanaged_forest_map,
                       self.carbon_unmanaged_forest_table,
                       self.carbon_unmanaged_wetland_map,
                       self.carbon_unmanaged_wetland_table,
                       self.carbon_unreported_peatland_map,
                       self.carbon_unreported_peatland_table,
                       self.carbon_opening_stock_proxy,
                       self.carbon_harvest_wood_proxy,
                       self.carbon_closing_stock_proxy,
                       self.aquatic_ecosystems_csv,
                       self.crop_type_proxy,
                       self.crop_yield_proxy,
                       self.crop_productivity_proxy,
                       self.croppol_SGSI_long,
                       self.croppol_total_yield,
                       self.croppol_croptypemap,
                       self.croppol_APPL,
                       self.croppol_CITR,
                       self.croppol_OFRU,
                       self.croppol_OOIL,
                       self.croppol_PULS,
                       self.croppol_RAPE,
                       self.croppol_SOYA,
                       self.croppol_SUNF,
                       self.croppol_TEXT,
                       self.croppol_TOMA
                       ):
            line_edit = widget.lineEdit()
            line_edit.setNullValue('Optional')
            line_edit.setValue(None)

    def list_widgets(self, element):
        result = []
        if isinstance(element, QtCore.QObject):
            result.append(element)
        elif isinstance(element, dict):
            for x in element.values():
                result = result + self.list_widgets(x)
        elif isinstance(element, list):
            for x in element:
                result = result + self.list_widgets(x)
        else:
            pass
        return result

    def list_config_widgets(self):
        return self.list_widgets(self.main_config_template) + self.list_widgets(self.service_config_templates)

    def loadSettings(self):
        proj = QgsProject.instance()
        for widget in self.list_config_widgets():
            value, ok = proj.readEntry('inca', widget.objectName())
            if ok:
                writeWidget(widget, value)
            else:
                QgsMessageLog.logMessage(f'Project has no settings for widget {widget.objectName()}.', level=Qgis.Info)
        self.updateRegions()

    def saveSettings(self):
        proj = QgsProject.instance()
        for widget in self.list_config_widgets():
            proj.writeEntry('inca', widget.objectName(), str(readWidget(widget)))

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

    def saveConfig(self):
        """Save current ui state as a yaml config file."""
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save Config File", "config.yaml", "*.yaml")
        if not filename:
            return
        with open(filename, 'wt') as f:
            f.write(yaml.dump(self.make_inca_config()))

    def loadConfig(self):
        """Load a yaml config file."""
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Load Config File", "",
                                                            "Config files (*.yaml);; All files (*.*)")
        if not filename:
            return

        try:
            with open(filename, 'rt') as f:
                config = yaml.safe_load(f)

            # Clear existing configuration:
            for widget in self.list_config_widgets():
                writeWidget(widget, None)

            # Handle year selection
            self.year.setValue(config['years'][0])
            # need to set the reporting shape here so selected regions combobox is populated when we select regions:
            self.reporting_choice.set(config.get('reporting_shape'))
            self.updateRegions()
            self.fill_month_selection()
            # remaining config values can be read automatically using the config templates
            main_template = {key: value for key, value in self.main_config_template.items()
                             if key not in ('years', 'reporting_shape')}
            service_template = self.service_config_templates[config['service']]

            self.load_template(config, main_template)
            self.load_template(config, service_template)
        except BaseException as e:
            QtWidgets.QMessageBox.critical(self, 'Error loading config', f'Could not load config {filename}: {e}.')

        if 'metadata' in config:
            self.metadata[config['service']] = config['metadata'][config['years'][0]]

    def load_template(self, config, template):
        """Update the widgets listed in the template with values from config.

        We use recursion to process te nested dictionary structure of the template and config.  This is more or less
        the inverse of expand_template().

        NOTE: Assumes widget self.year was already set to the correct value."""
        if type(template) == dict:
            for key, template_value in template.items():
                # special case for yearly datasets ...
                if key == self.year:
                    key = self.year.value()
                try:
                    self.load_template(config[key], template_value)
                except KeyError as e:
                    QgsMessageLog.logMessage(f'While loading config: missing config key {e}.', level=Qgis.Warning)
        elif type(template) in (int, bool, str, float):
            # We're only interested in the widgets referenced from the template.
            pass
        elif isinstance(template, QtCore.QObject):
            writeWidget(template, config)
        else:
            raise RuntimeError(f'Failed to process template {template}.')

    def set_up_config_templates(self):
        """Create a mapping between INCA config keys and corresponding interface widgets.

        This mapping is used to transform the UI state into an INCA config.
        """
        self.main_config_template = {
            'service': self.service,
            'output_dir': self.output_dir,
            'years': [self.year],
            'continue': self.continue_run,
            'statistics_shape': self.data_choice,
            'reporting_shape': self.reporting_choice,
            'selected_regions': self.selected_regions,
            'land_cover': {self.year: self.land_cover},
            'land_classification': self.land_classification,
        }
        self.service_config_templates = {
            inca.soilretention.SoilRun.service_name: {
                'run_name': self.soil_retention_run_name,
                inca.soilretention.SoilRun.service_name: {
                    'parameters': {
                        'avg_soil_formation': self.soil_formation_factor},
                    'biophysical': {
                        'cfactor': {self.year: self.cfactor},
                        'k-factor': self.kfactor,
                        'ls-factor': self.lsfactor,
                        'p-factor': self.pfactor,
                        'r-factor': self.rfactor}}},
            inca.floodcontrol.FloodRun.service_name: {
                'run_name': self.floodcontrol_run_name,
                inca.floodcontrol.FloodRun.service_name: {
                    'slope': self.floodcontrol_slope,
                    'hydro_soil': self.floodcontrol_hydro_soil,
                    'impervious_density': {self.year: self.floodcontrol_impervious},
                    'soil_land_cn': self.floodcontrol_cn,
                    'riparian_zones': self.floodcontrol_riparian,
                    'dem': self.floodcontrol_dem,
                    'flood_plains': self.floodcontrol_floodplains,
                    'land_econ_unit': self.floodcontrol_econ_unit,
                    'catchments': self.floodcontrol_catchments,
                    'population': {self.year: self.floodcontrol_population},
                    'hydro_basin': self.floodcontrol_hydro_basin
                }},
            inca.cropprovision.CropRun.service_name: {
                'run_name': self.crop_run_name,
                inca.cropprovision.CropRun.service_name: {
                    'operation-mode': self.crop_operation_mode,
                    'lut_crops': self.crop_lookup_table,
                    'crop_yield': self.crop_yield,
                    'proxy_cropyield': {self.year: self.crop_yield_proxy},
                    'proxy_croptype': {self.year: self.crop_type_proxy},
                    'proxy_productivity': {self.year: self.crop_productivity_proxy},
                }
            },
            inca.globalclimate.GlobalClimateRun.service_name: {
                'run_name': self.carbon_run_name,
                inca.globalclimate.GlobalClimateRun.service_name: {
                    'carbon-net-sequestration': {
                        'env_air_gge': self.carbon_env_air_gge,
                        'productivity_proxy_map': {self.year: self.carbon_productivity_map},
                        'unmanaged_forest_map': {self.year: self.carbon_unmanaged_forest_map},
                        'unmanaged_forest_table': {self.year: self.carbon_unmanaged_forest_table},
                        'unmanaged_wetland_map': {self.year: self.carbon_unmanaged_wetland_map},
                        'unmanaged_wetland_table': {self.year: self.carbon_unmanaged_wetland_table},
                        'non-reported_peatland_map': {self.year: self.carbon_unreported_peatland_map},
                        'non-reported_peatland_table': {self.year: self.carbon_unreported_peatland_table}},
                    'carbon-retention': {
                        'reten_operation_mode': self.carbon_retention_operation_mode,
                        'opening_carbon_stock_table': {self.year: self.carbon_opening_stock_table},
                        'opening_carbon_stock_proxy_map': {self.year: self.carbon_opening_stock_proxy},
                        'carbon_stock_lookup_table': {self.year: self.carbon_stock_table},
                        'harvest_wood_proxy_map': {self.year: self.carbon_harvest_wood_proxy},
                        'closing_carbon_stock_table': {self.year: self.carbon_closing_stock_table},
                        'closing_carbon_stock_proxy_map': {self.year: self.carbon_closing_stock_proxy}}}},
            inca.localclimate.LocalClimateRun.service_name: {
                'run_name': self.local_climate_run_name,
                inca.localclimate.LocalClimateRun.service_name: {
                    inca.localclimate.STATION_DATA: self.local_climate_station_data,
                    inca.localclimate.LST: {self.year: self.local_climate_lst},
                    inca.localclimate.TREECOVER: self.local_climate_tcd,
                    inca.localclimate.EVEG: self.local_climate_eveg,
                    inca.localclimate.URBAN_AREAS: self.local_climate_urban_areas
                }},
            inca.tourism.TourismRun.service_name: {
                'run_name': self.tourism_run_name,
                inca.tourism.TourismRun.service_name: {
                    'base_visits_data': self.tourism_overnightstays,
                    'typeAreaContributionTable': self.tourism_typeofareacontribution,
                    'data_area_cat_map': self.tourism_data_area_map,
                    'data_area_table': self.tourism_data_area_table,
                    'step3_ecosystemTypeAttractiveness_table': self.tourism_ecosystemweights,
                    'accessibility_table': self.tourism_accessibilityweights,
                    'facilities_table': self.tourism_facilitiesweights,
                    'attractiveness_table': self.tourism_landscapeattractivenessweights,
                    # accessibility/facilities/attractiveness map widgets serve a double purpose.  We select either the
                    # categorical map or the weight map in make_tourism_proxy_settings()
                    'accessibility_cat_map': {self.year: self.tourism_accessibilitymap},
                    'accessibility_weight_map': {self.year: self.tourism_accessibilitymap},
                    'facilities_cat_map': {self.year: self.tourism_facilitiesmap},
                    'facilities_weight_map': {self.year: self.tourism_facilitiesmap},
                    'attractiveness_cat_map': {self.year: self.tourism_landscapeattractivenessmap},
                    'attractiveness_weight_map': {self.year: self.tourism_landscapeattractivenessmap},
                }
            },
            inca.woodprovision.WoodRun.service_name: {
                'run_name': self.wood_run_name,
                inca.woodprovision.WoodRun.service_name: {
                    'for_vol_efa': self.wood_for_vol_efa,
                    'proxy': {self.year: self.wood_proxy},
                    'proxy_weights': {self.year: self.wood_proxy_weights}
                }},
            inca.airfiltration.AirFiltrationRun.service_name: {
                'run_name': self.air_run_name,
                inca.airfiltration.AirFiltrationRun.service_name: {
                    'lai_map': {self.year: self.air_lai_dir},
                    'pollution_map': {self.year: self.air_pollution_dir},
                    'wind_map': {self.year: self.air_wind_dir},
                    'ecotyping_map': {self.year: self.ecotyping_map_file},
                    'pollution_type': self.air_pollution_type,
                    'model': self.lai_model_cbbox,
                    'aggregate_type': self.air_input_frequency,
                    'wind_table': self.wind_table_csv,
                    'deposition_table': self.deposition_table_csv,
                    'aquatic_ecosystems': self.aquatic_ecosystems_csv
                }
            },
            inca.croppollination.PollinationRun.service_name: {
                'run_name': self.croppol_run_name,
                inca.croppollination.PollinationRun.service_name: {
                    'lc_table': self.croppol_lc_table,
                    'meteo_data': {_RADIATION: {self.year: self.croppol_monthly_radiation},
                                   _DAYLENGTH: {self.year: self.croppol_monthly_daylength},
                                   _TEMPERATURE: {self.year: self.croppol_monthly_mean_temperature}
                                   },
                    'osm_mask': {self.year: self.croppol_osm_mask},
                    'total_yield' : {self.year: self.croppol_total_yield},
                    'croptypemap' : {self.year: self.croppol_croptypemap},
                    'SGSI': {'short': {self.year: self.croppol_SGSI_short},
                             'long': {self.year: self.croppol_SGSI_long}},
                    'path_LUT_contribution': self.croppol_path_LUT_contribution,
                    'parameters': {'long': {'SGSI_weight' : float(self.croppol_param_long_SGSI_weight.text()),
                                            'SPA_threshold': float(self.croppol_param_long_SPA_threshold.text()),
                                            'distance': float(self.croppol_param_long_distance.text()),
                                            'edge_distance': float(self.croppol_param_long_edge_distance.text()),
                                            _SUMMER: [self.croppol_param_long_summer_months.itemData(i) for i in range(self.croppol_param_long_summer_months.model().rowCount())
                                                      if self.croppol_param_long_summer_months.model().item(i).checkState() == QtCore.Qt.Checked]},
                                   'short': {'SGSI_weight' : float(self.croppol_param_short_SGSI_weight.text()),
                                             'SPA_threshold': float(self.croppol_param_short_SPA_threshold.text()),
                                             'distance': float(self.croppol_param_short_distance.text()),
                                             'edge_distance': float(self.croppol_param_short_edge_distance.text()),
                                             _SUMMER: [self.croppol_param_short_summer_months.itemData(i) for i in range(self.croppol_param_short_summer_months.model().rowCount())
                                                       if self.croppol_param_short_summer_months.model().item(i).checkState() == QtCore.Qt.Checked]}
                                   },
                    'path_yields': {self.year: {'APPL': self.croppol_APPL,
                                                'CITR': self.croppol_CITR,
                                                'OFRU': self.croppol_OFRU,
                                                'OOIL': self.croppol_OOIL,
                                                'PULS': self.croppol_PULS,
                                                'RAPE': self.croppol_RAPE,
                                                'SOYA': self.croppol_SOYA,
                                                'SUNF': self.croppol_SUNF,
                                                'TEXT': self.croppol_TEXT,
                                                'TOMA': self.croppol_TOMA
                                                }
                                    }
                }
            }
        }

    def make_template(self):
        """Create an INCA config template based on the current ui settings."""
        service = self.service.currentText()

        # Combine the main config template with the specific template for the selected service:
        return {
            **self.main_config_template,
            **self.service_config_templates[service]
        }

    def make_inca_config(self):
        """Create an INCA config dict based on the current ui settings."""
        # Read settings from widgets:
        config_dict = expand_template(self.make_template())
        # Add metadata for this service if it exists:
        config_dict['metadata'] = {self.year.value(): self.metadata.get(config_dict['service'], None)}
        # Special case for tourism:
        tourism_config = config_dict.get(inca.tourism.TourismRun.service_name)
        if tourism_config:
            make_tourism_proxy_settings(tourism_config)

        return config_dict

    def expand_year(self, template):
        """Recursively replace all occurrences of the self.year widget as a dict key by the year value.

        This is needed so we can look up widgets by their config path when we catch a
        ConfigError."""
        if type(template) == dict:  # nested dictionary
            result = {}
            for key, value in template.items():
                result_val = self.expand_year(value)
                if key is self.year:
                    result[self.year.value()] = result_val
                else:
                    result[key] = result_val
            return result
        else:
            return template

    def run(self):
        if not self.edit_metadata():  # User clicked cancel -> don't start run.
            return

        config = self.make_inca_config()
        widget_dict = self.expand_year(self.make_template())

        runtypes = {run_class.service_name: run_class for run_class in
                    (inca.woodprovision.WoodRun, inca.cropprovision.CropRun,
                     inca.globalclimate.GlobalClimateRun, inca.localclimate.LocalClimateRun,
                     inca.tourism.TourismRun, inca.soilretention.SoilRun, inca.floodcontrol.FloodRun,
                     inca.airfiltration.AirFiltrationRun, inca.croppollination.PollinationRun)}

        year = self.year.value()
        output_rasters = {inca.cropprovision.CropRun.service_name:
                              [os.path.join('maps', f'crop-provision_map_use_tonne_{year}.tif')],
                          inca.woodprovision.WoodRun.service_name:
                              [os.path.join('maps', f'wood-provision_map_use_1000-m3_{year}_FAWS.tif'),
                               os.path.join('maps', f'wood-provision_map_use_1000-m3_{year}_OWL_AWS.tif')],
                          inca.tourism.TourismRun.service_name:
                              [os.path.join('maps', f'tourism_map_supply_amountOvernightStays-foreign_{year}.tif'),
                               os.path.join('maps', f'tourism_map_supply_amountOvernightStays-national_{year}.tif'),
                               os.path.join('maps', f'tourism_map_supply_amountOvernightStays_{year}.tif')],
                          inca.soilretention.SoilRun.service_name:
                              [os.path.join('maps', f'Soil-retention_map_use_tonnes_{year}.tif')],
                          inca.globalclimate.GlobalClimateRun.service_name:
                              [os.path.join('maps', f'carbon-net-sequestration_map_use_tonnes_{year}.tif'),
                               os.path.join('maps', f'carbon-retention_map_closing-stock_tonnes_{year}.tif')],
                          inca.floodcontrol.FloodRun.service_name:
                              [os.path.join('maps', f'flood-control_map_use_hectare_{year}.tif')],
                          inca.airfiltration.AirFiltrationRun.service_name:
                              [os.path.join('maps', f'air-filtration_map_use_tonnes_{year}.tif')],
                          inca.croppollination.PollinationRun.service_name:
                              [os.path.join('maps', f'crop-pollination_map_supply_tonnes_{year}.tif'),
                              os.path.join('maps', f'crop-pollination_map_use_tonnes_{year}.tif')],
                          inca.localclimate.LocalClimateRun.service_name:
                              [os.path.join('maps', f'Local-Climate-Regulation_cooling_degree_{year}.tif')]
                          }

        service = self.service.currentText()
        task = IncaTask(f'{service} run {config["run_name"]}', config, widget_dict, runtypes[service],
                        output_rasters=output_rasters.get(service, []))
        _inca_tasks.append(task)

        QgsMessageLog.logMessage('Submitting INCA task', level=Qgis.Info)
        QgsApplication.taskManager().addTask(task)

    def edit_metadata(self):
        service_name = self.service.currentText()
        editor = self.metadata_editors.get(service_name)
        if editor is None:  # No editor for this service -> nothing to be done
            return True
        dialog = editor(self)  # pass window instance to editor constructor so we have access to widgets from there
        if dialog.exec() == QtWidgets.QDialog.Accepted:
            self.metadata[service_name] = expand_template(dialog.metadata_template())
            return True
        else:  # 'Cancel' clicked
            return False


class IncaTask(QgsTask):

    def __init__(self, description, config, widget_dict, runtype, output_rasters=[]):
        super().__init__(description)
        self.config = config
        self.widget_dict = widget_dict
        self.runtype = runtype
        self.output_rasters = output_rasters
        self.run_thread = None

    def cancel(self):
        """If the task is canceled, raise an inca.Cancelled exception in the run thread to stop it."""
        super().cancel()
        if self.run_thread is not None:
            # Use the Python C API to raise an exception in another thread:
            ret = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_ulong(self.run_thread.ident),
                                                             ctypes.py_object(inca.Cancelled))
            # ref: http://docs.python.org/c-api/init.html#PyThreadState_SetAsyncExc
            if ret == 0:
                QgsMessageLog.logMessage('Failed to cancel INCA run thread.', level=Qgis.Critical)

    def run(self):
        try:
            self.run_thread = threading.current_thread()  # save current thread so we can stop it if task is canceled.
            run = self.runtype(self.config)
            run.start(progress_callback=self.setProgress)
            return True
        except Exception as e:
            self.exception = e
            return False

    def finished(self, result):
        if result:
            QgsMessageLog.logMessage('INCA task completed', level=Qgis.Info)
            for raster in self.output_rasters:
                path = os.path.join(self.config['output_dir'], self.config['run_name'], raster)
                iface.addRasterLayer(path)
        else:
            if self.exception is None:
                QgsMessageLog.logMessage('INCA task failed for unknown reason')
            elif isinstance(self.exception, inca.Cancelled):
                QtWidgets.QMessageBox.information(iface.mainWindow(), 'Cancelled', 'Run was cancelled by user.')
            elif isinstance(self.exception, ConfigError):
                widget = reduce(operator.getitem, self.exception.path, self.widget_dict)
                if isinstance(widget, QgsFileWidget):  # TODO clean up!
                    widget = widget.lineEdit()
                # widget.setStyleSheet('border: 1px solid red')
                QtWidgets.QMessageBox.warning(iface.mainWindow(), 'INCA configuration error', self.exception.message)
                # widget.setStyleSheet('')
            elif isinstance(self.exception, Error):
                QtWidgets.QMessageBox.warning(iface.mainWindow(), 'INCA error', str(self.exception.message))
            elif isinstance(self.exception, BaseException):
                QtWidgets.QMessageBox.critical(iface.mainWindow(), 'INCA unexpected error',
                                               f'Something went wrong: "{self.exception}".  Please refer to the '
                                               f'log file at {inca.get_logfile()} for more details.')
        _inca_tasks.remove(self)


def make_mutually_exclusive(widget1: QgsFileWidget, widget2: QgsFileWidget):
    """Set up signals and slots so only one of both filewidgets can get a non-null value at a time."""

    # disable the other widget if this widget has a filename, unless the other widget already has a non-empty filename.
    # -> we want to avoid that both widgets become disabled if we load a config file from text where both config values
    #    are set
    widget1.lineEdit().valueChanged.connect(
        lambda value: widget2.setEnabled((not value) or bool(widget2.lineEdit().value()))
    )
    widget2.lineEdit().valueChanged.connect(
        lambda value: widget1.setEnabled((not value) or bool(widget1.lineEdit().value()))
    )


def make_tourism_proxy_settings(tourism_config):
    """Helper function to deal with special nature-based tourism spatial proxy settings.

    For tourism, the facilities/attractiveness/accessibility map inputs from the GUI may contain a categorical
    map *or* a weight map.  When starting an INCA run, we must make sure that the correct config setting is passed
    on, depending on whether the user has provided a classification table.  If a classification table is present,
    the map should be a categorical map, if not, the map should contain weights."""
    if tourism_config['facilities_table']:
        del tourism_config['facilities_weight_map']
    else:
        del tourism_config['facilities_cat_map']
    if tourism_config['attractiveness_table']:
        del tourism_config['attractiveness_weight_map']
    else:
        del tourism_config['attractiveness_cat_map']
    if tourism_config['accessibility_table']:
        del tourism_config['accessibility_weight_map']
    else:
        del tourism_config['accessibility_cat_map']
