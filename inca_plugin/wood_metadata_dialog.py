# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

import os

from qgis.PyQt import QtCore, uic


from .qt_tools import writeWidget


FORM_CLASS, BASE_CLASS = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'wood_metadata_dialog.ui'))


class WoodMetadataDialog(BASE_CLASS, FORM_CLASS):

    def __init__(self, inca_dialog=None, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.label_reportingyear.setText(str(inca_dialog.year.value()))

        load_template(inca_dialog.metadata.get('Wood Provision'), self.metadata_template())

    def metadata_template(self):
        return {
            'faws': {
                'estimate_all_faws': self.check_allfaws,
                'which_forests': self.edit_whichfaws,
                'default_biophysical': self.check_faws_default_biophysical,
                'describe_settings_biophysical': self.edit_settings_faws_biophysical,
                'default_valuation': self.check_faws_default_valuation,
                'describe_settings_valuation': self.edit_settings_faws_valuation,
                'alternative_data_sources': self.text_faws_alternativedata,
                'spatial_data_resolution': self.faws_spatial_data_res,
                'gap_filling_applied': self.check_faws_gapfilling,
                'describe_gap_filling': self.text_faws_gapfilling,
                'insitu_verification': self.check_faws_insitu,
                'describe_insitu_verification': self.text_faws_insitu,
                'sensitivity_analysis': self.check_faws_sensitivity,
                'describe_sensitivity_analysis': self.text_faws_sensitivity,
                'uncertainties_assessed': self.check_faws_uncertainties,
                'describe_uncertainty_assessment': self.text_faws_uncertainties,
                'peer_reviewed': self.check_faws_peerreview,
                'describe_peer_review': self.text_faws_peerreview
            },
            'fnaws': {
                'estimate_all_fnaws': self.check_allfnaws,
                'which_forests': self.edit_whichfnaws,
                'default_biophysical': self.check_fnaws_default_biophysical,
                'describe_settings_biophysical': self.edit_fnaws_biophysical,
                'default_valuation': self.check_fnaws_default_valuation,
                'describe_settings_valuation': self.edit_fnaws_valuation,
                'alternative_data_sources': self.edit_fnaws_alternativedata,
                'spatial_data_resolution': self.fnaws_spatial_data_res,
                'gap_filling_applied': self.check_fnaws_gapfilling,
                'describe_gap_filling': self.edit_fnaws_gapfilling,
                'insitu_verification': self.check_fnaws_verification,
                'describe_insitu_verification': self.edit_fnaws_verification,
                'sensitivity_analysis': self.check_fnaws_sensitivity,
                'describe_sensitivity_analysis': self.edit_fnaws_sensitivity,
                'uncertainties_assessed': self.check_fnaws_uncertainties,
                'describe_uncertainty_assessment': self.edit_fnaws_uncertainties,
                'peer_reviewed': self.check_fnaws_peerreview,
                'describe_peer_review': self.edit_fnaws_peerreview
            }

        }


def load_template(config, template):
    """Update the widgets listed in the template with values from config."""
    if config is None:
        return
    if type(template) == dict:
        for key, value in template.items():
            load_template(config.get(key), value)
    elif isinstance(template, QtCore.QObject):
        writeWidget(template, config)
    else:
        raise RuntimeError(f'Failed to process template {template}.')
