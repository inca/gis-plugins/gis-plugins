# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Create a zip file with the plugin files, substituting the git version hash where needed.

This is a quick-and-dirty script which will only run properly from inside the INCA repository.  Use at own risk!
"""

import os
import subprocess
import zipfile
from string import Template


def zip_dir(git_root, dirname, zipfile):
    for root, _, files in os.walk(os.path.join(git_root, dirname)):
        for file in files:
            filename = os.path.join(root, file)
            print(f'adding file "{filename}"')
            zipfile.write(filename, arcname=os.path.relpath(filename, git_root))


def main():
    git_rev = subprocess.run(
        ["git", "rev-parse", "--short", "HEAD"], capture_output=True, text=True
    ).stdout.strip()

    git_root = subprocess.run(
        ["git", "rev-parse', '--show-toplevel"], capture_output=True, text=True
    ).stdout.strip("\n")

    # Check that marvin_qgis_tools submodule is correctly checked out and the symlink in inca_plugin is working:
    if not os.path.isdir(os.path.join(git_root, "inca_plugin", "marvin_qgis_tools")):
        print(
            'ERROR: cannot find subdirectory "marvin_qgis_tools" in inca_plugin.  Make sure the qgis_tools submodule '
            "is initialized and a symbolic link to marvin_qgis_tools is found in inca_plugin."
        )
        exit(1)

    # Compile resources.qrc
    resources = os.path.join(git_root, "inca_plugin", "resources")
    cmd = ["pyrcc5", resources + ".qrc", "-o", resources + ".py"]
    osgeo_root = os.getenv("OSGEO4W_ROOT")
    if osgeo_root:  # We are on windows
        # Seems the only bullet-proof way to set up the environment correctly for pyrcc5, is running o4w_env.bat first.
        cmd = [
            "cmd.exe",
            "/c",
            os.path.join(osgeo_root, "bin", "o4w_env.bat"),
            "&&",
        ] + cmd
    subprocess.run(cmd, check=True)

    # metadata.txt is a template where we want to insert the current git revision before zipping it:
    with open(
        os.path.join(git_root, "inca_plugin", "metadata.txt"), "rt", encoding="utf-8"
    ) as f:
        template = Template(f.read())
        metadata = template.substitute(GIT_REVISION=git_rev)

    # only include files tracked in git for now:
    git_files = subprocess.run(
        ["git", "ls-tree", "-r", "--full-tree", "--name-only", "HEAD", "inca_plugin"],
        capture_output=True,
        text=True,
    ).stdout.strip("\n")

    plugin_file = f"inca_plugin_{git_rev}.zip"

    with zipfile.ZipFile(plugin_file, "w") as plugin_zip:
        plugin_zip.writestr("inca_plugin/metadata.txt", metadata)
        plugin_zip.write(
            os.path.join(git_root, "inca_plugin", "resources.py"),
            "inca_plugin/resources.py",
        )  # resources.py is not in git
        for filename_git in git_files.split("\n"):
            if filename_git in (
                ".gitignore",
                "inca_plugin/metadata.txt",
                "inca_plugin/resources.qrc",
            ):  # skip these...
                continue
            filename = os.path.join(git_root, filename_git)
            if os.path.isdir(filename):  # symbolic links to directories
                zip_dir(git_root, filename_git, plugin_zip)
            else:
                print(f'adding file "{filename}"')
                plugin_zip.write(filename, arcname=filename_git)
    print(f"Created {os.path.abspath(plugin_file)}")


if __name__ == "__main__":
    main()
