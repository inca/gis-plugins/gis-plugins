# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Helper script to convert an official NUTS shapefile into a gpkg, containing only EU-2020 member states."""

import argparse
import os

import geopandas as gpd

import inca.common.estat


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('shapefile')

    return parser.parse_args()


def main():
    args = parse_args()

    output_file = os.path.splitext(args.shapefile)[0] + '_LEVL012_EU2020.gpkg'

    gdf= gpd.read_file(args.shapefile)
    gdf['FID'] = range(len(gdf))
    gdf[gdf.CNTR_CODE.isin(inca.common.estat.eu_members(2020)) & gdf.LEVL_CODE.isin((0, 1, 2))].to_file(output_file, driver='GPKG')


if __name__ == '__main__':
    main()
