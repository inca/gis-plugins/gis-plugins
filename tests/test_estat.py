import gzip
import os.path

import inca.common.estat as estat


def test_membership():
    """Test EU membership table."""
    membership = estat.is_member

    assert(membership.loc['UK', 2019])
    assert(not membership.loc['UK', 2020])
    assert(membership.loc['BE'].all())
    assert(not membership.loc['US'].any())


def test_tsv():
    """Test tsv file methods."""
    with gzip.open(os.path.join(os.path.dirname(__file__), 'for_eco_cp.tsv.gz')) as f:
        df = estat.read_tsv(f)

    assert(df.loc[('MIO_EUR', 'P1')].loc['BE', 2017] == 392.0)
    assert(df.loc[('MIO_EUR', 'P2')].loc['BG', 2018] == 486.38)


def test_eu27():
    """Test filtering on 2020 EU27 countries."""
    with gzip.open(os.path.join(os.path.dirname(__file__), 'for_eco_cp.tsv.gz')) as f:
        df = estat.read_tsv(f)

    p1 = df.loc[('MIO_EUR', 'P1')][2017]
    p1_eu27 = estat.filter_eu27(p1)

    # compare our own sum of EU27 results with the one provided by Eurostat.
    assert(p1_eu27.sum() == p1['EU27_2020'])
