The NUTS region administrative boundary data was obtained from

https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units

The permission to use this data is granted on the condition that

1. the data will not be used for commercial purposes;

2. the source will be acknowledged. A copyright notice, as specified below, will have to be visible on any printed or
   electronic publication using the data downloaded from this page.

Copyright notice

When the data contained in NUTS_RG_03M_2021_3035_LEVL012_EU2020.gpkg is used in any printed or electronic publication,
the data source will have to be acknowledged in the legend of the map and in the introductory page of the publication
with the following copyright notice:

EN: © EuroGeographics for the administrative boundaries

FR: © EuroGeographics pour les limites administratives

DE: © EuroGeographics bezüglich der Verwaltungsgrenzen

For publications in languages other than English, French or German, the translation of the copyright notice in the
language of the publication shall be used.

If you intend to use the data commercially, please contact EuroGeographics for information regarding their licence
agreements.  https://eurogeographics.org/products-and-services/licensing
