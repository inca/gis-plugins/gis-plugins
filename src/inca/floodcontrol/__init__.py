# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

import logging
import math
import os
import re
from contextlib import ExitStack, nullcontext
from functools import reduce
import fiona

import geopandas as gpd
import numpy as np
import pandas as pd
import rasterio
import rasterio.mask
from rasterio.merge import merge
import richdem as rd
from shapely.geometry import mapping

import inca.common.estat
import inca.common.ecosystem as ecosystem
from inca.common.ecosystem import ecotype_l2_cat, ECOTYPE, ECOTYPE_L2, LANDCOVER_ID, SHAPE_ID, ECO_ID, types_l1
from inca.common.config_check import ConfigItem, ConfigRaster, ConfigShape, ConfigKeyValue, check_csv, YEARLY
from inca.common.nuts import NUTS_ID
from inca.common.suts import write_sut, use_sector_cat
from inca.common.geoprocessing import block_window_generator, pixel_area, RasterType, statistics_byArea_byET

logger = logging.getLogger(__name__)

# Some string constants
_SOIL = 'Soil'
_CN = 'CN'
_THRESHOLD = 'threshold'
_NUTS_NUM = 'NUTS_NUM'
_COUNT = 'count'
_MEAN = 'mean'
_STDDEV = 'stddev'
_SUM = 'sum'
_VAR = 'var'

# Ecosystems categorized as 'artificial', 'agricultural' and 'natural' for supply calculation
_ECO_CAT = 'Ecosystem Category'
ARTIFICIAL = 'Artificial'
AGRICULTURAL = 'Agricultural'
NATURAL = 'Natural and semi-natural'
_INTERMEDIATE = 'intermediate_data'
_FLOODCONTROL = 'flood-control'

# When calculating thresholds for service providing areas, ecosystem types are split into three categories.
# We also produce use maps for artificial and agricultural areas separately.
_ecosystem_cats = {ARTIFICIAL: [1.1, 1.2, 1.3, 1.4, 1.5],
                   AGRICULTURAL: [2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 3.1],
                   NATURAL: [3.2, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 5.1, 5.2, 5.3, 6.1, 6.2, 7.1, 7.2]}
_df_ecosystem_cats = pd.DataFrame([
    (cat, ecotype) for cat in _ecosystem_cats for ecotype in _ecosystem_cats[cat]],
    columns=[_ECO_CAT, ECOTYPE]).astype({ECOTYPE: ecotype_l2_cat,
                                         _ECO_CAT: 'category'}).set_index(ECOTYPE)

# Ecosystem types to be listed in supply table:
_ecotype_supply = [ecosystem.ARTIFICIAL, ecosystem.CROP, ecosystem.FOREST, ecosystem.GRASSLAND,
                   ecosystem.HEATH, ecosystem.SPARSE, ecosystem.WETLANDS]

ECONOMIC_UNIT = 'Economic Unit'


class FloodRun(inca.Run):
    service_name = 'Flood Control'

    def __init__(self, config):
        super().__init__(config)
        # Adjusting rasters during config check can take ~30% of run time for a small run.  Assuming this includes a lot
        # of fixed overhead and will not increase as much as the computing intensive part of the run for larger runs, we
        # give it a final weight of 20% in the progress bar.
        self._progress_weight_run = 0.8

        self.config_template.update({'land_classification': ConfigItem(check_csv, optional=True,
                                                                       required_columns=[ECOTYPE_L2, LANDCOVER_ID],
                                                                       unique_columns=[LANDCOVER_ID],
                                                                       dtypes={ECOTYPE_L2: ecotype_l2_cat}),
                                     self.service_name: {
                                         'slope': ConfigRaster(raster_type=RasterType.ABSOLUTE_POINT),
                                         'hydro_soil': ConfigRaster(raster_type=RasterType.CATEGORICAL),
                                         'impervious_density':
                                             {YEARLY: ConfigRaster(raster_type=RasterType.ABSOLUTE_POINT)},
                                         'soil_land_cn': ConfigItem(check_csv,
                                                                    required_columns=[LANDCOVER_ID, 'Soil', _CN],
                                                                    unique_columns=[(LANDCOVER_ID, 'Soil')]),
                                         'riparian_zones': ConfigRaster(raster_type=RasterType.CATEGORICAL),
                                         'dem': ConfigRaster(raster_type=RasterType.ABSOLUTE_POINT),
                                         'land_econ_unit': ConfigItem(check_csv,
                                                                      required_columns=[LANDCOVER_ID, ECONOMIC_UNIT],
                                                                      unique_columns=[LANDCOVER_ID],
                                                                      dtypes={ECONOMIC_UNIT: use_sector_cat}),
                                         'population':
                                             {YEARLY: ConfigRaster(raster_type=RasterType.ABSOLUTE_VOLUME)},
                                         'catchments': ConfigShape(),
                                         'hydro_basin': ConfigShape()

                                     }})

        logger.debug('self.monetary = %s', self.monetary)

        if self.monetary:  # Inputs used only for monetary evaluation:
            logger.debug('Add config_template for monetary')
            self.config_template[self.service_name].update({
                'damage_functions': ConfigItem(),
                'flood_maps': ConfigKeyValue(
                    ConfigRaster(raster_type=RasterType.ABSOLUTE_POINT)),
            })
        else:  # Inputs used only if *no* monetary evalutation
            logger.debug('Add config_template for non-monetary')
            self.config_template[self.service_name].update({
                'flood_plains': ConfigRaster(raster_type=RasterType.ABSOLUTE_POINT)
            })

        self.use_sectors = None
        self.make_output_filenames()
        self.ecotypes_l1 = None
        self.ecotypes_l2 = None
        self.landcover_agri_use = None  # landcover values with use by _AGRICULTURAL category
        self.landcover_artif_use = None  # landcover values with use by _ARTIFICIAL category

    def make_output_filenames(self):
        """Create standard names for the different output files."""
        self.dem = os.path.join(self.temp_dir, 'depressionfilled.tif')
        self.accuflow_total = os.path.join(self.temp_dir, 'total_flow_accumulation.tif')
        self.potential = os.path.join(self.additional_results, f'{_FLOODCONTROL}_map_potential_''{year}.tif')
        self.catchment_population = os.path.join(self.additional_results,
                                                 f'{_FLOODCONTROL}_map_catchment_population_''{year}.gpkg')
        self.spa = os.path.join(self.additional_results, f'{_FLOODCONTROL}_map_spa_''{year}.tif')
        self.spa_maes = {ecotype: os.path.join(self.temp_dir,
                                               f'{_FLOODCONTROL}_map_SPA-mask-{ecotype.replace(" ", "-")}_''{year}.tif')
                         for ecotype in _ecotype_supply}
        self.demand = os.path.join(self.additional_results, f'{_FLOODCONTROL}_map_demand-LC_''{year}.tif')
        self.pop_exposure = os.path.join(self.additional_results,
                                         f'{_FLOODCONTROL}_map_population-exposure_count_''{year}.tif')
        self.lc_retention_factors = os.path.join(self.additional_results,
                                                 f'{_FLOODCONTROL}_table_landcover-retention-factors_''{year}.csv')
        self.spa_thresholds = os.path.join(self.additional_results,
                                           f'{_FLOODCONTROL}_table_SPA-thresholds_''{year}.csv')
        self.flow_map = os.path.join(self.maps, f'{_FLOODCONTROL}_map_use_hectare_''{year}.tif')
        self.flow_agri = os.path.join(self.maps, f'{_FLOODCONTROL}_map_use-{AGRICULTURAL}_hectare_''{year}.tif')
        self.flow_artif = os.path.join(self.maps, f'{_FLOODCONTROL}_map_use-{ARTIFICIAL}_hectare_''{year}.tif')
        self.pop_use = os.path.join(self.maps, f'{_FLOODCONTROL}_map_use_population_''{year}.tif')
        self.pop_unmet_demand = os.path.join(self.maps, f'{_FLOODCONTROL}_map_unmet_demand_population_''{year}.tif')
        self.population_national = os.path.join(self.statistic_dir,
                                                f'{_FLOODCONTROL}_statistics_population_national_''{year}.csv')
        self.unmet = os.path.join(self.maps, f'{_FLOODCONTROL}_map_mismatch_hectare_''{year}.tif')
        self.unmet_agri = os.path.join(self.maps, f'{_FLOODCONTROL}_map_mismatch-{AGRICULTURAL}_hectare_''{year}.tif')
        self.unmet_artif = os.path.join(self.maps, f'{_FLOODCONTROL}_map_mismatch-{ARTIFICIAL}_hectare_''{year}.tif')
        self.value_map = os.path.join(self.maps, f'{_FLOODCONTROL}_map_value_EURO-real_''{year}.tif')
        self.value_map_nominal = os.path.join(self.maps, f'{_FLOODCONTROL}_map_value_EURO-current_''{year}.tif')
        self.value_agri = os.path.join(self.maps, f'{_FLOODCONTROL}_map_value-{AGRICULTURAL}_EURO-real_''{year}.tif')
        self.value_artif = os.path.join(self.maps, f'{_FLOODCONTROL}_map_value-{ARTIFICIAL}_EURO-real_''{year}.tif')
        self.use = os.path.join(self.statistic_dir, f'{_FLOODCONTROL}_statistics_use_hectare_''{year}.csv')
        self.use_monetary = os.path.join(self.statistic_dir, f'{_FLOODCONTROL}_statistics_use_EURO-real_''{year}.csv')
        self.use_monetary_nominal = os.path.join(self.statistic_dir,
                                                 f'{_FLOODCONTROL}_statistics_use_EURO-current_''{year}.csv')
        self.supply = os.path.join(self.statistic_dir, f'{_FLOODCONTROL}_statistics_supply_hectare_''{year}.csv')
        self.supply_monetary = os.path.join(self.statistic_dir,
                                            f'{_FLOODCONTROL}_statistics_supply_EURO-real_''{year}.csv')
        self.supply_monetary_nominal = os.path.join(self.statistic_dir,
                                                    f'{_FLOODCONTROL}_statistics_supply_EURO-current_''{year}.csv')
        self.sut_physical = os.path.join(self.suts, f'{_FLOODCONTROL}_report_SUT-physical_hectare_''{year}.xlsx')
        self.sut_monetary = os.path.join(self.suts, f'{_FLOODCONTROL}_report_SUT-monetary_M-EURO-real_''{year}.xlsx')
        self.sut_monetary_nominal = os.path.join(self.suts,
                                                 f'{_FLOODCONTROL}_report_SUT-monetary_M-EURO-current_''{year}.xlsx')
        if self.monetary:
            self.flood_damage = os.path.join(self.additional_results,
                                             f'{_FLOODCONTROL}_map_flood-damage-rp''{rp}_EUR-real_{year}.tif')
        self.catchment_spa = os.path.join(self.additional_results,
                                          f'{_FLOODCONTROL}_table_catchment-potential_ha_''{year}.gpkg')
        self.catchment_demand = os.path.join(self.additional_results,
                                             f'{_FLOODCONTROL}_table_catchment-demand_ha_''{year}.gpkg')
        self.catchment_use = os.path.join(self.additional_results,
                                          f'{_FLOODCONTROL}_table_catchment-use-mismatch-value_''{year}.gpkg')
        self.demand_national = os.path.join(self.additional_results,
                                            f'{_FLOODCONTROL}_table_national-data-demand-unmet-demand_ha_''{year}.xlsx')
        self.potential_national = os.path.join(self.additional_results,
                                               f'{_FLOODCONTROL}_table_national-data-potential_ha_''{year}.xlsx')
        self.flow_accumulation_maps = {
            ecotype: os.path.join(self.temp_dir,
                                  f'{_FLOODCONTROL}_map_flow-accumulation-{ecotype.replace(" ", "-")}_''{year}.tif')
            for ecotype in _ecotype_supply}

    def _start(self):
        self.ecotypes_l1 = self.get_ecosystem_mapping(level=1)
        self.ecotypes_l2 = self.get_ecosystem_mapping(level=2)

        # TODO: check at least 2 flood maps provided?
        # TODO: check land cover types in use_sectors correspond to (landcover_agri + landcover_artif)
        self.soil_land_eco = self.get_soil_land_cn()
        self.use_sectors = read_use_sectors(self.config[self.service_name]['land_econ_unit'])
        ecocat_landcover = _df_ecosystem_cats.merge(self.ecotypes_l2, on=ECOTYPE).set_index(_ECO_CAT)
        # For demand by agriculture, exclude ecosystem type 2.2 'Rice fields':
        self.landcover_agri_use = ecocat_landcover[ecocat_landcover[ECOTYPE] != 2.2].loc[AGRICULTURAL, LANDCOVER_ID]
        self.landcover_artif_use = ecocat_landcover.loc[ARTIFICIAL, LANDCOVER_ID]

        logger.debug('landcover/Soil/CN/Ecosystem cat:\n%s', self.soil_land_eco)
        # Create addtional folders
        os.makedirs(self.additional_results, exist_ok=True)
        os.makedirs(os.path.join(self.run_dir, 'TEMP', 'DEM'), exist_ok=True)
        os.makedirs(os.path.join(self.run_dir, 'TEMP', 'Flow_accumulation'), exist_ok=True)
        os.makedirs(os.path.join(self.run_dir, 'TEMP', 'accu_per_eco'), exist_ok=True)

        progress_weight_physical = 0.8 if self.monetary else 1.
        self.total_flow_accumulation(add_progress=lambda p: self.add_progress(
            0.05 / progress_weight_physical * p))  # 5%
        for year in self.years:
            logger.info('Process %d', year)
            self.calculate_potential_demand(year, add_progress=lambda p: self.add_progress(
                0.05 / progress_weight_physical * p))  # 5%
            self.weighted_flow_accumulation(year, add_progress=lambda p: self.add_progress(
                0.2 / progress_weight_physical * p))  # 20%
            self.use_and_unmet_demand(year)  # 2.5%
            self.add_progress(2.5 / progress_weight_physical)
            if self.monetary:
                self.monetary_value(year, add_progress=lambda p: self.add_progress(0.2 * p))  # 20%

            self.calculate_sut(year)  # 1%
            self.add_progress(1.0 / progress_weight_physical)
            self.reporting_stats(year)  # 1.5%
            self.add_progress(1.5 / progress_weight_physical)
            self.catchment_statistics(year, add_progress=lambda p: self.add_progress(
                0.45 / progress_weight_physical * p))  # 45%
            self.cut_all_maps(year)

        return

    def cut_all_maps(self, year):
        """Cut the main output rasters to reporting region extent."""
        all_maps = [self.flow_map, self.flow_agri, self.flow_artif,
                    self.unmet, self.unmet_agri, self.unmet_artif]
        if self.monetary:
            all_maps += [self.value_map, self.value_map_nominal,
                         self.value_agri, self.value_artif]
        for map in all_maps:
            self.cut_to_reporting_regions(map.format(year=year))

    def get_semi_natural_landcovers(self):
        """Semi-natural landcovers in riparian zones get a special curve number value."""
        # In (Vallecillo et al., 2020), semi-natural landcovers are defined in terms of CORINE land cover types:
        #   "As natural and semi-natural land covers, we considered agro-forestry areas [CLC 244], forest and
        #    semi-natural areas [CLC 311–313], scrub and/or herbaceous vegetation associations [CLC 321–324],
        #    wetlands [CLC 411–423]."
        # We define them here in terms of corresponding L2 ecosystem types, so we can use the definition for other land
        # cover data sets as well:
        semi_natural_ecotypes = [2.4, 3.2, 4.1, 4.2, 4.3, 4.4, 4.5, 5.1, 5.2, 5.3, 7.1, 7.2, 10.3, 11.4]
        return self.ecotypes_l2[LANDCOVER_ID][self.ecotypes_l2[ECOTYPE].isin(semi_natural_ecotypes)]

    def get_soil_land_cn(self):
        """Create dataframe with landcover_id, soil type, curve number, ecosystem category and SPA threshold."""
        soil_land = pd.read_csv(self.config[self.service_name]['soil_land_cn'],
                                usecols=[LANDCOVER_ID, _SOIL, _CN],
                                dtype={LANDCOVER_ID: int, _SOIL: int, _CN: np.float32})
        soil_land_ecotype = soil_land.merge(self.ecotypes_l2[[LANDCOVER_ID, ECOTYPE]],
                                            on=LANDCOVER_ID)
        soil_land_cn = soil_land_ecotype.merge(_df_ecosystem_cats.reset_index(), on=ECOTYPE)[
            [LANDCOVER_ID, _SOIL, _CN, _ECO_CAT]]
        return soil_land_cn.set_index([LANDCOVER_ID, _SOIL])

    def calculate_potential_demand(self, year, add_progress, block_shape=(2048, 2048)):
        """Calculate which pixels may contribute to flood control, and which pixels count towards demand."""
        file_landcover = self.config['land_cover'][year]
        file_hydro = self.config[self.service_name]['hydro_soil']
        file_impervious = self.config[self.service_name]['impervious_density'][year]
        file_slope = self.config[self.service_name]['slope']
        file_riparian = self.config[self.service_name]['riparian_zones']

        if self.monetary:
            # Use flood map with largest return period to define extent of flood protection demand:
            flood_maps = self.config[self.service_name]['flood_maps']
            max_rp = sorted(flood_maps)[-1]
            file_floodplains = flood_maps[max_rp]
            logger.debug('Calculate demand based on flood map for return period %s.', max_rp)
        else:
            file_floodplains = self.config[self.service_name]['flood_plains']

        semi_natural_landcovers = self.get_semi_natural_landcovers()
        cn_min = self.soil_land_eco['CN'].min()

        logger.debug('Assigning min CN %f to semi-natural land in riparian zones.', cn_min)

        with rasterio.open(file_landcover) as ds_lc:
            profile_potential = ds_lc.profile
        profile_potential.update(dtype='float32', nodata=np.NAN, compress='lzw')
        profile_spa = profile_potential.copy()
        profile_spa.update(dtype='uint8', nodata=255)
        profile_demand = profile_potential.copy()
        profile_demand.update(dtype='int16', nodata=-1)

        # While we iterate over input raster blocks, we will keep track of the sum/count/mean/var of flood retention
        # coefficients per land cover category in the following variable:
        aggs = []
        logger.info('Calculating demand and potential maps.')
        with rasterio.open(file_hydro) as ds_hydro, \
                rasterio.open(file_landcover) as ds_lc, \
                rasterio.open(file_impervious) as ds_impervious, \
                rasterio.open(file_slope) as ds_slope, \
                rasterio.open(file_riparian) as ds_riparian, \
                rasterio.open(file_floodplains) as ds_floodplains, \
                rasterio.open(self.reporting_raster) as ds_reporting, \
                rasterio.open(self.potential.format(year=year), 'w', **profile_potential) as ds_potential, \
                rasterio.open(self.demand.format(year=year), 'w', **profile_demand) as ds_demand, \
                ExitStack() as stack:
            nblocks = math.ceil(float(ds_lc.height) / block_shape[0]) * math.ceil(float(ds_lc.width) / block_shape[1])
            for _, window in block_window_generator(block_shape, ds_hydro.profile['height'], ds_hydro.profile['width']):
                soil = ds_hydro.read(1, window=window)
                landcover = ds_lc.read(1, window=window)

                # Initial CN value: join with soil_land_eco lookup table
                df_lc_hydro = pd.DataFrame(data={LANDCOVER_ID: landcover.flatten(), _SOIL: soil.flatten()}).join(
                    self.soil_land_eco[_CN], on=[LANDCOVER_ID, _SOIL])
                cn = df_lc_hydro[_CN].to_numpy().reshape(soil.shape)

                # Correct for imperviousness
                impervious_pct = ds_impervious.read(1, window=window)
                # Copernicus imperviousness data: 1-100 = impervious area %; 254, 255: nodata/outside area
                # no correction for nodata/outside area -> set imperviousness to 0 there:
                impervious_pct[impervious_pct > 100] = 0
                cn = 0.98 * impervious_pct + (1 - impervious_pct / 100.) * cn

                # Correct for slope
                slope = ds_slope.read(1, window=window)
                slope[slope == ds_slope.profile['nodata']] = 0
                slope = slope / 100.  # percentage to ratio
                cn *= (322.79 + 15.63 * slope) / (slope + 323.52)

                # Special treatment of CN in riparian zones (Vallecillo et al., 2020):
                # "Given the importance of natural and semi-natural land covers in riparian zones in controlling floods
                # (Bennett and Simon, 2013), we assigned them the min CNFinal score.
                riparian_zone = ds_riparian.read(1, window=window) == 1
                cn[np.isin(landcover, semi_natural_landcovers) & riparian_zone] = cn_min

                # Curve number is a measure of how much water runs off, i.e. higher CN -> lower flood retention.
                # We prefer an indicator that increases with better runoff retention, therefore subtract the calculated
                # CN from the (presumed?) upper limit 100 to arrive at a final retention factor.
                retention = 100 - cn
                ds_potential.write(retention.astype(rasterio.float32), window=window, indexes=1)

                # Finally, we calculate new SPA threshold values based on this year's data:
                # To determine thresholds for service providing areas, we need the mean and std deviation of the flood
                # retention coefficient per land use class
                df_lc_hydro['retention'] = retention.flatten()
                aggs.append(df_lc_hydro.set_index(LANDCOVER_ID)['retention'].dropna().groupby(LANDCOVER_ID).agg(
                    [_COUNT, _SUM, _MEAN, _VAR]))

                # Demand: economic assets (landcover_agri and landcover_artif) in flood plains
                flood = ds_floodplains.read(1, window=window)
                demand = landcover.copy()
                # mask out other land use and non-flooded areas:
                demand[~(np.isin(landcover, self.landcover_agri_use) | np.isin(landcover, self.landcover_artif_use))
                       | (flood <= 0) | (flood == ds_floodplains.nodata)] = profile_demand['nodata']
                # mask out non-eu members using nuts_nums raster; masked pixels in demand will propagate to use etc:
                shape_ids = ds_reporting.read(1, window=window)
                demand[shape_ids == ds_reporting.nodata] = profile_demand['nodata']
                ds_demand.write(demand.astype(rasterio.int16), window=window, indexes=1)
                add_progress(100. / nblocks)
            metadata = self.accord.metadata
            metadata.update_dataset_tags(ds_potential, 'Flood retention score', 'dimensionless',
                                         file_landcover, file_hydro, file_impervious, file_slope, file_riparian)
            metadata.update_dataset_tags(ds_demand, 'Flood control demand', 'CORINE class (dimensionless)',
                                         file_landcover, file_floodplains)

        logger.info('Calculating population in service demanding areas.')
        ### so the idea is that instead of warping the masks we warp the pop data to the correct projection and resolution
        # we want epsg3035 and 100m  and than we apply the masks.

        # the input is most likely to be in Mollweide

        # metadata = Metadata('VITO', 'flood control')
        # file_demand_warped = helper_warp(self.demand.format(year=year), metadata, self.run_dir, wResampling='near')
        # metadata.profile.update(dtype='uint8', nodata=255)

        with rasterio.open(self.demand.format(year=year), 'r') as ds_demand, \
                rasterio.open(self.config[self.service_name]['population'][year], 'r') as ds_population, \
                rasterio.open(self.pop_exposure.format(year=year), 'w', **ds_population.profile) as ds_pop_exposure:
            for _, window in block_window_generator(block_shape, ds_population.profile['height'],
                                                    ds_population.profile['width']):
                # Mask population by demand:
                demand = ds_demand.read(1, window=window)
                population = ds_population.read(1, window=window)
                population[demand == ds_demand.profile['nodata']] = ds_population.profile['nodata']
                ds_pop_exposure.write(population, window=window, indexes=1)
        # TODO Demand: population in service demanding areas

        logger.info('Aggregating flood retention coefficients per landcover type.')
        # Combine per-block landcover code aggregates into total aggregates:
        aggs = pd.concat(aggs)  # first concatenate per-window aggregations into a single dataframe
        # for the mean, just sum the group counts and sums, and take the ratio:
        total_aggregate = aggs[[_COUNT, _SUM]].groupby(LANDCOVER_ID).sum()
        total_aggregate[_MEAN] = total_aggregate[_SUM] / total_aggregate[_COUNT]
        # for variance, we need a bit more work:
        aggs = aggs.join(total_aggregate[_MEAN].rename('group_mean'))
        total_aggregate[_VAR] = (aggs[_COUNT] * (aggs[_MEAN] - aggs['group_mean']) ** 2 +
                                 (aggs[_COUNT] - 1) * aggs[_VAR]).groupby(LANDCOVER_ID).sum() \
                                / (total_aggregate[_COUNT] - 1)
        total_aggregate[_STDDEV] = np.sqrt(total_aggregate[_VAR])

        # Calculate thresholds that determine service providing areas for three ecosystem types
        total_aggregate = total_aggregate.join(
            self.ecotypes_l2.set_index(LANDCOVER_ID)[ECOTYPE]).join(
            _df_ecosystem_cats, on=ECOTYPE)
        total_aggregate[_THRESHOLD] = np.NAN
        # For artificial and agricultural ecosystem types, take 'mean + var' as threshold
        non_natural = (total_aggregate[_ECO_CAT] == ARTIFICIAL) \
                      | (total_aggregate[_ECO_CAT] == AGRICULTURAL)
        total_aggregate.loc[non_natural, _THRESHOLD] = total_aggregate.loc[non_natural, _MEAN] \
                                                       + total_aggregate.loc[non_natural, _STDDEV]
        # For natural and semi-natural ecosystem types, take 'mean - var' as threshold
        natural = total_aggregate[_ECO_CAT] == 'Natural and semi-natural'
        total_aggregate.loc[natural, _THRESHOLD] = total_aggregate.loc[natural, _MEAN] \
                                                   - total_aggregate.loc[natural, _STDDEV]

        logger.debug('Aggregated flood retention:\n%s', total_aggregate)
        total_aggregate.to_csv(self.lc_retention_factors.format(year=year))
        spa_thresholds = total_aggregate.groupby(_ECO_CAT).agg({_THRESHOLD: _MEAN})
        spa_thresholds.to_csv(self.spa_thresholds.format(year=year))
        logger.debug('Ecosystem type SPA thresholds:\n%s', spa_thresholds)

        # Get threshold per landcover id by joining ecosystem_types, ecosystem cats and spa_thresholds:
        landcover_thresholds = self.ecotypes_l2.set_index(ECOTYPE).join(
            _df_ecosystem_cats, how='right').join(
            spa_thresholds, on=_ECO_CAT).set_index(LANDCOVER_ID)[_THRESHOLD]

        # Now use threshold values to mark Service Providing Areas
        with rasterio.open(file_landcover) as ds_lc, \
                rasterio.open(self.potential.format(year=year)) as ds_potential, \
                rasterio.open(self.spa.format(year=year), 'w', **profile_spa) as ds_spa, \
                ExitStack() as stack:
            # Open a dataset to save SPA raster for each Maes class:
            spa_maes = {ecotype: stack.enter_context(rasterio.open(self.spa_maes[ecotype].format(year=year), 'w',
                                                                   **profile_spa))
                        for ecotype in _ecotype_supply}

            # Dict of landcover values for each supply ecosystem type
            eco_mapping = self.ecotypes_l1.set_index('name')
            ecotype_landcovers = {ecotype: eco_mapping.loc[ecotype, LANDCOVER_ID].values
                                  for ecotype in spa_maes}

            for _, window in block_window_generator(block_shape, ds_lc.profile['height'], ds_lc.profile['width']):
                landcover = ds_lc.read(1, window=window)
                retention = ds_potential.read(1, window=window)
                df_lc = pd.DataFrame({LANDCOVER_ID: landcover.flatten()}).join(landcover_thresholds, on=LANDCOVER_ID)

                # SPA are all pixels where retention is above the threshold for the pixel's ecosystem type:
                spa = (retention >= df_lc[_THRESHOLD].to_numpy().reshape(landcover.shape)).astype(rasterio.uint8)
                ds_spa.write(spa, window=window, indexes=1)
                # we also keep rasters of SPA per ecosystem type:
                for ecotype, dataset in spa_maes.items():
                    spa_maes_raster = spa.copy()
                    # Only keep SPA for pixels belonging to this ecosystem type:
                    spa_maes_raster[~np.isin(landcover, ecotype_landcovers[ecotype])] = 0
                    dataset.write(spa_maes_raster, window=window, indexes=1)

            metadata.update_dataset_tags(ds_spa, 'Service providing areas', 'dimensionless',
                                         file_landcover, file_hydro, file_impervious, file_slope, file_riparian)
            for ecotype, ds in spa_maes.items():
                metadata.update_dataset_tags(ds, f'Service providing areas with ecosystem type "{ecotype}"',
                                             'dimensionless',
                                             file_landcover, file_hydro, file_impervious, file_slope, file_riparian)

    def catchment_statistics(self, year, add_progress):
        """Aggregate supply, demand and use per catchment."""
        logger.info('Calculate catchment statistics.')
        catchments = gpd.read_file(self.config[self.service_name]['catchments'])
        catchments = catchments[catchments.geometry.within(
            self.statistics_shape.geometry.buffer(1000).unary_union.convex_hull)]

        logger.info('Calculate potential (SPA) per catchment')
        # SPA in hectare
        catchments_spa = catchments.copy()
        # Number of mask operations for each geometry in catchments
        num_masks = 6 if self.monetary else 5
        progress_per_mask = 100. / (num_masks * len(catchments_spa))
        with rasterio.open(self.spa.format(year=year)) as ds_SPA:
            spa_ha = []
            pixel_area_ha = pixel_area(ds_SPA.crs, ds_SPA.transform) / 10000.  # area in m2 -> area in ha
            for geometry in catchments.geometry:
                try:
                    catchment_spa, _ = rasterio.mask.mask(ds_SPA, [geometry], crop=True, indexes=1)
                    spa_ha.append(np.count_nonzero(catchment_spa == 1) * pixel_area_ha)
                except ValueError:  # if shape does not lie completely within ds_SPA boundaries
                    spa_ha.append(0.)
                add_progress(progress_per_mask)
            catchments_spa['SPA_ha'] = spa_ha
        catchments_spa.to_file(self.catchment_spa.format(year=year))

        # Demand in hectare
        # TODO population exposure per catchment
        logger.info('Calculate demand per catchment')
        catchment_demand = catchments.copy()
        catchment_population = catchments.copy()
        with rasterio.open(self.demand.format(year=year)) as ds_demand, \
                rasterio.open(self.config[self.service_name]['population'][year], 'r') as ds_population, \
                rasterio.open(self.pop_exposure.format(year=year)) as ds_exposure:
            pixel_area_ha = pixel_area(ds_demand.crs, ds_demand.transform) / 10000.
            demand_total = []
            population_total = []
            population_sda = []
            demand_agri = []
            demand_artif = []
            for geometry in catchments['geometry']:
                # Note: we use a separate try/catch block for demand and population: rasterio.mask raises a ValueError
                # if a shape does not overlap the raster.  Population and demand datasets have different
                # resolution and these exceptions may appear at different times for both raster resolutions.
                try:
                    dmd, _ = rasterio.mask.mask(ds_demand, [geometry], crop=True, indexes=1)
                    demand_total.append(pixel_area_ha * np.count_nonzero(dmd != ds_demand.nodata))
                    demand_agri.append(pixel_area_ha * np.count_nonzero(np.isin(dmd, self.landcover_agri_use)))
                    demand_artif.append(pixel_area_ha * np.count_nonzero(np.isin(dmd, self.landcover_artif_use)))
                    assert (demand_total[-1] == demand_agri[-1] + demand_artif[-1])
                except ValueError:
                    demand_total.append(0.)
                    demand_agri.append(0.)
                    demand_artif.append(0.)
                try:
                    mask, _, window = rasterio.mask.raster_geometry_mask(ds_population, [geometry], crop=True)
                    population = ds_population.read(1, window=window)
                    exposure = ds_exposure.read(1, window=window)
                    pop = np.sum(population[~mask & (population != ds_population.nodata)])
                    exp = np.sum(exposure[~mask & (exposure != ds_exposure.nodata)])
                    population_total.append(pop)
                    population_sda.append(exp)
                except ValueError:
                    population_total.append(0.)
                    population_sda.append(0.)
                add_progress(progress_per_mask)
        catchment_demand['demand_ha'] = demand_total
        catchment_demand['artif_ha'] = demand_artif
        catchment_demand['agri_ha'] = demand_agri
        catchment_demand.to_file(self.catchment_demand.format(year=year))
        catchment_population['pop_total'] = population_total
        catchment_population['pop_sda'] = population_sda
        catchment_population.to_file(self.catchment_population.format(year=year))

        logger.info('Calculate use, mismatch%s per catchment',
                    ', monetary value' if self.monetary else '')
        catchment_use = catchments.copy()
        with rasterio.open(self.flow_map.format(year=year)) as ds_flow, \
                rasterio.open(self.demand.format(year=year)) as ds_demand, \
                rasterio.open(self.unmet.format(year=year)) as ds_unmet, \
                rasterio.open(self.pop_use.format(year=year), 'r') as ds_pop_use, \
                rasterio.open(self.pop_unmet_demand.format(year=year), 'r') as ds_pop_unmet, \
                rasterio.open(self.value_map.format(year=year)) if self.monetary else nullcontext() as ds_value:
            flow_total = []
            flow_agri = []
            flow_artificial = []
            unmet_total = []
            value_total = []
            pop_use_total = []
            pop_unmet_total = []
            for geometry in catchments['geometry']:
                try:
                    flow, _ = rasterio.mask.mask(ds_flow, [geometry], crop=True, indexes=1)
                    add_progress(progress_per_mask)
                    dmd, _ = rasterio.mask.mask(ds_demand, [geometry], crop=True, indexes=1)
                    add_progress(progress_per_mask)
                    unmet, _ = rasterio.mask.mask(ds_unmet, [geometry], crop=True, indexes=1)
                    add_progress(progress_per_mask)
                    flow_total.append(np.sum(flow, where=(flow != ds_flow.nodata)))
                    flow_agri.append(np.sum(flow, where=np.isin(dmd, self.landcover_agri_use)))
                    flow_artificial.append(np.sum(flow, where=np.isin(dmd, self.landcover_artif_use)))
                    unmet_total.append(np.sum(unmet, where=(unmet != ds_unmet.nodata)))
                    if self.monetary:
                        value, _ = rasterio.mask.mask(ds_value, [geometry], crop=True, indexes=1)
                        value_total.append(np.sum(value, where=(value != ds_value.nodata)))
                        add_progress(progress_per_mask)
                except ValueError:
                    flow_total.append(0.)
                    flow_agri.append(0.)
                    flow_artificial.append(0.)
                    unmet_total.append(0.)
                    value_total.append(0.)
                # separate try/catch for population, see above
                try:
                    pop_unmet, _ = rasterio.mask.mask(ds_pop_unmet, [geometry], crop=True, indexes=1)
                    pop_use, _ = rasterio.mask.mask(ds_pop_use, [geometry], crop=True, indexes=1)
                    pop_unmet_total.append(np.sum(pop_unmet, where=(pop_unmet != ds_pop_unmet.nodata)))
                    pop_use_total.append(np.sum(pop_use, where=(pop_use != ds_pop_use.nodata)))
                except ValueError:
                    pop_unmet_total.append(0.)
                    pop_use_total.append(0.)
                add_progress((num_masks - 2) * progress_per_mask)
        catchment_use['use_ha'] = flow_total
        catchment_use['use_agr_ha'] = flow_agri
        catchment_use['use_art_ha'] = flow_artificial
        catchment_use['unmet_ha'] = unmet_total
        catchment_use['pop_use'] = pop_use_total
        catchment_use['pop_unmet'] = pop_unmet_total
        if self.monetary:
            catchment_use['value_eur'] = value_total
        catchment_use.to_file(self.catchment_use.format(year=year))

    def use_and_unmet_demand(self, year, block_shape=(2048, 2048)):
        """Compare flow accumulation from service providing areas to total flow accumulation.

        Flood control use is defined as the ratio between total flow accumulation in a pixel where we have demand, and
        the flow accumulation from areas providing flood control (multiplied by the pixel area).  Unmet demand is its
        complement.
        """
        with rasterio.open(self.accuflow_total) as ds_flow_accumulation:
            profile_flow = ds_flow_accumulation.profile
        profile_flow.update(nodata=-9999)

        with rasterio.open(self.demand.format(year=year)) as ds_demand, \
                rasterio.open(self.accuflow_total) as ds_flow_accumulation, \
                rasterio.open(self.flow_map.format(year=year), 'w', **profile_flow) as ds_actual_flow, \
                rasterio.open(self.flow_agri.format(year=year), 'w', **profile_flow) as ds_actual_flow_agri, \
                rasterio.open(self.flow_artif.format(year=year), 'w', **profile_flow) as ds_actual_flow_artif, \
                rasterio.open(self.unmet.format(year=year), 'w', **profile_flow) as ds_unmet_demand, \
                rasterio.open(self.unmet_agri.format(year=year), 'w', **profile_flow) as ds_unmet_demand_agri, \
                rasterio.open(self.unmet_artif.format(year=year), 'w', **profile_flow) as ds_unmet_demand_artif, \
                ExitStack() as stack:
            # Open Accumulated flow rasters for each Maes class that supplies the service
            accuflow_datasets = {maes_class: stack.enter_context(rasterio.open(file.format(year=year)))
                                 for maes_class, file in self.flow_accumulation_maps.items()}

            pixel_area_ha = pixel_area(ds_demand.crs, ds_demand.transform) / 10000.
            for _, window in block_window_generator(block_shape, ds_demand.profile['height'],
                                                    ds_demand.profile['width']):
                accuflow = {maes_class: ds.read(1, window=window) for maes_class, ds in accuflow_datasets.items()}
                demand = ds_demand.read(1, window=window)
                for raster in accuflow.values():
                    # Only keep accuflow where we have demand:
                    raster[demand == ds_demand.nodata] = 0.
                # accuflow_spa: Sum of accumulated flow from different SPA Maes classes ~> total accuflow from SPA:
                accuflow_spa = reduce(np.add, accuflow.values())
                # total accumulated flow from all upstream pixels (fixed input, depends only on DEM)
                accuflow_total = ds_flow_accumulation.read(1, window=window)

                # Actual flow = ratio of accumulated flow from SPA to total accumulated flow:
                actual_flow = pixel_area_ha * accuflow_spa / accuflow_total
                actual_flow[demand == ds_demand.nodata] = ds_actual_flow.nodata
                ds_actual_flow.write(actual_flow, window=window, indexes=1)

                # Mapping of actual flow per sector: split use into 'agricultural' and 'artificial'
                actual_flow_agri = actual_flow.copy()
                # agricultural: mask out non-agricultural areas
                actual_flow_agri[~np.isin(demand, self.landcover_agri_use)] = ds_actual_flow_agri.nodata
                ds_actual_flow_agri.write(actual_flow_agri, window=window, indexes=1)
                actual_flow_artif = actual_flow.copy()
                # artificial: mask out non-artificial areas
                actual_flow_artif[~np.isin(demand, self.landcover_artif_use)] = ds_actual_flow_artif.nodata
                ds_actual_flow_artif.write(actual_flow_artif, window=window, indexes=1)

                # Unmet demand
                # Unmet demand = demand(ha) - actual_flow(ha)
                unmet_demand = pixel_area_ha - actual_flow
                # mask out nodata points; actual flow nodata includes all demand nodata values
                unmet_demand[actual_flow == ds_actual_flow.nodata] = ds_unmet_demand.nodata
                ds_unmet_demand.write(unmet_demand, window=window, indexes=1)
                unmet_demand_agri = unmet_demand.copy()
                unmet_demand_agri[~np.isin(demand, self.landcover_agri_use)] = ds_unmet_demand_agri.nodata
                ds_unmet_demand_agri.write(unmet_demand_agri, window=window, indexes=1)
                unmet_demand_artif = unmet_demand.copy()
                unmet_demand_artif[~np.isin(demand, self.landcover_artif_use)] = ds_unmet_demand_artif.nodata
                ds_unmet_demand_artif.write(unmet_demand_artif, window=window, indexes=1)
            metadata = self.accord.metadata
            flood_use_input_rasters = [ds.name for ds in list(accuflow_datasets.values())
                                       + [ds_demand, ds_flow_accumulation]]
            metadata.update_dataset_tags(ds_actual_flow, 'Flood control use', 'hectare', *flood_use_input_rasters)
            metadata.update_dataset_tags(ds_actual_flow_agri, 'Flood control use by agriculture',
                                         'hectare', *flood_use_input_rasters)
            metadata.update_dataset_tags(ds_actual_flow_artif, 'Flood control use in areas with artificial land cover',
                                         'hectare', *flood_use_input_rasters)

        logger.info('Calculating use by population.')
        # Population use: multiply population exposure by ratio accumulated_flow_SPA / accumulated_flow_total
        file_population = self.pop_exposure.format(year=year)
        # metadata = Metadata('VITO', 'flood control', file_population)
        # file_flow_250 = helper_warp(self.flow_map[year], metadata, self.run_dir, wResampling='average')
        with rasterio.open(self.flow_map.format(year=year), 'r') as ds_flow, \
                rasterio.open(file_population, 'r') as ds_population, \
                rasterio.open(self.pop_use.format(year=year), 'w', **ds_flow.profile) as ds_pop_use, \
                rasterio.open(self.pop_unmet_demand.format(year=year), 'w', **ds_flow.profile) as ds_pop_unmet:
            for _, window in block_window_generator(block_shape, ds_flow.profile['height'],
                                                    ds_flow.profile['width']):
                flow = ds_flow.read(1, window=window)  # *divide* by actual_flow pixel size in ha
                population = ds_population.read(1, window=window)
                pop_use = np.multiply(flow, population,
                                      where=(flow != ds_flow.nodata) & (population != ds_population.nodata),
                                      out=np.full(population.shape, ds_pop_use.nodata), dtype=np.float64)
                ds_pop_use.write(pop_use, window=window, indexes=1)

                pop_unmet = np.multiply(1. - flow, population,
                                        where=(flow != ds_flow.nodata) & (population != ds_population.nodata),
                                        out=np.full(population.shape, ds_pop_use.nodata), dtype=np.float64)
                ds_pop_unmet.write(pop_unmet, window=window, indexes=1)
        #  Population use: multiply population exposure by ratio accumulated_flow_SPA / accumulated_flow_total
        #        logger.info('Calculating use by population.')

    def total_flow_accumulation(self, add_progress):
        """Run depression filling and flow accumulation on the entire provided DEM."""

        logger.debug('Fill DEM depressions and flow accumulation iterative based on hydrological basins.')
        # Read hydrological basins and select only in AOI
        hydro_basin = gpd.read_file(self.config[self.service_name]['hydro_basin'])
        # transform to epsg 3035
        hydro_basin = hydro_basin.to_crs(crs="EPSG:3035")
        hydro_basin = gpd.clip(hydro_basin, self.statistics_shape, keep_geom_type=True)
        # Loop over the DEM with a mask of every hydro basins
        # excuting flow accumulation per hydro basin and writing it out
        for index, basin in hydro_basin.iterrows():
            basin_id = basin['HYBAS_ID']

            # Convert MultiPolygon to list of Polygon geometries
            if basin['geometry'].geom_type == 'MultiPolygon':
                basin_geometries = list(basin['geometry'].geoms)
            else:
                basin_geometries = [basin['geometry']]

            # Crop the DEM to the catchment boundary using rasterio
            with rasterio.open(self.config[self.service_name]['dem']) as src:
                out_meta = src.meta.copy()
                basin_mask, out_transform = rasterio.mask.mask(src, basin_geometries, crop=True)
                out_meta.update({"driver": "GTiff",
                                 "height": basin_mask.shape[1],
                                 "width": basin_mask.shape[2],
                                 "transform": out_transform,
                                 "compress": "LZW"})

            with rasterio.open(os.path.join(self.temp_dir, 'DEM', f'dem_{basin_id}.tif'), "w", **out_meta) as dest:
                dest.write(basin_mask)
            dem = rd.LoadGDAL(os.path.join(self.temp_dir, 'DEM', f'dem_{basin_id}.tif'))
            # next function is self and adjust itself, notice the in_place argument
            rd.FillDepressions(dem, epsilon=True, in_place=True)
            accuflow_total = rd.FlowAccumulation(dem, method='D8')
            # write result
            rd.SaveGDAL(os.path.join(self.temp_dir, 'Flow_accumulation', f'accuflow_{basin_id}.tif'), accuflow_total)

        dir_flow_acc = os.path.join(self.temp_dir, 'Flow_accumulation')
        file_names = os.listdir(dir_flow_acc)
        full_path_names = [os.path.join(dir_flow_acc, file_name) for file_name in file_names]
        self.accord.merge_raster(full_path_names, os.path.join(self.temp_dir, 'total_flow_accumulation.tif'),
                                 mode='statistic')

        add_progress(100.0)

    def weighted_flow_accumulation(self, year, add_progress):
        """Calculate flow accumulation from service providing areas per ecosystem type."""
        # Loop over catchment areas to reduce calculation
        hydro_basin = gpd.read_file(self.config[self.service_name]['hydro_basin'])
        # transform to epsg 3035
        hydro_basin = hydro_basin.to_crs(crs="EPSG:3035")
        hydro_basin = gpd.clip(hydro_basin, self.statistics_shape, keep_geom_type=True)
        # hydro_basin = gpd.overlay(hydro_basin, self.statistics_shape, how='intersection')
        # Loop over the DEM with a mask of every hydro basins
        # excuting flow accumulation per hydro basin and writing it out
        # initilza
        dict_paths = {ecotype: [] for ecotype in _ecotype_supply}
        for index, basin in hydro_basin.iterrows():
            basin_id = basin['HYBAS_ID']
            # store output file name

            # Convert MultiPolygon to list of Polygon geometries
            if basin['geometry'].geom_type == 'MultiPolygon':
                basin_geometries = list(basin['geometry'].geoms)
            else:
                basin_geometries = [basin['geometry']]

            # Crop the DEM to the catchment boundary using rasterio
            with rasterio.open(self.config[self.service_name]['dem']) as src:
                out_meta = src.meta.copy()
                basin_mask, out_transform = rasterio.mask.mask(src, basin_geometries, crop=True)
                out_meta.update({"driver": "GTiff",
                                 "height": basin_mask.shape[1],
                                 "width": basin_mask.shape[2],
                                 "transform": out_transform,
                                 "compress": "LZW"})
            with rasterio.open(os.path.join(self.temp_dir, 'DEM', f'dem_{basin_id}.tif'), "w", **out_meta) as dest:
                dest.write(basin_mask)
            dem = rd.LoadGDAL(os.path.join(self.temp_dir, 'DEM', f'dem_{basin_id}.tif'))
            # next function is self and adjust itself, notice the in_place argument
            rd.FillDepressions(dem, epsilon=True, in_place=True)
            # for loop over ecosystem types per catchment
            for ecotype, spa_mask in self.spa_maes.items():
                logger.debug('Flow accumulation for service providing areas with ecosystem %s', ecotype)
                with rasterio.open(spa_mask.format(year=year)) as ds:
                    weights_eco_mask, out_transform = rasterio.mask.mask(ds, basin_geometries, crop=True, indexes=1)
                    # weights = weights_eco_mask.read(1)
                    out_meta = ds.meta.copy()
                    #             # use the meta data from the parent raster and update height and width
                    out_meta.update({"driver": "GTiff",
                                     "height": weights_eco_mask.shape[0],
                                     "width": weights_eco_mask.shape[1],
                                     "transform": out_transform,
                                     "dtype": np.float32,
                                     "nodata": -1}
                                    )
                    accuflow = rd.FlowAccumulation(dem, weights=weights_eco_mask.astype(np.float64),
                                                   # richdem wants float64 weights
                                                   method='D8')
                    with rasterio.open(os.path.join(self.temp_dir, "accu_per_eco", f'_{ecotype}_{year}_{basin_id}.tif'),
                                       'w', **out_meta) as dest:
                        dest.write(accuflow, 1)
                    dict_paths[ecotype].append(
                        os.path.join(self.temp_dir, "accu_per_eco", f'_{ecotype}_{year}_{basin_id}.tif'))

        # write result
        for ecotype in _ecotype_supply:
            dir_flow_acc = os.path.join(self.temp_dir, 'accu_per_eco')
            file_names = os.listdir(dir_flow_acc)
            full_path_names = [os.path.join(dir_flow_acc, file_name) for file_name in file_names
                               if re.search(f'{ecotype}', file_name)]
            if full_path_names:
                logger.info("searching fill names of basin based files")
            else:
                full_path_names = [os.path.join(dir_flow_acc, file_name) for file_name in file_names
                                   if re.search('pastures', file_name)]
            self.accord.merge_raster(full_path_names, self.flow_accumulation_maps[ecotype].format(year=year),
                                     mode='statistic')

        add_progress(100. / len(self.spa_maes))

    def monetary_value(self, year, add_progress, block_shape=(2048, 2048)):
        """Calculate monetary value of flood control use.

        We use flood maps for different return periods to calculate the expected avoided flood damage costs.
        """
        # map flood depths to damage function level:
        depth_thresholds = np.array([0.250,  # [0-000.250]: z0
                                     0.500,  # ]0.250-0.500]: z05
                                     1.000,  # ]0.500-1.000]: z1
                                     1.500,  # ]1.000-1.500]: z15
                                     2.500,  # ]1.500-2.500]: z2
                                     3.500,  # ]2.500-3.500]: z3
                                     4.500,  # ]3.500-4.500]: z4
                                     5.500,  # ]4.500-5.500]: z5
                                     np.inf])  # ]5.500-inf]: z6

        # damage functions: flood damage [EUR/m2] depending on data area, land cover code, and flood depth
        damage_functions = pd.read_excel(self.config[self.service_name]['damage_functions'],
                                         sheet_name=list(self.statistics_shape.index),
                                         index_col=0, engine='openpyxl')
        # transform depth column labels to integers 0-8
        for df in damage_functions.values():
            df.rename(columns={key: number for number, key in enumerate(df.columns)}, inplace=True)
        # transform damage functions into single series indexed by SHAPE_ID, land cover and depth:
        damage_functions = pd.concat((damage_functions[nuts_id].stack() for nuts_id in self.statistics_shape.index),
                                     keys=self.statistics_shape[SHAPE_ID])
        damage_functions.index.names = [SHAPE_ID, LANDCOVER_ID, 'depth_idx']
        logger.debug('Damage functions, 2007 prices:\n%s', damage_functions)
        # adjust damage functions, which are given in 2007 prices, to year 2000 prices:
        deflator = self.deflator.join(self.statistics_shape[SHAPE_ID], how='right').set_index(SHAPE_ID)
        damage_functions = damage_functions.multiply(deflator[2000] / deflator[2007],
                                                     axis=0, level=SHAPE_ID)
        logger.debug('Damage functions, 2000 prices:\n%s', damage_functions)
        damage_functions.rename('damage', inplace=True)  # Must have a name for later joins
        damage_functions_nominal = damage_functions.multiply(deflator[year] / deflator[2000],
                                                             axis=0, level=SHAPE_ID).rename('damage_nominal')

        file_landcover = self.config['land_cover'][year]
        with rasterio.open(file_landcover) as ds_lc, \
                rasterio.open(self.flow_map.format(year=year)) as ds_flow, \
                rasterio.open(self.statistics_raster) as ds_data_areas, \
                rasterio.open(self.value_map.format(year=year), 'w', **ds_flow.profile) as ds_value, \
                rasterio.open(self.value_map_nominal.format(year=year), 'w', **ds_flow.profile) as ds_value_nominal, \
                rasterio.open(self.value_agri.format(year=year), 'w', **ds_flow.profile) as ds_value_agri, \
                rasterio.open(self.value_artif.format(year=year), 'w', **ds_flow.profile) as ds_value_artif, \
                ExitStack() as stack:
            # Open flood depth datasets for each return period
            flood_depth_datasets = {return_period: stack.enter_context(rasterio.open(file))
                                    for return_period, file in self.config[self.service_name]['flood_maps'].items()}
            flood_damage_datasets = {return_period: stack.enter_context(
                rasterio.open(self.flood_damage.format(rp=return_period, year=year), 'w',
                              **dict(ds_depth.profile, compress='lzw', dtype=rasterio.float32, nodata=-9999.)))
                for return_period, ds_depth in flood_depth_datasets.items()}

            nblocks = math.ceil(float(ds_lc.height) / block_shape[0]) * math.ceil(float(ds_lc.width) / block_shape[1])
            for _, window in block_window_generator(block_shape, ds_lc.profile['height'], ds_lc.profile['width']):
                shape_ids = ds_data_areas.read(1, window=window)
                lc = ds_lc.read(1, window=window)
                flow = ds_flow.read(1, window=window)
                avoided_cost = {}
                avoided_cost_nominal = {}
                for rp, ds_depth in flood_depth_datasets.items():
                    ds_out = flood_damage_datasets[rp]
                    depth = ds_depth.read(1, window=window)
                    # find depth index for each point using numpy trick:
                    # depth_idx is the index of the first depth_threshold which is greater or equal to the depth value:
                    depth_idx = np.argmax(np.greater_equal(depth_thresholds, depth[:, :, np.newaxis]), axis=2)
                    depth_idx[depth == ds_depth.nodata] = -1
                    # assign damage depending on SHAPE_ID, land cover and depth_idx:
                    flood_damage = pd.DataFrame({SHAPE_ID: shape_ids.flatten(),
                                                 LANDCOVER_ID: lc.flatten(),
                                                 'depth_idx': depth_idx.flatten()}).join(
                        damage_functions, on=[SHAPE_ID, LANDCOVER_ID, 'depth_idx']).join(
                        damage_functions_nominal, on=[SHAPE_ID, LANDCOVER_ID, 'depth_idx'])
                    flood_damage[flood_damage.isna()] = ds_out.nodata
                    flood_damage_arr = flood_damage['damage'].to_numpy().astype(rasterio.float32).reshape(depth.shape)
                    ds_out.write(flood_damage_arr, window=window, indexes=1)

                    flow_nodata = flow == ds_flow.nodata

                    # Avoided cost: flow has unit [ha], flood damage has unit [EUR/m2] -> factor 10000
                    avoided_cost[rp] = np.multiply(flood_damage_arr * 10000, flow,
                                                   where=(flood_damage_arr != ds_out.nodata) & ~flow_nodata,
                                                   out=np.full(flow.shape, 0.))

                    flood_damage_nominal_arr = flood_damage['damage_nominal'].to_numpy().reshape(depth.shape)
                    avoided_cost_nominal[rp] = np.multiply(flood_damage_nominal_arr * 10000, flow,
                                                           where=((flood_damage_nominal_arr != ds_out.nodata)
                                                                  & ~flow_nodata),
                                                           out=np.full(flow.shape, 0.))
                    add_progress(100. / (nblocks * len(flood_depth_datasets)))

                value = expected_value(avoided_cost)
                value_nominal = expected_value(avoided_cost_nominal)
                # 3. set to nodata outside of actual flow:
                value[flow_nodata] = ds_value.nodata
                value_nominal[flow_nodata] = ds_value_nominal.nodata

                ds_value.write(value.astype(rasterio.float32), window=window, indexes=1)
                ds_value_nominal.write(value_nominal.astype(rasterio.float32), window=window, indexes=1)
                value_agri = value.copy()
                value_agri[~np.isin(lc, self.landcover_agri_use)] = ds_value_agri.nodata
                ds_value_agri.write(value_agri.astype(rasterio.float32), window=window, indexes=1)
                value_artif = value.copy()
                value_artif[~np.isin(lc, self.landcover_artif_use)] = ds_value_artif.nodata
                ds_value_artif.write(value_artif.astype(rasterio.float32), window=window, indexes=1)
            metadata = self.accord.metadata
            monetary_value_input_rasters = [ds_flow.name, ds_lc.name] + list(
                self.config[self.service_name]['flood_maps'].values())
            metadata.update_dataset_tags(ds_value, 'Flood control use monetary value',
                                         'million EUR (reference year 2000 value)',
                                         *monetary_value_input_rasters)
            metadata.update_dataset_tags(ds_value_agri, 'Flood control use by agriculture, monetary value',
                                         'million EUR (reference year 2000 value)',
                                         *monetary_value_input_rasters)
            metadata.update_dataset_tags(ds_value_artif,
                                         'Flood control use in areas with artificial land cover, monetary value',
                                         'million EUR (reference year 2000 value)',
                                         *monetary_value_input_rasters)
            metadata.update_dataset_tags(ds_value_nominal, 'Flood control use monetary value',
                                         'million EUR (nominal value)',
                                         *monetary_value_input_rasters)

    def reporting_stats(self, year, block_shape=(2048, 2048)):
        """Aggregate potential, demand, unmet demand, ... per reporting NUTS region."""
        logger.info('Calculate additional reporting statistics.')

        label_spa = 'Area with high potential runoff retention'
        label_demand = 'Extent of economic assets located in flood plains'
        label_unmet = 'Extent of economic assets in flood plains not protected by upstream ecosystems'

        # TODO calculate these aggregates together with the original maps
        with rasterio.open(self.spa.format(year=year)) as ds_spa, \
                rasterio.open(self.unmet.format(year=year)) as ds_unmet, \
                rasterio.open(self.demand.format(year=year)) as ds_demand, \
                rasterio.open(self.config['land_cover'][year]) as ds_landcover, \
                rasterio.open(self.reporting_raster) as ds_reporting:
            agg_potential = []
            agg_demand_unmet = []
            for _, window in block_window_generator(block_shape, ds_landcover.profile['height'],
                                                    ds_landcover.profile['width']):
                spa = ds_spa.read(1, window=window).flatten()
                lc = ds_landcover.read(1, window=window).flatten()
                demand = ds_demand.read(1, window=window).flatten()
                unmet = ds_unmet.read(1, window=window).flatten()

                shape_ids_report = ds_reporting.read(1, window=window).flatten()

                df_potential = pd.DataFrame({SHAPE_ID: shape_ids_report[spa == 1],
                                             LANDCOVER_ID: lc[spa == 1]})
                df_potential[label_spa] = 1  # Need to add a dummy column for count() to work?

                demand_flag = np.where(demand == ds_demand.nodata, 0, 1)

                df_demand_unmet = pd.DataFrame({SHAPE_ID: shape_ids_report,
                                                label_demand: demand_flag,
                                                label_unmet: unmet})
                df_demand_unmet.loc[unmet == ds_unmet.nodata, label_unmet] = 0.

                agg_potential.append(df_potential.groupby([SHAPE_ID, LANDCOVER_ID]).count())
                agg_demand_unmet.append(df_demand_unmet.groupby(SHAPE_ID).sum())

        # df_shape_id_nuts to translate SHAPE_ID's back to NUTS_ID's
        df_shape_id_nuts = self.reporting_shape[SHAPE_ID].reset_index().set_index(SHAPE_ID)
        # join with df_shape_id_nuts to retrieve NUTS_ID
        agg_demand_unmet = pd.concat(agg_demand_unmet).groupby(SHAPE_ID).sum().join(df_shape_id_nuts)
        agg_demand_unmet.set_index(NUTS_ID).to_excel(self.demand_national.format(year=year))

        agg_potential = pd.concat(agg_potential).groupby([SHAPE_ID, LANDCOVER_ID]).sum()
        agg_potential = agg_potential.join(df_shape_id_nuts)
        # group land cover in ecosystem types classes, and move ecosystem type to columns:
        ecotype_l1_landover = self.ecotypes_l1.set_index([ECOTYPE, LANDCOVER_ID])
        agg_potential = agg_potential.join(ecotype_l1_landover).groupby([NUTS_ID, 'short_name'])[label_spa].sum()
        agg_potential.unstack(level='short_name').to_excel(self.potential_national.format(year=year))
        # populations calculations
        label_demand_pop = 'Inhabitants in flood plains'
        label_use_pop = 'Inhabitants in flood plains protected by upstream ecosystems'
        label_unmet_pop = 'Inhabitants in flood plains not protected by upstream ecosystems'
        with rasterio.open(self.pop_exposure.format(year=year)) as ds_pop_demand, \
                rasterio.open(self.pop_use.format(year=year)) as ds_pop_use, \
                rasterio.open(self.pop_unmet_demand.format(year=year)) as ds_pop_unmet, \
                rasterio.open(self.reporting_raster) as ds_nuts:
            agg_population = []
            for _, window in block_window_generator(block_shape, ds_nuts.profile['height'],
                                                    ds_nuts.profile['width']):
                demand = ds_pop_demand.read(1, window=window).flatten()
                use = ds_pop_use.read(1, window=window).flatten()
                unmet = ds_pop_unmet.read(1, window=window).flatten()
                nuts = ds_nuts.read(1, window=window).flatten()

                demand[demand == ds_pop_demand.nodata] = 0
                use[use == ds_pop_use.nodata] = 0
                unmet[unmet == ds_pop_unmet.nodata] = 0

                df_pop = pd.DataFrame({_NUTS_NUM: nuts,
                                       label_demand_pop: demand,
                                       label_use_pop: use,
                                       label_unmet_pop: unmet})
                agg_population.append(df_pop.groupby(_NUTS_NUM).sum())
        agg_population = pd.concat(agg_population).groupby(_NUTS_NUM).sum().join(df_shape_id_nuts,
                                                                                 on=_NUTS_NUM,
                                                                                 how='inner')
        agg_population.set_index(NUTS_ID).to_csv(self.population_national.format(year=year))

    def calculate_sut(self, year, block_shape=(2048, 2048)):
        """Aggregate statistics per reporting region and ecosystem type."""
        logger.info('Calculating use table.')
        # SUT tables:
        # Use table: sum actual flow for pixels belonging to each land cover class
        # care I use the ecotype functions but replace it with the consumers classes!!!!!!
        use_series = self.use_sectors.set_index(ECONOMIC_UNIT)[LANDCOVER_ID]

        report_per_area_per_ecosystem_type_use = statistics_byArea_byET(
            self.flow_map.format(year=year),
            self.reporting_raster,
            self.reporting_shape[SHAPE_ID],
            self.demand.format(year=year),
            use_series)
        use_table = report_per_area_per_ecosystem_type_use.fillna(0.).groupby([NUTS_ID, ECOTYPE]).sum()['sum'].unstack(
            ECOTYPE)
        use_table.to_csv(self.use.format(year=year), index_label=NUTS_ID)

        if self.monetary:
            report_per_area_per_ecosystem_type_use_nominal_value = statistics_byArea_byET(
                self.value_map_nominal.format(year=year),
                self.reporting_raster,
                self.reporting_shape[SHAPE_ID],
                self.demand.format(year=year),
                use_series)
            use_table_nominal_value = \
            report_per_area_per_ecosystem_type_use_nominal_value.fillna(0.).groupby([NUTS_ID, ECOTYPE]).sum()[
                'sum'].unstack(ECOTYPE)
            use_table_nominal_value.to_csv(self.use.format(year=year), index_label=NUTS_ID)

            report_per_area_per_ecosystem_type_use_real_value = statistics_byArea_byET(
                self.value_map.format(year=year),
                self.reporting_raster,
                self.reporting_shape[SHAPE_ID],
                self.demand.format(year=year),
                use_series)
            use_table_real_value = \
            report_per_area_per_ecosystem_type_use_real_value.fillna(0.).groupby([NUTS_ID, ECOTYPE]).sum()[
                'sum'].unstack(ECOTYPE)
            use_table_real_value.to_csv(self.use.format(year=year), index_label=NUTS_ID)

        # 1. sum accuflow_spa from each Maes class per NUTS region
        logger.info('Calculating supply table.')
        supply_weights = {}
        for ecotype in _ecotype_supply:
            logger.debug('Aggregating flow accumulation from %s per NUTS region', ecotype)
            with rasterio.open(self.flow_accumulation_maps[ecotype].format(year=year)) as ds_accuflow, \
                    rasterio.open(self.demand.format(year=year)) as ds_demand:
                ecotype_flow = []
                for tuple in self.reporting_shape.itertuples():
                    try:
                        accuflow, _ = rasterio.mask.mask(ds_accuflow, [tuple.geometry], crop=True, indexes=1)
                        demand, _ = rasterio.mask.mask(ds_demand, [tuple.geometry], crop=True, indexes=1)
                        ecotype_flow.append(np.sum(accuflow,
                                                   where=((demand != ds_demand.nodata) &
                                                          (accuflow != ds_accuflow.nodata))))
                    except BaseException:
                        logger.exception('Error aggregating flow accumulation from %s for NUTS region %s.',
                                         ecotype, tuple.Index)
                supply_weights[ecotype] = ecotype_flow
        supply_weights = pd.DataFrame(supply_weights, index=self.reporting_shape.index)
        logger.debug('Supply weights (before correction):\n%s', supply_weights)
        # 2. adjust contribution for each ecosystem type class using correction factor based on average curve number
        # Mean CN per ecosystem type:
        ecotype_l1_landcover = self.ecotypes_l1.set_index([ECOTYPE, LANDCOVER_ID])
        mean_CN = self.soil_land_eco.join(ecotype_l1_landcover)[_CN].groupby(ECOTYPE).mean()
        correction_factor = ((100. - mean_CN) / 100.).rename('Correction factor')
        logger.debug('Supply table correction factor:\n%s', pd.concat([mean_CN, correction_factor], axis=1))
        # supply_weights columns have ecosystem type names -> index correction factor by names as well:
        correction_factor = pd.merge(correction_factor, ecosystem.types_l1.set_index(ECOTYPE),
                                     on=ECOTYPE).set_index('name')['Correction factor']
        supply_weights = supply_weights * correction_factor
        logger.debug('Weights after correction:\n%s', supply_weights)
        supply_weights = supply_weights.div(supply_weights.sum(axis=1), axis=0)
        logger.debug('Final weights:\n%s', supply_weights)
        # 3. multiply actual flow by weight factors for each ecosystem type
        total_use = use_table.sum(axis=1)
        logger.debug('Total use per NUTS region:\n%s', total_use)
        dict_supply = dict(ecosystem.types_l1[['name', 'short_name']].values)
        supply_table = supply_weights.mul(total_use, axis=0)
        supply_table = supply_table.fillna('')
        supply_table.rename(columns=dict_supply).to_csv(self.supply.format(year=year), index_label=NUTS_ID)
        if self.monetary:
            total_use_monetary = use_table_real_value.sum(axis=1)
            logger.debug('Total use per NUTS region [EUR]:\n%s', total_use_monetary)
            supply_table_monetary = supply_weights.mul(total_use_monetary, axis=0)
            supply_table_monetary.rename(columns=dict_supply).to_csv(
                self.supply_monetary.format(year=year), index_label=NUTS_ID)
            supply_table_monetary_nominal = supply_weights.mul(use_table_nominal_value.sum(axis=1), axis=0)
            supply_table_monetary_nominal.rename(columns=dict_supply).to_csv(
                self.supply_monetary_nominal.format(year=year), index_label=NUTS_ID)

        logger.info('Write SUT xlsx reports.')

        write_sut(self.sut_physical.format(year=year), supply_table, use_table,
                  self.service_name, year, 'ha')
        if self.monetary:
            write_sut(self.sut_monetary.format(year=year),
                      (supply_table_monetary / 10 ** 6).fillna(''), use_table_real_value / 10 ** 6,
                      self.service_name, year, 'Million EUR (2000 value)')
            write_sut(self.sut_monetary_nominal.format(year=year),
                      (supply_table_monetary_nominal / 10 ** 6).fillna(''), use_table_nominal_value / 10 ** 6,
                      self.service_name, year, 'Million EUR (nominal)')


def read_use_sectors(filename):
    """Read the CSV file mapping landcover values to economic units for the use table."""
    return pd.read_csv(filename, usecols=[LANDCOVER_ID, ECONOMIC_UNIT])
    # TODO ensure economic unit is one of standard use sectors


def expected_value(avoided_cost):
    """Calculate expected avoided cost: integrate function (flood_frequency, avoided_cost) using trapezoid rule.

    :param avoided_cost: Dict of {return_period: avoided_cost}
    :return: trapezoid sum.
    """
    # 1. order avoided cost rasters by frequency:
    freq_cost = [(1. / return_period, avoided_cost[return_period])
                 for return_period in reversed(sorted(avoided_cost))]
    # To estimate expected avoided cost for floods with higher return periods than the ones we calculated,
    # use the cost of the highest return period, with a frequency of 0
    _, avoided_cost0 = freq_cost[0]
    freq_cost = [(0., avoided_cost0)] + freq_cost
    # 2. trapezoid rule
    value = sum(0.5 * (cost1 + cost2) * (freq2 - freq1)
                for (freq1, cost1), (freq2, cost2) in zip(freq_cost[:-1], freq_cost[1:]))
    return value
