#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Soil Retention Service calculation.
"""
################## Importing modules ##################
# standard
import logging
import os
import rasterio
import pandas as pd
import numpy as np
import shutil
# own modules
import inca
from inca.common.config_check import ConfigItem, ConfigRaster, check_csv, YEARLY
from inca.common.ecosystem import types_l1, ECO_ID, SHAPE_ID
from inca.common.ecosystem import CROP
from inca.common.nuts import NUTS_ID
from inca.common.errors import ConfigError
from inca.common.geoprocessing import SUM, statistics_byArea_byET, block_window_generator, number_blocks, RasterType
from inca.common.suts import IND, write_sut
############## END importing modules ##################

logger = logging.getLogger(__name__)

# some globals for faster adaptation
_SR = 'Soil Retention'

# end of globals


class SoilRun(inca.Run):
    """processing class for the Soil Retention accounting service

    :param config: dictionary containing the service configuration (input YAML file information)
    """
    service_name = _SR

    def __init__(self, config):
        super().__init__(config)

        self.config_template.update({
            _SR: {
                'parameters': {
                    'avg_soil_formation': ConfigItem()},
                'biophysical': {
                    'cfactor': {YEARLY: ConfigRaster(raster_type=RasterType.RELATIVE)},
                    'k-factor': ConfigRaster(raster_type=RasterType.RELATIVE),
                    'ls-factor': ConfigRaster(raster_type=RasterType.RELATIVE),
                    'p-factor': ConfigRaster(raster_type=RasterType.RELATIVE),
                    'r-factor': ConfigRaster(raster_type=RasterType.RELATIVE)}
                }})

        if self.monetary:  # Inputs used only for monetary evaluation:
            logger.debug('Add config_template for monetary')
            self.config_template[self.service_name].update({
                'monetary': {
                    'retention_rate': ConfigItem(check_function=check_csv,
                                                 required_columns=[NUTS_ID, 'RR']),
                    'topsoil_N_content': ConfigRaster(raster_type=RasterType.RELATIVE),
                    'topsoil_P_content': ConfigRaster(raster_type=RasterType.RELATIVE),
                    'avg_prices': ConfigItem(check_function=check_csv,
                                             required_columns=['year', 'N_EUR_per_ton',
                                                               'P_EUR_per_ton', 'bulk_soil_EUR_per_ton'])}
            })
            self.config_template[self.service_name]['parameters'].update({
                'scaling_N_content': ConfigItem(),
                'scaling_P_content': ConfigItem()
            })

        # init some important variables
        self.result_files = {'dataframes': {}, 'map_paths': {}, 'sut_paths': {}}
        self.prices = None
        self.retention_rate = None
        self.annual_maps = {}
        self.make_output_filenames()

    def make_output_filenames(self):
        """generates the filenames for all final output files."""
        self.result_files['map_paths'].update(
            physical={year: os.path.join(self.maps, f'{_SR.replace(" ","-")}_map_use_tonnes_{year}.tif')
                      for year in self.years},
            monetary_current={year: os.path.join(self.maps,
                                                 f'{_SR.replace(" ","-")}_map_use-cropland_EURO-current_{year}.tif')
                              for year in self.years},
            monetary_real={year: os.path.join(self.maps,
                                              f'{_SR.replace(" ","-")}_map_use-cropland_EURO-real_{year}.tif')
                           for year in self.years})
        self.result_files['sut_paths'].update(
            physical={year: os.path.join(self.suts, f'{_SR.replace(" ","-")}_report_SUT-physical_tonnes_{year}.xlsx')
                      for year in self.years},
            monetary_current={year: os.path.join(self.suts,
                                                 f'{_SR.replace(" ","-")}_report_SUT-monetary_EURO-current_{year}.xlsx')
                              for year in self.years},
            monetary_real={year: os.path.join(self.suts,
                                              f'{_SR.replace(" ","-")}_report_SUT-monetary_EURO-real_{year}.xlsx')
                           for year in self.years})
        self.result_files['dataframes'].update(
            physical={year: None for year in self.years},
            monetary_current={year: None for year in self.years},
            monetary_real={year: None for year in self.years})

    def _start(self):
        """start of the service processing."""
        # create missing folder
        os.makedirs(self.additional_results, exist_ok=True)

        # get some universal tables
        if self.monetary:
            self.prices = pd.read_csv(self.config[_SR]['monetary']['avg_prices'], comment='#').set_index('year')
            self.retention_rate = pd.read_csv(self.config[_SR]['monetary']['retention_rate'],
                                              comment='#').set_index(NUTS_ID)
        # get weights for progressbar for processing steps
        weights = self.get_progress_weights()

        # loop over all processing years
        for year in self.years:
            self.year = year
            logger.info(f'=> running processing for year {self.year}')

            # run some additional configuration checks for this processing year
            if self.monetary:
                self.annual_config_checks()

            # Ecosystem Service Potential
            try:
                self.calculatePotential(block_shape=(4096, 4096),
                                        add_progress=lambda p: self.add_progress(p * weights['potential']))
            except Exception:
                logger.exception('Error during Potential calculation')
                raise

            # Ecosystem Service Demand, USE
            try:
                self.calculateDemandErosion(block_shape=(4096, 4096),
                                            add_progress=lambda p: self.add_progress(p * weights['demand&erosion']))
            except Exception:
                logger.exception('Error during Demand&Erosion calculation')
                raise

            # Ecosystem Service FLOW and unmet-demand
            try:
                self.calculateFlowMismatch(block_shape=(4096, 4096),
                                           add_progress=lambda p: self.add_progress(p * weights['flow&unmet']))
            except Exception:
                logger.exception('Error during Flow&Mismatch calculation')
                raise

            if self.monetary:
                # calculate N_retained, P_retained and structural component for monetary evaluation
                try:
                    self.prepareAuxMonetary(block_shape=(4096, 4096),
                                            add_progress=lambda p: self.add_progress(p * weights['prepareMoney']))
                except Exception:
                    logger.exception('Error during generation of Aux datasets for monetary evaluation')
                    raise

                # Monetary Evaluation
                try:
                    self.runMonetaryEvaluation(block_shape=(4096, 4096),
                                               add_progress=lambda p: self.add_progress(p * weights['money']))
                except Exception:
                    logger.exception('Error during monetary evaluation')
                    raise

            # statistic extraction for reporting regions (sequestration & retention)
            self.reporting_area_statistics(add_progress=lambda p: self.add_progress(p * weights['statistics']))

            # masking and cutting of all maps to reporting regions
            self.final_map_preparation(add_progress=lambda p: self.add_progress(p * weights['final_map']))

            # reporting of SUT and CSV statistics (sequestration & retention)
            self.reporting()
        return

    def calculatePotential(self, add_progress, block_shape=(4096, 4096)):
        """function to calculate the annual Ecosystem Service Potentials.

        :param add_progress: progressbar object
        :param block_shape: size of blocks in processing
        """
        logger.info('Calculate annual Ecosystem Service Potential')

        # get files together
        path_c_factor = self.config[_SR]['biophysical']['cfactor'][self.year]
        logger.debug('* usage of c-factor file: {}'.format(os.path.basename(path_c_factor)))

        # create output file path
        self.annual_maps['potential'] = os.path.join(self.additional_results,
                                                     f'{_SR.replace(" ","-")}_map_potential_ratio_{self.year}.tif')
        # set breaker if file already exists
        if os.path.exists(self.annual_maps['potential']):
            return

        try:
            # set up dst profile and metadata
            profile_dst = self.accord.ref_profile.copy()
            profile_dst.update(count=1,
                               dtype=rasterio.float32,
                               nodata=np.nan)
            self.accord.metadata.read_raster_tags([path_c_factor])

            # run block processing
            with rasterio.open(path_c_factor) as src, \
                    rasterio.open(self.statistics_raster) as src_extent, \
                    rasterio.open(self.annual_maps['potential'], 'w', **profile_dst) as dst:
                # set tags
                dst.update_tags(**self.accord.metadata.prepare_raster_tags('Generation of Ecosystem Service Potential '
                                                                           'for year {}.'.format(self.year), 'ratio'))
                nblocks = number_blocks(self.accord.ref_profile, block_shape)
                # now iterate over the blocks of src files and process
                for index, window in block_window_generator(block_shape, src.height, src.width):
                    # read data as masked array
                    aCFactor = src.read(1, window=window, masked=True)
                    # calculate potential
                    aPotential = ((0.55 - aCFactor) - 0.0001) / (0.55 - 0.0001)
                    # JRC wished adaptation - negative potentials are pushed to zero
                    aPotential[aPotential < 0] = 0

                    # apply additional wished masking of areas outside extent
                    aMask = src_extent.read(1, window=window)
                    aPotential[aMask == src_extent.nodata] = profile_dst['nodata']

                    # write block out
                    dst.write(aPotential.filled(profile_dst['nodata']).astype(profile_dst['dtype']),
                              1, window=window)
                    add_progress(100. / nblocks)
        except Exception:
            # here we delete file to rerun after error
            if os.path.exists(self.annual_maps['potential']):
                os.remove(self.annual_maps['potential'])
            raise IOError('generation of Potential file for year {} was not successful'.format(self.year))

    def calculateDemandErosion(self, add_progress, max_loss_rate=325.0, block_shape=(4096, 4096)):
        """function to calculate the annual Ecosystem Service Demand and annual Soil Erosion.

        :param add_progress: progressbar object
        :param max_loss_rate: max soil loss rate for masking extremes (in tonnes/hectare)
        :param block_shape: size of blocks in processing
        """
        logger.info('Calculate annual Ecosystem Service Demand and Soil Erosion')

        # get files together
        path_k = self.config[_SR]['biophysical']['k-factor']
        path_ls = self.config[_SR]['biophysical']['ls-factor']
        path_p = self.config[_SR]['biophysical']['p-factor']
        path_r = self.config[_SR]['biophysical']['r-factor']
        path_c_factor = self.config[_SR]['biophysical']['cfactor'][self.year]

        logger.debug('* usage of c-factor file: {}'.format(os.path.basename(path_c_factor)))
        logger.debug('* usage of k-factor file: {}'.format(os.path.basename(path_k)))
        logger.debug('* usage of ls-factor file: {}'.format(os.path.basename(path_ls)))
        logger.debug('* usage of p-factor file: {}'.format(os.path.basename(path_p)))
        logger.debug('* usage of r-factor file: {}'.format(os.path.basename(path_r)))

        # create output file paths
        self.annual_maps['demand'] = os.path.join(self.additional_results,
                                                  f'{_SR.replace(" ","-")}_map_demand_tonnes_{self.year}.tif')
        self.annual_maps['erosion'] = os.path.join(self.additional_results,
                                                   f'{_SR.replace(" ","-")}_map_erosion_tonnes_{self.year}.tif')
        # set breaker if file already exists
        if os.path.exists(self.annual_maps['demand']) and os.path.exists(self.annual_maps['erosion']):
            return

        try:
            # set up dst profile and metadata
            profile_dst = self.accord.ref_profile.copy()
            profile_dst.update(count=1,
                               dtype=rasterio.float32,
                               nodata=np.nan)
            # create tags for demand
            self.accord.metadata.read_raster_tags([path_k, path_ls, path_p, path_r])
            tags_demand = self.accord.metadata.prepare_raster_tags(
                'Generation of Ecosystem Service Demand for year {}. C factor is fixed to 0.55. Extreme values are set '
                'to nan using clamping mask from use (erosion) dataset.'.format(self.year), 'tonnes/pixel/year')
            # create tags for erosion
            self.accord.metadata.read_raster_tags([path_c_factor, path_k, path_ls, path_p, path_r])
            tags_erosion = self.accord.metadata.prepare_raster_tags(
                'Generation of annual soil erosion estimation using the RUSLE function for year {}. Extreme values are '
                'set to nan using a maximum soil erosion rate of {}.'.format(self.year, max_loss_rate),
                'tonnes/pixel/year')

            # Note: the RUSLE function calculates the soil loss per unit of area, where the unit is given by
            #       the unit of the K dataset for the period selected by R dataset
            #       --> in INCA we get the soil loss in tonnes/hectare/year
            #       Since in the tool maps are generated in absolute units (per pixel) to ease the statistic extraction
            #       --> we need an area adjustment
            px_adjustemnt_factor = self.accord.pixel_area_m2() / 10000.

            # run block processing
            with rasterio.open(path_k) as src_k, \
                    rasterio.open(path_ls) as src_ls, \
                    rasterio.open(path_p) as src_p, \
                    rasterio.open(path_r) as src_r, \
                    rasterio.open(path_c_factor) as src_c, \
                    rasterio.open(self.statistics_raster) as src_extent,\
                    rasterio.open(self.annual_maps['demand'], 'w', **profile_dst) as dst_demand, \
                    rasterio.open(self.annual_maps['erosion'], 'w', **profile_dst) as dst_erosion:
                # set tags
                dst_demand.update_tags(**tags_demand)
                dst_erosion.update_tags(**tags_erosion)
                nblocks = number_blocks(self.accord.ref_profile, block_shape)
                # now iterate over the blocks of src files and process
                for index, window in block_window_generator(block_shape, src_extent.height, src_extent.width):
                    # read data as masked array
                    aCFactor = src_c.read(1, window=window, masked=True)
                    aKFactor = src_k.read(1, window=window, masked=True)
                    aLSFactor = src_ls.read(1, window=window, masked=True)
                    aPFactor = src_p.read(1, window=window, masked=True)
                    aRFactor = src_r.read(1, window=window, masked=True)
                    aMask = src_extent.read(1, window=window)

                    # calculate rusle per pixel (not in tonnes/hectare)
                    # rusle = rfct * lsfct * kfct_filed * annual_c * pfct_wo_null
                    old_warn = np.seterr(all='ignore')  # suppress floating point errors from masked values
                    aRusle = aRFactor * aLSFactor * aKFactor * aCFactor.astype(np.float64) * aPFactor \
                             * px_adjustemnt_factor
                    np.seterr(**old_warn)
                    # create mask to get rid of extremes
                    mInValid = aRusle.data > (max_loss_rate * px_adjustemnt_factor)
                    # apply mask
                    aRusle[mInValid] = profile_dst['nodata']
                    # apply extent mask
                    aRusle[aMask == src_extent.nodata] = profile_dst['nodata']

                    # write to disk
                    dst_erosion.write(aRusle.filled(profile_dst['nodata']).astype(profile_dst['dtype']), 1,
                                      window=window)

                    # calculate demand = rfct * lsfct * kfct_filed * 0.55 * pfct_wo_null
                    old_warn = np.seterr(all='ignore')
                    aDemand = aRFactor * aLSFactor * aKFactor * 0.55 * aPFactor.astype(np.float64) \
                              * px_adjustemnt_factor
                    np.seterr(**old_warn)
                    # mask out areas outside the valid annual cfactor
                    # needed since the annual generated cfactor map includes
                    # the annual valid landcover regions & countries
                    aDemand[aCFactor.mask] = profile_dst['nodata']
                    # now we mask out also the areas which are extrems in the erosion dataset
                    aDemand[mInValid] = profile_dst['nodata']
                    # and now apply the extent mask over that
                    aDemand[aMask == src_extent.nodata] = profile_dst['nodata']
                    # write block out
                    dst_demand.write(aDemand.filled(profile_dst['nodata']).astype(profile_dst['dtype']),
                                     1, window=window)
                    add_progress(100. / nblocks)
        except Exception:
            # here we delete file to rerun after error
            if os.path.exists(self.annual_maps['demand']):
                os.remove(self.annual_maps['demand'])
            if os.path.exists(self.annual_maps['erosion']):
                os.remove(self.annual_maps['erosion'])
            raise IOError(
                'generation of  Demand and soil erosion file for year {} was not successful'.format(self.year))

    def calculateFlowMismatch(self, add_progress, block_shape=(2048, 2048)):
        """function to calculate the annual Ecosystem Service Flow and Mismatch.

        :param add_progress: progressbar object
        :param block_shape: size of blocks in processing
        """
        logger.info('Calculate annual Ecosystem Service Flow & Mismatch')

        # get files together
        path_demand = self.annual_maps['demand']
        path_erosion = self.annual_maps['erosion']
        logger.debug('* usage of demand file: %s', os.path.basename(path_demand))
        logger.debug('* usage of soil erosion file: %s', os.path.basename(path_erosion))

        # create output file paths
        self.annual_maps['physical'] = os.path.join(self.temp_dir,
                                                    os.path.basename(self.result_files['map_paths']['physical']
                                                                     [self.year]).split('.tif')[0] +
                                                    r'_statistical-regions.tif')
        self.annual_maps['mismatch'] = os.path.join(self.additional_results,
                                                    f'{_SR.replace(" ","-")}_map_mismatch_tonnes_{self.year}.tif')
        # set breaker if file already exists
        if os.path.exists(self.annual_maps['physical']) and os.path.exists(self.annual_maps['mismatch']):
            return

        try:
            # set up dst profile and metadata
            profile_dst = self.accord.ref_profile.copy()
            profile_dst.update(count=1,
                               dtype=rasterio.float32,
                               nodata=np.nan)
            # create tags for flow
            self.accord.metadata.read_raster_tags([path_demand, path_erosion])
            tags_flow = self.accord.metadata.prepare_raster_tags(
                'Generation of Ecosystem Service Flow (aka use or soil retained) for year {}.'.format(
                    self.year), 'tonnes/pixel/year')
            # create tags for unmet demand
            self.accord.metadata.read_raster_tags([path_erosion])
            tags_mismatch = self.accord.metadata.prepare_raster_tags(
                'Generation of Ecosystem Service Mismatch (Unmet Demand) for year {}.'.format(
                    self.year), 'tonnes/pixel/year')

            # Note: the average soil formation factor is given in tonnes/hectare/year
            #       Since in the demand and erosion (use) are calculated in tonnes/pixel
            #       --> we need an area adjustment for the average soil formation factor
            #       IMPORTANT: given under the assumption that factor is equal within the specified area
            px_adjustemnt_factor = self.accord.pixel_area_m2() / 10000.

            # run block processing
            with rasterio.open(path_demand) as src_demand, \
                    rasterio.open(path_erosion) as src_erosion, \
                    rasterio.open(self.annual_maps['physical'], 'w', **profile_dst) as dst_flow, \
                    rasterio.open(self.annual_maps['mismatch'], 'w', **profile_dst) as dst_mismatch:
                # set tags
                dst_flow.update_tags(**tags_flow)
                dst_mismatch.update_tags(**tags_mismatch)
                nblocks = number_blocks(self.accord.ref_profile, block_shape)
                # now iterate over the blocks of src files and process
                for index, window in block_window_generator(block_shape, src_demand.height, src_demand.width):
                    # read data as masked array
                    aDemand = src_demand.read(1, window=window, masked=True)
                    aErosion = src_erosion.read(1, window=window, masked=True)

                    # calculate flow
                    aFlow = aDemand - aErosion
                    # write block out
                    dst_flow.write(aFlow.filled(profile_dst['nodata']).astype(profile_dst['dtype']),
                                   1, window=window)

                    # calculate unmet demand
                    # TODO: the avearge soil formation rate of the EU could be replaced by country specific numbers
                    aMismatch = aErosion - (float(self.config[_SR]['parameters']['avg_soil_formation']) *
                                            px_adjustemnt_factor)
                    # mask out values below 0
                    aMismatch[aMismatch < 0] = 0
                    # write block out
                    dst_mismatch.write(aMismatch.filled(profile_dst['nodata']).astype(profile_dst['dtype']),
                                       1, window=window)
                    add_progress(100. / nblocks)
        except Exception:
            # here we delete file to rerun after error
            if os.path.exists(self.annual_maps['physical']):
                os.remove(self.annual_maps['physical'])
            if os.path.exists(self.annual_maps['mismatch']):
                os.remove(self.annual_maps['mismatch'])
            raise IOError('generation of Service Flow and Mismatch files '
                          'for year {} was not successful'.format(self.year))

    def prepareAuxMonetary(self, add_progress, block_shape=(4096, 4096)):
        """function to generate the auxiliary information to do the monetary evaluation.

        IN detail: calculated the nutrient N_retained, P_retained and the retained structural component from the
        soilRetained

        :param add_progress: progressbar object
        :param block_shape: size of blocks for processing
        """
        logger.info('Generate auxiliary datasets for monetary evaluation')

        # get files together
        path_n_content = self.config[_SR]['monetary']['topsoil_N_content']
        path_p_content = self.config[_SR]['monetary']['topsoil_P_content']
        path_soil_retained = self.annual_maps['physical']

        logger.debug('* usage of flow file: %s', os.path.basename(path_soil_retained))
        logger.debug('* usage of nitrogen content file: %s', os.path.basename(path_n_content))
        logger.debug('* usage of phosphorous content file: %s', os.path.basename(path_p_content))

        # create output file path
        self.annual_maps['Nretained'] = os.path.join(self.temp_dir,
                                                     f'{_SR.replace(" ","-")}_map_N-retained_tonnes_{self.year}.tif')
        self.annual_maps['Pretained'] = os.path.join(self.temp_dir,
                                                     f'{_SR.replace(" ","-")}_map_P-retained_tonnes_{self.year}.tif')
        self.annual_maps['BSretained'] = os.path.join(self.temp_dir,
                                                      f'{_SR.replace(" ","-")}_map_BulkSoil_tonnes_{self.year}.tif')
        # set breaker if file already exists
        if os.path.exists(self.annual_maps['Nretained']) and os.path.exists(self.annual_maps['Pretained']) and \
                os.path.exists(self.annual_maps['BSretained']):
            return

        try:
            # set up dst profile and metadata
            profile_dst = self.accord.ref_profile.copy()
            profile_dst.update(count=1,
                               dtype=rasterio.float32,
                               nodata=np.nan)
            # create tags for N_retained
            self.accord.metadata.read_raster_tags([path_soil_retained, path_n_content])
            tags_N_retained = self.accord.metadata.prepare_raster_tags(
                'Calculation of pure Nitrogen (metric tonnes) retained in the '
                'Ecosystem Service Flow (soil retained) for year {}.'.format(
                    self.year), 'ton/pixel/year')
            # create tags for P_retained
            self.accord.metadata.read_raster_tags([path_soil_retained, path_p_content])
            tags_P_retained = self.accord.metadata.prepare_raster_tags(
                'Calculation of pure Phophorous (metric tonnes) retained in the '
                'Ecosystem Service Flow (soil retained) for year {}.'.format(
                    self.year), 'ton/pixel/year')
            # create tags for bulksoil_retained
            self.accord.metadata.read_raster_tags([path_soil_retained, path_p_content, path_n_content])
            tags_bulksoil_retained = self.accord.metadata.prepare_raster_tags(
                'Calculation of structural component (metric tonnes) retained in the '
                'Ecosystem Service Flow (soil retained) for year {}.'.format(
                    self.year), 'ton/pixel/year')

            # run block processing
            with rasterio.open(path_soil_retained) as src_flow, \
                    rasterio.open(path_n_content) as src_Ncontent, \
                    rasterio.open(path_p_content) as src_Pcontent, \
                    rasterio.open(self.annual_maps['Nretained'], 'w', **profile_dst) as dst_Nretained, \
                    rasterio.open(self.annual_maps['Pretained'], 'w', **profile_dst) as dst_Pretained, \
                    rasterio.open(self.annual_maps['BSretained'], 'w', **profile_dst) as dst_BSretained:
                # set tags
                dst_Nretained.update_tags(**tags_N_retained)
                dst_Pretained.update_tags(**tags_P_retained)
                dst_BSretained.update_tags(**tags_bulksoil_retained)
                nblocks = number_blocks(self.accord.ref_profile, block_shape)
                # now iterate over the blocks of src files and process
                for index, window in block_window_generator(block_shape, src_flow.height, src_flow.width):
                    # read data as masked array
                    aSoilretained = src_flow.read(1, window=window, masked=True)
                    aNcontent = src_Ncontent.read(1, window=window, masked=True)
                    aPcontent = src_Pcontent.read(1, window=window, masked=True)

                    # calculate N and P retained
                    old_warn = np.seterr(all='ignore')
                    aNretained = aSoilretained * (aNcontent * (1. / float(self.config[_SR]['parameters']
                                                                          ['scaling_N_content'])))
                    aPretained = aSoilretained * (aPcontent * (1. / float(self.config[_SR]['parameters']
                                                                          ['scaling_P_content'])))
                    np.seterr(**old_warn)
                    # here comes a tricky part - if now N or P is masked the structural component could be still
                    # valid.  Therefore, we have to fill masked values with 0 for the correct calculation
                    # nevertheless in the writeout the fill value can be different
                    aBSretained = aSoilretained - aNretained.filled(0) - aPretained.filled(0)

                    # write block out
                    dst_Nretained.write(aNretained.filled(profile_dst['nodata']).astype(profile_dst['dtype']),
                                        1, window=window)
                    dst_Pretained.write(aPretained.filled(profile_dst['nodata']).astype(profile_dst['dtype']),
                                        1, window=window)
                    dst_BSretained.write(aBSretained.filled(profile_dst['nodata']).astype(profile_dst['dtype']),
                                         1, window=window)
                    add_progress(100. / nblocks)
        except Exception:
            # here we delete file to rerun after error
            if os.path.exists(self.annual_maps['Nretained']):
                os.remove(self.annual_maps['Nretained'])
            if os.path.exists(self.annual_maps['Pretained']):
                os.remove(self.annual_maps['Pretained'])
            if os.path.exists(self.annual_maps['BSretained']):
                os.remove(self.annual_maps['BSretained'])
            raise IOError('generation of auxiliary data for monetary '
                          'evaluation for year {} was not successful'.format(self.year))

    def runMonetaryEvaluation(self, add_progress, block_shape=(2048, 2048)):
        """function to run the monetary evaluation in the soil_retention module.

        Note: only evaluation for cropland is currently implemented

        :param add_progress: progressbar object
        :param block_shape: size of blocks for processing
        """
        logger.info('run the Monetary Evaluation')

        # get files together
        path_Nretained = self.annual_maps['Nretained']
        path_Pretained = self.annual_maps['Pretained']
        path_Structural = self.annual_maps['BSretained']
        path_clc = self.get_ecosystem_raster(self.year, level=1)
        path_regions = self.statistics_raster
        # get additional data
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()
        region_dic = self.statistics_shape[SHAPE_ID].to_dict()
        Nprice = self.prices.loc[int(self.year), 'N_EUR_per_ton']
        Pprice = self.prices.loc[int(self.year), 'P_EUR_per_ton']
        BSprice = self.prices.loc[int(self.year), 'bulk_soil_EUR_per_ton']
        factor_real_price = self.deflator[2000] / self.deflator[self.year]

        logger.debug('* usage of lc file: {}'.format(os.path.basename(path_clc)))
        logger.debug('* usage of Nretained file: {}'.format(os.path.basename(path_Nretained)))
        logger.debug('* usage of Pretained file: {}'.format(os.path.basename(path_Pretained)))
        logger.debug('* usage of BulkSoil file: {}'.format(os.path.basename(path_Structural)))
        logger.debug('* usage of nominal N price: {}'.format(Nprice))
        logger.debug('* usage of nominal P price: {}'.format(Pprice))
        logger.debug('* usage of nominal BulkSoil price: {}'.format(BSprice))

        # create output file path
        self.annual_maps['monetary_current'] = os.path.join(self.temp_dir,
                                                            os.path.basename(self.result_files['map_paths']
                                                                             ['monetary_current'][self.year]).
                                                            split('.tif')[0] + r'_statistical-regions.tif')
        self.annual_maps['monetary_real'] = os.path.join(self.temp_dir,
                                                         os.path.basename(self.result_files['map_paths']
                                                                          ['monetary_real'][self.year]).
                                                         split('.tif')[0] + r'_statistical-regions.tif')
        # set breaker if file already exists
        if os.path.exists(self.annual_maps['monetary_current']) and os.path.exists(self.annual_maps['monetary_real']):
            return

        try:
            # set up dst profile and metadata
            profile_dst = self.accord.ref_profile.copy()
            profile_dst.update(count=1,
                               dtype=rasterio.float32,
                               nodata=np.nan)
            # create tags for nominal map
            self.accord.metadata.read_raster_tags([path_Nretained, path_Pretained, path_Structural,
                                                   path_clc, path_regions])
            tags_nominal = self.accord.metadata.prepare_raster_tags(f'Monetary evaluation for replacement costs of '
                                                                    f'soil retention in cropland for year {self.year} '
                                                                    f'in nominal prices. Prices for N ({Nprice}), '
                                                                    f'P ({Pprice}) and bulk soil ({BSprice}) per ton.',
                                                                    'Euro/pixel/year')
            self.accord.metadata.read_raster_tags([path_Nretained, path_Pretained, path_Structural,
                                                   path_clc, path_regions])
            tags_real = self.accord.metadata.prepare_raster_tags(f'Monetary evaluation for replacement costs of soil '
                                                                 f'retention in cropland for year {self.year} in real '
                                                                 f'prices. Prices for N, P and bulk soil per ton are '
                                                                 f'adjusted to the year 2000 by region specific '
                                                                 f'deflator values.',
                                                                 'Euro/pixel/year')

            # run block processing
            with rasterio.open(path_Nretained) as src_Nretained, \
                    rasterio.open(path_Pretained) as src_Pretained, \
                    rasterio.open(path_Structural) as src_BSretained, \
                    rasterio.open(path_clc) as src_LC, \
                    rasterio.open(path_regions) as src_regions, \
                    rasterio.open(self.annual_maps['monetary_current'], 'w', **profile_dst) as dst_current, \
                    rasterio.open(self.annual_maps['monetary_real'], 'w', **profile_dst) as dst_real:
                # set tags
                dst_current.update_tags(**tags_nominal)
                dst_real.update_tags(**tags_real)
                nblocks = number_blocks(self.accord.ref_profile, block_shape)
                # now iterate over the blocks of src files and process
                for index, window in block_window_generator(block_shape, src_LC.height, src_LC.width):
                    # read data for monetary evaluation
                    aN = src_Nretained.read(1, window=window, masked=True)
                    aP = src_Pretained.read(1, window=window, masked=True)
                    aBS = src_BSretained.read(1, window=window, masked=True)
                    aCLC = src_LC.read(1, window=window)
                    aRegions = src_regions.read(1, window=window)

                    aNominal = np.full_like(aN, fill_value=profile_dst['nodata'], dtype=profile_dst['dtype'])
                    aReal = np.full_like(aN, fill_value=profile_dst['nodata'], dtype=profile_dst['dtype'])

                    # calculate Euro per pixel in nominal prices and real prices
                    # (€_N_component_per_ton * retention coefficient * N_tonne_per_pixel)
                    #    + (€_P_component_per_ton * retention coefficient * P_tonne_per_pixel)
                    #    + (€_bulk_soil_per_ton * tSR_per_pixel)
                    for region, region_value in region_dic.items():
                        # first nominal price
                        RetCoef = float(self.retention_rate.loc[region, 'RR'])
                        mValid = (aRegions == region_value)
                        old_warn = np.seterr(all='ignore')  # suppress floating point errors from masked values
                        aNominal[mValid] = (aN[mValid].filled(0) * RetCoef * float(Nprice)) + \
                                           (aP[mValid].filled(0) * RetCoef * float(Pprice)) + \
                                           (aBS[mValid] * float(BSprice)).filled(profile_dst['nodata'])
                        # second real price
                        conversionFactor = factor_real_price.loc[region]
                        aReal[mValid] = aNominal[mValid] * conversionFactor
                        np.seterr(**old_warn)
                    # mask out all areas which are not cropland
                    aNominal[aCLC != eco_dic[CROP]] = profile_dst['nodata']
                    aReal[aCLC != eco_dic[CROP]] = profile_dst['nodata']
                    # write block out
                    dst_current.write(aNominal.astype(profile_dst['dtype']), 1, window=window)
                    dst_real.write(aReal.astype(profile_dst['dtype']), 1, window=window)
                    add_progress(100. / nblocks)
        except Exception:
            # here we delete file to rerun after error
            if os.path.exists(self.annual_maps['monetary_current']):
                os.remove(self.annual_maps['monetary_current'])
            if os.path.exists(self.annual_maps['monetary_real']):
                os.remove(self.annual_maps['monetary_real'])
            raise IOError('generation of monetary evaluation for year {} was not successful'.format(self.year))

    def reporting(self):
        """export of reporting area statistics to SUT Excel files and CSV statistic files."""
        logger.info(f'statistics export')
        logger.info(f'* CSV statistic files')
        # in CSV files we use the short names
        dic_short = types_l1.set_index('name')['short_name'].to_dict()

        lReporting = ['physical']
        if self.monetary:
            lReporting = lReporting + ['monetary_real', 'monetary_current']

        for element in lReporting:
            path_export = os.path.join(self.statistic_dir,
                                       os.path.basename(self.result_files['sut_paths'][element][self.year])
                                       .replace('_report_SUT-', '_statistics_').split('.xlsx')[0] + '.csv')
            self.result_files['dataframes'][element][self.year]\
                .rename(columns=dic_short).to_csv(path_export, columns=types_l1['short_name'].tolist(),
                                                  index_label=NUTS_ID, float_format='%.3f')

        logger.info(f'* SUT Excel files')
        # prepare the unit and scaling dic
        scaling_dic = {'physical': 1000, 'monetary_real': 1000000, 'monetary_current': 1000000}
        unit_dic = {'physical': '1000 tonnes',
                    'monetary_real': 'Million EUR (2000 value)',
                    'monetary_current': 'Million EUR (nominal)'}

        for element in lReporting:
            path_export = self.result_files['sut_paths'][element][self.year]
            df_supply = self.result_files['dataframes'][element][self.year]
            df_use = df_supply[[CROP]].sum(axis=1).to_frame(name=IND)

            write_sut(path_export, df_supply / scaling_dic[element], df_use / scaling_dic[element],
                      service_name=f'{_SR}', year=self.year, unit=unit_dic[element])

    def reporting_area_statistics(self, add_progress):
        """extraction of the statistics for the reporting regions and assignment to results dict.

        :param add_progress: progressbar object
        """
        logger.info(f'extract statistics for reporting regions')
        # some basic parameters
        region_path = self.reporting_raster
        region_dic = self.reporting_shape[SHAPE_ID].to_dict()
        eco_path = self.get_ecosystem_raster(self.year, level=1)
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()

        stats_maps = ['physical']
        if self.monetary:
            stats_maps = stats_maps + ['monetary_current', 'monetary_real']

        # first for carbon net sequestration
        for element in stats_maps:
            df = statistics_byArea_byET(self.annual_maps[element], region_path, region_dic,
                                        eco_path, eco_dic,
                                        add_progress=lambda p: add_progress(p / len(stats_maps)),
                                        block_shape=(4096, 4096))
            self.result_files['dataframes'][element][self.year] = df[[SUM]].unstack(1)[SUM]

    def final_map_preparation(self, add_progress):
        """function which checks  all generated maps for the statistical regions and limit them to the
        reporting regions (in case reporting_regions != statistical_regions). includes masking and raster cropping.

        :param add_progress: progressbar object
        """
        logger.info(f'prepare maps for the reporting regions')
        result_maps = ['physical']
        if self.monetary:
            result_maps = result_maps + ['monetary_current', 'monetary_real']
        # first run check if reporting_regions == statistical_regions
        if self.config['statistics_shape'] == self.config['reporting_shape']:
            logger.info(f'* reporting regions == statistic regions -> copy data')
            try:
                for element in result_maps:
                    shutil.copy2(self.annual_maps[element], self.result_files['map_paths'][element][self.year])
                    add_progress(100. / len(result_maps))
            except BaseException:
                raise IOError('Could not copy the final GCR maps. Check for write protection in output folder.')
        else:
            logger.info(f'* maps have to be masked & cut to reporting regions')
            try:
                for element in result_maps:
                    self.accord.crop_2_reporting_AOI(self.annual_maps[element],
                                                     self.result_files['map_paths'][element][self.year],
                                                     self.reporting_raster,
                                                     add_progress=lambda p: add_progress(p / len(result_maps)),
                                                     block_shape=(4096, 4096))
            except BaseException:
                raise RuntimeError('the raster maps of the statistical regions could not transferred and masked '
                                   'to regional regions')

    def annual_config_checks(self):
        """ runs some important annual configuration checks to allow smooth operation."""
        # check that annual price exist in the needed tables
        if self.year not in self.prices.index:
            raise ConfigError(f'The processing year {self.year} is not present in the average nutrient & '
                              f'bulksoil prices.', [_SR, 'monetary', 'avg_prices'])

        missing_regions = ~self.statistics_shape.index.isin(self.retention_rate.index)
        if missing_regions.any():
            raise ConfigError('Retention Rate table does not contain all data areas: missing ' +
                              ', '.join(self.statistics_shape.index[missing_regions]) + '.',
                              [_SR, 'monetary', 'retention_rate'])

    def get_progress_weights(self):
        """Return a dict desribing the relative amount of time spent in each part of the calculation, based on an
         example run, and the relative time taken up by the main calculation vs init."""
        if self.monetary:
            time_spent = {'potential': 5,
                          'demand&erosion': 23,
                          'flow&unmet': 14,
                          'prepareMoney': 23,
                          'money': 21,
                          'statistics': 8,
                          'final_map': 0. if self.config['statistics_shape'] == self.config['reporting_shape'] else 8}
        else:
            time_spent = {'potential': 5,
                          'demand&erosion': 23,
                          'flow&unmet': 14,
                          'prepareMoney': 0,
                          'money': 0,
                          'statistics': 8,
                          'final_map': 0. if self.config['statistics_shape'] == self.config['reporting_shape'] else 3}
        total_time = sum(time_spent.values())
        return {key: time / total_time for key, time in time_spent.items()}
