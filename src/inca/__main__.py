# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Command line interface for INCA."""

import argparse
import logging
import sys

import yaml

import inca
import inca.cropprovision
import inca.floodcontrol
import inca.woodprovision
import inca.soilretention
import inca.tourism
import inca.airfiltration
import inca.globalclimate
import inca.croppollination
import inca.localclimate

from inca.common.errors import Error, ConfigError


logger = logging.getLogger(inca.__name__)


def parse_args():
    """Parse command line arguments."""
    common_parser = argparse.ArgumentParser(prog=inca.__name__, add_help=False)
    common_parser.add_argument('--output-dir', help='Default OUTPUT-DIR is current working directory.', metavar='DIR')
    common_parser.add_argument('--run-name', help='Output is written to OUTPUT-DIR/RUN-NAME.', metavar='NAME')
    common_parser.add_argument('--continue', action='store_const', const=True,
                               help='Continue from an existing run directory.')
    common_parser.add_argument('--verbose', action='store_const', const=True)
    common_parser.add_argument('--years', type=int, nargs='+',
                               metavar='YEAR', help='Years for which to run the calculation.')
    common_parser.add_argument('config', nargs='?', help='yaml configuration file for the account calculation.')

    # TODO replace by BooleanOptionalAction once we move to Python >= 3.9.
    grp_monetary = common_parser.add_mutually_exclusive_group()
    grp_monetary.add_argument('--monetary', action='store_const', const=True,
                              help='Run monetary evaluation if availabe for current service.')
    grp_monetary.add_argument('--no-monetary', action='store_const', const=False, dest='monetary',
                              help='Do not run monetary evaluation.')

    parser = argparse.ArgumentParser(prog=inca.__name__)
    subparsers = parser.add_subparsers(title='subcommands', required=True, dest='ecosystem service')

    parser_crop = subparsers.add_parser('crop_provision', parents=[common_parser])
    parser_crop.set_defaults(func=inca.cropprovision.CropRun)
    inca.cropprovision.set_cmdline_args(parser_crop)

    parser_flood = subparsers.add_parser('flood_control', parents=[common_parser])
    parser_flood.set_defaults(func=inca.floodcontrol.FloodRun)

    parser_tourism = subparsers.add_parser('tourism', parents=[common_parser])
    parser_tourism.set_defaults(func=inca.tourism.TourismRun)

    parser_airfiltration = subparsers.add_parser('air_filtration', parents=[common_parser])
    parser_airfiltration.set_defaults(func=inca.airfiltration.AirFiltrationRun)
    inca.airfiltration.set_cmdline_args(parser_airfiltration)

    parser_wood = subparsers.add_parser('wood_provision', parents=[common_parser])
    parser_wood.set_defaults(func=inca.woodprovision.WoodRun)

    parser_soil = subparsers.add_parser('soil_retention', parents=[common_parser])
    parser_soil.set_defaults(func=inca.soilretention.SoilRun)

    parser_globalClimate = subparsers.add_parser('global_climate_regulation', parents=[common_parser])
    parser_globalClimate.set_defaults(func=inca.globalclimate.GlobalClimateRun)
    inca.globalclimate.set_cmdline_args(parser_globalClimate)

    parser_pollination = subparsers.add_parser('crop_pollination', parents=[common_parser])
    parser_pollination.set_defaults(func=inca.croppollination.PollinationRun)

    parser_localclimate = subparsers.add_parser('local_climate_regulation', parents=[common_parser])
    parser_localclimate.set_defaults(func=inca.localclimate.LocalClimateRun)

    return parser.parse_args()


def main():
    """Start a run from the command line ."""
    args = parse_args()
    inca.set_up_console_logging(verbose=args.verbose)
    if args.config:
        with open(args.config) as f:
            config = yaml.safe_load(f)
    else:
        config = dict()

    # Override config keys with 'not None' command line arguments:
    config.update({key: val for key, val in vars(args).items() if val is not None})

    # Set default output dir if not specified at command line or in config file.
    if 'output_dir' not in config:
        config['output_dir'] = '.'

    try:
        run = args.func(config)  # Construct the ecosystem Run for the selected subparser, and pass the config dict.
        run.start()
        logger.info('End.')
    except ConfigError as e:
        logger.error('INCA configuration error: %s Check configuration section "%s"',
                     e.message, ': '.join(str(x) for x in e.path))
        sys.exit(1)
    except Error as e:
        logger.error('INCA error: %s', e.message)
        sys.exit(1)


if __name__ == '__main__':
    main()