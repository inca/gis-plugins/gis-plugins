# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Eurostat tsv reading tools."""


import pandas as pd


_FREQ = 'freq'
_ANNUAL = 'A'


flags = {
    'b': 'break in time series',
    'c': 'confidential',
    'd': 'definition differs, see metadata',
    'e': 'estimated',
    'f': 'forecast',
    'n': 'not significant',
    'p': 'provisional',
    'r': 'revised',
    's': 'Eurostat estimate',
    'u': 'low reliability',
    'z': 'not applicable'
}


AL = 'AL'
AM = 'AM'
AR = 'AR'
AT = 'AT'
AU = 'AU'
AZ = 'AZ'
BA = 'BA'
BE = 'BE'
BG = 'BG'
BR = 'BR'
BY = 'BY'
CA = 'CA'
CH = 'CH'
CN_X_HK = 'CN_X_HK'
CY = 'CY'
CZ = 'CZ'
DE = 'DE'
DK = 'DK'
DZ = 'DZ'
EE = 'EE'
EG = 'EG'
EL = 'EL'
ES = 'ES'
FI = 'FI'
GE = 'GE'
FR = 'FR'
HK = 'HK'
HR = 'HR'
HU = 'HU'
IE = 'IE'
IL = 'IL'
IN = 'IN'
IS = 'IS'
IT = 'IT'
JO = 'JO'
JP = 'JP'
KR = 'KR'
KS = 'XK'
LB = 'LB'
LI = 'LI'
LT = 'LT'
LU = 'LU'
LV = 'LV'
LY = 'LY'
MA = 'MA'
MD = 'MD'
ME = 'ME'
MK = 'MK'
MT = 'MT'
MX = 'MX'
NG = 'NG'
NL = 'NL'
NO = 'NO'
NZ = 'NZ'
PL = 'PL'
PS = 'PS'
PT = 'PT'
RO = 'RO'
RS = 'RS'
RU = 'RU'
SE = 'SE'
SG = 'SG'
SI = 'SI'
SK = 'SK'
SY = 'SY'
TN = 'TN'
TR = 'TR'
TW = 'TW'
UA = 'UA'
UK = 'UK'
US = 'US'
XK = 'XK'
ZA = 'ZA'


country_names = {
    AL: 'Albania',
    AM: 'Armenia',
    AR: 'Argentina',
    AT: 'Austria',
    AZ: 'Azerbaijan',
    AU: 'Australia',
    BA: 'Bosnia and Herzegovina',
    BE: 'Belgium',
    BG: 'Bulgaria',
    BR: 'Brazil',
    BY: 'Belarus',
    CA: 'Canada',
    CH: 'Switzerland',
    CN_X_HK: 'China (except Hong Kong)',
    CY: 'Cyprus',
    CZ: 'Czechia',
    DE: 'Germany',
    DK: 'Denmark',
    DZ: 'Algeria',
    EG: 'Egypt',
    EE: 'Estonia',
    EL: 'Greece',
    ES: 'Spain',
    FI: 'Finland',
    FR: 'France',
    GE: 'Georgia',
    HR: 'Croatia',
    HU: 'Hungary',
    IE: 'Ireland',
    IL: 'Israel',
    IS: 'Iceland',
    IT: 'Italy',
    JO: 'Jordan',
    LB: 'Lebanon',
    LI: 'Liechtenstein',
    LT: 'Lithuania',
    LU: 'Luxembourg',
    LV: 'Latvia',
    LY: 'Lybia',
    KR: 'South Korea',
    MA: 'Morocco',
    MD: 'Moldova',
    ME: 'Montenegro',
    MK: 'North Macedonia',
    MT: 'Malta',
    MX: 'Mexico',
    NG: 'Nigeria',
    NL: 'Netherlands',
    NO: 'Norway',
    NZ: 'New Zealand',
    PL: 'Poland',
    PS: 'Palestine',
    PT: 'Portugal',
    RO: 'Romania',
    RS: 'Serbia',
    RU: 'Russia',
    SG: 'Singapore',
    SE: 'Sweden',
    SI: 'Slovenia',
    SK: 'Slovakia',
    SY: 'Syria',
    TN: 'Tunisia',
    TR: 'Turkey',
    TW: 'Taiwan',
    UA: 'Ukraine',
    UK: 'United Kingdom',
    US: 'United States',
    XK: 'Kosovo',
    ZA: 'South Africa'}


def make_membership_table():
    """Return a DataFrame indexed by NUTS_ID, with boolean columns for membership status for the years 2000-2021."""
    # Tuples with accession and (optionally) secession date for EU member countries
    membership_dates = {
        BE: (1958,),
        FR: (1958,),
        IT: (1958,),
        LU: (1958,),
        NL: (1958,),
        DE: (1958,),
        DK: (1973,),
        IE: (1973,),
        UK: (1973, 2020),
        EL: (1981,),
        PT: (1986,),
        ES: (1986,),
        AT: (1995,),
        FI: (1995,),
        SE: (1995,),
        CY: (2004,),
        CZ: (2004,),
        EE: (2004,),
        HU: (2004,),
        LV: (2004,),
        LT: (2004,),
        MT: (2004,),
        PL: (2004,),
        SK: (2004,),
        SI: (2004,),
        BG: (2007,),
        RO: (2007,),
        HR: (2013,),
    }

    df = pd.DataFrame(columns=list(range(2000, 2022)), index=list(country_names.keys()), data=False)
    for country, dates in membership_dates.items():
        accession = dates[0]
        df.loc[country, df.columns >= accession] = True
        if len(dates) == 2:  # i.e. we have a country that left, i.e. the UK ;-)
            secession = dates[1]
            df.loc[country, df.columns >= secession] = False
    return df


# DataFrame with a 'geo' index and a boolean column for each year, indicating membership status:
is_member = make_membership_table()


def eu_members(year: int):
    """Return an index containing only the EU member countries for the given year."""
    return is_member.index[is_member[year].values]


def string_to_number(x: pd.Series):
    """Remove all superfluous string formatting from a CSV column and convert to float."""
    x = x.str.replace(',', '')  # remove thousands separators
    x = x.str.replace(':', '')  # ':' indicates missing value

    # strip trailing spaces and flags defined by eurostat which may appear:
    allflags = ''.join(flags.keys())
    x = x.str.rstrip(' ' + allflags)

    return pd.to_numeric(x, errors='coerce')  # coerce empty string to NaN


def read_tsv(tsvfile):
    """Read .tsv file from the Eurostat website into a DataFrame.

    Format is documented at https://wikis.ec.europa.eu/display/EUROSTATHELP/Which+are+the+available+formats.
    """
    # Read data as strings because read_csv's builtin str->float conversion options aren't flexible enough for ESTAT's
    # TSV files:
    df = pd.read_csv(tsvfile, delimiter='\t', dtype='str')
    # First column is actually a comma-separated multi-index -> recreate multi-index and drop the column
    names = df.columns[0].split('\\')[0].split(',')  # first column label contains spurious '\time' -> throw that away
    df.index = pd.MultiIndex.from_frame(df.iloc[:, 0].str.split(',', expand=True), names=names)
    df.drop(df.columns[0], axis=1, inplace=True)

    # Custom TSV files downloaded from the Eurostat data explorer may have an extra index level 'freq' (data frequency).
    # If this is the case, only select data at annual level.
    # 1. drop time period columns for other than yearly data.  These are:
    #  - months (e.g. 1999-01),
    #  - quarters (e.g. 2015-Q1),
    #  - semesters (e.g. 2000-S1)
    df.drop((col for col in df.columns if not col.rstrip().isdigit()), axis=1, inplace=True)
    # 2. Cross-sect to keep only annual values from index level 'freq'.
    if _FREQ in df.index.names:
        df = df.xs(_ANNUAL, level=_FREQ)

    # Transform column year labels (which include trailing spaces...) to ints
    df.columns = [int(name) for name in df.columns]
    # Convert
    return df.apply(string_to_number)


def write_tsv(df, filename):
    """Write a DataFrame in EuroStat .tsv format."""
    # Join the multi-index columns with ',':
    index_label = ','.join(df.index.names)
    index_data = [df.index.get_level_values(level).values for level in df.index.names]
    df[index_label] = [','.join(index_values) for index_values in zip(*index_data)]

    # Write as tab-separated file using the new index.
    df.set_index(index_label).fillna(':').to_csv(filename, sep='\t')


def filter_eu27(df, index_level=-1):
    """Reduce a DataFrame indexed by country code to just those countries in the 2020 EU-27.

    :param df: The input DataFrame.
    :param index_level: level of the country code in the DataFrame index.
    """
    eu_27 = is_member[2020]

    # boolean index
    df_is_member = eu_27.reindex(df.index.get_level_values(index_level), fill_value=False).values

    return df[df_is_member]
