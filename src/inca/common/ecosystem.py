# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""EU ecosystem type classification.

We have ecosystem classifications at level 1 and level 2.  At level 1, we have types numbered from 1 to 12.  Level 2
types are a subdivision of the 12 level 1 types, numbered 1.1, 1.2, ... 12.9.  For each type, we also have a name (text
string), and an ecosystem_id, which is an integer number that can be used in categorical maps.  Using floating point
numbers for categorical data complicates things unnecessarily, and can lead to issues with floating point precision.

Except for categorical maps, the ecosystem_id's are an implementation detail that shouldn't concern users.  User input
data should use the ecosystem types and/or full text names
"""

import numpy as np
import pandas as pd

from inca.common.errors import Error

_MAES = 'Maes'
ECOTYPE = 'ecosystem_type'
ECOTYPE_L1 = 'ecosystem_type_L1'
ECOTYPE_L2 = 'ecosystem_type_L2'
ECO_ID = 'ecosystem_id'
LANDCOVER_ID = 'landcover_id'
# SHAPE_ID: GeoDataFrames will have a column with an integer identifier used for rasterization
SHAPE_ID = 'SHAPE_ID'

# canonical names:
ARTIFICIAL = 'Settlements and other artificial areas'
CROP = 'Cropland'
GRASSLAND = 'Grassland (pastures, semi-natural and natural grassland)'
FOREST = 'Forest and woodland'
HEATH = 'Heathland and shrub'
SPARSE = 'Sparsely vegetated ecosystems'
WETLANDS = 'Inland wetlands'
RIVERS = 'Rivers and canals'
LAKES = 'Lakes and reservoirs'
MARINE_TRANSITIONAL = 'Marine inlets and transitional waters'
COASTAL = 'Coastal beaches, dunes and wetlands'
MARINE = 'Marine ecosystems (offshore coastal shelf and open ocean)'

ecotype_l1_cat = pd.CategoricalDtype(range(1, 13), ordered=True)
ecotype_l2_cat = pd.CategoricalDtype([1.1, 1.2, 1.3, 1.4, 1.5,
                                      2.1, 2.2, 2.3, 2.4, 2.5, 2.6,
                                      3.1, 3.2,
                                      4.1, 4.2, 4.3, 4.4, 4.5, 4.6,
                                      5.1, 5.2, 5.3,
                                      6.1, 6.2, 6.3,
                                      7.1, 7.2,
                                      8.1, 8.2,
                                      9.1, 9.2, 9.3,
                                      10.1, 10.2, 10.3, 10.4,
                                      11.1, 11.2, 11.3, 11.4,
                                      12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8], ordered=True)

#: Standard level 1 ecosystem types
types_l1 = pd.DataFrame([[1, ARTIFICIAL, 'URB'],
                         [2, CROP, 'CRP'],
                         [3, GRASSLAND, 'GRS'],
                         [4, FOREST, 'WOO'],
                         [5, HEATH, 'HEA'],
                         [6, SPARSE, 'SVL'],
                         [7, WETLANDS, 'WET'],
                         [8, RIVERS, 'RIC'],
                         [9, LAKES, 'LAR'],
                         [10, MARINE_TRANSITIONAL, 'MTR'],
                         [11, COASTAL, 'CBD'],
                         [12, MARINE, 'MAE']],
                        columns=[ECOTYPE, 'name', 'short_name']).astype({ECOTYPE: ecotype_l1_cat})
types_l1[ECO_ID] = types_l1[ECOTYPE].astype(int)

#: Standard level 2 ecosystem types
types_l2 = pd.DataFrame([[1.1, 'Continuous settlement area'],
                         [1.2, 'Discontinuous settlement area'],
                         [1.3, 'Infrastructure'],
                         [1.4, 'Urban greenspace'],
                         [1.5, 'Other artificial areas'],
                         [2.1, 'Annual cropland'],
                         [2.2, 'Rice fields'],
                         [2.3, 'Permanent crops'],
                         [2.4, 'Agro-forestry areas'],
                         [2.5, 'Mixed farmland'],
                         [2.6, 'Other farmland'],
                         [3.1, 'Sown pastures and fields (modified grasslands)'],
                         [3.2, 'Natural and semi-natural grasslands'],
                         [4.1, 'Broadleaved deciduous forest'],
                         [4.2, 'Coniferous forests'],
                         [4.3, 'Broadleaved evergreen forest'],
                         [4.4, 'Mixed forests'],
                         [4.5, 'Transitional forest and woodland shrub'],
                         [4.6, 'Plantations'],
                         [5.1, 'Tundra'],
                         [5.2, 'Heathland and (sub-)alpine shrub'],
                         [5.3, 'Sclerophyllous vegetation'],
                         [6.1, 'Bare rocks'],
                         [6.2, '(Semi-)desert and other sparsely vegetated areas'],
                         [6.3, 'Ice sheets glaciers and perennial snow fields'],
                         [7.1, 'Inland marshes on mineral soils'],
                         [7.2, 'Mires bogs and fens'],
                         [8.1, 'Rivers'],
                         [8.2, 'Canals ditches and drains'],
                         [9.1, 'Lakes'],
                         [9.2, 'Artificial reservoirs'],
                         [9.3, 'Geothermal pools and wetlands (Iceland)'],
                         [10.1, 'Coastal lagoons'],
                         [10.2, 'Estuaries and bays'],
                         [10.3, 'Intertidal flats'],
                         [10.4, 'Deepwater coastal inlets'],
                         [11.1, 'Artificial shorelines'],
                         [11.2, 'Coastal dunes beaches and sandy and muddy shores'],
                         [11.3, 'Rocky shores'],
                         [11.4, 'Coastal saltmarshes and salines'],
                         [12.1, 'Marine macrophytes'],
                         [12.2, 'Coral reefs'],
                         [12.3, 'Shellfish beds and reefs'],
                         [12.4, 'Subtidal sand beds and mud plains'],
                         [12.5, 'Subtidal rocky substrates'],
                         [12.6, 'Continental and island slopes'],
                         [12.7, 'Deepwater benthic and pelagic ecosystems'],
                         [12.8, 'Sea ice']],
                        columns=[ECOTYPE, 'name']).astype({ECOTYPE: ecotype_l2_cat})
# For L2 ecosystem_id, transform floating point codes 1.1, 1.2, ... 12.9 to integers 1001, 1002, ... 120009
types_l2[ECO_ID] = [1000 * int(l1) + int(l2) for l1, l2 in
                    (str(x).split('.') for x in types_l2[ECOTYPE])]


# Standard mapping of CORINE land use types to level 2 ecosystem types
# level 1 ecosystem types are derived from this mapping (drop the subtype to get a number from 1-12)
# Limitations to be treated by pre-processing and revised mapping table if using CLCACC
# - (no impact L1) split urbanGreen and discontinuedUrbanFabric into Other artificial (cementaries, archaelogical)
# - (no impact L1) split Annual crop anc complex cultivation into Other farmland (nurseries, bioenergy, ...)
# - (no impact L1) split Broad-leaved forest in deciduous and evergreen
# - (no impact L1) split forest categories in Plantations
# - (no impact L1) split Moors and heathland into tundra, scrub and heathland
# - (impact L1) split 331 bare rocks between L1 sparse veg & L1 coastal ecosystem
# - (impact L1) split 332 semi-desert, desert between L1 sparse veg & L1 coastal ecosystem
# - (impact L1) split 333 sparse veg between L1 sparse veg & L1 coastal ecosystem
# - (impact L1) 511 (water courses) can also be ET 8 rivers and canals
# - (impact L1) 334 assign burnt area to previous 'year' ET or to balancing item/unallocated
_corine_mapping_l2 = {1.1: [111], 1.2: [112, 121],  # Urban fabric
                      1.3: [122, 123, 124,   # Industrial, commercial and transport units
                            131, 132, 133],  # Mine, dump and construction sites
                      1.4: [141, 142],       # Green urban areas
                      1.5: [],               # Other artificial areas (part of 141 and 112)
                      2.1: [211, 212],             # Arable land
                      2.2: [213],                  # Rice fields
                      2.3: [221, 222, 223, 241],   # Permanent crops
                      2.4: [244],                  # Agro-forestry
                      2.5: [242, 243],             # Heterogeneous agricultural areas
                      2.6: [],                     # Other farmland (part of 241 and 242)
                      3.1: [231],            # Sown pastures and other (modified) grass
                      3.2: [321],            # Natural and semi-natural grassland
                      4.1: [311],            # Broadleaved deciduous forest
                      4.2: [312],            # Coniferous forest
                      4.3: [],               # Broadleaved evergreen forest (part of 311)
                      4.4: [313],            # Mixed forest
                      4.5: [324],            # Transitional woodland-shrub
                      4.6: [],               # Plantations (part of 311, 312, 323)
                      5.1: [],                     # Tundra (part of 322)
                      5.2: [322],                  # Scrub and heathland
                      5.3: [323],                  # Sclerophyllous vegetation
                      6.1: [332],            # Bare rocks
                      6.2: [333],            # Semi-desert, desert and other sparsely vegetated areas
                      6.3: [335],            # Glaciers and perpetual snow
                      7.1: [411],                  # Inland marshes and other wetlands on mineral soils
                      7.2: [412],                  # Mires, bogs and fens
                      8.1: [511],            # Rivers
                      8.2: [],               # Canals, ditches and drains (part of 511)
                      9.1: [512],            # Lakes and ponds
                      9.2: [],               # Artificial reservoirs (part of 512)
                      9.3: [],               # Geothermal pools and wetlands (Iceland)
                      10.1: [521],                 # Coastal lagoons
                      10.2: [522],                 # Estuaries and bays
                      10.3: [423],                 # Intertidal flats
                      10.4: [],                    # Salt marshes and salines (part of 421 and 422)
                      11.1: [],              # Artificial shorelines (part of 111, 112, 122, 123)
                      11.2: [331],           # Coastal dunes, beaches and sandy & muddy shores (using coastal zonation)
                      11.3: [],              #
                      11.4: [421, 422],      # Coastal salt marshes, Salines
                      12.1: [], 12.2: [], 12.3: [], 12.4: [], 12.5: [], 12.6: [], 12.7: [], 12.8: []}

def df_corine_mapping(level=1):
    """Create a DataFrame relating CORINE landcover id's with EU L2 ecosystem type and name."""
    df_l2 = pd.DataFrame([(type_l2, clc_code) for type_l2 in _corine_mapping_l2
                          for clc_code in _corine_mapping_l2[type_l2]],
                         columns=[ECOTYPE, LANDCOVER_ID]).astype({ECOTYPE: ecotype_l2_cat,
                                                                  LANDCOVER_ID: np.int16})
    if level == 1:
        # for level 1, transform level 2 id's to int and rename to column to ECOTYPE_L1:
        df_l1 = df_l2.astype({ECOTYPE: int}).astype({ECOTYPE: ecotype_l1_cat})
        # join with level 1 names and id's
        return df_l1.merge(types_l1, on=ECOTYPE)
    elif level == 2:
        return df_l2.merge(types_l2, on=ECOTYPE)
    else:
        raise ValueError(f'df_corine_mapping() should be called with level=1 or 2, got {level}.')


def read_mapping(filename, level):
    """Read a csv file describing the mapping of landcover codes to ecosystem types."""
    df = pd.read_csv(filename, dtype={ECOTYPE_L1: ecotype_l1_cat, ECOTYPE_L2: ecotype_l2_cat})

    if level == 1:
        if (ECOTYPE_L2 in df.columns) and (ECOTYPE_L1 not in df.columns):
            # if L1 requested, but input mapping contains L2, convert L2 to L1.
            # First convert to int, then convert to the L1 category.
            df[ECOTYPE] = df[ECOTYPE_L2].astype(float).fillna(-1).astype(int).astype(ecotype_l1_cat)
        else:
            df[ECOTYPE] = df[ECOTYPE_L1]
        # Merge with L1 categories for 'name' and ECO_ID column:
        result = df[[ECOTYPE, LANDCOVER_ID]].merge(types_l1, on=ECOTYPE)
    elif level == 2:
        # Note: presence of ECOTYPE_L2 should be verified during initial config_check for services that need it
        df[ECOTYPE] = df[ECOTYPE_L2]
        # Merge with L2 categories for 'name' and ECO_ID column:
        result = df[[ECOTYPE, LANDCOVER_ID]].merge(types_l2, on=ECOTYPE)

    # Check all ecosystem type values could be converted to a known category:
    if result[ECOTYPE].isna().any():
        raise Error(f'Illegal L1 or L2 Ecosystem type value in file {filename}.')
    # Convert ECO_ID to int before returning
    return result.astype({ECO_ID: int})
