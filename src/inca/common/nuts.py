# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Utility functions to work with the bundled NUTS shapes."""

import sys

import shapely.geometry

if sys.version_info[:2] >= (3, 9):
    import importlib.resources as importlib_resources
else:
    import importlib_resources

import geopandas as gpd

import inca.common.estat

NUTS_ID = 'NUTS_ID'


def load_nuts_gpkg() -> gpd.GeoDataFrame:
    """Return the bundled NUTS geopackage contents."""
    ref = importlib_resources.files('inca.data').joinpath('NUTS_RG_03M_2021_3035_LEVL012_EU2020.gpkg')
    with ref.open('rb') as f:
        return gpd.read_file(f, encoding='utf-8').set_index(NUTS_ID).sort_index()


_nuts012 = load_nuts_gpkg()


def get_nuts_shape(level=0, clip=True):
    """Return a GeoDataFrame with 2021 NUTS regions, indexed by NUTS_ID."""
    shape = _nuts012[_nuts012['LEVL_CODE'] == level]
    if clip:
        return gpd.clip(shape, bbox_eu_main)
    return shape


def get_eu_2020() -> gpd.GeoDataFrame:
    """Return EU2020 member states from the bundled 2021 NUTS-0 regions, clipped to the default INCA bounding box."""
    return get_nuts_shape().reindex(inca.common.estat.eu_members(2020))


# Default bounding box excluding overseas territories
bbox_eu_main = shapely.geometry.box(2549000, 1386000, 6558000, 5447000)
