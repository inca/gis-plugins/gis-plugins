# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Create supply and use tables in .xlsx format."""

from typing import Union
import os
import pandas as pd
import logging
import xlsxwriter

import inca.common.ecosystem as ecosystem

logger = logging.getLogger(__name__)

IND = 'Intermediate consumption by industries'
GOV = 'Government final consumption'
HSH = 'Households final consumption'
GCF = 'Gross capital formation'
EXP = 'Exports'

SECTOR = 'Sector'

#: Economic sectors for supply tables.
use_sector_cat = pd.CategoricalDtype([IND, GOV, HSH, GCF, EXP])

df_sectors = pd.DataFrame([[IND, 'P2'],
                           [GOV, 'P3_S13'],
                           [HSH, 'P3_S14'],
                           [GCF, 'P5'],
                           [EXP, 'P6']],
                          columns=[SECTOR, 'short_name']).astype({SECTOR: use_sector_cat})

_NUMBER_FORMAT_FLOAT = '0.00;-0.00;-;@'
_NUMBER_FORMAT_INT = '0;-0;-;@'


def write_sut(filename, supply, use, service_name='N/A', year='N/A', unit='N/A', region='all regions'):
    if os.path.exists(filename):
        # filename = filename.replace('.xlsx', f'_{datetime.now().strftime("%Y%m%d%H%M%S")}.xlsx')
        os.remove(filename)

    workbook = xlsxwriter.Workbook(filename)
    try:
        sheet_use = workbook.add_worksheet('Use')
        sheet_supply = workbook.add_worksheet('Supply')

        if isinstance(use, pd.Series):
            use = use.to_frame()
        if isinstance(supply, pd.Series):
            supply = supply.to_frame()

        assert all(col_name in df_sectors[SECTOR].values for col_name in use), \
            'Use table column labels should match standard sector names.'
        assert all(col_name in ecosystem.types_l1['name'].values for col_name in supply), \
            'Supply table column labels should match standard ecosystem type names.'

        title = f'{service_name} ({year}) [{unit}]'
        _write_table_xlsxwriter(sheet_use, use, df_sectors[SECTOR], title, region=region, workbook=workbook)
        _write_table_xlsxwriter(sheet_supply, supply, ecosystem.types_l1['name'], title, region=region, workbook=workbook)
    except Exception as e:
        logger.warning('It seems that something went wrong with the creation of the SUT: ' + str(e))
    finally:
        workbook.close()

def write_fuzzy_sut(filename, supply: Union[pd.DataFrame, pd.Series], use: Union[pd.DataFrame, pd.Series],
                    service_name='N/A', year='N/A', unit='N/A', region='all regions'):

    """Write a formatted Excel spreadsheet with supply and use tables.

     :param filename: output file name
     :param use: use table
     :param supply: supply table
     """
    if os.path.exists(filename):
        # filename = filename.replace('.xlsx', f'_{datetime.now().strftime("%Y%m%d%H%M%S")}.xlsx')
        os.remove(filename)

    workbook = xlsxwriter.Workbook(filename)
    try:
        sheet_use = workbook.add_worksheet('Use')
        sheet_supply = workbook.add_worksheet('Supply')

        if isinstance(use, pd.Series):
            use = use.to_frame()
        if isinstance(supply, pd.Series):
            supply = supply.to_frame()

        title = f'{service_name} ({year}) [{unit}]'
        _write_table_xlsxwriter(sheet_use, use, use.columns, title, region=region, workbook=workbook)
        _write_table_xlsxwriter(sheet_supply, supply, supply.columns, title, region=region, workbook=workbook)
    except Exception as e:
        logger.warning('It seems that something went wrong with the creation of the SUT: ' + str(e))
    finally:
        workbook.close()

def _write_table_xlsxwriter(worksheet, data, col_labels, title, region='all regions', workbook=None):
    # Define formats
    bold = workbook.add_format({'bold': True})
    number_format_float = workbook.add_format({'num_format': '0.00', 'border': 1})
    number_format_int = workbook.add_format({'num_format': '0', 'border': 1})
    header_format = workbook.add_format({'bold': True, 'text_wrap': True, 'border': 1})
    header_format.set_rotation(90)

    # Title
    worksheet.merge_range('B1:Z1', title, bold)

    # Setting up column widths and headers
    max_col_width = max(len(max(data.index, key=len)), len('all regions'))  # Find the maximum column width
    worksheet.set_column('B:B', max_col_width)  # Adjust the column width
    max_col_height = len(max(data.columns, key=len))  # Find the maximum column width
    worksheet.set_row(1, max_col_height * 6)  # Adjust the column width

    # Write column labels
    for col_num, value in enumerate(col_labels, 3):  # Start from column 2
        worksheet.write(1, col_num, value, header_format)
    worksheet.write(1, 2, 'total', header_format)

    # Write data
    for row_num, (idx, row_data) in enumerate(data.iterrows(), 2):  # Start from row 2
        for col_num, col in enumerate(col_labels, 3):
            if col in data.columns:
                value = row_data.loc[col]
                format_to_use = number_format_float if isinstance(value, float) else number_format_int
                worksheet.write(row_num, col_num, value, format_to_use)
            else:
                worksheet.write(row_num, col_num, '-', number_format_int)
        formula = f'=SUM({xlsxwriter.utility.xl_col_to_name(3)}{row_num + 1}:{xlsxwriter.utility.xl_col_to_name(len(col_labels) + 2)}{row_num + 1})'
        worksheet.write(row_num, 2, formula, number_format_int)

    # Write region in the first column
    for row_num, region_value in enumerate(data.index, 2):  # Start from row 2
        worksheet.write(row_num, 1, region_value)
    worksheet.write(len(data.index) + 2, 1, 'all regions', bold)

    # Write totals using formulas
    for col_num in range(2, len(col_labels) + 3):
        column_letter = xlsxwriter.utility.xl_col_to_name(col_num)
        formula = f'=SUM({column_letter}3:{column_letter}{len(data) + 2})'
        worksheet.write(len(data) + 2, col_num, formula, number_format_int)
    # write total of totals
    row_num = len(data) + 3
    formula = f'=SUM({xlsxwriter.utility.xl_col_to_name(3)}{row_num}:{xlsxwriter.utility.xl_col_to_name(len(col_labels) + 2)}{row_num})'
    worksheet.write(len(data.index) + 2, 2, formula, number_format_int)
