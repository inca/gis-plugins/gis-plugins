# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""INCA custom exceptions."""


class Error(Exception):
    """Base class for exceptions in inca.

    Raise :obj:`Error` (or a subclass) for known errors where we can provide a meaningful error message.
    """

    def __init__(self, message):
        """Create an INCA error with a message.

        :param message: Error message for the user.
        """
        self.message = message  #: Error message for users.


class ConfigError(Error):
    """Subclass to signal errors in the configuration or input files provided by the user."""

    def __init__(self, message, path):
        """Create an Exception which refers to a specific element of the run configuration.

        :obj:`ConfigError` contains a reference to the config section where the problem is found.  This can be used to
        tell the user which section of the configuration should be changed, or, when using a GUI, which input widget
        they should look at.  For example if there is a problem with the configuration value ``config['input']['maps'][
        'land_use']``, :attr:`ConfigError.path` should have the value ``['input', 'maps', 'land_use']``.

        :param message: Error message.
        :param path: List of keys pointing to the config

        """
        super().__init__(message)
        self.path = path  #: List of configuration keys pointing to the config value which caused the error.
