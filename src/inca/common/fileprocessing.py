# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Helper functions to locate files by a filename pattern."""

import logging
import os
import re

import pandas as pd

logger = logging.getLogger(__name__)


def scantree(path, fsymlinks):
    """Recursively yield DirEntry objects (only file names) for given directory.

    :param path: absolute path to the search directory
    :param fsymlinks: if symbolic links have to be followed
    """
    for entry in os.scandir(path):
        if entry.is_dir(follow_symlinks=fsymlinks) and not entry.name.startswith('.'):
            for entry in scantree(entry.path, fsymlinks):
                yield entry
        else:
            yield entry


def createDataFrame(path, pattern, fsymlinks=False, search_years=False):
    """Recursively find all files matching ``pattern`` in a given directory and get their path into a Pandas DataFrame.

    :param path: directory to search
    :param pattern: pattern to match
    :param fsymlinks: also list linked files
    :param search_years: optional - search for year interpretation in file name and adds column "year" in dataframe
    :return: dataframe listing the files
    """
    # ini empty list to hold the file info
    data = []

    # scan recursively the input folder and populate a list with data
    for entry in scantree(path, fsymlinks):
        if entry.is_file(follow_symlinks=fsymlinks) and entry.name.lower().endswith(pattern):
            data.append(os.path.normpath(entry.path))

    if len(data) != 0:
        df = pd.DataFrame(data, columns=['path'])
        df['basename'] = df['path'].apply(lambda x: os.path.basename(x))

        if search_years:
            # search for years in file names and extract
            lYears = []
            unknown_counter = 0
            for row in df.itertuples():
                match = re.match(r'.*([1-3][0-9]{3})', row.basename)
                if match is not None:
                    lYears.append([row.basename, int(match.group(1))])
                else:
                    lYears.append([row.basename, 'unknown'])
                    unknown_counter += 1

            if df.shape[0] == unknown_counter:
                df['year'] = 'universal'
            else:
                df_results = pd.DataFrame.from_records(lYears, columns=['basename', 'year'])
                df = df.merge(df_results, how='left', left_on='basename', right_on='basename')

        return df
    else:
        logger.exception('scantree-dataframe creation error: No valid files with the chosen pattern were found during '
                         'the file search and no data was extracted.')
        raise
