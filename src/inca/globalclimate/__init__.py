#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Global Climate Regulation (GlobalClimateRegulation) supply calculation.
"""
# Importing modules ##################
# standard
import logging
import os
import rasterio
import pandas as pd
import numpy as np
import shutil
import tempfile
# own modules
import inca
from inca.common.config_check import ConfigItem, ConfigRaster, ConfigChoice, check_csv, check_tsv, YEARLY
from inca.common.ecosystem import ECOTYPE_L2, LANDCOVER_ID, types_l1, ECOTYPE, ECO_ID, types_l2, SHAPE_ID
from inca.common.ecosystem import FOREST, CROP, GRASSLAND, WETLANDS, ARTIFICIAL, HEATH, SPARSE, COASTAL
from inca.common.nuts import NUTS_ID
from inca.common.errors import Error, ConfigError
from inca.common.geoprocessing import SUM, statistics_byArea_byET, block_window_generator, number_blocks, RasterType
from inca.common.estat import read_tsv
from inca.common.classification import reclassification
from inca.common.suts import GOV, write_sut
# END importing modules ##################

logger = logging.getLogger(__name__)

# some globals for faster adaptation
_GCR = 'Global Climate Regulation'
_CSEQ = 'carbon-net-sequestration'
_CRET = 'carbon-retention'

# Some index keys for EuroStat GGE tables:
_UNIT = 'THS_T'  # 1000 tonnes
_AIRPOL = 'CO2'  # GGE
_SRC_CRF = ["CRF4A", "CRF4B", "CRF4C", "CRF4D", "CRF4E", "CRF4F"]  # list of emmitters
_WOODREMOVAL = ["CRF4G"]  # carbon removal due to wood harvest

# some conversions on GGE tables
_UNIT_2_BASE = 1000.
_CO2_2_CARBON = 0.273
_SRC_CRF_DIC = {"CRF4A": FOREST, "CRF4B": CROP,
                "CRF4C": GRASSLAND, "CRF4D": WETLANDS,
                "CRF4E": ARTIFICIAL, "CRF4F": "otherlands"}  # GGE landtypes to ECOTYPE L1

# naming of retention calculation methods for opening stock
STOCK_DIFFERENCE = 'stock-difference'
GAIN_LOSS = 'gain-loss'
GAIN_LOSS_BASE = 'gain-loss_base-map'

# units for monetary evaluation
EURO_TON_CO2 = 'EURO_ton_CO2'
EURO_TON_C = 'EURO_ton_C'
# end of globals


def set_cmdline_args(parser):
    """ function to implement additional command line arguments for the GlobalClimateRegulation run for developer

    :param parser: subparser object for GlobalClimateRegulation case in the __main__
    """
    parser.add_argument('--jrcmethod', action='store_true',
                        help='To run the carbon sequestration in the Global Climate Regulation in the JRC method.')


class GlobalClimateRun(inca.Run):
    """processing class for the Global Climate Regulation accounting service

    :param config: dictionary containing the service configuration (input YAML file information)
    """
    service_name = _GCR

    def __init__(self, config):
        super().__init__(config)

        self._progress_weight_run = 0.68

        self.config_template.update({
            # override default land_classification config item: we need an ecosystem_type_L2 column
            'land_classification': ConfigItem(check_csv, optional=True, required_columns=[LANDCOVER_ID, ECOTYPE_L2]),
            _GCR: {
                _CSEQ: {
                    'env_air_gge': ConfigItem(check_function=check_tsv),
                    'productivity_proxy_map': {YEARLY: ConfigRaster(raster_type=RasterType.RELATIVE)},
                    'unmanaged_forest_map': {YEARLY: ConfigRaster(optional=True, check_projected=True,
                                                                  raster_type=RasterType.ABSOLUTE_VOLUME)},
                    'unmanaged_forest_table': {YEARLY: ConfigItem(check_function=check_csv, optional=True,
                                                                  required_columns=[NUTS_ID, 'unmanaged_forests'])},
                    'unmanaged_wetland_map': {YEARLY: ConfigRaster(optional=True, check_projected=True,
                                                                   raster_type=RasterType.ABSOLUTE_VOLUME)},
                    'unmanaged_wetland_table': {YEARLY: ConfigItem(check_function=check_csv, optional=True,
                                                                   required_columns=[NUTS_ID,
                                                                                     'unmanaged_wetland_inland',
                                                                                     'unmanaged_wetland_coastal'])},
                    'non-reported_peatland_map': {YEARLY: ConfigRaster(optional=True, check_projected=True,
                                                                       raster_type=RasterType.ABSOLUTE_VOLUME)},
                    'non-reported_peatland_table': {YEARLY: ConfigItem(check_function=check_csv, optional=True,
                                                                       required_columns=[NUTS_ID,
                                                                                         'peatland_grass',
                                                                                         'peatland_heathland-shrub'])}
                },
                _CRET: {
                    'reten_operation_mode': ConfigChoice(STOCK_DIFFERENCE, GAIN_LOSS, GAIN_LOSS_BASE,
                                                         default=GAIN_LOSS),
                    'opening_carbon_stock_table': {YEARLY: ConfigItem(check_function=check_csv, optional=True,
                                                                      required_columns=[NUTS_ID]+_SRC_CRF)},
                    'opening_carbon_stock_proxy_map': {YEARLY: ConfigRaster(optional=True,
                                                                            raster_type=RasterType.RELATIVE)},
                    'carbon_stock_lookup_table': {YEARLY: ConfigItem(check_function=check_csv, optional=True,
                                                                     required_columns=[ECOTYPE_L2,
                                                                                       'AGB', 'BGB', 'SOC'])},
                    'harvest_wood_proxy_map': {YEARLY: ConfigRaster(optional=True,
                                                                    raster_type=RasterType.RELATIVE)},
                    'closing_carbon_stock_table': {YEARLY: ConfigItem(check_function=check_csv, optional=True,
                                                                      required_columns=[NUTS_ID]+_SRC_CRF)},
                    'closing_carbon_stock_proxy_map': {YEARLY: ConfigRaster(optional=True,
                                                                            raster_type=RasterType.RELATIVE)}
                }
            }
        })

        if self.monetary:  # Inputs used only for monetary evaluation:
            logger.debug('Add config_template for monetary')
            self.config_template[self.service_name].update({
                'monetary': {
                    'seq_carbon_price_table': ConfigItem(check_function=check_csv,
                                                         required_columns=['year', 'EUR_per_ton_current']),
                    'unit_sequ_price': ConfigChoice(EURO_TON_CO2, EURO_TON_C, default=EURO_TON_CO2)}
            })

        # here follow some GlobalClimateRegulation specific checks for the configuration file
        if len(self.years) != 1:
            raise ConfigError(f'The {_GCR} service run is currently limited to one ' +
                              'processing year. Please reduce the processing years to one.',
                              ['years'])
        # if a developer wants to run the JRCmethod we need some config changes
        if self.config.get('jrcmethod'):
            self.config['statistics_shape'] = 'NUTS-0'
            self.config['reporting_shape'] = 'NUTS-0'
            self.config['selected_regions'] = ['EU27']

        # init some important variables
        self.result_files = {_CSEQ: {'dataframes': {}, 'map_paths': {}, 'sut_paths': {}},
                             _CRET: {'dataframes': {}, 'map_paths': {}, 'sut_paths': {}}
                             }
        self.seq_price_table = None
        # sequestration specific
        self.df_gge = None
        self.df_seq = None
        self.df_seq_flow = None
        self.df_seq_current = None
        self.df_seq_real = None
        self.seq_maps = {}
        # retention specific
        self.ret_maps = {}
        self.flag_neg_stock = False
        self.neg_countries = []
        self.make_output_filenames()

    def make_output_filenames(self):
        """ generates the filenames for all final output files."""
        # carbon net sequestration
        self.result_files[_CSEQ]['map_paths'].update(
            physical={year: os.path.join(self.maps, f'{_CSEQ}_map_use_tonnes_{year}.tif')
                      for year in self.years},
            monetary_current={year: os.path.join(self.maps,
                                                 f'{_CSEQ}_map_use_monetary_EURO-current_{year}.tif')
                              for year in self.years},
            monetary_real={year: os.path.join(self.maps,
                                              f'{_CSEQ}_map_use_monetary_EURO-real_{year}.tif')
                           for year in self.years})
        self.result_files[_CSEQ]['sut_paths'].update(
            physical={year: os.path.join(self.suts, f'{_CSEQ}_report_SUT-physical_tonnes_{year}.xlsx')
                      for year in self.years},
            monetary_current={year: os.path.join(self.suts,
                                                 f'{_CSEQ}_report_SUT-monetary_EURO-current_{year}.xlsx')
                              for year in self.years},
            monetary_real={year: os.path.join(self.suts,
                                              f'{_CSEQ}_report_SUT-monetary_EURO-real_{year}.xlsx')
                           for year in self.years})
        self.result_files[_CSEQ]['dataframes'].update(
            physical={year: None for year in self.years},
            monetary_current={year: None for year in self.years},
            monetary_real={year: None for year in self.years})

        # carbon retention
        self.result_files[_CRET]['map_paths'].update(
            physical={year: os.path.join(self.maps, f'{_CRET}_map_closing-stock_tonnes_{year}.tif')
                      for year in self.years},
            monetary_current={year: None for year in self.years},
            monetary_real={year: None for year in self.years})
        self.result_files[_CRET]['sut_paths'].update(
            physical={year: os.path.join(self.suts, f'{_CRET}_report_SUT-physical_tonnes_{year}.xlsx')
                      for year in self.years},
            monetary_current={year: None for year in self.years},
            monetary_real={year: None for year in self.years})
        self.result_files[_CRET]['dataframes'].update(
            physical={year: None for year in self.years},
            monetary_current={year: None for year in self.years},
            monetary_real={year: None for year in self.years})

    def _start(self):
        """start of the service processing."""
        # create missing folder
        os.makedirs(self.additional_results, exist_ok=True)

        # get some universal tables
        if self.monetary:
            self.seq_price_table = pd.read_csv(self.config[_GCR]['monetary']['seq_carbon_price_table'],
                                               comment='#').set_index('year')
        # get weights for progressbar for processing steps
        weights = self.get_progress_weights()

        # loop over all processing years
        for year in self.years:
            self.year = year

            # run some additional configuration checks for this processing year
            self.annual_config_checks()

            # get the area statistics of the ecosystem map at level 1 for statistic areas
            self.df_eco_area = self.get_ecosystem_map_area_statistics(
                self.year, level=1, region='statistic',
                add_progress=lambda p: self.add_progress(p * weights['ecosystem_stats']))

            # carbon net sequestration map generation
            self.net_sequestration_map_generation(
                add_progress=lambda p: self.add_progress(p * weights['sequestration_map']))

            # carbon retention map generation
            self.retention_map_generation(
                add_progress=lambda p: self.add_progress(p * weights['retention_map']))

            # JRC requested post-processing of net sequestration
            # Note: must be done after the retention map is generated since we need the negative values for that
            self.net_sequestration_post_processing()

            # statistic extraction for reporting regions (sequestration & retention)
            self.reporting_area_statistics(
                add_progress=lambda p: self.add_progress(p * weights['statistics']))

            # masking and cutting of all maps to reporting regions
            self.final_map_preparation(
                add_progress=lambda p: self.add_progress(p * weights['final_map']))

            # reporting of SUT and CSV statistics (sequestration & retention)
            self.reporting()
        return

    def reporting(self):
        """ export of reporting area statistics to SUT Excel files and CSV statistic files"""
        logger.info(f'statistics export')
        logger.info(f'* CSV statistic files')
        # in CSV files we use the short names
        dic_short = types_l1.set_index('name')['short_name'].to_dict()
        result_df = ['physical']
        if self.monetary:
            result_df = result_df + ['monetary_current', 'monetary_real']

        # first, carbon net sequestration
        for element in result_df:
            path_export = os.path.join(self.statistic_dir,
                                       os.path.basename(self.result_files[_CSEQ]
                                                        ['sut_paths'][element][self.year])
                                       .replace('_report_SUT-', '_statistics_').split('.xlsx')[0] + '.csv')
            self.result_files[_CSEQ]['dataframes'][element][self.year]\
                .rename(columns=dic_short).to_csv(path_export, columns=types_l1['short_name'].tolist(),
                                                  index_label=NUTS_ID, float_format='%.3f')
        # second, carbon retention
        for element in ['physical']:
            path_export = os.path.join(self.statistic_dir,
                                       os.path.basename(self.result_files[_CRET]
                                                        ['sut_paths'][element][self.year])
                                       .replace('_report_SUT-', '_statistics_').split('.xlsx')[0] + '.csv')
            self.result_files[_CRET]['dataframes'][element][self.year]\
                .rename(columns=dic_short).to_csv(path_export, columns=types_l1['short_name'].tolist(),
                                                  index_label=NUTS_ID, float_format='%.3f')

        logger.info(f'* SUT Excel files')
        # prepare the unit and scaling dic
        scaling_dic = {'physical': 1000, 'monetary_real': 1000000, 'monetary_current': 1000000}
        unit_dic = {'physical': '1000 tonnes',
                    'monetary_real': 'Million EUR (2000 value)',
                    'monetary_current': 'Million EUR (nominal)'}
        # first, carbon net sequestration
        for element in result_df:
            path_export = self.result_files[_CSEQ]['sut_paths'][element][self.year]
            df_supply = self.result_files[_CSEQ]['dataframes'][element][self.year]
            df_use = df_supply.sum(axis=1).to_frame(name=GOV)

            write_sut(path_export, df_supply / scaling_dic[element], df_use / scaling_dic[element],
                      service_name=f'{_GCR} - {_CSEQ}', year=self.year, unit=unit_dic[element])
        # second, carbon retention
        for element in ['physical']:
            path_export = self.result_files[_CRET]['sut_paths'][element][self.year]
            df_supply = self.result_files[_CRET]['dataframes'][element][self.year]
            df_use = df_supply.sum(axis=1).to_frame(name=GOV)

            write_sut(path_export, df_supply / scaling_dic[element], df_use / scaling_dic[element],
                      service_name=f'{_GCR} - {_CRET}', year=self.year, unit=unit_dic[element])

    def reporting_area_statistics(self, add_progress):
        """Extract the statistics for the reporting regions and assign to results dict.

        :param add_progress: progressbar object
        """
        logger.info(f'extract statistics for reporting regions')
        # some basic parameters
        region_path = self.reporting_raster
        region_dic = self.reporting_shape[SHAPE_ID].to_dict()
        eco_path = self.get_ecosystem_raster(self.year, level=1)
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()

        # weights for sub-process for progress bar
        if self.monetary:
            pWeightSub = 0.25
        else:
            pWeightSub = 0.5

        # first for carbon net sequestration
        df = statistics_byArea_byET(self.seq_maps['physical'], region_path, region_dic,
                                    eco_path, eco_dic, block_shape=(4096, 4096),
                                    add_progress=lambda p: add_progress(p * pWeightSub))
        self.result_files[_CSEQ]['dataframes']['physical'][self.year] = df[[SUM]].unstack(1)[SUM]

        if self.monetary:
            df = statistics_byArea_byET(self.seq_maps['real'], region_path, region_dic,
                                        eco_path, eco_dic, block_shape=(4096, 4096),
                                        add_progress=lambda p: add_progress(p * pWeightSub))
            self.result_files[_CSEQ]['dataframes']['monetary_real'][self.year] = df[[SUM]].unstack(1)[SUM]

            df = statistics_byArea_byET(self.seq_maps['current'], region_path, region_dic,
                                        eco_path, eco_dic, block_shape=(4096, 4096),
                                        add_progress=lambda p: add_progress(p * pWeightSub))
            self.result_files[_CSEQ]['dataframes']['monetary_current'][self.year] = df[[SUM]].unstack(1)[SUM]

        # second for carbon retention
        df = statistics_byArea_byET(self.ret_maps['closing_stock'], region_path, region_dic,
                                    eco_path, eco_dic, block_shape=(4096, 4096),
                                    add_progress=lambda p: add_progress(p * pWeightSub))
        self.result_files[_CRET]['dataframes']['physical'][self.year] = df[[SUM]].unstack(1)[SUM]
        # for carbon retention make 100% sure that we have no negative values even if we have already check for that
        self.result_files[_CRET]['dataframes']['physical'][self.year].clip(lower=0, inplace=True)

        if self.monetary:
            # TODO: add here files after monetary evaluation for retention is introduced
            pass

    def net_sequestration_post_processing(self, block_shape=(4096, 4096)):
        """this JRC requested post-processing will delete negative pixel values in the net_sequestration map

        Note: original metadata will be taken over

        :param block_shape: block size for processing
        """
        # TODO: check if this post-processor is needed following the FINAL guidance note!!!
        logger.info(f'post-processing of net sequestration maps')

        lFiles = [self.seq_maps['physical']]
        if self.monetary:
            lFiles = lFiles + [self.seq_maps['real'], self.seq_maps['current']]

        with tempfile.TemporaryDirectory() as tmpdir:
            for element in lFiles:
                logger.info(f'* processing file: {os.path.basename(element)}')
                tmpfile = os.path.join(tmpdir, os.path.basename(element))
                shutil.copy2(element, tmpfile)

                # run block-wise processing of destination file
                with rasterio.open(tmpfile, 'r') as src_data:
                    with rasterio.open(element, 'w', **dict(src_data.profile.copy(), dtype=src_data.dtypes[0],
                                                            nodata=src_data.nodatavals[0])) as ds_out:
                        # copy tags to dst
                        ds_out.update_tags(**src_data.tags())
                        for _, window in block_window_generator(block_shape, ds_out.height, ds_out.width):
                            # get data & mask
                            aData = src_data.read(1, window=window, masked=True)
                            aData[aData < 0] = 0
                            ds_out.write(aData.filled(ds_out.nodata), window=window, indexes=1)

    def retention_gain_loss_neg_stock_fix(self, block_shape=(4096, 4096)):
        """this JRC requested post-processing will delete negative pixel values in the carbon retention map
            (closing stock)

        Note: original metadata will be taken over

        :param block_shape: block size for processing
        """
        # TODO: check if this post-processor is needed following the FINAL guidance note!!!
        logger.info(f'fixing negative pixel values in the carbon retention closing stock map')
        element = self.ret_maps['closing_stock']

        # first copy original file to TEMP folder and rename as error map
        tmpfile = os.path.splitext(element)[0] + f'_error-map.tif'
        try:
            shutil.copy2(element, tmpfile)
        except:
            logger.warning('could not copy the error map. Just overwrite original results.')
            raise RuntimeError('fixing the negative values in the closing stock map failed. could not copy'
                               'the original file to new location as error map.')

        # run block-wise processing of destination file
        with rasterio.open(tmpfile, 'r') as src_data:
            with rasterio.open(element, 'w', **dict(src_data.profile.copy(), dtype=src_data.dtypes[0],
                                                    nodata=src_data.nodatavals[0])) as ds_out:
                # copy tags to dst
                ds_out.update_tags(**src_data.tags())
                for _, window in block_window_generator(block_shape, ds_out.height, ds_out.width):
                    # get data & mask
                    aData = src_data.read(1, window=window, masked=True)
                    aData[aData < 0] = 0
                    ds_out.write(aData.filled(ds_out.nodata), window=window, indexes=1)

    def final_map_preparation(self, add_progress):
        """Check all generated maps for the statistical regions and limit them to the reporting regions (in case
        reporting_regions != statistical_regions). Takes care of masking and raster cropping.

        :param add_progress: progressbar object
        """
        logger.info(f'prepare maps for the reporting regions')
        # first run check if reporting_regions == statistical_regions
        if self.config['statistics_shape'] == self.config['reporting_shape']:
            logger.info(f'* reporting regions == statistic regions -> copy data')
            try:
                # net sequestration
                shutil.copy2(self.seq_maps['physical'], self.result_files[_CSEQ]['map_paths']['physical'][self.year])
                if self.monetary:
                    shutil.copy2(self.seq_maps['real'],
                                 self.result_files[_CSEQ]['map_paths']['monetary_real'][self.year])
                    shutil.copy2(self.seq_maps['current'],
                                 self.result_files[_CSEQ]['map_paths']['monetary_current'][self.year])
                # carbon retention
                shutil.copy2(self.ret_maps['closing_stock'],
                             self.result_files[_CRET]['map_paths']['physical'][self.year])
                if self.monetary:
                    # TODO: add here files after monetary evaluation for retention is introduced
                    pass
            except BaseException:
                raise IOError('Could not copy the final GCR maps. Check for write protection in output folder.')
        else:
            logger.info(f'* maps have to be masked & cut to reporting regions')
            if self.monetary:
                pWeightSub = 0.25
            else:
                pWeightSub = 0.5
            try:
                # net sequestration
                self.accord.crop_2_reporting_AOI(self.seq_maps['physical'],
                                                 self.result_files[_CSEQ]['map_paths']['physical'][self.year],
                                                 self.reporting_raster, block_shape=(4096, 4096),
                                                 add_progress=lambda p: add_progress(p * pWeightSub))
                if self.monetary:
                    self.accord.crop_2_reporting_AOI(self.seq_maps['real'],
                                                     self.result_files[_CSEQ]['map_paths']['monetary_real'][self.year],
                                                     self.reporting_raster, block_shape=(4096, 4096),
                                                     add_progress=lambda p: add_progress(p * pWeightSub))
                    self.accord.crop_2_reporting_AOI(self.seq_maps['current'],
                                                     self.result_files[_CSEQ]['map_paths']['monetary_current'][self.year],
                                                     self.reporting_raster, block_shape=(4096, 4096),
                                                     add_progress=lambda p: add_progress(p * pWeightSub))
                # carbon retention
                self.accord.crop_2_reporting_AOI(self.ret_maps['closing_stock'],
                                                 self.result_files[_CRET]['map_paths']['physical'][self.year],
                                                 self.reporting_raster, block_shape=(4096, 4096),
                                                 add_progress=lambda p: add_progress(p * pWeightSub))
                if self.monetary:
                    # TODO: add here files after monetary evaluation for retention is introduced
                    pass
            except BaseException:
                raise RuntimeError('the raster maps of the statistical regions could not transferred and masked '
                                   'to regional regions')

    def retention_map_generation(self, add_progress):
        """Generate the carbon retention maps for the statistical regions.

        :param add_progress: progressbar object
        """
        logger.info(f'start generation of carbon retention map for year: {self.year}')

        # physical map
        if self.config[_GCR][_CRET]['reten_operation_mode'] in [GAIN_LOSS, GAIN_LOSS_BASE]:
            logger.info(f'* using the {GAIN_LOSS} method')
            # generate open stock map
            self.carbon_opening_stock_map_generation(add_progress=lambda p: add_progress(p * 0.34))

            # generate map for carbon removal due to wood harvest (LULUCF CRF4G)
            self.wood_harvest_map(add_progress=lambda p: add_progress(p * 0.34))

            # carbon retention (closing stock) map generation
            self.gain_loss_method_map_generation(add_progress=lambda p: add_progress(p * 0.16))

            # cleanup of the closing stock map in case pixels with negative values exist
            if self.flag_neg_stock:
                logger.warning('The carbon retention map (closing stock map) contains negative pixel values. '
                               'Please check your opening stock map if in certain regions the carbon stock is '
                               'under estimated. Or if your net sequestration map shows to high carbon losses in some '
                               'regions which are higher as the given carbon stock. You can also check the closing'
                               ' stock error map showing the negative values saved in the TEMP folder.')
                # get the names of the countries
                lNumbers = list(set(self.neg_countries))
                lCountries = self.statistics_shape.loc[self.statistics_shape.SHAPE_ID.isin(lNumbers)].index.to_list()
                logger.warning(f'The following areas show negative pixel values: {lCountries}')


                self.retention_gain_loss_neg_stock_fix()
                self.flag_neg_stock = False
                self.neg_countries = []

            # statistics extraction per statistical area to provide closing stock in LULUCF classes and in CO2 unit
            self.closing_stock_export(add_progress=lambda p: add_progress(p * 0.16), block_shape=(4096, 4096))
        elif self.config[_GCR][_CRET]['reten_operation_mode'] == STOCK_DIFFERENCE:
            logger.info(f'* using the {STOCK_DIFFERENCE} method')
            # carbon retention (closing stock) map generation
            self.stock_difference_method_map_generation(add_progress)
        else:
            raise ValueError('this retention calculation method is not defined. adapt code.')

        # monetary evaluation maps in current and real currency
        # TODO: implement monetary evaluation of carbon retention
        if self.monetary:
            logger.warning('* The monetary evaluation of the carbon retention is currently still under development ' +
                           'and not implemented.')

    def closing_stock_export(self, add_progress, block_shape=(4096, 4096)):
        """To allow retention map generation with the GAIN-LOSS method for following years we have to export the
        generated closing stock as CSV file in LULUCF categories and in CO2 measured in kilotonne.  -->  User can
        read in the closing stock as opening stock of the following year.

        :param add_progress: callback to update the progress bar
        :param block_shape: block size for processing
        """
        logger.info('** export of closing stock data for usage in GAIN-LOSS calculation of following years')
        # extract the statistics per ET class and region
        df = statistics_byArea_byET(self.ret_maps['closing_stock'],
                                    self.statistics_raster,
                                    self.statistics_shape[SHAPE_ID].to_dict(),
                                    self.get_ecosystem_raster(self.year, level=1),
                                    types_l1.set_index('name')[ECO_ID].to_dict(),
                                    block_shape=block_shape,
                                    add_progress=add_progress)
        # extract needed data
        df = df[[SUM]].unstack(1)[SUM]
        # convert to CO2 in kilotonne --> Note: no inverse since we generate a stock and no change
        df = df.fillna(0) * (1. / _CO2_2_CARBON) * (1. / _UNIT_2_BASE)
        # group heathland, sparse and coastal to otherlands
        df[_SRC_CRF_DIC['CRF4F']] = df[SPARSE] + df[HEATH] + df[COASTAL]
        # rename to LULUCF category names
        df.rename(columns=dict(map(reversed, _SRC_CRF_DIC.items())), inplace=True)
        # since there can be no negative stock we clamp negative values to ZERO
        df.clip(lower=0, inplace=True)
        # setup file name for export of table
        path_export = os.path.join(self.additional_results, f'Closing-stock_GAIN-LOSS_CO2-kilotonne_{self.year}.csv')
        df.to_csv(path_export, columns=_SRC_CRF, index_label=NUTS_ID)

        # export also the map for statistical regions as proxy map
        path_export = os.path.join(self.additional_results, f'Closing-stock_GAIN-LOSS_proxy_{self.year}.tif')
        try:
            shutil.copy2(self.ret_maps['closing_stock'], path_export)
        except BaseException:
            raise IOError('Could not copy the closing stock proxy. Check for write protection in output folder.')

    def stock_difference_method_map_generation(self, add_progress, block_shape=(4096, 4096)):
        """Generate the closing stock (carbon retention) using the STOCK-DIFFERENCE method.

        User provides the closing stock table and (optional) a proxy for spatial disaggregation.

        :param add_progress: callback to update progress bar
        :param block_shape: size of processing blocks for raster processing
        """
        logger.info(f'** prepare the closing stock table for spatial disaggregation')
        # read in the closing stock table
        df = pd.read_csv(self.config[_GCR][_CRET]['closing_carbon_stock_table'][self.year], comment='#')
        # run a checks
        lMissing = [x for x in self.statistics_shape.index if x not in df[NUTS_ID].unique().tolist()]
        if len(lMissing) != 0:
            raise ConfigError(f'not all needed statistic regions are included in the closing stock file. missing:' +
                              ', '.join(str(x) for x in lMissing), [_GCR, _CRET, 'closing_carbon_stock_table',
                                                                    self.year])
        # filter to needed regions
        df = df.set_index(NUTS_ID).reindex(self.statistics_shape.index, fill_value=0)
        # convert to carbon --> Note: stocks are always positive so no inversion needed
        df = df.fillna(0) * _CO2_2_CARBON * _UNIT_2_BASE
        # rename columns to ecosystem types clear names
        df = df.rename(columns=_SRC_CRF_DIC)

        # split the "other" category in known types
        # get the areas of the three classes breaking up CRF4F
        df_areas = self.df_eco_area['area_ha'].unstack(level=1, fill_value=0)[[SPARSE, HEATH, COASTAL]].copy()
        # calculate the percentage distribution
        df_areas['total'] = df_areas.sum(axis=1)
        percentage = df_areas.loc[:, df_areas.columns != 'total'].divide(df_areas['total'], axis=0).fillna(0)
        # split the otherlands class by the percentage distribution
        df_other = percentage.merge(df.otherlands, how='left', left_index=True, right_index=True)
        df_other = df_other.loc[:, df_other.columns != 'otherlands'].multiply(df_other['otherlands'], axis=0)
        # clean up
        df = df.merge(df_other, how='left', left_index=True,
                      right_index=True).drop(columns=['otherlands'])
        # since stock have to be always positive - clip negative numbers if exist
        df.clip(lower=0, inplace=True)

        # fill missing ecosystem types with 0 and prepare for spatial disaggregation
        df_data = df.reindex(columns=types_l1['name'].to_list(), fill_value=0)
        df_data.columns.rename(ECOTYPE, inplace=True)
        df_data = df_data.stack(0).fillna(0).to_frame(name='physical')

        logger.info(f'** generation of the closing stock map')
        # prepare output file name
        self.ret_maps['closing_stock'] = os.path.join(self.temp_dir,
                                                      os.path.basename(self.result_files[_CRET]['map_paths']['physical']
                                                                       [self.year]).split('.tif')[0] +
                                                      r'_statistical-regions.tif')
        # get some additional parameters
        if self.config[_GCR][_CRET]['closing_carbon_stock_proxy_map'][self.year]:
            path_proxy = self.config[_GCR][_CRET]['closing_carbon_stock_proxy_map'][self.year]
        else:
            path_proxy = self.get_ecosystem_raster(self.year, level=1)
        path_ETmap = self.get_ecosystem_raster(self.year, level=1)
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()
        region_path = self.statistics_raster
        region_dic = self.statistics_shape[SHAPE_ID].to_dict()

        # spatially disaggregate
        self.accord.spatial_disaggregation_byArea_byET(path_proxy, df_data['physical'],
                                                       region_path, region_dic,
                                                       path_ETmap, eco_dic,
                                                       self.ret_maps['closing_stock'],
                                                       processing_info=f'Carbon Closing Stock map (carbon retention) ' +
                                                                       f'for year {self.year} using the ' +
                                                                       f'STOCK-DIFFERENCE method.',
                                                       unit_info='tonnes/pixel',
                                                       block_shape=block_shape,
                                                       add_progress=add_progress)

    def gain_loss_method_map_generation(self, add_progress, block_shape=(4096, 4096)):
        """Generate the closing stock (carbon retention) by using the GAIN-LOSS method.

        closing_stock = opening_stock + net_sequestration - wood_harvest

        :param add_progress: callback to update the progress bar
        :param block_shape: size of processing blocks for raster processing
        """
        logger.info('** run generation of the closing stock map')
        # get output file name
        self.ret_maps['closing_stock'] = os.path.join(self.temp_dir,
                                                      os.path.basename(self.result_files[_CRET]['map_paths']['physical']
                                                                       [self.year]).split('.tif')[0] +
                                                      r'_statistical-regions.tif')
        # prepare raster metadata
        self.accord.metadata.read_raster_tags([self.ret_maps['opening_stock'], self.seq_maps['physical'],
                                               self.ret_maps['wood_harvest']])
        tags = self.accord.metadata.prepare_raster_tags(f'Carbon Closing Stock map (carbon retention) for year '
                                                        f'{self.year} using the GAIN-LOSS method.', 'tonne/pixel')
        # run blockwise processing
        with rasterio.open(self.ret_maps['opening_stock'], 'r') as ds_open, \
                rasterio.open(self.seq_maps['physical'], 'r') as ds_seq, \
                rasterio.open(self.ret_maps['wood_harvest'], 'r') as ds_harvest, \
                rasterio.open(self.statistics_raster, 'r') as ds_mask, \
                rasterio.open(self.ret_maps['closing_stock'], 'w', **dict(self.accord.ref_profile.copy(),
                                                                          dtype=rasterio.float32,
                                                                          nodata=np.nan)) as ds_out:
            nblocks = number_blocks(self.accord.ref_profile, block_shape)
            ds_out.update_tags(**tags)
            for _, window in block_window_generator(block_shape, ds_open.height, ds_open.width):
                aStock = ds_open.read(1, window=window, masked=True)
                aSeq = ds_seq.read(1, window=window, masked=True)
                aHarvest = ds_harvest.read(1, window=window, masked=True)
                # calculate the closing stock pixel wise
                aData = aStock.filled(0) + aSeq.filled(0) - aHarvest.filled(0)
                # mask areas outside
                aMask = ds_mask.read(1, window=window)
                aData[aMask == 0] = ds_out.nodata
                ds_out.write(aData, window=window, indexes=1)
                # check if we have negative closing stock values
                if (aData[aMask != 0].astype(rasterio.float32) < 0).any():
                    self.flag_neg_stock = True
                    self.neg_countries = self.neg_countries + np.unique(aMask[(aData < 0) & (aMask != 0)]).tolist()
                add_progress(100. / nblocks)

    def wood_harvest_map(self, add_progress):
        """Generate the wood harvest map out of GGE class CRF4G.

        Note: if no extra proxy is provided the GGE data is spatial distributed using the land cover map.

        :param add_progress: progressbar object
        """
        logger.info('** generation of the wood removal map')
        # get the wood removal data for needed year and convert to carbon and sequestration positive
        df = self.df_gge[int(self.year)].fillna(0) * _CO2_2_CARBON * _UNIT_2_BASE * -1
        df = df.unstack(0)[_WOODREMOVAL]
        df.index.rename(NUTS_ID, inplace=True)
        # clamp data --> neg. values show emissions which can be not interpreted as harvest
        df.clip(lower=0, inplace=True)
        # positive numbers can be interpreted as wood harvest --> generate a map
        # prepare Dataframe with MultiIndex where FOREST class gets the wood harvest and all other ET get zero
        df_data = df.rename(columns={_WOODREMOVAL[0]: FOREST}).reindex(columns=types_l1['name'].to_list(), fill_value=0)
        df_data.columns.rename(ECOTYPE, inplace=True)  # that step is important that MultiIndex works correctly
        df_data = df_data.stack(0).fillna(0).to_frame(name='physical')

        # prepare output file name
        self.ret_maps['wood_harvest'] = os.path.join(self.temp_dir,
                                                     f'{_CRET}_map_wood-harvest_tonnes_{self.year}.tif')
        # get some additional parameters
        if self.config[_GCR][_CRET]['harvest_wood_proxy_map'][self.year]:
            path_proxy = self.config[_GCR][_CRET]['harvest_wood_proxy_map'][self.year]
        else:
            path_proxy = self.get_ecosystem_raster(self.year, level=1)
        path_ETmap = self.get_ecosystem_raster(self.year, level=1)
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()
        region_path = self.statistics_raster
        region_dic = self.statistics_shape[SHAPE_ID].to_dict()

        # spatially disaggregate
        self.accord.spatial_disaggregation_byArea_byET(path_proxy, df_data['physical'],
                                                       region_path, region_dic,
                                                       path_ETmap, eco_dic,
                                                       self.ret_maps['wood_harvest'],
                                                       processing_info=f'carbon of wood harvest for year {self.year}.',
                                                       unit_info='tonnes/pixel',
                                                       block_shape=(4096, 4096),
                                                       add_progress=add_progress)

    def carbon_opening_stock_map_generation(self, add_progress):
        """Generatie the carbon opening stock map based on chosen method (GAIN-LOSS or GAIN-LOSS-BASE method).

        :param add_progress: progressbar object
        """
        logger.info('** generation of the carbon opening stock map')

        if self.config[_GCR][_CRET]['reten_operation_mode'] == GAIN_LOSS_BASE:
            logger.info(f'*** using the {GAIN_LOSS_BASE} method')
            self.opening_stock_base_map(add_progress, block_shape=(4096, 4096))
        elif self.config[_GCR][_CRET]['reten_operation_mode'] == GAIN_LOSS:
            logger.info(f'*** using the {GAIN_LOSS} method')
            self.load_opening_stock_map(add_progress, block_shape=(4096, 4096))
        else:
            raise ValueError('this open stock calculation method is not defined. adapt code.')

    def load_opening_stock_map(self, add_progress, block_shape=(4096, 4096)):
        """Load a CSV file containing the opening stock data in LULUCF categories and in CO2 in kilotonnes and generate
         an open stock map using a proxy.

        :param add_progress: callback to update the progress bar
        :param block_shape: block size for the processing
        """
        logger.info(f'*** prepare the opening stock table for spatial disaggregation')
        # read in the opening stock table (closing stock table of previous year)
        df = pd.read_csv(self.config[_GCR][_CRET]['opening_carbon_stock_table'][self.year], comment='#')
        # run a checks
        lMissing = [x for x in self.statistics_shape.index if x not in df[NUTS_ID].unique().tolist()]
        if len(lMissing) != 0:
            raise ConfigError(f'not all needed statistic regions are included in the opening stock file. missing:' +
                              ', '.join(str(x) for x in lMissing), [_GCR, _CRET, 'opening_carbon_stock_table',
                                                                    self.year])
        # filter to needed regions
        df = df.set_index(NUTS_ID).reindex(self.statistics_shape.index, fill_value=0)
        # convert to carbon --> Note: stocks are always positive so no inversion needed
        df = df.fillna(0) * _CO2_2_CARBON * _UNIT_2_BASE
        # rename columns to ecosystem types clear names
        df = df.rename(columns=_SRC_CRF_DIC)

        # split the "other" category in known types
        # get the areas of the three classes breaking up CRF4F
        df_areas = self.df_eco_area['area_ha'].unstack(level=1, fill_value=0)[[SPARSE, HEATH, COASTAL]].copy()
        # calculate the percentage distribution
        df_areas['total'] = df_areas.sum(axis=1)
        percentage = df_areas.loc[:, df_areas.columns != 'total'].divide(df_areas['total'], axis=0).fillna(0)
        # split the otherlands class by the percentage distribution
        df_other = percentage.merge(df.otherlands, how='left', left_index=True, right_index=True)
        df_other = df_other.loc[:, df_other.columns != 'otherlands'].multiply(df_other['otherlands'], axis=0)
        # clean up
        df = df.merge(df_other, how='left', left_index=True,
                      right_index=True).drop(columns=['otherlands'])

        # fill missing ecosystem types with 0 and prepare for spatial disaggregation
        df_data = df.reindex(columns=types_l1['name'].to_list(), fill_value=0)
        df_data.columns.rename(ECOTYPE, inplace=True)
        df_data = df_data.stack(0).fillna(0).to_frame(name='physical')

        logger.info(f'** generation of the opening stock map')
        # prepare output file name
        self.ret_maps['opening_stock'] = os.path.join(self.temp_dir,
                                                      f'{_CRET}_map_opening-stock_tonnes_{self.year}.tif')
        # get some additional parameters
        if self.config[_GCR][_CRET]['opening_carbon_stock_proxy_map'][self.year]:
            path_proxy = self.config[_GCR][_CRET]['opening_carbon_stock_proxy_map'][self.year]
        else:
            path_proxy = self.get_ecosystem_raster(self.year, level=1)
        path_ETmap = self.get_ecosystem_raster(self.year, level=1)
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()
        region_path = self.statistics_raster
        region_dic = self.statistics_shape[SHAPE_ID].to_dict()

        # spatially disaggregate
        self.accord.spatial_disaggregation_byArea_byET(path_proxy, df_data['physical'],
                                                       region_path, region_dic,
                                                       path_ETmap, eco_dic,
                                                       self.ret_maps['opening_stock'],
                                                       processing_info=f'Carbon Opening Stock map of year {self.year}.',
                                                       unit_info='tonnes/pixel',
                                                       block_shape=block_shape,
                                                       add_progress=add_progress)

    def opening_stock_base_map(self, add_progress, block_shape=(4096, 4096)):
        """Generate carbon opening stock map for GAIN-LOSS method using LUT to generate basemap.

        :param add_progress: callback to update progress bar
        :param block_shape: processing block size
        """
        logger.info('*** create needed ecosystem type map')
        # generate the ecosystem map at level 2
        path_ETmap = self.get_ecosystem_raster(self.year, level=2, add_progress=lambda p: add_progress(0.5 * p))

        # prepare reclassification dictionary
        logger.info('*** prepare carbon stock values per ecosystem type')
        # first: read LUT table given biomass in ton C/ha
        path_LUT = self.config[_GCR][_CRET]['carbon_stock_lookup_table'][self.year]
        df_biomass = pd.read_csv(path_LUT, comment='#').astype({ECOTYPE_L2: inca.common.ecosystem.ecotype_l2_cat})

        # second: run some checks
        if df_biomass[ECOTYPE_L2].isna().any():
            raise ConfigError(f'Biomass lookup table contains unknown ecosystem types.',
                              [_GCR, _CRET, 'carbon_stock_lookup_table', self.year])
        # biomass list should include all ecosystem types
        lMissing = [x for x in self.get_ecosystem_mapping(2)[ECOTYPE] if x not in df_biomass[ECOTYPE_L2].tolist()]
        if len(lMissing) != 0:
            raise ConfigError(f'Biomass LUT does not have entries for all Level-2 Ecosystem Types, missing: ' +
                              ', '.join(str(x) for x in lMissing), [_GCR, _CRET, 'carbon_stock_lookup_table',
                                                                    self.year])

        # third: add ecosystem_id (raster value identification)
        df_biomass = df_biomass.join(types_l2.set_index(ECOTYPE)[ECO_ID], on=ECOTYPE_L2)

        # fourth: get total stock & convert ton C/ha in ton C/pixel
        df_biomass['stock'] = df_biomass[['AGB', 'BGB', 'SOC']].sum(axis=1)
        pixel_area_ha = self.accord.pixel_area_m2() / 10000.
        df_biomass['stock'] *= pixel_area_ha

        # fifth: get reclassification dict
        dRemap_Biomass = df_biomass.set_index(ECO_ID)['stock'].to_dict()

        # reclassify ecosystem map to generate retention opening stock map
        logger.info('*** run reclassification and map generation')
        self.ret_maps['opening_stock'] = os.path.join(self.temp_dir,
                                                      f'{_CRET}_map_opening-stock_tonnes_{self.year}.tif')
        # prepare raster metadata
        self.accord.metadata.read_raster_tags([path_ETmap, self.statistics_raster])
        tags = self.accord.metadata.prepare_raster_tags(f'Carbon Opening Stock map for year {self.year}.',
                                                        'tonne/pixel')
        # run blockwise processing
        with rasterio.open(path_ETmap, 'r') as ds_lc, \
                rasterio.open(self.statistics_raster, 'r') as ds_mask, \
                rasterio.open(self.ret_maps['opening_stock'], 'w', **dict(self.accord.ref_profile.copy(),
                                                                          dtype=rasterio.float32,
                                                                          nodata=np.nan)) as ds_out:
            nblocks = number_blocks(self.accord.ref_profile, block_shape)
            ds_out.update_tags(**tags)
            for _, window in block_window_generator(block_shape, ds_lc.height, ds_lc.width):
                aData = reclassification(ds_lc.read(1, window=window), dict_classes=dRemap_Biomass,
                                         nodata_in=ds_lc.nodata, nodata_out=ds_out.nodata,
                                         outputtype=ds_out.profile['dtype'])[0]
                aMask = ds_mask.read(1, window=window)
                aData[aMask == 0] = ds_out.nodata
                ds_out.write(aData, window=window, indexes=1)
                add_progress(0.5 * 100. / nblocks)

    def net_sequestration_map_generation(self, add_progress):
        """Generate the carbon net sequestration maps for the statistical regions.

        :param add_progress: progressbar object
        """
        logger.info(f'start generation of carbon net sequestration map for year: {self.year}')

        # preprocess full provided GGE TSV file for all areas (including area check)
        self.df_seq = self.preprocess_gge(self.statistics_shape.index.to_list())

        # modify some LULUCF classes
        self.sequestration_class_modification()

        # calculate final flow based on chosen sequestration method
        self.calculate_sequestration_flow()

        # monetary evaluation for net sequestration
        if self.monetary:
            self.sequestration_monetary()

        # generate the maps for the statistical areas
        logger.info('* spatial disaggregate the sequestration by proxy')
        # prepare standard input for spatial disaggregation by Area and by ETclass
        path_proxy = self.config[_GCR][_CSEQ]['productivity_proxy_map'][self.year]
        path_ETmap = self.get_ecosystem_raster(self.year, level=1)
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()
        region_path = self.statistics_raster
        region_dic = self.statistics_shape[SHAPE_ID].to_dict()

        # sub-process weights for progressbar
        if self.monetary:
            pWeightSub = 3.
        else:
            pWeightSub = 1.

        logger.debug('** physical flow')
        # prepare Dataframe with MultiIndex
        df_data = self.df_seq_flow.stack(0).fillna(0).to_frame(name='physical')
        # prepare output file name
        self.seq_maps['physical'] = os.path.join(self.temp_dir,
                                                 os.path.basename(self.result_files[_CSEQ]['map_paths']['physical']
                                                                  [self.year]).split('.tif')[0] +
                                                 r'_statistical-regions.tif')
        # spatially disaggregate
        df_proxy_sum = self.accord.spatial_disaggregation_byArea_byET(path_proxy, df_data['physical'],
                                                                      region_path, region_dic,
                                                                      path_ETmap, eco_dic,
                                                                      self.seq_maps['physical'],
                                                                      processing_info=f'Carbon Net Sequestration flow '
                                                                                      f'for year {self.year}.',
                                                                      unit_info='tonnes/pixel',
                                                                      block_shape=(4096, 4096),
                                                                      add_progress=lambda p: add_progress(p /
                                                                                                          pWeightSub))
        if self.monetary:
            logger.debug('** monetary current')
            # prepare Dataframe with MultiIndex
            df_data = self.df_seq_current.stack(0).fillna(0).to_frame(name='current')
            # prepare output file name
            self.seq_maps['current'] = os.path.join(self.temp_dir,
                                                    os.path.basename(self.result_files[_CSEQ]['map_paths']
                                                                     ['monetary_current'][self.year]).split('.tif')[0] +
                                                    r'_statistical-regions.tif')
            # spatially disaggregate
            self.accord.spatial_disaggregation_byArea_byET(path_proxy, df_data['current'],
                                                           region_path, region_dic,
                                                           path_ETmap, eco_dic,
                                                           self.seq_maps['current'], proxy_sums=df_proxy_sum,
                                                           processing_info=f'Monetary evaluation of Carbon Net '
                                                                           f'Sequestration flow for year '
                                                                           f'{self.year} in Euro (current).',
                                                           unit_info='EURO/pixel',
                                                           block_shape=(4096, 4096),
                                                           add_progress=lambda p: add_progress(p / pWeightSub))
            logger.debug('** monetary real')
            # prepare Dataframe with MultiIndex
            df_data = self.df_seq_real.stack(0).fillna(0).to_frame(name='real')
            # prepare output file name
            self.seq_maps['real'] = os.path.join(self.temp_dir,
                                                 os.path.basename(self.result_files[_CSEQ]['map_paths']
                                                                  ['monetary_real'][self.year]).split('.tif')[0] +
                                                 r'_statistical-regions.tif')
            # spatially disaggregate
            self.accord.spatial_disaggregation_byArea_byET(path_proxy, df_data['real'],
                                                           region_path, region_dic,
                                                           path_ETmap, eco_dic,
                                                           self.seq_maps['real'], proxy_sums=df_proxy_sum,
                                                           processing_info=f'Monetary evaluation of Carbon Net '
                                                                           f'Sequestration flow for year '
                                                                           f'{self.year} in Euro (real).',
                                                           unit_info='EURO/pixel',
                                                           block_shape=(4096, 4096),
                                                           add_progress=lambda p: add_progress(p / pWeightSub))

    def sequestration_monetary(self):
        """ run the monetary evaluation for carbon net sequestration in real and current."""
        logger.info('* run monetary evaluation')

        # check if sequestration prices have to be converted
        if self.config[_GCR]['monetary']['unit_sequ_price'] == EURO_TON_CO2:
            priceConversionFactor = 1. / _CO2_2_CARBON
        elif self.config[_GCR]['monetary']['unit_sequ_price'] == EURO_TON_C:
            priceConversionFactor = 1.
        else:
            raise Error('this conversion case for GGE table data is currently not forseen')

        # first current prices
        current_price = self.seq_price_table.loc[self.year, 'EUR_per_ton_current']
        self.df_seq_current = self.df_seq_flow * (current_price * priceConversionFactor)
        # backup
        self.df_seq_current.reset_index().to_csv(os.path.join(self.temp_dir,
                                                              f'{_CSEQ}_statistics_flow-monetary_EURO-'
                                                              f'current_{self.year}.csv'))

        # second real prices
        deflator = self.deflator[self.deflator.index.isin(self.df_seq_current.index.tolist())]
        self.df_seq_real = self.df_seq_current.multiply(deflator[2000] / deflator[self.year], axis='index')
        # backup
        self.df_seq_real.reset_index().to_csv(os.path.join(self.temp_dir,
                                                           f'{_CSEQ}_statistics_flow-monetary_EURO-'
                                                           f'real_{self.year}.csv'))

    def sequestration_class_modification(self):
        """ function to modify sequestration table values extracted from GGE by external additional data
           - split 'other lands' class into ETclasses by area percentage in NUTS region
           - check if we have unmanaged forest, peatland, or wetland --> process and include in dataframe
        """
        logger.info('* run some ET class modifications')

        # first the split of the class other_lands
        logger.info('** split the LULUCF class CRF4F')
        # get the areas of the three classes breaking up CRF4F
        df = self.df_eco_area['area_ha'].unstack(level=1, fill_value=0)[[SPARSE, HEATH, COASTAL]].copy()
        # calculate the percentage distribution
        df['total'] = df.sum(axis=1)
        percentage = df.loc[:, df.columns != 'total'].divide(df['total'], axis=0).fillna(0)
        # split the GGE otherlands class by the percentage distribution
        df_other = percentage.merge(self.df_seq.otherlands, how='left', left_index=True, right_index=True)
        df_other = df_other.loc[:, df_other.columns != 'otherlands'].multiply(df_other['otherlands'], axis=0)
        # clean up
        self.df_seq = self.df_seq.merge(df_other, how='left', left_index=True,
                                        right_index=True).drop(columns=['otherlands'])
        self.df_seq.columns.rename(ECOTYPE, inplace=True)

        # second handle unmanaged forest
        logger.info('** include unmanaged forest')
        # if is given as table or map
        if self.config[_GCR][_CSEQ]['unmanaged_forest_table'][self.year]:
            logger.info('*** extract data from provided table')
            self.seq_include_unreported_table(self.config[_GCR][_CSEQ]['unmanaged_forest_table'][self.year],
                                              {FOREST: 'unmanaged_forests'})
        elif self.config[_GCR][_CSEQ]['unmanaged_forest_map'][self.year]:
            logger.info('*** extract data from provided map')
            self.seq_include_unreported_map(self.config[_GCR][_CSEQ]['unmanaged_forest_map'][self.year],
                                            [FOREST])
        else:
            logger.debug('*** no unmanaged forest data provided')

        # third handle unmanaged wetlands
        logger.info('** include unmanaged wetlands')
        # if is given as table or map
        if self.config[_GCR][_CSEQ]['unmanaged_wetland_table'][self.year]:
            logger.info('*** extract data from provided table')
            self.seq_include_unreported_table(self.config[_GCR][_CSEQ]['unmanaged_wetland_table'][self.year],
                                              {WETLANDS: 'unmanaged_wetland_inland',
                                               COASTAL: 'unmanaged_wetland_coastal'})
        elif self.config[_GCR][_CSEQ]['unmanaged_wetland_map'][self.year]:
            logger.info('*** extract data from provided map')
            self.seq_include_unreported_map(self.config[_GCR][_CSEQ]['unmanaged_wetland_map'][self.year],
                                            [WETLANDS, COASTAL])
        else:
            logger.debug('*** no unmanaged wetland data provided')

        # fourth handle non-reported peatlands
        logger.info('** include non-reported peatlands')
        # if is given as table or map
        if self.config[_GCR][_CSEQ]['non-reported_peatland_table'][self.year]:
            logger.info('*** extract data from provided table')
            self.seq_include_unreported_table(self.config[_GCR][_CSEQ]['non-reported_peatland_table'][self.year],
                                              {GRASSLAND: 'peatland_grass',
                                               HEATH: 'peatland_heathland-shrub'})
        elif self.config[_GCR][_CSEQ]['non-reported_peatland_map'][self.year]:
            logger.info('*** extract data from provided map')
            self.seq_include_unreported_map(self.config[_GCR][_CSEQ]['non-reported_peatland_map'][self.year],
                                            [GRASSLAND, HEATH])
        else:
            logger.debug('*** no non-reported peatlands data provided')

    def seq_include_unreported_map(self, path_map, ETclasses):
        """ function to included sequestration data from provided maps for specific classes into
            the sequestration master table generated from the GHG inventory

        :param path_map: path to external map providing non-reported/unmanaged ecosystems
        :param ETclasses: list of ecosystem names to process
        """
        # run data extraction from map
        df = statistics_byArea_byET(path_map,
                                    self.statistics_raster,
                                    self.statistics_shape[SHAPE_ID].to_dict(),
                                    self.get_ecosystem_raster(self.year, level=1),
                                    types_l1.set_index('name')[ECO_ID][ETclasses].to_dict(),
                                    block_shape=(4096, 4096))
        # extract needed data
        df = df[[SUM]].unstack(1)[SUM]
        # convert to carbon from CO2 in kilotonne and inverse to make sequestration positive
        df = df.fillna(0) * _CO2_2_CARBON * _UNIT_2_BASE * -1
        # add extracted values to sequestration master table
        for element in ETclasses:
            self.df_seq[element] = self.df_seq[element].add(df[element])

    def seq_include_unreported_table(self, path_table, dic_ETclasses):
        """ function to included sequestration data from provided tables for specific classes into
            the sequestration master table generated from the GHG inventory

        :param path_table: path to external tables providing non-reported/unmanaged ecosystems
        :param dic_ETclasses: dictionary providing relation of ET class to column name in table
        """
        # get table data
        df = pd.read_csv(path_table, comment='#')
        # filter to needed regions and set missing to zero
        df = df.set_index(NUTS_ID).reindex(self.statistics_shape.index, fill_value=0)
        # convert to C in tonne & sequestration positive
        df = df.fillna(0) * _CO2_2_CARBON * _UNIT_2_BASE * -1
        # add to master table
        for key, value in dic_ETclasses.items():
            self.df_seq[key] = self.df_seq[key].add(df[value])

    def calculate_sequestration_flow(self):
        """ calculate carbon net sequestration flow based on chosen calculation method."""
        # check which calculation method we use
        if self.config.get('jrcmethod'):
            self.JRCmethod()
        else:
            # normal flow calculation
            logger.info('* run flow calculation')
            flow_physical = self.df_seq.copy()
            # cleanup table and bring all missing classes with Zero
            self.df_seq_flow = flow_physical.reindex(columns=types_l1['name'].to_list(), fill_value=0)

        # backup the flow as csv
        self.df_seq_flow.reset_index().to_csv(os.path.join(self.temp_dir,
                                                           f'{_CSEQ}_statistics_flow-physical_tonnes_{self.year}.csv'))

    def JRCmethod(self):
        """ the JRC internal method to calculate net sequestration.

        Note: currently only ET class forest is reported
        """
        logger.info('* run flow calculation using the JRC internal method')
        # first get the EU wide net sequestration
        net = self.df_seq.sum().sum()
        logger.debug('EU wide Net sequestration is {} tons of Carbon'.format(net))

        # second calculate overall EU wide uptake when ET class is positive in EU (used as correction factor)
        keys = []
        correction_factor = 0
        for key in self.df_seq.columns:
            # total uptake (by MS)
            if self.df_seq[key].sum() < 0:
                continue
            keys.append(key)  # that we know which ET classes produces a positive uptake on full EU scale
            # calculate overall EU uptake for all ET classes with EU wide positive balance
            correction_factor += self.df_seq[key].apply(lambda x: max(x, 0)).sum()

        # ini result dataframe
        flow_physical = pd.DataFrame(index=self.df_seq.index)

        # third calculate a country specific and ET class specific uptake ratio based on the net sequestration
        for key in keys:
            # calculate uptake of ET class on EU level (emmission set to 0)
            UWms = self.df_seq[key].apply(lambda x: max(x, 0))

            # ratio
            ratio = UWms / correction_factor

            # physical flow for the ET class for all MS
            # calculate by taking the percentage of the MS uptake on the overall EU uptake and
            # multiply with the EU net uptake
            phys_flow = ratio * net

            # add this series result to the result dataframe
            flow_physical = flow_physical.merge(phys_flow.to_frame(), how='left', left_index=True, right_index=True)

        # now we can clean up the table
        # first limit results to forest class, add all missing ET classes again and set them to zero
        self.df_seq_flow = flow_physical[[FOREST]].reindex(columns=types_l1['name'].to_list(), fill_value=0)

    def preprocess_gge(self, lRegions):
        """ load the GGE TSV file and preprocess to selected regions and year

        :param lRegions: list of NUTS_IDs to extract the GGE data for
        :return: pandas DataFrame containing the GGE values for chosen regions, year and LULUCF categories
        """
        # run the gge data processing f
        logger.info('* generating the gge values from {}'.format(self.config[_GCR][_CSEQ]['env_air_gge']))

        # reading in TSV file and filter by MultiIndex
        self.df_gge = read_tsv(self.config[_GCR][_CSEQ]['env_air_gge']).loc[(_UNIT,    # needed unit
                                                                             _AIRPOL,  # emitter type
                                                                             _SRC_CRF + _WOODREMOVAL,  # LULUCF groups
                                                                             lRegions  # list of regions
                                                                             ), :].droplevel(level=[0, 1])
        # run checks
        if self.year not in self.df_gge.columns:
            raise ConfigError(f'the processing year {self.year} is not in the provided TSV file',
                              [_GCR, _CSEQ, 'env_air_gge'])

        lMissing = [x for x in lRegions if x not in self.df_gge.index.get_level_values('geo').unique().tolist()]
        if len(lMissing) != 0:
            raise ConfigError(f'not all needed statistic regions are included in the TSV file. missing:' +
                              ', '.join(str(x) for x in lMissing), [_GCR, _CSEQ, 'env_air_gge'])

        lMissing = [x for x in _SRC_CRF if x not in self.df_gge.index.get_level_values('src_crf').unique().tolist()]
        if len(lMissing) != 0:
            raise ConfigError(f'not all requested LULUCF types are included in the TSV file. missing:' +
                              ', '.join(str(x) for x in lMissing), [_GCR, _CSEQ, 'env_air_gge'])

        # extract the needed year and convert to ton of carbon PLUS reverse the sign to have emitter as neg
        df = self.df_gge[int(self.year)].fillna(0) * _CO2_2_CARBON * _UNIT_2_BASE * -1
        df = df.unstack(0).rename(columns=_SRC_CRF_DIC)
        df.drop(_WOODREMOVAL, axis=1, inplace=True)
        df.index.rename(NUTS_ID, inplace=True)
        df.columns.rename(ECOTYPE, inplace=True)
        return df

    def annual_config_checks(self):
        """ runs some important annual configuration checks to allow smooth operation."""
        # if in sequestration only table or map exist for some entries AND not both
        for element in ['unmanaged_forest', 'unmanaged_wetland', 'non-reported_peatland']:
            if self.config[_GCR][_CSEQ][element + '_map'][self.year] \
                    and self.config[_GCR][_CSEQ][element + '_table'][self.year]:
                raise ConfigError(f'The {element} input in {_CSEQ} must be a table or a map.' +
                                  'Not both options can be filled for the processing year. Please remove the table.',
                                  [_GCR, _CSEQ, element + '_table', self.year])

        # that valid retention mode has correct annual input file
        if self.config[_GCR][_CRET]['reten_operation_mode'] == GAIN_LOSS_BASE:
            if not self.config[_GCR][_CRET]['carbon_stock_lookup_table'][self.year]:
                raise ConfigError(f'For the {GAIN_LOSS_BASE} method the carbon_stock_lookup_table is needed.',
                                  [_GCR, _CRET, 'carbon_stock_lookup_table', self.year])
        if self.config[_GCR][_CRET]['reten_operation_mode'] == GAIN_LOSS:
            if not self.config[_GCR][_CRET]['opening_carbon_stock_table'][self.year]:
                raise ConfigError(f'For the {GAIN_LOSS} method the opening_carbon_stock_table is needed.',
                                  [_GCR, _CRET, 'opening_carbon_stock_table', self.year])
        if self.config[_GCR][_CRET]['reten_operation_mode'] == STOCK_DIFFERENCE:
            if not self.config[_GCR][_CRET]['closing_carbon_stock_table'][self.year]:
                raise ConfigError(f'For the {STOCK_DIFFERENCE} method the closing_carbon_stock_table is needed.',
                                  [_GCR, _CRET, 'closing_carbon_stock_table', self.year])

        if self.monetary:
            # check that annual price exists
            if self.year not in self.seq_price_table.index:
                raise ConfigError(f'The processing year {self.year} is not present in the {_CSEQ} prices.',
                                  [_GCR, 'monetary', 'seq_carbon_price_table'])

    def get_progress_weights(self):
        """Return a dict desribing the relative amount of time spent in each part of the calculation, based on an
         example run, and the relative time taken up by the main calculation vs init."""
        time_spent = {'ecosystem_stats': 1.2,
                      'sequestration_map': 6.0,
                      'retention_map': 1.5 if self.config[_GCR][_CRET]['reten_operation_mode'] == STOCK_DIFFERENCE
                      else 4.5,
                      'statistics': 2.5,
                      'final_map': 0. if self.config['statistics_shape'] == self.config['reporting_shape'] else 1.6}
        total_time = sum(time_spent.values())
        return {key: time / total_time for key, time in time_spent.items()}
