#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Air Filtration supply / use calculation."""
import logging
import os
from contextlib import ExitStack
from itertools import zip_longest

import rasterio
import numpy as np
from scipy.ndimage import convolve

import inca
import inca.common.ecosystem as ecosystem
import inca.common.suts as suts
import inca.common.config_check as confcheck
from inca.common.config_check import ConfigRasterDir, ConfigChoice, ConfigItem, check_csv, ConfigRaster
from inca.common.errors import ConfigError
from inca.common.classification import reclassification, CSV_2_dict
from inca.common.geoprocessing import statistics_byArea_byET, block_window_generator, number_blocks, pixel_area, \
    RasterType
from inca.common.ecosystem import SHAPE_ID

logger = logging.getLogger(__name__)

_AF = 'Air Filtration'

PM10 = 'pm10'
PM2P5 = 'pm2p5'
SEASONAL = 'seasonal'
MONTHLY = 'monthly'
YEARLY = 'yearly'  # careful not to clash with inca.common.config_check.YEARLY ...
_WIND_MAP = 'wind_map'
_POLLUTION_MAP = 'pollution_map'
_LAI_MAP = 'lai_map'
LAI_MODEL = 'LAI-based'
TABLE_MODEL = 'LUT-based'
num_maps = {YEARLY: 1,
            SEASONAL: 4,
            MONTHLY: 12}

correction_factors = {YEARLY: 3.1536 / 1000., SEASONAL: 0.7884 / 1000., MONTHLY: 0.2628 / 1000.}

wind_VDlai_dict = {0: 0,
                   1: 0.030,
                   2: 0.087,
                   3: 0.143,
                   4: 0.160,
                   5: 0.176,
                   6: 0.182,
                   7: 0.504,
                   8: 0.819,
                   9: 0.810,
                   10: 1.836,
                   11: 1.772,
                   12: 1.688,
                   13: 1.625}


def set_cmdline_args(parser):
    """Additional command line arguments for the Air Filtration.

    :param parser: subparser object for Air Filtration.
    """
    parser.add_argument('--disable_wind', action='store_true',
                        help='Disable wind correction.  Only has an effect for PM2p5; for PM10, wind correction is '
                             'automatically disabled.')


class AirFiltrationRun(inca.Run):
    service_name = _AF

    def __init__(self, config):
        super().__init__(config)

        self.config_template.update({
            self.service_name: {'pollution_type': ConfigChoice(PM2P5, PM10),
                                'aggregate_type': ConfigChoice(YEARLY, SEASONAL, MONTHLY),
                                'model': ConfigChoice(LAI_MODEL, TABLE_MODEL),
                                'wind_table' : ConfigItem(check_csv, optional = True, required_columns=['wind_speed','Vdlai']),
                                'deposition_table' : ConfigItem(check_csv, optional = True, required_columns=['Code','DepositionVelocityPM2p5','DepositionVelocityPM10']),
                                'ecotyping_map' : {confcheck.YEARLY: ConfigRaster(optional = True, raster_type=RasterType.CATEGORICAL)},
                                'aquatic_ecosystems' : ConfigItem(check_csv, optional = True, required_columns=['Code','Aquatic']),

                                # TODO check the raster_type for all three ConfigRasterDir calls!!!
                                _LAI_MAP: {confcheck.YEARLY: ConfigRasterDir(optional=True, raster_type=RasterType.RELATIVE)},
                                # wind_map is only required for specific scenarios, we check those manually below:
                                _WIND_MAP: {confcheck.YEARLY: ConfigRasterDir(optional=True,
                                                                              raster_type=RasterType.ABSOLUTE_POINT)},
                                _POLLUTION_MAP: {confcheck.YEARLY: ConfigRasterDir(raster_type=RasterType.RELATIVE)}}})

        self.wind_correction = True  # default True, disabled for PM10, or if ---disable_wind is used.
        self.lai_res_correction = False # default False, enabled  if ---res_correction is used.
        self.alt_typing = False #if alt_typing ecotyping and deposition table do not follow L1 or L2 typing

        self.make_output_filenames()

    def make_output_filenames(self):
        """Generates the filenames for all final output files."""
        air_filtration = _AF.lower().replace(' ', '-')
        self.map_path = os.path.join(self.maps, f'{air_filtration}_map_use_tonnes_''{year}.tif')
        self.SUT_physical_path = os.path.join(self.suts, f'{air_filtration}_report_SUT-physical_1000_tonnes_'
                                                         '{year}.xlsx')
        self.statistics_csv = os.path.join(self.statistic_dir, f'{air_filtration}_statistics_tonnes_''{year}.csv')

    def _start(self):
        """Generate the airfiltration account"""
        # No wind correction if pollution_type == pm10
        if self.config[_AF]['pollution_type'] == PM10:
            self.wind_correction = False
        # No wind correction if disabled from command line
        if self.config.get('disable_wind'):
            self.wind_correction = False
        # res_correction if enabled from command line
        if self.config.get('res_correction'):
            self.lai_res_correction = False
        #no wind correction if look-up tables are used
        if self.config[_AF]['model'] == TABLE_MODEL:
            self.wind_correction = False

        #check for available LAI-maps config:
        if self.config[_AF]['model'] == LAI_MODEL:
            if _LAI_MAP not in self.config[_AF]:
                raise (f"{LAI_MODEL} model can only be run with {_LAI_MAP} available")
        if self.wind_correction:
            if 'wind_table' not in self.config[_AF]:
                raise (f"{LAI_MODEL} model can only be run with wind_table available")


        if self.config[_AF]['ecotyping_map'][self.years[0]]:
            self.alt_typing = True


        # Arrange input maps
        input_maps = self.collect_input_maps()

        # Progress weight for different steps:
        # Relative amount of time spent in map_generation vs calculate_sut depends on the number of input rasters
        # used, i.e. on whether we have monthly/seasonal/yearly input and whether we have wind maps or not.
        # cut to reporting area processes a single raster -> weight 1
        # calculate_sut each processes a single raster, and does ecosystem classification -> weight 2
        num_maps_aggregation = num_maps[self.config[_AF]['aggregate_type']]
        if self.wind_correction:
            work_map_generation = 3 * num_maps_aggregation  # pollution + lai + wind
        else:
            work_map_generation = 2 * num_maps_aggregation
        total_work = 3. + work_map_generation
        weight_map_generation = work_map_generation / total_work
        weight_sut = 1. / total_work
        weight_cut_map = 2. / total_work

        # Processing per year:
        for year in self.years:
            self.AF_map_generation(year, input_maps,
                                   add_progress=lambda p: self.add_progress(weight_map_generation * p))
            self.cut_to_reporting_regions(self.map_path.format(year=year),
                                          add_progress=lambda p: self.add_progress(weight_cut_map * p))
            self.calculate_sut(year, add_progress=lambda p: self.add_progress(weight_sut * p))

    def AF_map_generation(self, year, input_maps, add_progress, block_shape=(1024, 1024)):
        """Generation of the airfiltration map
        :param year: year to process
        :param input_maps: list of ordered input_maps. Pollution map is mandatory.
        :param add_progress: part that handles progress bar
        :param block_shape: processing block size tuple
        """
        logger.info('Start AF_map_generation.')
        self.accord.metadata.read_raster_tags((self.config[_AF][_LAI_MAP][year] if self.config[_AF]['model'] == LAI_MODEL else [])
                                       + (self.config[_AF][_WIND_MAP][year] if self.wind_correction else [])
                                       + self.config[_AF][_POLLUTION_MAP][year])
        tags = self.accord.metadata.prepare_raster_tags(f'Air Filtration map for year {year} based on '
                                                 f'{self.config[_AF]["pollution_type"]} with '
                                                 f'a time resolution of {self.config[_AF]["aggregate_type"]}.',
                                                 'tonne/year')

        #read in aquatic ecosystem mapping

        if self.config[_AF]['model'] == TABLE_MODEL:
            reclass_dict_VD = CSV_2_dict(self.config[_AF]["deposition_table"], old_class='Code',
                                         new_class=f'DepositionVelocity{self.config[_AF]["pollution_type"].replace("pm","PM")}')
        elif (self.config[_AF]["aquatic_ecosystems"] is not None) and (self.config[_AF]["aquatic_ecosystems"] != ''):
            reclass_aqua = CSV_2_dict(self.config[_AF]["aquatic_ecosystems"], old_class='Code', new_class='Aquatic')

        # run blockwise processing
        with ExitStack() as stack, \
                rasterio.open(self.map_path.format(year=year), 'w',
                              **dict(self.accord.ref_profile.copy(), dtype=rasterio.float32, nodata=np.nan,
                                     tiled=True, compress='DEFLATE')) as ds_out:
            lai_dss = [stack.enter_context(rasterio.open(path))
                       for path in input_maps[_LAI_MAP][year]] if self.config[_AF]['model'] == LAI_MODEL else []
            wind_dss = [stack.enter_context(rasterio.open(path))
                        for path in input_maps[_WIND_MAP][year]] if self.wind_correction else []
            pol_dss = [stack.enter_context(rasterio.open(path)) for path in input_maps[_POLLUTION_MAP][year]]
            ET_dss_L1 = stack.enter_context(rasterio.open(self.get_ecosystem_raster(year)))
            ET_dss_L2 = stack.enter_context(rasterio.open(self.get_ecosystem_raster(year, level = 2)))
            if self.alt_typing :
                ET_dss_alt = stack.enter_context(rasterio.open(self.config[_AF]['ecotyping_map'][year]))

            if self.config[_AF]['model'] == TABLE_MODEL:
                if not self.alt_typing:
                    def split_with_default(string, delimiter, min_splits, default_value):
                        parts = string.split(delimiter)
                        if len(parts) >= min_splits:
                            return parts
                        else:
                            return parts + [default_value]* (min_splits - len(parts))

                    reclass_dict_VD_L2 = {1000 * int(split_with_default(str(key),'.',2,'0')[0]) +
                                       int(split_with_default(str(key),'.',2,'0')[1]) : value
                                       for key, value in reclass_dict_VD.items()}
                    reclass_dict_VD_L1 = {key // 1000 : value
                                        for key, value in reclass_dict_VD_L2.items() if key % 1000 == 0}

            pixel_area_ha = pixel_area(ds_out.crs, ds_out.transform) / 10000.  # m² to ha

            nblocks = number_blocks(self.accord.ref_profile, block_shape)
            # if wind correction is done, progress bar should account for reclassification step.
            if self.wind_correction:
                weight_wind = 0.5
            else:
                weight_wind = 0.
            ds_out.update_tags(**tags)
            for _, window in block_window_generator(block_shape, pol_dss[0].height, pol_dss[0].width):
                pol_data = np.ma.zeros((len(pol_dss), window[0][1] - window[0][0], window[1][1] - window[1][0]))
                wind_data = pol_data.copy()
                lai_data = pol_data.copy()
                ET_data_L1 = ET_dss_L1.read(1, window=window)
                ET_data_L2 = ET_dss_L2.read(1, window=window)
                if self.alt_typing:
                    ET_data_alt = ET_dss_alt.read(1, window=window)

                for idx, (ds_lai, ds_wind, ds_pol) in enumerate(zip_longest(lai_dss, wind_dss, pol_dss)):
                    if self.config[_AF]['model'] == LAI_MODEL:
                        masked_lai = np.ma.masked_values(ds_lai.read(1, window=window, fill_value=ds_lai.nodata),
                                                         ds_lai.nodata)
                        lai_data[idx] = masked_lai * ds_lai.scales + ds_lai.offsets
                        #try to find something suitable to dived aquatic area to surrounding area's


                    if self.wind_correction:
                        masked_wind = np.ma.masked_values(ds_wind.read(1, window=window, fill_value=ds_wind.nodata),
                                                          ds_wind.nodata)
                        wind_data[idx] = masked_wind * ds_wind.scales + ds_wind.offsets

                    #it seems that masked_pol does not really have a nodata value
                    if ds_pol.nodata == None:
                        masked_pol = np.ma.masked_values(ds_pol.read(1, window=window, fill_value=np.nan),
                                                        np.nan)
                    else:
                        masked_pol = np.ma.masked_values(ds_pol.read(1, window=window, fill_value=ds_pol.nodata),
                                                     ds_pol.nodata)
                    pol_data[idx] = masked_pol * ds_pol.scales + ds_pol.offsets
                    add_progress((1. - weight_wind) * 100. / (len(pol_dss) * nblocks))

                if self.config[_AF]['model'] == TABLE_MODEL:
                    if self.alt_typing:
                        VD, dict_classes  = reclassification(ET_data_alt, reclass_dict_VD, ET_dss_alt.nodata, ET_dss_alt.nodata, outputtype = np.float64)
                    else:
                        VD_L2, dict_classes  = reclassification(ET_data_L2, reclass_dict_VD_L2, ET_dss_L2.nodata, 0, outputtype = np.float64)
                        VD_L1, dict_classes2  = reclassification(ET_data_L1, reclass_dict_VD_L1, ET_dss_L1.nodata, 0, outputtype = np.float64)
                        #There shouldn't be any overlap between VDa en VDb
                        if (sum(VD_L2[VD_L1 != 0]) != 0) or (sum(VD_L1[VD_L2 != 0]) != 0):
                            logging.error("There is some overlap between L1 and L2 data please check deposition table for errors")

                        VD = VD_L1 + VD_L2
                        VD = np.ma.masked_where((ET_data_L2==ET_dss_L2.nodata) | (ET_data_L1==ET_dss_L1.nodata), VD)

                    # convert kg/m^3 to yg/m^3
                    deposition = VD * pol_data * (10. ** 9) * correction_factors.get(
                        self.config[_AF]['aggregate_type'])
                else:
                    if self.lai_res_correction:
                        # Get the shape of lai_data
                        bands, rows, cols = lai_data.shape

                        # Find the indices where aq_mask is 1
                        mask_indices = np.where(aq_mask == 1)
                        lai_data.harden_mask()
                        # get the orginal cell values
                        values = lai_data[:, mask_indices[0], mask_indices[1]].copy()
                        # Set the masked cells to 0

                        lai_data[:, mask_indices[0], mask_indices[1]] = 0

                        #create kernel
                        kernel =np.ones((9, 9))

                        # Create a binary mask of surrounding cells with mask pixel equal to 0
                        surrounding_mask = convolve((aq_mask == 0).astype(np.int_), kernel , mode='constant', cval=0)

                        # Count the number of surrounding cells for each masked cell
                        num_surrounding_cells = surrounding_mask[mask_indices[0],mask_indices[1]]

                        # Calculate the distribution over the surrounding cells
                        distributed_values = values / num_surrounding_cells
                        distributed_values = np.nan_to_num(distributed_values)  # Replace NaN with 0

                        #restribute values
                        value_mask = np.repeat(aq_mask[np.newaxis,:,:]==1, bands, axis=0).astype(np.float64)
                        value_mask[:,mask_indices[0],mask_indices[1]] =  distributed_values.filled(0)
                        redistribute = convolve(value_mask , [kernel] , mode='constant', cval=0)
                        redistribute[:,mask_indices[0],mask_indices[1]] = 0

                        lai_data += redistribute

                    if self.wind_correction:
                        wind_data[np.isnan(wind_data)] = wind_dss[0].nodata
                        floor_VDlai = reclassification(np.floor(wind_data),
                                                       dict_classes=wind_VDlai_dict,
                                                       nodata_in=wind_dss[0].nodata,
                                                       nodata_out=ds_out.nodata,
                                                       outputtype=ds_out.profile['dtype'])[0]
                        add_progress(0.5 * weight_wind * 100. / nblocks)
                        ceil_VDlai = reclassification(np.ceil(wind_data),
                                                      dict_classes=wind_VDlai_dict,
                                                      nodata_in=wind_dss[0].nodata,
                                                      nodata_out=ds_out.nodata,
                                                      outputtype=ds_out.profile['dtype'])[0]
                        add_progress(0.5 * weight_wind * 100. / nblocks)
                        VDlai = floor_VDlai + (np.ceil(wind_data) - np.floor(wind_data)) * (ceil_VDlai - floor_VDlai)
                    else :
                            # convert 0.0064 m/s to 0.64 cm/s since VDlai should be in cm/s
                            VDlai = np.ma.ones(lai_data.shape) * 0.64 / 6.

                    # convert kg/m^3 to yg/m^3
                    deposition = VDlai * lai_data * pol_data * (10. ** 9) * correction_factors.get(
                        self.config[_AF]['aggregate_type'])

                deposition_total = np.ma.sum(deposition, axis=0)
                # convert tonne/ha to tonne/pixel.
                deposition_total *= pixel_area_ha

                if (self.config[_AF]['model'] == LAI_MODEL) and (self.config[_AF]["aquatic_ecosystems"] is not None) \
                        and (self.config[_AF]["aquatic_ecosystems"] != ''):
                    #mask aquatic ecosysytems
                    aq_mask, dict_classes  = reclassification(ET_data_L1, reclass_aqua, ET_dss_L1.nodata, ds_out.nodata)
                    deposition_total.harden_mask()
                    deposition_total[aq_mask==1] = 0


                ds_out.write(deposition_total.filled(ds_out.nodata).astype(ds_out.profile['dtype']), window=window, indexes=1)

    def calculate_sut(self, year, add_progress):
        """Calculate SUT's
        :param year: year to process
        :param add_progress: part that handles progress bar
        """
        logger.info('Calculating statistics per reporting region and per ecosystem.')
        stats = statistics_byArea_byET(self.map_path.format(year=year),
                                       self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                       self.get_ecosystem_raster(year, add_progress=lambda p: add_progress(0.5 * p)),
                                       ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                       add_progress=lambda p: add_progress(0.5 * p))

        # Pivot ecosystem type to columns
        supply = stats['sum'].unstack(ecosystem.ECOTYPE)
        # All use is household final consumption
        use = supply.sum(axis=1).rename(suts.HSH)
        suts.write_sut(self.SUT_physical_path.format(year=year), (supply/1000),
                       (use/1000), self.service_name,
                       year=year, unit=f'1000 tonnes of {self.config[_AF]["pollution_type"]}')

        supply.rename(columns=dict(ecosystem.types_l1[['name', 'short_name']].values)).to_csv(
            self.statistics_csv.format(year=year))

    def collect_input_maps(self):
        """Check all required input maps are there, and sort them in the right order."""
        # check if we have all data  available
        aggregate_type = self.config[_AF]['aggregate_type']
        num_maps_required = num_maps[aggregate_type]

        aggregate_maps = {_POLLUTION_MAP: dict()}
        if self.wind_correction:
            aggregate_maps[_WIND_MAP] = dict()
        if self.config[_AF]['model'] == LAI_MODEL:
            aggregate_maps[_LAI_MAP] = dict()

        for year in self.years:
            for map_type, maps in aggregate_maps.items():
                if not self.config[_AF][map_type][year] or len(self.config[_AF][map_type][year]) != num_maps_required:
                    raise ConfigError(f'For aggregation type "{aggregate_type}", the {map_type} directory must contain '
                                      f'exactly {num_maps_required} raster files.', [_AF, map_type, year])
                if num_maps_required == 1:
                    aggregate_maps[map_type][year] = self.config[_AF][map_type][year]
                else:
                    # If we have more than 1 map, map files should be numbered, with the format '<number>_*.tif', so
                    # we know which maps belong together.  We sort maps according to that number, and check if we have
                    # exactly the numbers we expect.
                    map_error = ConfigError(f'For aggregation type "{aggregate_type}", the {map_type} directory must '
                                            f'contain raster files numbered 1 to {num_maps_required}.',
                                            [_AF, map_type, year])
                    try:
                        # Extract leading number from filenames, and use that to sort files:
                        sorted_maps = sorted([(int(os.path.basename(f).split('_')[0]), f)
                                              for f in self.config[_AF][map_type][year]],
                                             key=lambda x: x[0])
                        # Check files are numbered from 1 to 'num_maps':
                        if any(expected_num != map_num for expected_num, (map_num, _) in enumerate(sorted_maps, 1)):
                            raise map_error
                        # All is good:
                        maps[year] = [file for _, file in sorted_maps]
                    except ValueError:
                        raise map_error

        return aggregate_maps
