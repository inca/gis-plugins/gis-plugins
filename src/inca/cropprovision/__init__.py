# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.
"""Crop provision."""

import sys

if sys.version_info[:2] >= (3, 9):
    import importlib.resources as importlib_resources
else:
    import importlib_resources

import logging
import os

import numpy as np
import pandas as pd
import rasterio

import inca
import inca.common.estat as estat
import inca.common.suts as suts
from inca.common.config_check import ConfigItem, check_csv, check_tsv, ConfigRaster, YEARLY
from inca.common.errors import ConfigError
from inca.common.ecosystem import ECOTYPE, ECOTYPE_L1, ecotype_l1_cat, types_l1, ECO_ID, SHAPE_ID
from inca.common.geoprocessing import statistics_byArea_byET, RasterType, number_blocks, block_window_generator
from inca.common.nuts import NUTS_ID

logger = logging.getLogger(__name__)

# some globals for faster adaptation
_CROPPROVISION = 'Crop Provision'
_OPERATION_MODE = 'operation-mode'
_LUT_CROPS = 'lut_crops'
_CROP_YIELD = 'crop_yield'
_PROXY_CROPTYPE = 'proxy_croptype'
_PROXY_CROPYIELD = 'proxy_cropyield'
_PROXY_PRODUCTIVITY = 'proxy_productivity'

# naming of calculation methods based on data source
MAT_FLOW   = 'material-flow'
AGRI_STATS = 'agriculture-statistics'
GEOSPATIAL = 'agriculture-yield-map'

CODE_MFA = 'code_mfa'
CODE_APRO= 'code_apro'
AACT_UV  = 'aact_uv01'
ECOCON   = 'ecocon'
CROPTYPE = 'code_croptype'

# units
EUR_TONNE = 'EUR_T'  # code for prices in euro per tonne in unit value table
EUR_TOTAL = 'MIO_EUR' # code for prices in euro (total production value) in unit value table
EU_STD_HUMIDITY = 'PR_HU_EU'  # code for Harvested production in eu standard humidity (1000 t)
EU_AREA = 'MA'       # code for area in Harvested production to be converted into yield for a few crop types
_UNIT = 'unit'
_THOUSAND_TONNE = 'THS_T'

_INDIC_ENV = 'indic_env'
_DOMESTIC_EXTRACTION = 'DE'

#factors in LookUp Table to be applied on statistics
_AREA_TO_YIELD = 1
_RESIDUES_FACTOR = 2
_HUMIDITY_FACTOR = 3
_LIVESTOCK_FACTOR = 4

mfa_codes = {'MF111','MF112','MF113','MF114','MF115','MF116','MF117','MF118','MF119','MF11A', \
             'MF1211','MF1212','MF1221','MF1222'}

mfa_mapping_subtotals = {'MF11':['MF111','MF112','MF113','MF114','MF115','MF116','MF117','MF118','MF119','MF11A'], \
                             'MF121':['MF1211','MF1212'], 'MF122':['MF1221','MF1222'], \
                             'MF12':['MF121', 'MF122']}

mfa_labels = {'MF11': 'MF1.1 crops (excluding fodder crops)',
              'MF111': 'MF1.1.1 cereals',
              'MF112': 'MF1.1.2 roots and tubers',
              'MF113': 'MF1.1.3 sugar crops',
              'MF114': 'MF1.1.4 pulses',
              'MF115': 'MF1.1.5 nuts',
              'MF116': 'MF1.1.6 oil-bearing crops',
              'MF117': 'MF1.1.7 vegetables',
              'MF118': 'MF1.1.8 fruits',
              'MF119': 'MF1.1.9 fibres',
              'MF11A': 'MF1.1.A other crops (excluding fodder crops) n.e.c.',
              'MF12':  'MF1.2 Crop residues (used) and fodder crops and grazed biomass',
              'MF121': 'MF1.2.1 Crop residues (used)',
              'MF1211': 'MF1.2.1.1 Straw',
              'MF1212': 'MF1.2.1.2 Other crop residues (sugar and fodder beet leaves and other)',
              'MF122': 'MF1.2.2 Fodder crops and grazed biomass',
              'MF1221': 'MF1.2.2.1 Fodder crops (including biomass harvest from grassland)',
              'MF1222': 'MF1.2.2.2 Grazed biomass'}

output_labels = {'SWHE': 'soft wheat',
                 'DWHE': 'durum wheat',
                 'BARL': 'barley',
                 'OATS': 'oats',
                 'MAIZ': 'maize',
                 'OCER': 'other cereals',
                 'RAPE': 'rape',
                 'SUNF': 'sunflower',
                 'MAIF': 'fodder maize',
                 'OFAR': 'other forage',
                 'PULS': 'protein crops',
                 'POTA': 'potatoes',
                 'SUGB': 'sugar beet'}


class CropRun(inca.Run):

    service_name = _CROPPROVISION

    def __init__(self, config):
        super().__init__(config)

        if config.get(_OPERATION_MODE) == MAT_FLOW:  # Currently no monetary evaluation for material flow
            self.monetary = False

        self.config_template.update({
            self.service_name: {
                _OPERATION_MODE: ConfigItem(default=MAT_FLOW),
                _LUT_CROPS: ConfigItem(check_csv, optional=True, required_columns=[CODE_MFA, 'label_mfa', ECOTYPE_L1]),
                _CROP_YIELD: ConfigItem(check_tsv),  # apro_cpsh1 table for AGRI_STATS, env_ac_mfa table for MAT_FLOW
                _PROXY_CROPTYPE: {YEARLY: ConfigRaster(optional=True, raster_type=RasterType.CATEGORICAL)},
                _PROXY_CROPYIELD: {YEARLY: ConfigRaster(optional=True, raster_type=RasterType.RELATIVE)},
                _PROXY_PRODUCTIVITY: {YEARLY: ConfigRaster(optional=True, raster_type=RasterType.RELATIVE)},
            }
        })
        if self.monetary:  # Inputs used only for monetary evaluation:
            logger.debug('Add config_template for monetary')
            self.config_template[self.service_name].update({
                AACT_UV: ConfigItem(check_tsv)
            })

        # if a developer wants to run the original KIP-INCA JRC method, some parameters are to be overruled
        # this method is not available from QGIS and requires a command line option
        if self.config.get('jrcmethod'):
            logger.warning('jrcmethod overwrite set.  Operation mode is %s; crop_yield should be in apro_cpsh1 format.',
                           AGRI_STATS)
            self.config_template[self.service_name].update({
                    ECOCON: ConfigItem(check_csv, required_columns=[NUTS_ID] + list(output_labels))})

            self.config[self.service_name][_OPERATION_MODE] = AGRI_STATS
            self.config['statistics_shape'] = 'NUTS-0'
            self.config['reporting_shape'] = 'NUTS-0'
            self.config['selected_regions'] = ['EU27']

        self.lut_crops = None
        self.make_output_filenames()

    def _start(self):

        # preprocess statistics input table
        production, production_area, crops = self.preprocess_yield_statistics()

        # filter on crops to account for operation mode: MFA, APRO, GEO
        # subset crop statistics to avoid double counting, remove if not in LUT
        crops_removal = crops.difference(self.lut_crops.index)
        logger.warning('Crops removed from statistics as not present in lookup table {}'.format(crops_removal.to_list()))
        crops = crops.drop(crops_removal)
        if self.config[_CROPPROVISION][_OPERATION_MODE] == AGRI_STATS:
            # crops that require conversion from area to yield
            crops_area = self.lut_crops[self.lut_crops.code_convert == 1].index
        if self.config[_CROPPROVISION][_OPERATION_MODE] == MAT_FLOW:
            crops_removals = crops.difference(self.lut_crops.index)
        if self.config.get('jrcmethod'):
            crops = self.lut_crops.dropna(subset=['code_CAPRI']).index

        nyears = len(self.years)
        # loop over all processing years
        for year in self.years:
            logger.debug('year: %d', year)
            self.year = year

            # create empty frame for results
            ecosystems = self.lut_crops.ecosystem_type_L1.unique().to_list()

            l3 = np.tile(ecosystems, len(mfa_codes) * len(self.statistics_shape.index.to_list()))
            l2 = np.tile(np.repeat(list(mfa_codes), len(ecosystems)), len(self.statistics_shape.index.to_list()))
            l1 = np.repeat(self.statistics_shape.index.to_list(),len(ecosystems)*len(mfa_codes))
            self.result_tonne = pd.DataFrame(index=pd.MultiIndex.from_arrays([l1,l2,l3], \
                  names=['NUTS_ID', CODE_MFA, ECOTYPE_L1]), columns=crops)
            self.result_eur = pd.DataFrame(index=pd.MultiIndex.from_arrays([l1, l2, l3], \
                  names=['NUTS_ID', CODE_MFA, ECOTYPE_L1]), columns=crops)

            # extract production statistics per crop and map to MFA
            self.crop_statistics(production, production_area, crops)

            # map MFA production statistics to ecosystem types, prepare for reporting
            self.supply_tonne = self.reporting_to_ET()

            # Disaggregate statistics using provided proxy/proxies or default via ecosystem extent map
            # get links to proxy/proxies, if available
            proxy_raster_crop_type = None
            proxy_raster_crop_yield = None
            proxy_raster_productivity = None
            if self.config[_CROPPROVISION][_PROXY_CROPYIELD][self.year]:
                proxy_raster_crop_yield = self.config[self.service_name][_PROXY_CROPYIELD][self.year]
            if self.config[_CROPPROVISION][_PROXY_CROPTYPE][self.year]:
                proxy_raster_crop_type = self.config[self.service_name][_PROXY_CROPTYPE][self.year]
            if self.config[_CROPPROVISION][_PROXY_PRODUCTIVITY][self.year]:
                proxy_raster_productivity = self.config[self.service_name][_PROXY_PRODUCTIVITY][self.year]
            # generate map based on statistics
            self.crop_provision_map_generation(proxy_raster_crop_type, proxy_raster_crop_yield, proxy_raster_productivity,
                                               add_progress=lambda p: self.add_progress(0.5 * p / nyears))
            self.cut_to_reporting_regions(self.flow_map_physical[year])

            # Statistics extraction for reporting regions
            self.reporting_area_statistics(add_progress=lambda p: self.add_progress(0.5 * p / nyears))

            # Export of reporting area statistics to SUT Exel file and CSV statistic files
            self.reporting()

        return

    def make_output_filenames(self):
        """Create standard output file names."""
        crop_provision = _CROPPROVISION.lower().replace(' ', '-')  # lower case, without spaces
        self.flow_map_physical = {year: os.path.join(self.maps,
                                                     f'{crop_provision}_map_use_tonne_{year}.tif')
                                  for year in self.years}
        self.flow_map_monetary_real = {year: os.path.join(self.maps,
                                                          f'{crop_provision}_map_use_EURO-real_{year}.tif')
                                       for year in self.years}
        self.flow_map_monetary_nominal = {year: os.path.join(self.maps,
                                                             f'{crop_provision}_map_use_EURO-current_{year}.tif')
                                          for year in self.years}
        self.use_physical = {year: os.path.join(self.statistic_dir, f'{crop_provision}_table_use_1000-tonne_{year}.csv')
                             for year in self.years}
        self.use_monetary = {year: os.path.join(self.statistic_dir,
                                                f'{crop_provision}_table_use_M-EURO-real_{year}.csv')
                             for year in self.years}
        self.use_monetary_nominal = {year: os.path.join(self.statistic_dir,
                                                        f'{crop_provision}_table_use_M-EURO-current_{year}.csv')
                                     for year in self.years}
        self.supply_physical = {year: os.path.join(self.statistic_dir,
                                                   f'{crop_provision}_table_supply_1000-tonne_{year}.csv')
                                for year in self.years}
        self.supply_monetary = {year: os.path.join(self.statistic_dir,
                                                   f'{crop_provision}_table_supply_M-EURO-real_{year}.csv')
                                for year in self.years}
        self.supply_monetary_nominal = {year: os.path.join(self.statistic_dir,
                                                           f'{crop_provision}_table_supply_M-EURO-current_{year}.csv')
                                        for year in self.years}
        self.sut_physical = {year: os.path.join(self.suts,
                                                f'{crop_provision}_report_SUT-physical_1000-tonne_{year}.xlsx')
                             for year in self.years}
        self.sut_monetary_real = {year: os.path.join(self.suts,
                                                     f'{crop_provision}_report_SUT-monetary_M-EURO-real_{year}.xlsx')
                                  for year in self.years}
        self.sut_monetary_nominal = {year: os.path.join(
            self.suts, f'{crop_provision}_report_SUT-monetary_M-EURO-current_{year}.xlsx')
            for year in self.years}

    def preprocess_yield_statistics(self):
        """Retrieve crop statistics and map to supply table format (MFA based)."""
        # read crop production:
        production = estat.read_tsv(self.config[self.service_name][_CROP_YIELD])

        # Check _CROP_YIELD has the right format:
        # For MAT_FLOW, crop yield table should have the env_ac_mfa.tsv format, for AGRI_STATS, we should have the
        # apro_cpsh1.tsv format.
        if self.config[_CROPPROVISION][_OPERATION_MODE] == MAT_FLOW:
            self.code = CODE_MFA
            expected_index_levels = ('indic_env', 'material', 'unit')
            standard_file = 'env_ac_mfa.tsv'
        elif self.config[_CROPPROVISION][_OPERATION_MODE] == AGRI_STATS:
            self.code = 'code_apro'
            expected_index_levels = ('crops', 'strucpro')
            standard_file = 'apro_cpsh1.tsv'
        elif self.config[_CROPPROVISION][_OPERATION_MODE] in [GEOSPATIAL]:
            raise ConfigError('operation-mode geospatial is not yet supported', [_CROPPROVISION, _OPERATION_MODE])
        else:
            raise ConfigError(f'operation-mode was set to "{self.config[_CROPPROVISION][_OPERATION_MODE]}", '
                              f'should be set to "{MAT_FLOW}" or "{AGRI_STATS}".',
                              [_CROPPROVISION, _OPERATION_MODE])
        # Check index has the expected levels
        if any(level not in production.index.names for level in expected_index_levels):
            raise ConfigError(f'Crop yield file "{self.config[self.service_name][_CROP_YIELD]}" does not have the '
                              f'correct format.  For operation mode {self.config[self.service_name][_OPERATION_MODE]}, '
                              f'the file should have the same structure as Eurostat standard dataset {standard_file}.',
                              [self.service_name, _CROP_YIELD])

        # read lookup crop mapping table from MFA to (optional) APRO and or GEO
        # If no file provided by user, use default LUT:
        ref = importlib_resources.files(inca.data).joinpath('cropprovision_lut.csv')
        file = self.config[self.service_name][_LUT_CROPS] if self.config[self.service_name][_LUT_CROPS] else ref
        with open(file, 'r') as f:
            lut_crops = pd.read_csv(f, comment='#', dtype={'code_val': str, ECOTYPE_L1: ecotype_l1_cat})

        if self.code not in lut_crops:
            raise ConfigError(f'Missing column in file "{self.config[self.service_name][_LUT_CROPS]}".  For method '
                              f'"{_OPERATION_MODE}" method, the crop lookup table must have a column "{self.code}".',
                              [_CROPPROVISION, _LUT_CROPS])
        self.lut_crops = lut_crops.set_index(self.code)

        # clean production statistics table
        production_area = None
        assert (self.code in ('code_apro', CODE_MFA)), \
            'Crop provision service supports only material flows "env_ac_mfa" or agriculture statistics "apro_cpsh1"'
        if self.code == 'code_apro':
            # extra for area statistics, to be converted to yield
            production_area = production.xs(EU_AREA, level='strucpro')
            production = production.xs(EU_STD_HUMIDITY, level='strucpro')
        elif self.code == CODE_MFA:
            # cross-sect dataframe Domestic Extraction, keep croptype and geo
            production = production.xs((_DOMESTIC_EXTRACTION, _THOUSAND_TONNE), level=(_INDIC_ENV, _UNIT))
            self.lut_crops[CODE_MFA] = self.lut_crops.index

        # read unit values at basic prices:
        if self.monetary:
            self.bEurTonnes = True  # prices expressed in euro per tonnes
            unit = EUR_TONNE
            self.unit_value = estat.read_tsv(self.config[self.service_name][AACT_UV])
            if EUR_TOTAL in self.unit_value.index.get_level_values(level='unit').to_list():
                unit = EUR_TOTAL
                self.bEurTonnes = False
            self.unit_value = self.unit_value.xs(unit, level='unit')
            if not self.bEurTonnes:
                # select Production value at basic price
                self.unit_value = self.unit_value.reindex(['PROD_BP'], level=1, fill_value=np.nan)
                self.unit_value = self.unit_value.droplevel(1)
            # Manually replace 0. by pd.NA, as method .replace(0., pd.NA) throws RecursionError on some pandas versions.
            for col in self.unit_value.columns:
                self.unit_value.loc[self.unit_value[col] == 0., col] = pd.NA

        # subset based on reporting areas
        production = production.reindex(self.statistics_shape.index, level=1, fill_value=np.nan)
        crops = production.index.unique(level=0)  # apro: level 0 == 'crops', mfa: level 0 == 'material'
        if not production_area is None:
            production_area = production_area.reindex(self.statistics_shape.index, level=1, fill_value=np.nan)
            # add area-based crops in crops list, else statistics will be missed
            for missing_crop in self.lut_crops[self.lut_crops.code_convert == 1].index.to_list():
                if not missing_crop in crops:
                    logger.warning('Add area crop {}'.format(missing_crop))
                    crops = crops.append(pd.Index([missing_crop]))  #area based values are missing in production, so add to index
        if self.monetary:
            self.unit_value = self.unit_value.reindex(self.statistics_shape.index, level=1, fill_value=np.nan)

        return production, production_area, crops

    def crop_statistics(self, production, production_area, crops):
        """Map crop statistics to supply table format (MFA based)."""
        # read ecological contribution (only in KIP-INCA JRC-method mode)
        logger.info('Extracting crop statistics to accounting table')
        if self.config.get('jrcmethod'):
            ecocon = pd.read_csv(self.config[self.service_name][ECOCON], comment='#').set_index(NUTS_ID)
            crops = crops.index
            logger.debug('ecocon:\n%s', ecocon)

        # extract statistics per crop
        crops_assigned = []
        euro_assigned = []  #keep track which monetary values were already accounted for, avoid double counting
        bSkipResiduals_euro = False
        for crop in crops:
            logger.debug('extract statistics for crop %s', crop)
            #if crop == 'J0000':
            #    a=1

            if (crop not in self.lut_crops.index) or (np.array(pd.isnull(self.lut_crops.loc[crop].code_mfa)).any()):
                logger.warning('Skipping crop %s: not in lookup table or not mapped to Material-flow', crop)
                continue

            if self.monetary and \
                    (not self.config[_CROPPROVISION][_OPERATION_MODE] == MAT_FLOW) and \
                    (not pd.isna(self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val)) and \
                    (str(self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val).zfill(5) in self.unit_value.index.get_level_values(0).to_list()):
                price = mean_price(self.unit_value.loc[self.lut_crops.loc[crop].code_val.zfill(5)], self.year,
                                   self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].label_val)  # [eur/tonne]
            elif self.monetary and \
                    self.config[_CROPPROVISION][_OPERATION_MODE] == MAT_FLOW and \
                    self.bEurTonnes is False:
                price = 0
                # get pricing from AGRO mapping table, matching crop types in LUT
                if type(self.lut_crops.loc[crop].code_val) is str: vals = [self.lut_crops.loc[crop].code_val]
                elif type(self.lut_crops.loc[crop].code_val) is float:
                    logger.warning('No monetary value found for {}'.format(crop))
                    price =np.nan
                    vals = []
                else: vals = self.lut_crops.loc[crop].code_val.unique()
                for val in vals:
                    if type(val) is float:continue
                    val = val.zfill(5)
                    if not val in euro_assigned:
                        logger.debug('Add valuation for {}'.format(val))
                        price += mean_price(self.unit_value.loc[val], self.year,
                                       self.lut_crops.loc[crop].label_val)
                        euro_assigned.append(val)
            else:
                price = np.nan
                if self.monetary:
                    # MFA with tonnes/euro is not yet supported
                    logger.warning('Monetary valuation for MFA statistics with pricing in euro/tones not supported')

            # convert statistics from area to yield for specific crop types
            if not self.code == CODE_MFA and self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_convert == _AREA_TO_YIELD:
                logger.info('Convert area to yield for crop {}'.format(crop))
                crop_area = production_area.loc[crop, self.year]
                crop_tonne = crop_area * self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].yield_factor
                logger.info('Changed area {} to yield {} for crop {} using factor {} tons/ha'. \
                            format(crop_area,crop_tonne,crop,self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].yield_factor))
            # KIP-INCA JRCmethod takes average over 3 years, and uses ecosystem contribution 'emergy' approach
            elif self.config.get('jrcmethod'):
                tmp = pd.DataFrame(index=estat.eu_members(2020))
                # calculate mean over 3 years
                for y in (self.year - 1, self.year, self.year + 1):
                    if y in production.columns:
                        tmp[y] = production.loc[crop, y]
                    else:
                        logger.warning('No production value for crop %s in year %d',
                                       self.lut_crops.loc[crop].code_prod, y)
                prod = tmp.mean(axis=1)  # [1000 tonnes]
                crop_tonne = prod * ecocon[self.lut_crops.loc[crop].code_CAPRI]
            else:
                crop_tonne = production.loc[crop, self.year]  # in thousand tonnes
                if not self.code == CODE_MFA and self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_convert == _HUMIDITY_FACTOR: #humidity conversion for pastures
                    crop_tonne = crop_tonne * self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].yield_factor

            # express monetary valuation in millions euro
            if self.monetary and self.bEurTonnes:
                crop_eur = crop_tonne * price / 1000
            elif self.monetary and type(price) is int:
                #create a dataframe if no code_val
                crop_eur = crop_tonne * 0.0
            else:
                crop_eur = price  #expressed in Million euro

            # append results to dataframes (one for physical and one for monetary)
            crops_assigned.append(crop)
            mfa_name = self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_mfa   # use code, transform later to label
            et_code = self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].ecosystem_type_L1
            if not isinstance(mfa_name, str):
                mfa_name = mfa_name.unique()[0]
            if isinstance(et_code, pd.core.series.Series):
                if len(et_code.unique().to_list()) > 1:
                    logger.warning('Multiple ET codes found for crop {}: {}'.format(mfa_name, et_code.unique().to_list()))
                et_code =et_code.unique()[0]

            # subtract crops under glass or high accessible cover to avoid double counting
            if not self.code == CODE_MFA and not (crop.endswith('S')):
                if crop == 'V2000': crop_glass = 'V2300S'  #lettuce under glass
                else: crop_glass = crop+'S'
                if crop_glass in production.index.get_level_values(0).to_list():
                    crop_tonne = (production.loc[crop, self.year].replace(np.nan, 0.0) -
                     production.loc[crop_glass, self.year].replace(np.nan, 0.0))
                    if crop_tonne.lt(0).any().any():
                        logger.warning('Negative yield after subtract from "crops under glass or high accessible cover" \
                            {} for region {}'.format(crop, crop_tonne.index[crop_tonne<0]))
                        crop_tonne[crop_tonne < 0] = 0.0

            # apply recover rate factor for crop residues, according to MFA.1.2.1 Table 6
            crop_tonne_residue = crop_tonne * np.nan
            if not self.code == CODE_MFA and self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_convert == _RESIDUES_FACTOR:
                logger.info('Calculate used crop residues from crop yield for crop {}'.format(crop))
                crop_tonne_residue = crop_tonne * self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].yield_factor * 0.15 #15% wet to air dry biomass
                crop_eur_residue = crop_tonne_residue * price
                if self.monetary and self.bEurTonnes: crop_eur_residue = crop_eur_residue / 1000
                mfa_name_residue = 'MF1211'
                if crop == 'R2000': mfa_name_residue = 'MF1212'

            if not self.code == CODE_MFA and self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_convert == _LIVESTOCK_FACTOR:
                logger.info('Calcuate grazing biomass based on livestock and annual feed')
                # TODO: not supported in this version

            if crop in self.result_tonne.columns:
                for geo in crop_tonne.index.to_list():
                    # can not add to Nan, so push to zero
                    if np.isnan(crop_tonne[geo]): continue
                    if np.isnan(self.result_tonne.loc[(geo,mfa_name,et_code),crop]): self.result_tonne.loc[(geo,mfa_name,et_code),crop]=0
                    if np.isnan(self.result_eur.loc[(geo, mfa_name, et_code), crop]): self.result_eur.loc[(geo, mfa_name, et_code), crop] = 0
                    self.result_tonne.loc[(geo,mfa_name,et_code),crop] += crop_tonne[geo]
                    bSkipResiduals = False
                    if self.monetary and (not self.code == CODE_MFA) and \
                            (not self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val != self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val) and \
                            (not self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val=='') \
                            and (not self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val.zfill(5) in euro_assigned):
                        self.result_eur.loc[(geo,mfa_name,et_code), crop] += crop_eur[geo]
                        euro_assigned.append(self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val.zfill(5))
                    elif self.monetary and self.code == CODE_MFA and not type(crop_eur) is float and geo in crop_eur.index:
                        self.result_eur.loc[(geo,mfa_name,et_code), crop] += crop_eur[geo]
                        bSkipResiduals_euro = True
                    else:
                        bSkipResiduals_euro = True
                    # add crop residues for APRO mode
                    if np.isnan(crop_tonne_residue[geo]) or self.code == CODE_MFA: continue
                    if np.isnan(self.result_tonne.loc[(geo,mfa_name_residue,et_code),crop]): self.result_tonne.loc[(geo,mfa_name_residue,et_code),crop]=0
                    if np.isnan(self.result_eur.loc[(geo, mfa_name_residue, et_code), crop]): self.result_eur.loc[(geo, mfa_name_residue, et_code), crop] = 0
                    self.result_tonne.loc[(geo, mfa_name_residue, et_code), crop] += crop_tonne_residue[geo]
                    if not bSkipResiduals_euro:
                        self.result_eur.loc[(geo, mfa_name_residue, et_code), crop] += crop_eur_residue[geo]
            else:
                logger.error('!!!! Non-initialized crop {}, check lookup table'.format(crop))
                for geo in crop_tonne.index.to_list():
                    self.result_tonne.loc[(geo,mfa_name,et_code),crop] = crop_tonne[geo]
                    if not self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].code_val != self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val \
                            and not self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val=='' and \
                            not self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val.zfill(5) in euro_assigned:
                        self.result_eur.loc[(geo,mfa_name,et_code), crop] = crop_eur[geo]
                        euro_assigned.append(self.lut_crops[~self.lut_crops.index.duplicated(keep='first')].loc[crop].code_val.zfill(5))

        # "real" value in terms of 2000 inflation index
        if self.monetary:
            self.result_eur_real = self.result_eur.multiply(self.deflator[2000] / self.deflator[self.year],
                                                            axis='index')
            logger.debug('deflator values:\n%s', self.deflator[2000] / self.deflator[self.year])

        logger.info('Crops included in MFA table {}'.format(crops_assigned))
        if self.monetary:
            logger.debug('result_eur:\n%s', self.result_eur.groupby(level=0).sum().sum(axis=1))
        logger.debug('result_tonne:\n%s', self.result_tonne.groupby(level=0).sum().sum(axis=1))

        return

    def crop_provision_map_generation(self, proxy_raster_file_type, proxy_raster_file_yield, proxy_raster_productivity, add_progress):
        """Generate the crop provision maps for the statistical regions.

        :param proxy_raster_file_type: Crop type or Ecosystem type raster file
        :param proxy_raster_file_yield: Crop yield raster file
        :param proxy_raster_productivity: Productivity proxy (e.g. GPP or DMP) raster file
        :return: none (raster output in self.raster_statistics raster file)
        """
        logger.info('start generation of crop provision map for year %s', self.year)

        # Create dataframe indexed by NUTS_ID and ecosystem type:
        df_data = self.supply_tonne.droplevel(CODE_MFA)
        df_data = df_data.groupby([NUTS_ID, ECOTYPE_L1]).sum()
        # Make sure index contains all ecotypes, and rename to ECOTYPE instead of ECOTYPE_L1:
        idx = pd.MultiIndex.from_product([df_data.index.unique(NUTS_ID), types_l1[ECOTYPE]])
        df_data = df_data.reindex(idx, fill_value=0.)

        if not proxy_raster_file_yield:
            #no crop yield geospatial information available
            progress_weight_physical = 0.4 if self.monetary else 1.
            if proxy_raster_file_type is not None:
                logger.info('geospatial distribution through crop type map.')
                # get back crop types as needed for spatial disaggregation (if agriculture statistics, else mapped to ET)
                df_data = self.result_tonne.copy()
                #replace croplabels with croptype raster values
                lut = self.lut_crops[[CROPTYPE,ECOTYPE_L1]].copy()
                lut = lut[lut.index.notna()]
                # doing direct replacement gave maximum recursion depth error TODO
                #lut[CROPTYPE].fillna(lut[ECOTYPE_L1], inplace=True)
                lut = lut.reset_index()
                for index, row in lut.iterrows():
                    if np.isnan(row[CROPTYPE]):
                        lut.loc[index,[CROPTYPE]] = int(row[ECOTYPE_L1])
                    else:
                        lut.loc[index, [CROPTYPE]] = int(row[CROPTYPE])
                if self.config[_CROPPROVISION][_OPERATION_MODE] == AGRI_STATS: lut = lut.set_index([CODE_APRO])
                else: lut = lut.set_index([CODE_MFA])
                #check duplicates in croptype codes mapping to statistics (esp. using MFA statistics) : {MFcode=[{'code_croptype':value1},{'code_croptype':value2}],MFcode2=[...]}
                lut_duplicates_d = lut[lut.index.duplicated(keep=False)].drop(ECOTYPE_L1, axis=1)
                lut_duplicates_d = lut_duplicates_d.groupby(level=0).apply(lambda x: x.to_dict('records')).to_dict()
                lut_duplicates = dict()
                for k,v in lut_duplicates_d.items():
                    for dic in v:
                        lut_duplicates[int(dic[CROPTYPE])] = k
                #group all croplabels missing in croptype raster
                lut_mapping = dict(zip(lut.index, lut[CROPTYPE].astype(np.uint)))
                lut_missing = [x for x in df_data.columns.to_list() if x not in lut.index.to_list()]
                for crop in lut_missing:
                    if isinstance(self.lut_crops.loc[crop],pd.Series):
                        lut_mapping[crop] = self.lut_crops.loc[crop].ecosystem_type_L1
                    else:
                        lut_mapping[crop] = self.lut_crops.loc[crop].drop_duplicates().ecosystem_type_L1.values[0]
                #set replacement values for raster duplicates
                for k,v in lut_duplicates.items():
                    lut_duplicates[k] = lut_mapping[v]
                #convert lut_mapping to string
                new_lut_mapping = dict()
                #check if mapping table is correct (no nans left)
                if any(np.isnan(val) for val in lut_mapping.values()):
                    logger.exception('Some crop types not mapped to an ecosystem type, correct lookup table configuration file')
                for i in list(lut_mapping.keys()):
                    new_lut_mapping[i] = str(lut_mapping[i])

                # prepare dataframe for distribution of statistics
                df_data.columns.name = 'crops'
                df_data = df_data.stack(dropna=False).to_frame(name='physical').rename(index=new_lut_mapping)
                df_data = df_data.groupby(['NUTS_ID', 'crops'], dropna=False).sum()
                df_data.index.set_names(['NUTS_ID', ECOTYPE], inplace=True)

                # detail ecosystem type map with crop type information
                proxy_raster_file_type_ET = self.imprint_croptype_map(proxy_raster_file_type, df_data, lut, lut_duplicates,
                                    add_progress=lambda p: add_progress(progress_weight_physical * 0.5 * p))

                if not proxy_raster_productivity:
                    logger.info('no productivity proxy provided, distribute yield equally per crop type.')
                    proxy = proxy_raster_file_type_ET
                    processing_info = f'Crop provision use map for year {self.year} using the crop type map \
                    as proxy, missing crops are filled by ecosystem type'
                else:
                    logger.info('productivity proxy provided, distribute yield per crop type with weight from productivity.')
                    proxy = proxy_raster_productivity
                    processing_info = f'Crop provision use map for year {self.year} using the crop type and \
                    productivity (GPP) maps as proxy, missing crops are filled by ecosystem type'

                # finally we can create the geospatial map
                self.accord.spatial_disaggregation_byArea_byET(
                        proxy, (df_data['physical']*1000).astype(int),
                        self.statistics_raster, self.statistics_shape[SHAPE_ID],
                        proxy_raster_file_type_ET, dict(zip(map(str,df_data.index.levels[1].to_list()),\
                                                            [eval(i) for i in df_data.index.levels[1].to_list()])),
                        self.flow_map_physical[self.year],
                        add_progress=lambda p: add_progress(progress_weight_physical * p),
                        unit_info='tonne/ha', processing_info=processing_info)

                if self.monetary:
                    logger.info('  * spatial disaggregation of monetary valuation (experimental)')
                    # prepare dataframe for distribution of statistics for nominal pricing
                    df_data = self.result_eur.copy()
                    df_data.columns.name = 'crops'
                    df_data = df_data.stack(dropna=False).to_frame(name='monetary').rename(index=new_lut_mapping)
                    df_data = df_data.groupby(['NUTS_ID', 'crops'],dropna=False).sum()
                    df_data.index.set_names(['NUTS_ID', ECOTYPE], inplace=True)

                    self.accord.spatial_disaggregation_byArea_byET(
                            proxy, (df_data['monetary']*1000000).astype(int),
                            self.statistics_raster, self.statistics_shape[SHAPE_ID],
                            proxy_raster_file_type_ET, dict(zip(map(str, df_data.index.levels[1].to_list()), \
                                                                [eval(i) for i in df_data.index.levels[1].to_list()])),
                            self.flow_map_monetary_nominal[self.year],
                            add_progress=lambda p: add_progress(progress_weight_physical * p),
                            unit_info='EUR/ha', processing_info=processing_info)

                    # prepare dataframe for distribution of statistics of real pricing
                    df_data = self.result_eur_real.copy()
                    df_data.columns.name = 'crops'
                    df_data = df_data.stack(dropna=False).to_frame(name='monetary_real').rename(index=new_lut_mapping)
                    df_data = df_data.groupby(['NUTS_ID', 'crops'], dropna=False).sum()
                    df_data.index.set_names(['NUTS_ID', ECOTYPE], inplace=True)

                    self.accord.spatial_disaggregation_byArea_byET(
                            proxy, (df_data['monetary_real']*1000000).astype(int),
                            self.statistics_raster, self.statistics_shape[SHAPE_ID],
                            proxy_raster_file_type_ET, dict(zip(map(str, df_data.index.levels[1].to_list()), \
                                                                [eval(i) for i in df_data.index.levels[1].to_list()])),
                            self.flow_map_monetary_real[self.year],
                            add_progress=lambda p: add_progress(progress_weight_physical * p),
                            unit_info='EUR/ha (Reference year 2000 value)',
                            processing_info=processing_info)

            else:
                logger.warning('no proxy map provided for crop-types and/or crop-yield, '
                               'use default ecosystem extent map.')
                # disaggregate by ecosystem type map

                if not proxy_raster_productivity:
                    logger.info('no productivity proxy provided, distribute yield equally per crop type.')
                    proxy = self.get_ecosystem_raster(self.year, level=1)
                    processing_info = f'Crop provision use map for year {self.year} using the ecosystem extent map as proxy'
                else:
                    logger.info('productivity proxy provided, distribute yield per crop type with weight from productivity.')
                    proxy = proxy_raster_productivity
                    processing_info = f'Crop provision use map for year {self.year} using the ecosystem extent and \
                    productivity (GPP) maps as proxy.'

                self.accord.spatial_disaggregation_byArea_byET(
                        proxy, (df_data['physical']*1000).astype(int),
                        self.statistics_raster, self.statistics_shape[SHAPE_ID].to_dict(),
                        self.get_ecosystem_raster(self.year, level=1), types_l1.set_index(ECOTYPE)[ECO_ID].to_dict(),
                        self.flow_map_physical[self.year],
                        add_progress=add_progress, unit_info='tonnes/ha',
                        processing_info=processing_info)
                if self.monetary:
                    logger.info('  * spatial disaggregation of monetary valuation (experimental)')
                    self.accord.spatial_disaggregation_byArea_byET(
                            proxy, (df_data['monetary']*1000000).astype(int),
                            self.statistics_raster, self.statistics_shape[SHAPE_ID].to_dict(),
                            self.get_ecosystem_raster(self.year, level=1),
                            types_l1.set_index(ECOTYPE)[ECO_ID].to_dict(),
                            self.flow_map_monetary_nominal[self.year],
                            add_progress=add_progress, unit_info='EUR/ha',
                            processing_info=processing_info)
                    self.accord.spatial_disaggregation_byArea_byET(
                            proxy, (df_data['monetary_real']*1000000).astype(int),
                            self.statistics_raster, self.statistics_shape[SHAPE_ID].to_dict(),
                            self.get_ecosystem_raster(self.year, level=1),
                            types_l1.set_index(ECOTYPE)[ECO_ID].to_dict(),
                            self.flow_map_monetary_real[self.year],
                            add_progress=add_progress, unit_info='EUR/ha (Reference year 2000 value)',
                            processing_info=processing_info)

        else:  # have proxy_raster_file_yield:
            logger.warning('geospatial distribution through yield raster map, needs also crop type map')

            # If we have monetary evaluation, ~60% of time is spent on that, and 40% on physical part.  Without monetary
            # evaluation, 100% of time is spent on the physical part.
            progress_weight_physical = 0.4 if self.monetary else 1.
            proxy_sums = self.accord.spatial_disaggregation_byArea_byET(
                proxy_raster_file_yield, (df_data['physical']*1000).astype(int),
                self.statistics_raster, self.statistics_shape[SHAPE_ID],
                self.get_ecosystem_raster(self.year, level=1), types_l1.set_index(ECOTYPE)[ECO_ID],
                self.flow_map_physical[self.year],
                add_progress=lambda p: add_progress(progress_weight_physical * p),
                unit_info='tonne/ha', processing_info='Spatial disaggregation of use through yield map.')

            if self.monetary:
                logger.info('  * spatial disaggregation of monetary valuation (experimental)')
                self.accord.spatial_disaggregation_byArea_byET(
                    proxy_raster_file_yield, (df_data['monetary']*1000000).astype(int),
                    self.statistics_raster, self.statistics_shape[SHAPE_ID],
                    self.get_ecosystem_raster(self.year, level=1), types_l1.set_index(ECOTYPE)[ECO_ID],
                    self.flow_map_monetary_nominal[self.year],
                    add_progress=lambda p: self.add_progress(0.3 * p),
                    proxy_sums=proxy_sums,
                    unit_info='EUR/ha', processing_info='Spatial disaggregation of use through yield ma.')
                self.accord.spatial_disaggregation_byArea_byET(
                    proxy_raster_file_yield, (df_data['monetary_real']*1000000).astype(int),
                    self.statistics_raster, self.statistics_shape[SHAPE_ID],
                    self.get_ecosystem_raster(self.year, level=1), types_l1.set_index(ECOTYPE)[ECO_ID],
                    self.flow_map_monetary_real[self.year],
                    add_progress=lambda p: self.add_progress(0.3 * p),
                    proxy_sums=proxy_sums,
                    unit_info='EUR/ha (Reference year 2000 value)',
                    processing_info='Spatial disaggregation of use through yield ma.')
        return

    def imprint_croptype_map(self, proxy_raster_file_type, df_data, lut, lut_duplicates, add_progress, BLOCKSIZE=(2048,2048)):
        """Detail ecosystem type map with crop type map, through imprinting.

        :param proxy_raster_filt_type: file (name and location) of crop type raster
        :param df_data: pandas dataframe with multiindex (NUTS_ID, Digital Number in raster) and
               column (physical harvest in 1000 tonnes), where raster value represents either ecosytem_type or crop_type
        :param lut: pandas dataframe with index croptype name (code_apro) and columns crop digital number (code_croptype)
               and ecosystem type digital number (ecosystem_type_L1)
        :param lut_duplicates: dictionary with key (croptype number to imprint) and values (list with croptype numbers
               from input crop type map to be replaced by key)
        :return: file (name and location) of ecosystem extent map with crop types
        """
        logger.info("Imprint crop types from {} into ecosystem type L1 map".format(proxy_raster_file_type))
        progress_remain = 1.
        with rasterio.open(self.get_ecosystem_raster(self.year, level=1), 'r') as src_et, \
                rasterio.open(proxy_raster_file_type, 'r') as src_crop:
            dst_profile = src_crop.profile
            dst_profile.update(dtype=rasterio.uint16, nodata=0)
            # number of to process blocks
            nblocks = number_blocks(dst_profile, BLOCKSIZE)

            # init the output raster file
            proxy_raster_file_type_ET = os.path.join(self.temp_dir,
                                os.path.splitext(os.path.split(proxy_raster_file_type)[1])[0] + '_ET.tif')
            with rasterio.open(proxy_raster_file_type_ET, 'w', **dst_profile) as dst:
                # add metadata
                self.accord.metadata.read_raster_tags([proxy_raster_file_type, self.get_ecosystem_raster(self.year, level=1)])

                # count some statistics for logging
                countRemovals = 0
                countAdditions = 0
                countTotal = 0
                # now iterate over the blocks
                for _, window in block_window_generator(BLOCKSIZE, dst_profile['height'], dst_profile['width']):
                    # read data
                    aEcosystems = src_et.read(1, window=window, masked=True)
                    aCrops = src_crop.read(1, window=window, masked=True)

                    countTotal += aEcosystems[aEcosystems <= lut.ecosystem_type_L1.astype(np.uint16).unique().max()].count()

                    aData = np.zeros(aCrops.shape, dtype=np.uint16)
                    for crop in [int(i) for i in df_data.index.levels[1].to_list()]:
                        aMask = (aCrops == crop)
                        # overwrite if duplicated raster values for any statistic
                        if crop in lut_duplicates.keys():
                            crop = lut_duplicates[crop]
                        aData[aMask] = crop

                    # remove crop fields if not matching to ecosystem type
                    aMaskET = np.zeros(aCrops.shape, dtype=np.uint16)
                    for ET in self.lut_crops.ecosystem_type_L1.astype(np.uint16).unique():
                        aMask = (aEcosystems == ET)
                        aMaskET[aMask] = ET
                    aMask = ((aData != 0) & (aMaskET == 0))
                    countRemovals += np.count_nonzero(aMask)
                    aData[aMask] = 0

                    # assign missing crop types to ecosystem type
                    aMask = ((aData == 0) & (aMaskET != 0))
                    countAdditions += np.count_nonzero(aMask)
                    aData[aMask] = aMaskET[aMask]

                    # write to disk
                    dst.write(aData, 1, window=window)
                    add_progress(progress_remain / nblocks)

                # set tags
                tags = self.accord.metadata.prepare_raster_tags('Ecosystem type map with imprint crop types '
                        'for year {}. Removed {} crop type pixels, inconsistent with ET map. Missing {} crop type pixels,'
                        'mapped to ET'.format(self.year, countRemovals, countAdditions), 'classes')
                dst.update_tags(**tags)

        logger.warning('{} pixels ({:.1f}%) from crop type map removed, inconsistent with ET map'.format(countRemovals, (
                    countRemovals / countTotal * 100)))
        logger.warning('{} pixels ({:.1f}%) from ET added, missing in crop type map'.format(countAdditions, (
                    countAdditions / countTotal * 100)))

        return proxy_raster_file_type_ET

    def reporting(self):
        """Write statistics csv files and SUT spreadsheets."""
        logger.info('statistics export')
        logger.info('* CSV statistic files')

        # in CSV files we use the shortnames for ecosystem types
        ecotype_short_name = types_l1.set_index(ECOTYPE)['short_name']

        # merge with short names, and keep only physical and short_name columns
        result_tonne_csv = self.supply_tonne.merge(ecotype_short_name,
                             left_on=ECOTYPE_L1, right_index=True).sort_index()[['physical','short_name']]

        # replace MFA codes by longer MFA labels
        result_tonne_csv.rename(index=mfa_labels, inplace=True)
        result_tonne_csv.index.rename('MFA', level=CODE_MFA, inplace=True)

        # replace column labels
        result_tonne_csv.columns = ['supply', 'ecosystem type']
        result_tonne_csv.to_csv(self.supply_physical[self.year])

        if self.monetary:
            self.reporting_eur.to_csv(self.use_monetary[self.year], index_label='NUTS_ID')
            self.reporting_eur_real.to_csv(self.supply_monetary[self.year], index_label='NUTS_ID')

        logger.info(' * Write SUT aggregate xlsx')
        # supply/use tables: all use is intermediate consumption by industries, all supply is from cropland
        # TODO suts is not yet supporting subtotals, will be added later
        suts.write_sut(self.sut_physical[self.year],
                       self.reporting_tonne.unstack()/1000,
                       (self.reporting_tonne.unstack().sum(axis=1)/1000).rename(suts.IND),
                       self.service_name, self.year, '1000 tonnes')
        if self.monetary:
            suts.write_sut(self.sut_monetary_nominal[self.year],
                           self.reporting_eur.unstack()/1000000,
                           (self.reporting_eur.unstack().sum(axis=1)/1000000).rename(suts.IND),
                           self.service_name, self.year, 'Million EUR (2000 value)')
            suts.write_sut(self.sut_monetary_real[self.year],
                           self.reporting_eur_real.unstack()/1000000,
                           (self.reporting_eur_real.unstack().sum(axis=1)/1000000).rename(suts.IND),
                           self.service_name, self.year, 'Million EUR (nominal)')

        # temporary PATCH, write a single multi-crop table per NUTS_ID
        logger.info(' * Write SUT xlsx per NUTS_ID with MFA rows')
        df = result_tonne_csv.drop('ecosystem type', axis=1)
        df = df.unstack(2)
        for NUTS in df.index.get_level_values(0).unique().to_list():
            logger.info('   - SUT Excel with crop types for region {}'.format(NUTS))
            df2 = df.xs(NUTS)
            df2 = df2.droplevel(0, axis=1)
            for idx, row in types_l1.iterrows():
                if not row.ecosystem_id in df2.columns:
                    df2[[row['ecosystem_id']]] = 0
                df2.rename(columns={row['ecosystem_id']:row['name']},inplace=True)

            suts.write_sut(os.path.splitext(self.sut_physical[self.year])[0]+'_'+NUTS+'.xlsx',
                           df2,
                           df2.sum(axis=1).rename(suts.IND),
                           self.service_name, self.year, '1000 tonnes', region=NUTS)

        return

    def reporting_to_ET(self):
        """Map table crops to ecosystem types."""
        # sum all crop-types, only need values per ecosystem type (NUTS_ID, ET) PHYSICAL
        df = self.result_tonne.sum(axis=1).to_frame(name='physical')

        if self.monetary:
            # dataframe with monetary values
            df2 = self.result_eur.sum(axis=1).to_frame(name='monetary')
            # dataframe with monetary real values
            df3 = self.result_eur_real.sum(axis=1).to_frame(name='monetary_real')

            # join into one single dataframe
            df = df.join(df2, on=['NUTS_ID', 'code_mfa', 'ecosystem_type_L1'])
            df = df.join(df3, on=['NUTS_ID', 'code_mfa', 'ecosystem_type_L1'])

        return df

    def reporting_area_statistics(self, add_progress):
        """Extract statistics for the reporting regions."""
        logger.info('extract statistics for reporting regions')

        # If we have monetary evaluation, ~60% of time is spent on that, and 40% on physical part.  Without monetary
        # evaluation, 100% of time is spent on the physical part.
        progress_weight_physical = 0.4 if self.monetary else 1.

        ecosystem_raster = self.get_ecosystem_raster(self.year, level=1,
                                                     add_progress=lambda p: add_progress(
                                                         progress_weight_physical * 0.5 * p))

        self.reporting_tonne = statistics_byArea_byET(self.flow_map_physical[self.year],
                                                      self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                      ecosystem_raster,
                                                      types_l1.set_index('name')[ECO_ID],
                                                      lambda p: add_progress(progress_weight_physical * 0.5 * p))['sum']

        if self.monetary:
            self.reporting_eur = statistics_byArea_byET(self.flow_map_monetary_nominal[self.year],
                                                        self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                        ecosystem_raster,
                                                        types_l1.set_index('name')[ECO_ID],
                                                        lambda p: add_progress(0.3 * p))['sum']
            self.reporting_eur_real = statistics_byArea_byET(self.flow_map_monetary_real[self.year],
                                                             self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                             ecosystem_raster,
                                                             types_l1.set_index('name')[ECO_ID],
                                                             lambda p: add_progress(0.3 * p))['sum']

        return

def set_cmdline_args(parser):
    """Implement additional command line arguments.

    :param parser: subparser object for CropProvision case in the __main__
    :return:
    """
    parser.add_argument('--jrcmethod', action='store_true',
                        help='To run the crop provision in original KIP-INCA JRC method (emergy function)')

def mean_price(unit_value, year, cropname):
    """Calculate the average of the price for (year-1, year, year+1) from the unit_value table for a crop.

    Gap-filling:
    - if no prices available for those years, use the price from the nearest year for the same member state.
    - if no price for that member state at all, use the EU average for that year.
    """
    assert (not (unit_value == 0.).any(axis=None))  # prices of 0. must be replaced by NA values here.
    tmp = pd.DataFrame(index=unit_value.index)
    for y in (year - 1, year, year + 1):
        if y in unit_value.columns:
            tmp[y] = unit_value[y]
        else:
            logger.warning('No data for year %d and crop %s in unit_value table.', y, cropname)
    result = tmp.mean(axis=1)
    eu_average = estat.filter_eu27(result).mean()
    # countries where 3-year average is 0 or missing:
    for member_state in result.index[result.isna()]:
        # get all available non-zero prices for that member state:
        ms_prices = unit_value.loc[member_state].dropna()
        if ms_prices.empty:  # no actual prices for this member state
            result.loc[member_state] = eu_average
            #logger.debug('Use eu average price for %d %s %s: %f', year, member_state, cropname, eu_average)
        else:  # find the price from the nearest year
            idx_nearest_year = np.argmin(np.abs(ms_prices.index - year))
            result.loc[member_state] = ms_prices.iloc[idx_nearest_year]
            #logger.debug('Use price from nearest year (%d) for %d %s %s: %f',
            #             ms_prices.index[idx_nearest_year],
            #             year, member_state, cropname, result.loc[member_state])

    return result
