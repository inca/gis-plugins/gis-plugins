# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.
# inca local_climate_regulation 'C:\Users\DEROOL\OneDrive - VITO\Documents\INCA Tool\Local Climate\localclimate_test2.yaml' --verbose --continue
################## Importing modules ##################
# standard
import logging
import os
from contextlib import ExitStack

import pandas as pd
from pyproj import Transformer
import rasterio
import rasterio.mask
from rasterio.merge import merge
import numpy as np
import statsmodels.api as sm
import fiona
from shapely.geometry import shape
from statsmodels.tools.eval_measures import rmse
from statsmodels.tools.eval_measures import bias
from sklearn.metrics import mean_squared_error

# own modules
import inca
from inca.common.geoprocessing import RasterType, statistics_byArea, statistics_byArea_byET, SUM, COUNT, \
    block_window_generator
from inca.common.config_check import ConfigRaster, ConfigRasterList, ConfigShape, ConfigItem, check_csv, YEARLY
from inca.common.errors import ConfigError
from inca.common.ecosystem import SHAPE_ID, ECOTYPE, ECO_ID, types_l1
from inca.common.suts import write_sut, HSH
from inca.common.fileprocessing import createDataFrame


############## END importing modules ##################
logger = logging.getLogger(__name__)

# Global Parameters
_LCR = 'Local Climate Regulation'
STATION_DATA = 'weather_station_data'
LST = 'land_surface_temp'
EVEG = 'vegetation_evapo'
TREECOVER = 'tree_cover_density'
URBAN_AREAS = 'urban_areas'
START_DATE_SUMMER = 'start_date_summer'
END_DATE_SUMMER = 'end_date_summer'
# BUFFER = 200 dissabled

# field names
nameField = 'LAU_NAME'
numField = 'cat'
countryCode = 'CNTR_CODE'
CITY_ID = 'GISCO_ID'
class LocalClimateRun(inca.Run):
    service_name = 'Local Climate Regulation'

    def __init__(self, config):
        super().__init__(config)

        self.config_template.update({
            self.service_name: {
                # required input formats
                STATION_DATA: ConfigItem(check_function=check_csv),
                LST: {YEARLY: ConfigRasterList(raster_type=RasterType.ABSOLUTE_POINT)},
                EVEG: ConfigRaster(raster_type=RasterType.ABSOLUTE_POINT),  # [mm / day]
                TREECOVER: ConfigRaster(raster_type=RasterType.ABSOLUTE_POINT),
                URBAN_AREAS: ConfigShape()
            }
        })
        # init some important output variables
        self.result_files = {'dataframes': {}, 'map_paths': {}, 'statistic_dir': {}, 'sut_paths': {}}
        self.make_output_filenames()

    def make_output_filenames(self):
        # cooling map
        self.result_files['map_paths'].update(
            cooling={year: os.path.join(self.maps, f'{_LCR.replace(" ", "-")}_cooling_degree_{year}.tif')
                     for year in self.years}),
        self.result_files['statistic_dir'].update(
            cooling={
                year: os.path.join(self.statistic_dir, f'{_LCR.replace(" ", "-")}_LAU_report_cooling_degree_{year}.csv')
                for year in self.years}),
        self.result_files['dataframes'].update(
            cooling={year: None for year in self.years}),
        self.result_files['sut_paths'].update(
            cooling={year: os.path.join(self.suts, f'{_LCR.replace(" ", "-")}_report_cooling_degree_{year}.csv')
                     for year in self.years})
        self.SUT_physical_path = os.path.join(self.suts,
                                              f'{_LCR.replace(" ", "-")}_report_SUT_cooling_degree_''{year}.xlsx')

    def _start(self):

        """start of the service processing."""
        # create missing folder
        os.makedirs(self.additional_results, exist_ok=True)
        # make a new dir for the city rasters
        os.makedirs(os.path.join(self.temp_dir, 'temp_maps'), exist_ok=True)

        logger.debug('Starting Local Climate Regulation run.')
        config = self.config[self.service_name]
        logger.debug('input files:\n'
                     ' - station data: %s\n'
                     ' - surface temperature scenes: %s\n'
                     ' - vegetation evapotranspiration: %s\n'
                     ' - tree cover density: %s\n'
                     ' - urban area vector file: %s',
                     config[STATION_DATA], config[LST], config[EVEG], config[TREECOVER], config[URBAN_AREAS])
        logger.debug('Start preprocessing temperature data.')
        # check on compatibility of start and end date of summer
        self.check_dates_summer()

        # loop over all processing years
        for year in self.years:
            self.year = year
            # check input files
            self.check_urban_areas()
            # self.check_tree_cover_density()
            # Data preprocessing step
            self.summarize_land_surface_temperature()
            # rasterize urban shapefile to be more flexible
            # station data
            dfUS = self.data_preprocessing_weather_station()
            # main geoprocessing
            logger.debug('Start calculations on regression analysis and prediction of land surface and air temperature')
            df = self.calculating_df_for_cooling(dfUS=dfUS)
            #  creating a df from the list
            df = pd.DataFrame(df,
                              columns=['city_name', 'average evapotranspiration',
                                       'average tree cover density', 'average cooling', 'median cooling',
                                       'regress 0 const',
                                       'regress 0 tc', 'regress 0 transp', 'R_squared', 'regress 1 const',
                                       'regress 1 st',
                                       'regress Ycoor'])
            # creating output
            logger.debug('Creating report statistics on urban area level in statistics directory')
            df.to_csv(self.result_files['statistic_dir']['cooling'][self.year])

            logger.debug('Creating raster of cooling by vegetation raster')
            # creating consolidated raster
            self.writing_cooling_raster()
            self.accord.Bring2AOI(os.path.join(self.temp_dir, f'cooling_by_vegetation_overview_{self.year}.tif'),
                                  self.result_files['map_paths']['cooling'][self.year])

            logger.debug('Creating report statistics by area')
            # creating report per area
            report_per_area = statistics_byArea(self.result_files['map_paths']['cooling'][self.year],
                                                self.reporting_raster,
                                                self.reporting_shape[SHAPE_ID])
            report_per_area.to_csv(os.path.join(self.statistic_dir, f'report_area_per_cooling_degree_{year}.csv'))

            logger.debug('Creating report statistics by area and by ecosystem type')
            self.calculate_sut(year=self.year)

    # check function on the lau shapefile
    def check_urban_areas(self):
        with fiona.open(self.config[self.service_name][URBAN_AREAS]) as shp:
            list_features = [feature['properties'][CITY_ID] for feature in shp]
            if len(list_features) > len(set(list_features)):
                raise ConfigError('The urban unit shapefile should contain unique ID and unique names.',
                                  [self.service_name, URBAN_AREAS])  # raise error

    def check_dates_summer(self):
        if START_DATE_SUMMER < END_DATE_SUMMER:
            raise ConfigError('The start date of the chosen period is after the end date.',
                              [self.service_name, START_DATE_SUMMER])  # raise error

    # check tree cover density because it is encoded as 0-100 but in integers and shouldn't be higher than 100
    def check_tree_cover_density(self):
        with rasterio.open(self.config[self.service_name][TREECOVER]) as rst:
            values = rst.read()
            if values.max() > 100:
                raise ConfigError('Tree cover density is in % and should be a value between 0 and 100',
                                  [self.service_name, TREECOVER])

    # function to summarize multiple land surface temperature rasters
    def summarize_land_surface_temperature(self, block_shape=(2048, 2048)):
        """Method to combine (median) land surface temperature rasters to one consolidated"""
        logger.info('Start median surface temperature calculation')
        # initialize loop
        list_lst_rasters = self.config[self.service_name][LST][self.year]
        with rasterio.open(list_lst_rasters[0]) as src:
            profile = src.profile
        # run blockwise processing
        with ExitStack() as stack, \
                rasterio.open(os.path.join(self.temp_dir, f'median_surface_temperature_{self.year}.tif'),
                              'w',
                              **dict(self.accord.ref_profile.copy(), dtype=rasterio.float32, nodata=np.nan,
                                     tiled=True, compress='DEFLATE')) as ds_out:

            lst_dss = [stack.enter_context(rasterio.open(path)) for path in list_lst_rasters]
            for _, window in block_window_generator(block_shape, lst_dss[0].height, lst_dss[0].width):
                lst_data = np.ma.zeros((len(lst_dss), window[0][1] - window[0][0], window[1][1] - window[1][0]))
                raster_values = [src.read(1, window=window) for src in lst_dss]
                median_values = np.nanmedian(raster_values, axis=0)
                median_values = median_values -273.15
            #    for idx, ds_lst in enumerate(lst_dss):
            #        lst_raster_data = ds_lst.read(1, window=window)
            #        stacked_data = np.stack([lst_raster_data])
            #        median_data = np.ma.median(stacked_data, axis=0)
            #        lst_data[idx] = median_data
                # Calculate median along the time axis
            #    lst_median_data = np.ma.median(lst_data, axis=0)
            #    lst_median_data = lst_median_data - 273.15
                # Write the result to the output raster
                ds_out.write(median_values, indexes=1, window=window)

    # function to preprocess the weather data
    def data_preprocessing_weather_station(self):
        dfUS = pd.read_csv(self.config[self.service_name][STATION_DATA], comment='#')
        # remove negative values in the weather station data for max air temperature
        dfUS = dfUS.loc[dfUS.TempMax > 0]
        # check for missing values as well
        dfUS = dfUS.dropna(subset=['TempMax', 'st', 'Ycoor'])
        return dfUS

    # this function extracts the LAU area where to run the regression
    # can be removed out of class into geoprocessing
    def findWindow(self, shapeBound, mainRasterBnd, mainRasterCellSize):
        startRow = int((mainRasterBnd[3] - shapeBound[3]) / mainRasterCellSize)
        endRow = int((shapeBound[3] - shapeBound[1]) / mainRasterCellSize) + 1 + startRow
        startCol = int((shapeBound[0] - mainRasterBnd[0]) / mainRasterCellSize)
        endCol = int((shapeBound[2] - shapeBound[0]) / mainRasterCellSize) + 1 + startCol
        return startRow, endRow, startCol, endCol

    # calculation to measure the effect from vegetation in urban areas on the air temperature
    def calculating_df_for_cooling(self, dfUS):
        # creating an empty list to be filled with the regression values in the for loop
        cooling_results = []
        # create a transformer from epgs 3035 to wgs84
        # needed because the weather station data is in wgs84
        epgs_3035 = 'EPSG:3035'
        epgs_wgs84 = 'EPSG:4326'
        transformer = Transformer.from_crs(epgs_3035, epgs_wgs84, always_xy=True)

        helper = self.statistics_shape.copy()
        helper['dis'] = 666
        helper_dis = helper.dissolve(by='dis')


        # looping over the cities
        logger.info('Start iterating over cities')
        for pol in fiona.open(self.config[self.service_name][URBAN_AREAS]):
            lau_name = (pol['properties'][nameField])
            # lau_country = (pol['properties'][countryCode])
            # lau_num = (pol['properties'][numField])
            lau_id = (pol['properties'][CITY_ID])
            polygon = shape(pol['geometry'])

            # run check that polygon is completetly in the helper_dis -> if not continue
            if not helper_dis['geometry'].iloc[0].contains(polygon):
                continue

            centroid = polygon.centroid
            lon, lat = transformer.transform(centroid.x, centroid.y)
            poly_Ycoor = lat
            # next step to introduce a buffer of 150m(epgs3035)
            poly = [shape(pol['geometry']).buffer(150)]

            with rasterio.open(self.config[self.service_name][TREECOVER]) as rst_tc:
                out_image_rst_tc, out_transform_rst_tc = rasterio.mask.mask(rst_tc, poly, crop=True)
                tc_nodata = (rst_tc.meta.copy())['nodata']
                tc_ar = out_image_rst_tc.astype('float')
                tc_ar = tc_ar.flatten()

            with rasterio.open(os.path.join(self.temp_dir, f'median_surface_temperature_{self.year}.tif')) as rst_lst:
                out_image_rst_lst, out_transform_rst_lst = rasterio.mask.mask(rst_lst, poly, crop=True)
                lst_nodata = (rst_lst.meta.copy())['nodata']
                st = out_image_rst_lst.astype('float')
                st = st.flatten()

            with rasterio.open(self.config[self.service_name][EVEG]) as rst_EVEG:
                out_image_rst_EVEG, out_transform_rst_EVEG = rasterio.mask.mask(rst_EVEG, poly, crop=True, nodata=np.nan)

                eveg_nodata = (rst_EVEG.meta.copy())['nodata']
                transp = out_image_rst_EVEG.astype('float')
                transp = transp.flatten()

            # stacking the arrays into a df

            allArrays = np.dstack((tc_ar, st, transp))
            allArrays = allArrays[0, :, :]

            df = pd.DataFrame(allArrays, columns=['tc', 'st', 'transp'])

            #  create an additional df to keep track of pixel location with an index in order to
            # recreate the maps in the end
            df['num'] = np.arange(len(df))
            df1 = df
            total1 = len(df1)
            # adding extra columns
            df['lau_name'] = lau_name
            df['lau_id'] = lau_id
            df['Ycoor'] = poly_Ycoor

            # filter nodata values
            df = df[(df['st'] != lst_nodata) &
                    (df['tc'] != tc_nodata) &
                    (df['st'] > 0) &
                    (df['transp'] != eveg_nodata)
                    ]

            # drop remaining missing values
            df = df.dropna()

            # set
            if len(df) < 3:
                continue

            total = len(df)

            # first regression to estimate the effect of vegetation on the surface temperature
            logger.info('first regression')
            df = self.regression_lst(df)
            # second regression to estimate the air temperature
            logger.info('second regression')
            df = self.regression_airtemperature(df_regression_second=df, dfUS=dfUS)
            # appending all results together
            logger.info('post processing')
            df = self.cooling_post_processing(df=df)
            #   appending results in the empty list
            cooling_results.append([lau_id, df['transp'].mean(), df['tc'].mean(),
                                    df['cooling'].mean(), df['cooling'].median(), df['regress_0_const'].mean(),
                                    df['regress_0_tc'].mean(), df['regress_0_transp'].mean(), df['R_squared'].mean(),
                                    df['regress_1_const'].mean(),
                                    df['regress_1_st'].mean(), df['regress_1_Ycoor'].mean()])
            logger.info('raster consolidation')
            self.raster_consolidation(lau_id, poly_Ycoor, cooling_results, rst_lst, out_transform_rst_lst,
                                      out_image_rst_lst, out_image_rst_EVEG, out_image_rst_tc)
            logger.info('writing results')
            df.to_csv(os.path.join(self.temp_dir, f'{lau_id}.csv'))

        return cooling_results

    # first regression: estimating LST
    def regression_lst(self, df_regression_first):
        features_st_model = ['tc', 'transp']
        depvar_st_model = df_regression_first['st']
        indepvar_st_model = sm.add_constant(df_regression_first[features_st_model], has_constant='add')
        st_model = sm.OLS(depvar_st_model, indepvar_st_model)
        results_st_model = st_model.fit()
        df_regression_first['regress_0_const'] = results_st_model.params['const']
        df_regression_first['regress_0_tc'] = results_st_model.params['tc']
        df_regression_first['regress_0_transp'] = results_st_model.params['transp']
        df_regression_first['R_squared'] = results_st_model.rsquared

        ## for prediction the following code will be used:
        predict_st = results_st_model.get_prediction(indepvar_st_model)
        predict_st = predict_st.summary_frame(alpha=0.05)
        df_regression_first['estimated_st'] = predict_st['mean']

        # calculating the error
        df_regression_first['error_st'] = (np.abs(predict_st['obs_ci_upper'] - predict_st['obs_ci_lower'])) / 2.0
        # rmse, bias and si
        rmse_st = rmse(df_regression_first['st'], df_regression_first['estimated_st'])
        bias_st = bias(df_regression_first['st'], df_regression_first['estimated_st'])
        si_st = (rmse_st / df_regression_first['st'].mean()) * 100
        # normalized rmse and si
        df_regression_first['obs_st'] = (df_regression_first['st'] - min(df_regression_first['st'])) / (
                max(df_regression_first['st']) - min(df_regression_first['st']))

        df_regression_first['est_st'] = (df_regression_first['estimated_st'] - min(
            df_regression_first['estimated_st'])) / (
                                                max(df_regression_first['estimated_st']) - min(
                                            df_regression_first['estimated_st']))
        #rmse_st_norm = mean_squared_error(df_regression_first['obs_st'], df_regression_first['est_st'], squared=False)
        #si_st_norm = (rmse_st_norm / df_regression_first['obs_st'].mean()) * 100

        # prediction: calculating st in the no vegetation scenario
        df_regression_first['tc0'] = 0
        df_regression_first['transp0'] = 0
        features_st_model_tc0 = ['tc0', 'transp0']
        indepvar_st_model_tc0 = sm.add_constant(df_regression_first[features_st_model_tc0], has_constant='add')
        predict_st_tc0 = results_st_model.get_prediction(indepvar_st_model_tc0)
        predict_st_tc0 = predict_st_tc0.summary_frame(alpha=0.05)

        # create a col that shows the estimated st when tc is 0
        df_regression_first['estimated_st_tc0'] = predict_st_tc0['mean']
        df_regression_first['error_st_TC0'] = (np.abs(
            predict_st_tc0['obs_ci_upper'] - predict_st_tc0['obs_ci_lower'])) / 2.0
        return df_regression_first

    # second regression : estimating relationship between air temperature and lst and latitude

    def regression_airtemperature(self, df_regression_second, dfUS):
        # third regression: estimating airT on the basis of lst and latitude
        features_airmodel_us = ['st', 'Ycoor']
        depvar_airmodel_us = dfUS['TempMax']
        indepvar_airmodel_us = sm.add_constant(dfUS[features_airmodel_us], has_constant='add')
        # Fit and summarize OLS model
        airmodel_us = sm.OLS(depvar_airmodel_us, indepvar_airmodel_us)
        results_airmodel_us = airmodel_us.fit()
        param_airt = results_airmodel_us.params
        # save coefficients standard deviation
        df_regression_second['regress_1_const'] = results_airmodel_us.params['const']
        df_regression_second['regress_1_st'] = results_airmodel_us.params['st']
        df_regression_second['regress_1_Ycoor'] = results_airmodel_us.params['Ycoor']

        ## prediction
        predict_airmodel_us = results_airmodel_us.get_prediction(indepvar_airmodel_us)
        predict_airmodel_us = predict_airmodel_us.summary_frame(alpha=0.05)

        # Applying airT model (US data) on LAU data
        features_at_model = ['st', 'Ycoor']
        indepvar_air_model = sm.add_constant(df_regression_second[features_at_model], has_constant='add')
        predict_at = results_airmodel_us.get_prediction(indepvar_air_model)
        predict_at = predict_at.summary_frame(alpha=0.05)
        df_regression_second['estimated_air'] = predict_at['mean']
        df_regression_second['error_air_temp'] = (np.abs(predict_at['obs_ci_upper'] - predict_at['obs_ci_lower'])) / 2.0

        # Estimating air temperature from esitmated_st_tc0 (air T in the no-vegetation scenario)
        features_at_model_tc0 = ['estimated_st_tc0', 'Ycoor']
        indepvar_air_model_tc0 = sm.add_constant(df_regression_second[features_at_model_tc0], has_constant='add')
        predict_air_tc0 = results_airmodel_us.get_prediction(indepvar_air_model_tc0)
        predict_air_tc0 = predict_air_tc0.summary_frame(alpha=0.05)

        # create a col that shows the estiamted st when tc is 0
        df_regression_second['estimated_at_tc0'] = predict_air_tc0['mean']
        df_regression_second['error_air_temp_TC0'] = (np.abs(
            predict_air_tc0['obs_ci_upper'] - predict_air_tc0['obs_ci_lower'])) / 2.0

        return df_regression_second

    # post processing of df
    def cooling_post_processing(self, df):
        # calculating cooling
        df['cooling'] = df['estimated_at_tc0'] - df['estimated_air']
        df['cooling_st'] = df['estimated_st_tc0'] - df['estimated_st']
        # calculate fractional error
        df['overall_fractional_error'] = (np.sqrt((np.square(df['error_st'] / df['estimated_st'])) +
                                                  (np.square(df['error_st_TC0'] / df['estimated_st_tc0'])) +
                                                  (np.square(df['error_air_temp'] / df['estimated_air'])) +
                                                  (np.square(df['error_air_temp_TC0'] / df['estimated_at_tc0']))
                                                  )) * df['cooling']
        #    # now calculate upper bound and lower bound
        df['upper_cooling'] = df['cooling'] + (df['cooling'] * df['overall_fractional_error'])
        df['lower_cooling'] = df['cooling'] - (df['cooling'] * df['overall_fractional_error'])

        return df

    # function to create raster on level of each pixel to allow for consolidation and zonal statistics
    def raster_consolidation(self, city, Ycoor_city, cooling_cities_df, rst_lst_meta, rst_lst_transform, rst_lst_masked,
                             rst_EVEG_masked, rst_tc_masked):
        # geometry of urban raster (niet nodig)
        out_meta = rst_lst_meta.meta.copy()
        # create dataframe
        df = pd.DataFrame(cooling_cities_df)
        # calculate the estimated
        output_data_ST_VEG = (
                rst_EVEG_masked * df[df[0] == city][7].values + df[df[0] == city][6].values * rst_tc_masked +
                df[df[0] == city][5].values)
        output_data_ST_no_veg = (
                0 * df[df[0] == city][7].values + df[df[0] == city][6].values * 0 +
                df[df[0] == city][5].values)

        output_data_AIR_VEG = output_data_ST_VEG * df[df[0] == city][10].values + Ycoor_city * df[df[0] == city][
            11].values + df[df[0] == city][9].values
        output_data_AIR_NO_VEG = output_data_ST_no_veg * df[df[0] == city][10].values + Ycoor_city * df[df[0] == city][
            11].values + df[df[0] == city][9].values
        output_data = output_data_AIR_NO_VEG - output_data_AIR_VEG

        # Values smaller than 0 are set to 0 to avoid negative cooling
        output_data[output_data < 0] = 0
        # use the meta data from the parent raster and update height and width
        out_meta.update({"driver": "GTiff",
                         "height": rst_lst_masked.shape[1],
                         "width": rst_lst_masked.shape[2],
                         "transform": rst_lst_transform,
                         "nodata": np.nan}
                        )
        with rasterio.open(os.path.join(self.temp_dir, 'temp_maps', f'{city}.tif'), "w",
                           **out_meta) as dest:
            dest.write(output_data)



    def writing_cooling_raster(self):
        # get all created rasters
        #rasters = [f for f in os.listdir(os.path.join(self.temp_dir, 'temp_maps')) if re.search(r'\.tif$', f)]
        rasters = createDataFrame(os.path.join(self.temp_dir, 'temp_maps'), 'tif')
        rasters = rasters.path.tolist()

        with rasterio.open(rasters[0], "r") as src:
            out_meta = src.meta.copy()
        # making a list of all the rasters
        sources = [rasterio.open(raster, "r") for raster in rasters]
        # create array representing all source rasters mosaicked together and take the average of the overlap
        dest, out_transform = merge(sources, resampling=rasterio.enums.Resampling.average)
        out_meta.update({"driver": "GTiff",
                         "height": dest.shape[1],
                         "width": dest.shape[2],
                         "transform": out_transform})
        with rasterio.open(os.path.join(self.temp_dir, f'cooling_by_vegetation_overview_{self.year}.tif'),
                           "w", **out_meta) as dest1:
            dest1.write(dest)

    # built a wrapper around write_sut to have it more custom to local climate
    def calculate_sut(self, year):
        # creating report per area per ecosystem type
        eco_path = self.get_ecosystem_raster(self.year, level=1)
        eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()
        report_per_area_per_ecosystem_type = statistics_byArea_byET(
            self.result_files['map_paths']['cooling'][self.year],
            self.reporting_raster,
            self.reporting_shape[SHAPE_ID],
            eco_path,
            eco_dic)

        # Percentage ecoystem present
        report_per_area_per_ecosystem_type['% ecosystem of total area'] = report_per_area_per_ecosystem_type[
                                                                              COUNT] / \
                                                                          report_per_area_per_ecosystem_type[
                                                                              COUNT].sum()
        # average cooling per ecosystem
        report_per_area_per_ecosystem_type['Average cooling [C°]| (SUM cooling/#cells)'] = \
            report_per_area_per_ecosystem_type[SUM] / report_per_area_per_ecosystem_type[COUNT]
        # calculate the cooling by ecosystem type
        report_per_area_per_ecosystem_type[
            'Absolute proportion of total urban cooling [C°]| (SUM cooling/#cells) * % ecosystem of total area'] = \
            report_per_area_per_ecosystem_type[SUM] / report_per_area_per_ecosystem_type[COUNT].sum()
        # contribution in urban cooling
        report_per_area_per_ecosystem_type['Relative proportion of total urban cooling in [%]'] = \
            report_per_area_per_ecosystem_type[
                'Absolute proportion of total urban cooling [C°]| (SUM cooling/#cells) * % ecosystem of total area'] / \
            report_per_area_per_ecosystem_type[
                'Absolute proportion of total urban cooling [C°]| (SUM cooling/#cells) * % ecosystem of total area'].sum()

        report_per_area_per_ecosystem_type.to_csv(os.path.join(self.statistic_dir,
                                                               f'report_area_per_ET_cooling_degree_{year}.csv'))

        # Pivot ecosystem type to columns
        supply = report_per_area_per_ecosystem_type[
            'Absolute proportion of total urban cooling [C°]| (SUM cooling/#cells) * % ecosystem of total area'].unstack(
            ECOTYPE)
        # All use is household final consumption
        use = supply.sum(axis=1).rename(HSH)

        write_sut(self.SUT_physical_path.format(year=year),
                  supply,
                  use,
                  self.service_name,
                  year,
                  "Average weighted cooling [C°]")
