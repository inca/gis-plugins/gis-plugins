# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

import contextlib
import logging
import math
import os

import numpy as np
import pandas as pd
import rasterio

import inca
import inca.common.estat as estat
from inca.common.config_check import ConfigItem, ConfigRaster, YEARLY, check_tsv
from inca.common.ecosystem import (FOREST, CROP, GRASSLAND, HEATH,
                                   SPARSE, WETLANDS, RIVERS, LAKES, MARINE_TRANSITIONAL,
                                   COASTAL, MARINE, types_l1, SHAPE_ID)
import inca.common.ecosystem as ecosystem
from inca.common.geoprocessing import statistics_byArea, statistics_byArea_byET, block_window_generator, RasterType
from inca.common.suts import write_sut, IND
import inca.common.suts as suts

logger = logging.getLogger(__name__)

# Some index keys for EuroStat tables:
MIO_EUR = 'MIO_EUR'
THS_EUR = 'THS_EUR'
THS_M3 = 'THS_M3'
FOREST_TREES = 'P1_TR'
ROUNDWOOD = 'RW'
TOTAL = 'TOTAL'
EXPORTS = 'EXP'
NAI = 'NAI'  # Net Annual Increment
RMOV = 'RMOV'  # Removals
FAWS = 'FAWS'  # Forest available for wood supply
FNAWS = 'FNAWS'  # Forest not available for wood supply
OWL_AWS = 'OWL_AWS'  # Forest and other woodland available for wood supply
OLWTC_AWS = 'OLWTC_AWS'  # Other land with wooded trees available for wood supply
OWL = 'OWL'  # Other wooded land
FOR = 'FOR'  # Forest (all)
OTHER_LAND = [CROP, GRASSLAND, HEATH,
              SPARSE, WETLANDS, RIVERS, LAKES, MARINE_TRANSITIONAL,
              COASTAL, MARINE]

_WOODPROVISION = 'wood-provision'


class WoodRun(inca.Run):
    service_name = 'Wood Provision'

    def __init__(self, config):
        super().__init__(config)

        self.other_land_mask = None
        self.final_proxy = None
        self.rmov_other = None
        self.nai_owl_aws = None
        self.nai_faws = None

        self.statistic_monetary_nominal_faws = None
        self.statistic_monetary_faws = None
        self.statistic_physical_faws = None
        self.sut_monetary_nominal_faws = None
        self.sut_monetary_faws = None
        self.sut_physical_faws = None
        self.flow_map_monetary_nominal_faws = None
        self.flow_map_monetary_real_faws = None
        self.flow_map_physical_faws = None

        self.statistic_monetary_nominal_OWL_AWS = None
        self.statistic_monetary_OWL_AWS = None
        self.statistic_physical_OWL_AWS = None
        self.sut_monetary_nominal_OWL_AWS = None
        self.sut_monetary_OWL_AWS = None
        self.sut_physical_OWL_AWS = None
        self.flow_map_monetary_nominal_OWL_AWS = None
        self.flow_map_monetary_real_OWL_AWS = None
        self.flow_map_physical_OWL_AWS = None

        self.statistic_monetary_nominal_rmov_other = None
        self.statistic_monetary_rmov_other = None
        self.statistic_physical_rmov_other = None
        self.sut_monetary_nominal_rmov_other = None
        self.sut_monetary_rmov_other = None
        self.sut_physical_rmov_other = None
        self.flow_map_monetary_nominal_rmov_other = None
        self.flow_map_monetary_real_rmov_other = None
        self.flow_map_physical_rmov_other = None

        self.statistic_monetary_nominal_rmov_fnaws = None
        self.statistic_monetary_rmov_fnaws = None
        self.statistic_physical_rmov_fnaws = None
        self.sut_monetary_nominal_rmov_fnaws = None
        self.sut_monetary_rmov_fnaws = None
        self.sut_physical_rmov_fnaws = None
        self.flow_map_monetary_nominal_fnaws = None
        self.flow_map_monetary_real_rmov_fnaws = None
        self.flow_map_physical_rmov_fnaws = None


        self.config_template.update({
            self.service_name: {'for_vol_efa': ConfigItem(check_tsv),
                                'proxy': {YEARLY: ConfigRaster(raster_type=RasterType.RELATIVE)},
                                'proxy_weights': {YEARLY: ConfigRaster(optional=True,
                                                                       raster_type=RasterType.RELATIVE)}}})

        if self.monetary:
            self.config_template[self.service_name].update({
                'for_sup_cp': ConfigItem(check_tsv),
                'for_basic': ConfigItem(check_tsv)})

        self.make_output_filenames()

    def make_output_filenames(self):
        """Format standard output file names."""
        # Forest Available for wood supply : FAWS
        self.flow_map_physical_faws = os.path.join(self.maps, f'{_WOODPROVISION}_map_use_1000-m3_{{year}}_FAWS.tif')
        self.flow_map_monetary_real_faws = os.path.join(self.maps,
                                                        f'{_WOODPROVISION}_map_use_M-EURO-real_{{year}}_FAWS.tif')
        self.flow_map_monetary_nominal_faws = os.path.join(self.maps,
                                                           f'{_WOODPROVISION}_map_use_M-EURO-current_{{year}}_FAWS.tif')
        self.sut_physical_faws = os.path.join(self.suts,
                                              f'{_WOODPROVISION}_report_SUT-physical_1000-m3_{{year}}_FAWS.xlsx')
        self.sut_monetary_faws = os.path.join(self.suts,
                                              f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-real_{{year}}_FAWS.xlsx')
        self.sut_monetary_nominal_faws = os.path.join(self.suts,
                                                      f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-current_{{year}}_FAWS.xlsx')
        self.statistic_physical_faws = os.path.join(self.statistic_dir,
                                                    f'{_WOODPROVISION}_statistics_physical_1000-m3_{{year}}_FAWS.csv')
        self.statistic_monetary_faws = os.path.join(self.statistic_dir,
                                                    f'{_WOODPROVISION}_statistics_monetary_M-EURO-real_{{year}}_FAWS.csv')
        self.statistic_monetary_nominal_faws = os.path.join(
            self.statistic_dir, f'{_WOODPROVISION}_statistics_monetary_M-EURO-current_{{year}}_FAWS.csv')
        self.final_proxy = os.path.join(self.temp_dir, f'{_WOODPROVISION}_map_proxy_{{year}}.tif')

        # Other Wooded land available for wood supply : OWL_AWS
        self.flow_map_physical_OWL_AWS = os.path.join(self.maps,
                                                      f'{_WOODPROVISION}_map_use_1000-m3_{{year}}_OWL_AWS.tif')
        self.flow_map_monetary_real_OWL_AWS = os.path.join(self.maps,
                                                           f'{_WOODPROVISION}_map_use_M-EURO-real_{{year}}_OWL_AWS.tif')
        self.flow_map_monetary_nominal_OWL_AWS = os.path.join(self.maps,
                                                              f'{_WOODPROVISION}_map_use_M-EURO-current_{{year}}_OWL_AWS.tif')
        self.sut_physical_OWL_AWS = os.path.join(self.suts,
                                                 f'{_WOODPROVISION}_report_SUT-physical_1000-m3_{{year}}_OWL_AWS.xlsx')
        self.sut_monetary_OWL_AWS = os.path.join(self.suts,
                                                 f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-real_{{year}}_OWL_AWS.xlsx')
        self.sut_monetary_nominal_OWL_AWS = os.path.join(self.suts,
                                                         f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-current_{{year}}_OWL_AWS.xlsx')
        self.statistic_physical_OWL_AWS = os.path.join(self.statistic_dir,
                                                       f'{_WOODPROVISION}_statistics_physical_1000-m3_{{year}}_OWL_AWS.csv')
        self.statistic_monetary_OWL_AWS = os.path.join(self.statistic_dir,
                                                       f'{_WOODPROVISION}_statistics_monetary_M-EURO-real_{{year}}_OWL_AWS.csv')
        self.statistic_monetary_nominal_OWL_AWS = os.path.join(
            self.statistic_dir, f'{_WOODPROVISION}_statistics_monetary_M-EURO-current_{{year}}_OWL_AWS.csv')
        self.final_proxy = os.path.join(self.temp_dir, f'{_WOODPROVISION}_map_proxy_{{year}}.tif')

        # Other land that provides wood supply
        self.flow_map_physical_rmov_other = os.path.join(self.maps,
                                                         f'{_WOODPROVISION}_map_use_1000-m3_{{year}}_rmov_other.tif')
        self.flow_map_monetary_real_rmov_other = os.path.join(self.maps,
                                                              f'{_WOODPROVISION}_map_use_M-EURO-real_{{year}}_rmov_other.tif')
        self.flow_map_monetary_nominal_rmov_other = os.path.join(self.maps,
                                                                 f'{_WOODPROVISION}_map_use_M-EURO-current_{{year}}_rmov_other.tif')
        self.sut_physical_rmov_other = os.path.join(self.suts,
                                                    f'{_WOODPROVISION}_report_SUT-physical_1000-m3_{{year}}_rmov_other.xlsx')
        self.sut_monetary_rmov_other = os.path.join(self.suts,
                                                    f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-real_{{year}}_rmov_other.xlsx')
        self.sut_monetary_nominal_rmov_other = os.path.join(self.suts,
                                                            f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-current_{{year}}_rmov_other.xlsx')
        self.statistic_physical_rmov_other = os.path.join(self.statistic_dir,
                                                          f'{_WOODPROVISION}_statistics_physical_1000-m3_{{year}}_rmov_other.csv')
        self.statistic_monetary_rmov_other = os.path.join(self.statistic_dir,
                                                          f'{_WOODPROVISION}_statistics_monetary_M-EURO-real_{{year}}_rmov_other.csv')
        self.statistic_monetary_nominal_rmov_other = os.path.join(
            self.statistic_dir, f'{_WOODPROVISION}_statistics_monetary_M-EURO-current_{{year}}_rmov_other .csv')

        # forest not available that provides wood supply
        self.flow_map_physical_rmov_fnaws = os.path.join(self.maps,
                                                         f'{_WOODPROVISION}_map_use_1000-m3_{{year}}_rmov_fnaws.tif')
        self.flow_map_monetary_real_rmov_fnaws = os.path.join(self.maps,
                                                              f'{_WOODPROVISION}_map_use_M-EURO-real_{{year}}_rmov_fnaws.tif')
        self.flow_map_monetary_nominal_rmov_fnaws = os.path.join(self.maps,
                                                                 f'{_WOODPROVISION}_map_use_M-EURO-current_{{year}}_rmov_fnaws.tif')
        self.sut_physical_rmov_fnaws = os.path.join(self.suts,
                                                    f'{_WOODPROVISION}_report_SUT-physical_1000-m3_{{year}}_rmov_fnaws.xlsx')
        self.sut_monetary_rmov_fnaws = os.path.join(self.suts,
                                                    f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-real_{{year}}_rmov_fnaws.xlsx')
        self.sut_monetary_nominal_rmov_fnaws = os.path.join(self.suts,
                                                            f'{_WOODPROVISION}_report_SUT-monetary_M-EURO-current_{{year}}_rmov_fnaws.xlsx')
        self.statistic_physical_rmov_fnaws = os.path.join(self.statistic_dir,
                                                          f'{_WOODPROVISION}_statistics_physical_1000-m3_{{year}}_rmov_fnaws.csv')
        self.statistic_monetary_rmov_fnaws = os.path.join(self.statistic_dir,
                                                          f'{_WOODPROVISION}_statistics_monetary_M-EURO-real_{{year}}_rmov_fnaws.csv')
        self.statistic_monetary_nominal_rmov_fnaws = os.path.join(
            self.statistic_dir, f'{_WOODPROVISION}_statistics_monetary_M-EURO-current_{{year}}_rmov_fnaws.csv')

        self.final_proxy = os.path.join(self.temp_dir, f'{_WOODPROVISION}_map_proxy_{{year}}.tif')
        self.other_land_mask = os.path.join(self.temp_dir, f'{_WOODPROVISION}_map_other_land_proxy_{{year}}.tif')

    def make_forestry_proxy(self, year, add_progress):
        """Create the final proxy for forestry output using input proxy, ecosystem classification, and weights map."""
        ecosystem_mapping = self.get_ecosystem_mapping(level=1)
        landcover_codes_forest = ecosystem_mapping.set_index('name').loc[FOREST, 'landcover_id']
        logger.debug('landcover_codes_forest:\n%s', landcover_codes_forest)

        filename_out = self.final_proxy.format(year=year)
        with rasterio.open(self.config[self.service_name]['proxy'][year]) as ds_proxy, \
                rasterio.open(self.config['land_cover'][year]) as ds_landcover, \
                (rasterio.open(self.config[self.service_name]['proxy_weights'][year])
                if self.config[self.service_name]['proxy_weights'].get(year, None)
                else contextlib.nullcontext()) as ds_weights, \
                rasterio.open(filename_out, 'w',
                              **dict(ds_proxy.profile, dtype=rasterio.float32, compress='deflate')) as ds_out:
            block_shape = (2048, 2048)
            nblocks = math.ceil(ds_proxy.profile['height'] / block_shape[0]) * \
                      math.ceil(ds_proxy.profile['width'] / block_shape[1])
            for _, window in block_window_generator(block_shape, ds_proxy.profile['height'],
                                                    ds_proxy.profile['width']):
                proxy = ds_proxy.read(1, window=window)
                landcover = ds_landcover.read(1, window=window)
                # Initial mask: ecosystem type == forest
                mask = np.where(np.isin(landcover, landcover_codes_forest), 1.0, 0.0)
                result = proxy * mask  # we handle nodata below
                # Multiply by weights if provided:
                if ds_weights is not None:
                    # Multiply mask by weights if weights provided
                    weights = ds_weights.read(1, window=window)
                    result *= weights  # we handle nodata below
                    result[weights == ds_weights.nodata] = ds_out.nodata
                result[proxy == ds_proxy.nodata] = ds_out.nodata
                ds_out.write(result.astype(rasterio.float32), window=window, indexes=1)
                add_progress(100. / nblocks)
            proxy_inputs = [ds_landcover.name, ds_proxy.name]
            if ds_weights is not None:
                proxy_inputs.append(ds_weights.name)
            self.accord.metadata.update_dataset_tags(ds_out, 'Forestry activity proxy', 'dimensionless',
                                                     *proxy_inputs)
        return filename_out

    def make_mask_other_land(self, year, add_progress):
        """Create a mask for other land to be used in spatial dissaggragetion."""
        ecosystem_mapping = self.get_ecosystem_mapping(level=1)
        landcover_codes_forest = ecosystem_mapping.set_index('name').loc[FOREST, 'landcover_id']
        logger.debug('landcover_codes_forest:\n%s', landcover_codes_forest)

        filename_out = self.other_land_mask.format(year=year)
        with rasterio.open(self.config['land_cover'][year]) as ds_landcover, \
                rasterio.open(filename_out, 'w',
                              **dict(ds_landcover.profile, dtype=rasterio.float32, compress='deflate')) as ds_out:
            block_shape = (2048, 2048)
            nblocks = math.ceil(ds_landcover.profile['height'] / block_shape[0]) * \
                      math.ceil(ds_landcover.profile['width'] / block_shape[1])
            for _, window in block_window_generator(block_shape, ds_landcover.profile['height'],
                                                    ds_landcover.profile['width']):
                proxy = ds_landcover.read(1, window=window)
                landcover = ds_landcover.read(1, window=window)
                # Initial mask: ecosystem type == forest and that is 0 so all the rest is 1
                mask = np.where(np.isin(landcover, landcover_codes_forest), 0, 1.0)
                ds_out.write(mask.astype(rasterio.int8), window=window, indexes=1)
                add_progress(100. / nblocks)
        return filename_out

    def _start(self):
        global supply_table_monetary_faws, supply_table_monetary_owl_aws, supply_table_monetary_rmov_other, supply_table_monetary_rmov_fnaws
        logger.debug('Starting Wood Provisioning calculation with configuration %s', self.config)

        self.load_for_vol_efa()
        wood_physical_nai_faws = self.nai_faws
        wood_physical_nai_owl_aws = self.nai_owl_aws
        wood_physical_rmov_other = self.rmov_other
        wood_physical_rmov_fnaws = self.rmov_fnaws

        # physical supply table is just the NAI of FAWS in 1000m3:
        supply_table_physical_faws = wood_physical_nai_faws[self.years].loc[self.statistics_shape.index].sort_index()
        logger.debug('SUT physical FOREST:\n%s', supply_table_physical_faws)

        # physical supply table is just the NAI of OWL AWS in 1000m³:
        supply_table_physical_owl_aws = wood_physical_nai_owl_aws[self.years].loc[
            self.statistics_shape.index].sort_index()
        logger.debug('SUT physical OTHER WOODED LAND:\n%s', wood_physical_nai_owl_aws)

        # physical supply table is just the REMOVAL FROM OTHER LAND in 1000m³:
        supply_table_physical_rmov_other = wood_physical_rmov_other[self.years].loc[
            self.statistics_shape.index].sort_index()
        logger.debug('SUT physical OTHER WOODED LAND:\n%s', wood_physical_rmov_other)

        # physical supply table is just the REMOVAL FOREST NOT AVAILABLE in 1000m³:
        supply_table_physical_rmov_fnaws = wood_physical_rmov_fnaws[self.years].loc[
            self.statistics_shape.index].sort_index()
        logger.debug('SUT physical REMOVE FOREST NOT AVAILABLE:\n%s', wood_physical_rmov_fnaws)

        if self.monetary:
            for_sup_cp = estat.read_tsv(self.config[self.service_name]['for_sup_cp']).loc[(MIO_EUR, FOREST_TREES)]
            for_basic = estat.read_tsv(self.config[self.service_name]['for_basic']).loc[(ROUNDWOOD, TOTAL, EXPORTS)]
            logger.debug('for_basic: \n%s', for_basic.loc[:, range(2000, 2022)])

            unit_value_nai_faws = 1000 * for_sup_cp / supply_table_physical_faws  # convert [1000 0000 eur / 1000 m3] -> [eur/m3]
            unit_value_nai_owl_aws = 1000 * for_sup_cp / supply_table_physical_owl_aws  # convert [1000 0000 eur / 1000 m3] -> [eur/m3]
            unit_value_rmov_other = 1000 * for_sup_cp / supply_table_physical_rmov_other  # convert [1000 0000 eur / 1000 m3] -> [eur/m3]
            unit_value_rmov_fnaws = 1000 * for_sup_cp / supply_table_physical_rmov_fnaws  # convert [1000 0000 eur / 1000 m3] -> [eur/m3]

            logger.debug('unit value of NAI faws: \n%s', unit_value_nai_faws.loc[:, range(2000, 2022)])
            logger.debug('unit value of NAI OWL WAS: \n%s', unit_value_nai_owl_aws.loc[:, range(2000, 2022)])
            logger.debug('unit value of Other removal: \n%s', unit_value_nai_owl_aws.loc[:, range(2000, 2022)])

            unit_value_exports = (for_basic.loc[THS_EUR, range(2000, 2022)] /
                                  for_basic.loc[THS_M3, range(2000, 2022)]).replace(np.inf, np.nan)
            logger.debug('unit value of exports: \n%s', unit_value_exports)
            unit_value_exports_gf = gap_fill_unit_value_exports(unit_value_exports)
            logger.debug('unit value of exports, gap-filled: \n%s', unit_value_exports_gf)
            ratio_export_nai_faws = gap_fill_ratio_export_nai(unit_value_exports, unit_value_nai_faws)
            ratio_export_nai_owl_aws = gap_fill_ratio_export_nai(unit_value_exports, unit_value_nai_owl_aws)
            ratio_export_rmov_other = gap_fill_ratio_export_nai(unit_value_exports, unit_value_rmov_other)
            ratio_export_rmov_fnaws = gap_fill_ratio_export_nai(unit_value_exports, unit_value_rmov_fnaws)

            logger.debug('Ratio export/nai faws:\n%s', ratio_export_nai_faws)
            logger.debug('Ratio export/nai owl aws:\n%s', ratio_export_nai_owl_aws)
            logger.debug('Ratio export/nai owl aws:\n%s', ratio_export_rmov_other)

            unit_value_nai_faws_gap_filled = (unit_value_exports_gf / ratio_export_nai_faws).loc[
                self.statistics_shape.index]
            unit_value_nai_owl_aws_gap_filled = (unit_value_exports_gf / ratio_export_nai_owl_aws).loc[
                self.statistics_shape.index]
            unit_value_rmov_other_gap_filled = (unit_value_exports_gf / ratio_export_rmov_other).loc[
                self.statistics_shape.index]
            unit_value_rmov_fnaws_gap_filled = (unit_value_exports_gf / ratio_export_rmov_fnaws).loc[
                self.statistics_shape.index]

            logger.debug('unit value of NAI faws , gap filled:\n%s', unit_value_nai_faws_gap_filled)
            logger.debug('unit value of NAI owl aws, gap filled:\n%s', unit_value_nai_owl_aws_gap_filled)
            logger.debug('unit value of REMOVAL OTHER LAND, gap filled:\n%s', unit_value_rmov_other_gap_filled)
            logger.debug('unit value of REMOVE fnaws, gap filled:\n%s', unit_value_rmov_fnaws_gap_filled)

            # monetary: multiply unit value [eur/m3] by physical supply [1000 m3], and convert to millions of EUR:x
            supply_table_monetary_faws = supply_table_physical_faws * unit_value_nai_faws_gap_filled[self.years] / 1000
            supply_table_monetary_owl_aws = supply_table_physical_owl_aws * unit_value_nai_owl_aws_gap_filled[
                self.years] / 1000
            supply_table_monetary_rmov_other = supply_table_physical_rmov_other * unit_value_rmov_other_gap_filled[
                self.years] / 1000
            supply_table_monetary_rmov_fnaws = supply_table_physical_rmov_fnaws * unit_value_rmov_fnaws_gap_filled[
                self.years] / 1000

            logger.debug('SUT monetary FOREST:\n%s', supply_table_monetary_faws)
            logger.debug('SUT monetary Other Wooded Land:\n%s', supply_table_monetary_owl_aws)
            logger.debug('SUT monetary REMOVAL OTHER LAND:\n%s', supply_table_monetary_rmov_other)
            logger.debug('SUT monetary REMOVAL FOREST NOT AVAILABLE:\n%s', supply_table_monetary_rmov_fnaws)
        nyears = len(self.years)

        # Progress bar contribution:
        #
        # If we have monetary evaluation, roughly 50% of time is spent on physical use map, and 50% on monetary maps.
        # Without monetary evalutation, 100% of time is spent on the physical part.
        progress_weight_physical = 0.5 if self.monetary else 1.

        for year in self.years:
            physical_faws = supply_table_physical_faws[year]
            physical_owl_aws = supply_table_physical_owl_aws[year]
            physical_rmov_other = supply_table_physical_rmov_other[year]
            physical_rmov_fnaws = supply_table_physical_rmov_fnaws[year]

            proxy = self.make_forestry_proxy(year, add_progress=lambda p: self.add_progress(
                progress_weight_physical * 0.3 * p / nyears))

            mask = self.make_mask_other_land(year, add_progress=lambda p: self.add_progress(
                progress_weight_physical * 0.3 * p / nyears))

            # spatial disaggregation using data areas:
            proxy_sums_faws = self.accord.spatial_disaggregation_byArea(proxy, physical_faws,
                                                                        self.statistics_raster,
                                                                        self.statistics_shape[SHAPE_ID],
                                                                        self.flow_map_physical_faws.format(year=year),
                                                                        add_progress=lambda p: self.add_progress(
                                                                            progress_weight_physical * 0.5 * p / nyears),
                                                                        processing_info='Spatial disaggregation of use',
                                                                        unit_info='1000 m³')
            proxy_sums_owl_aws = self.accord.spatial_disaggregation_byArea(proxy, physical_owl_aws,
                                                                           self.statistics_raster,
                                                                           self.statistics_shape[SHAPE_ID],
                                                                           self.flow_map_physical_OWL_AWS.format(
                                                                               year=year),
                                                                           add_progress=lambda p: self.add_progress(
                                                                               progress_weight_physical * 0.5 * p / nyears),
                                                                           processing_info='Spatial disaggregation of use',
                                                                           unit_info='1000 m³')

            mask_sums_rmov_other = self.accord.spatial_disaggregation_byArea(mask, physical_rmov_other,
                                                                             self.statistics_raster,
                                                                             self.statistics_shape[SHAPE_ID],
                                                                             self.flow_map_physical_rmov_other.format(
                                                                                 year=year),
                                                                             add_progress=lambda p: self.add_progress(
                                                                                 progress_weight_physical * 0.5 * p / nyears),
                                                                             processing_info='Spatial disaggregation of use',
                                                                             unit_info='1000 m³')

            proxy_sums_rmov_fnaws = self.accord.spatial_disaggregation_byArea(proxy, physical_rmov_fnaws,
                                                                             self.statistics_raster,
                                                                             self.statistics_shape[SHAPE_ID],
                                                                             self.flow_map_physical_rmov_fnaws.format(
                                                                                 year=year),
                                                                             add_progress=lambda p: self.add_progress(
                                                                                 progress_weight_physical * 0.5 * p / nyears),
                                                                             processing_info='Spatial disaggregation of use',
                                                                             unit_info='1000 m³')

            self.cut_to_reporting_regions(self.flow_map_physical_faws.format(year=year))
            physical_report_faws = \
                statistics_byArea(self.flow_map_physical_faws.format(year=year), self.reporting_raster,
                                  self.reporting_shape[SHAPE_ID],
                                  add_progress=lambda p: self.add_progress(
                                      progress_weight_physical * 0.2 * p))['sum']

            self.cut_to_reporting_regions(self.flow_map_physical_OWL_AWS.format(year=year))
            physical_report_owl_aws = \
                statistics_byArea(self.flow_map_physical_OWL_AWS.format(year=year), self.reporting_raster,
                                  self.reporting_shape[SHAPE_ID],
                                  add_progress=lambda p: self.add_progress(
                                      progress_weight_physical * 0.2 * p))['sum']

            self.cut_to_reporting_regions(self.flow_map_physical_rmov_other.format(year=year))
            physical_report_rmov_other = \
                statistics_byArea_byET(self.flow_map_physical_rmov_other.format(year=year),
                                       self.reporting_raster,
                                       self.reporting_shape[SHAPE_ID],
                                       self.get_ecosystem_raster(year),
                                       ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                       add_progress=lambda p: self.add_progress(
                                           progress_weight_physical * 0.2 * p))['sum']
            supply_physical = physical_report_rmov_other.unstack(ecosystem.ECOTYPE)
            # All use is household final consumption
            use_physical = supply_physical.sum(axis=1).rename(suts.IND)

            self.cut_to_reporting_regions(self.flow_map_physical_rmov_fnaws.format(year=year))
            physical_report_rmov_fnaws = \
                statistics_byArea(self.flow_map_physical_rmov_fnaws.format(year=year), self.reporting_raster,
                                  self.reporting_shape[SHAPE_ID],
                                  add_progress=lambda p: self.add_progress(
                                      progress_weight_physical * 0.2 * p))['sum']

            # SUT: All use is by intermediate consumption by industries; all supply by forests.
            write_sut(self.sut_physical_faws.format(year=year),
                      physical_report_faws.rename(FOREST), physical_report_faws.rename(IND),
                      self.service_name, year, '1000 m³')

            write_sut(self.sut_physical_OWL_AWS.format(year=year),
                      physical_report_owl_aws.rename(FOREST), physical_report_owl_aws.rename(IND),
                      self.service_name, year, '1000 m³')

            write_sut(self.sut_physical_rmov_other.format(year=year),
                      supply_physical, use_physical,
                      self.service_name, year, '1000 m³')

            write_sut(self.sut_physical_rmov_fnaws.format(year=year),
                      physical_report_rmov_fnaws.rename(FOREST), physical_report_rmov_fnaws.rename(IND),
                      self.service_name, year, '1000 m³')

            # Statistics csv file with shorthand column name:
            forest_short = types_l1.set_index('name').loc[FOREST, 'short_name']
            physical_report_faws.rename(forest_short).to_csv(self.statistic_physical_faws.format(year=year))
            physical_report_owl_aws.rename(forest_short).to_csv(self.statistic_physical_OWL_AWS.format(year=year))
            physical_report_rmov_fnaws.rename(forest_short).to_csv(self.statistic_physical_rmov_fnaws.format(year=year))
            logger.debug('EU aggregate faws [thousand m3]:\n%s', supply_table_physical_faws.sum(axis=0))
            logger.debug('EU aggregate owl aws [thousand m3]:\n%s', supply_table_physical_owl_aws.sum(axis=0))
            logger.debug('EU aggregate owl aws [thousand m3]:\n%s', supply_table_physical_rmov_fnaws.sum(axis=0))

            physical_report_rmov_other.to_csv(self.statistic_physical_rmov_other.format(year=year))
            logger.debug('EU aggregate owl aws [thousand m3]:\n%s', physical_report_rmov_other.sum(axis=0))

            if self.monetary:
                monetary_faws = supply_table_monetary_faws[year]
                monetary_owl_aws = supply_table_monetary_owl_aws[year]
                monetary_rmov_other = supply_table_monetary_rmov_other[year]
                monetary_rmov_fnaws = supply_table_monetary_rmov_fnaws[year]
                # "real" prices for reference year 2000
                monetary_real_value_faws = (monetary_faws * self.deflator[2000] / self.deflator[year]).rename(
                    'Forestry')
                monetary_real_value_owl_aws = (monetary_owl_aws * self.deflator[2000] / self.deflator[year]).rename(
                    'Forestry')
                monetary_real_value_rmov_other = (
                        monetary_rmov_other * self.deflator[2000] / self.deflator[year]).rename(
                    'Forestry')
                monetary_real_value_rmov_fnaws = (
                        monetary_rmov_fnaws * self.deflator[2000] / self.deflator[year]).rename(
                    'Forestry')

                self.accord.spatial_disaggregation_byArea(proxy, monetary_faws,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_nominal_faws.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=proxy_sums_faws,
                                                          processing_info='Spatial disaggregation of use.',
                                                          unit_info='million EUR (nominal value)')

                self.accord.spatial_disaggregation_byArea(proxy, monetary_owl_aws,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_nominal_OWL_AWS.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=proxy_sums_owl_aws,
                                                          processing_info='Spatial disaggregation of use.',
                                                          unit_info='million EUR (nominal value)')

                self.accord.spatial_disaggregation_byArea(mask, monetary_rmov_other,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_nominal_rmov_other.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=mask_sums_rmov_other,
                                                          processing_info='Spatial disaggregation of use.',
                                                          unit_info='million EUR (nominal value)')

                self.accord.spatial_disaggregation_byArea(proxy, monetary_rmov_fnaws,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_nominal_rmov_fnaws.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=proxy_sums_rmov_fnaws,
                                                          processing_info='Spatial disaggregation of use.',
                                                          unit_info='million EUR (nominal value)')

                self.accord.spatial_disaggregation_byArea(proxy, monetary_real_value_faws,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_real_faws.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=proxy_sums_faws,
                                                          processing_info='Spatial disaggregation of use',
                                                          unit_info='million EUR (reference year 2000 value)')

                self.accord.spatial_disaggregation_byArea(proxy, monetary_real_value_owl_aws,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_real_OWL_AWS.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=proxy_sums_owl_aws,
                                                          processing_info='Spatial disaggregation of use',
                                                          unit_info='million EUR (reference year 2000 value)')

                self.accord.spatial_disaggregation_byArea(mask, monetary_real_value_rmov_other,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_real_rmov_other.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=mask_sums_rmov_other,
                                                          processing_info='Spatial disaggregation of use',
                                                          unit_info='million EUR (reference year 2000 value)')

                self.accord.spatial_disaggregation_byArea(proxy, monetary_real_value_rmov_fnaws,
                                                          self.statistics_raster,
                                                          self.statistics_shape[SHAPE_ID],
                                                          self.flow_map_monetary_real_rmov_fnaws.format(year=year),
                                                          add_progress=lambda p: self.add_progress(0.15 * p / nyears),
                                                          proxy_sums=proxy_sums_rmov_fnaws,
                                                          processing_info='Spatial disaggregation of use',
                                                          unit_info='million EUR (reference year 2000 value)')

                self.cut_to_reporting_regions(self.flow_map_monetary_nominal_faws.format(year=year))
                self.cut_to_reporting_regions(self.flow_map_monetary_nominal_OWL_AWS.format(year=year))
                self.cut_to_reporting_regions(self.flow_map_monetary_nominal_rmov_other.format(year=year))
                self.cut_to_reporting_regions(self.flow_map_monetary_nominal_rmov_fnaws.format(year=year))
                self.cut_to_reporting_regions(self.flow_map_monetary_real_faws.format(year=year))
                self.cut_to_reporting_regions(self.flow_map_monetary_real_OWL_AWS.format(year=year))
                self.cut_to_reporting_regions(self.flow_map_monetary_real_rmov_other.format(year=year))
                self.cut_to_reporting_regions(self.flow_map_monetary_real_rmov_fnaws.format(year=year))

                monetary_nominal_report_faws = statistics_byArea(self.flow_map_monetary_nominal_faws.format(year=year),
                                                                 self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                                 add_progress=lambda p: self.add_progress(0.1 * p))[
                    'sum']

                monetary_nominal_report_owl_aws = \
                    statistics_byArea(self.flow_map_monetary_nominal_OWL_AWS.format(year=year),
                                      self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                      add_progress=lambda p: self.add_progress(0.1 * p))[
                        'sum']

                monetary_nominal_report_rmov_fnaws= \
                    statistics_byArea(self.flow_map_monetary_nominal_rmov_fnaws.format(year=year),
                                      self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                      add_progress=lambda p: self.add_progress(0.1 * p))[
                        'sum']

                monetary_nominal_report_rmov_other = \
                    statistics_byArea_byET(self.flow_map_monetary_nominal_rmov_other.format(year=year),
                                           self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                           self.get_ecosystem_raster(year),
                                           ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                           add_progress=lambda p: self.add_progress(0.1 * p))[
                        'sum']
                supply_nominal = monetary_nominal_report_rmov_other.unstack(ecosystem.ECOTYPE)
                use_nominal = supply_nominal.sum(axis=1).rename(suts.IND)

                monetary_real_report_faws = statistics_byArea(self.flow_map_monetary_real_faws.format(year=year),
                                                              self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                              add_progress=lambda p: self.add_progress(0.1 * p))['sum']

                monetary_real_report_owl_aws = statistics_byArea(self.flow_map_monetary_real_OWL_AWS.format(year=year),
                                                                 self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                                 add_progress=lambda p: self.add_progress(0.1 * p))[
                    'sum']

                monetary_real_report_rmov_fnaws = statistics_byArea(self.flow_map_monetary_real_rmov_fnaws.format(year=year),
                                                                 self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                                 add_progress=lambda p: self.add_progress(0.1 * p))[
                    'sum']

                monetary_real_report_rmov_other = \
                    statistics_byArea_byET(self.flow_map_monetary_real_rmov_other.format(year=year),
                                           self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                           self.get_ecosystem_raster(year),
                                           ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                           add_progress=lambda p: self.add_progress(0.1 * p))[
                        'sum']
                supply_real = monetary_real_report_rmov_other.unstack(ecosystem.ECOTYPE)
                use_real = supply_real.sum(axis=1).rename(suts.IND)

                write_sut(self.sut_monetary_faws.format(year=year),
                          monetary_real_report_faws.rename(FOREST), monetary_real_report_faws.rename(IND),
                          self.service_name, year, 'Million EUR (2000 value)')

                write_sut(self.sut_monetary_OWL_AWS.format(year=year),
                          monetary_real_report_owl_aws.rename(FOREST), monetary_real_report_owl_aws.rename(IND),
                          self.service_name, year, 'Million EUR (2000 value)')

                write_sut(self.sut_monetary_rmov_fnaws.format(year=year),
                          monetary_real_report_rmov_fnaws.rename(FOREST), monetary_real_report_rmov_fnaws.rename(IND),
                          self.service_name, year, 'Million EUR (2000 value)')

                write_sut(self.sut_monetary_rmov_other.format(year=year),
                          supply_real,
                          use_real.rename(IND),
                          self.service_name, year, 'Million EUR (2000 value)')

                write_sut(self.sut_monetary_nominal_faws.format(year=year),
                          monetary_nominal_report_faws.rename(FOREST), monetary_nominal_report_faws.rename(IND),
                          self.service_name, year, 'Million EUR (nominal)')

                write_sut(self.sut_monetary_nominal_OWL_AWS.format(year=year),
                          monetary_nominal_report_owl_aws.rename(FOREST), monetary_nominal_report_owl_aws.rename(IND),
                          self.service_name, year, 'Million EUR (nominal)')

                write_sut(self.sut_monetary_nominal_rmov_fnaws.format(year=year),
                          monetary_nominal_report_rmov_fnaws.rename(FOREST), monetary_nominal_report_rmov_fnaws.rename(IND),
                          self.service_name, year, 'Million EUR (nominal)')

                write_sut(self.sut_monetary_nominal_rmov_other.format(year=year),
                          supply_nominal,
                          use_nominal.rename(IND),
                          self.service_name, year, 'Million EUR (nominal)')

                monetary_real_report_faws.rename(forest_short).to_csv(self.statistic_monetary_faws.format(year=year))
                monetary_real_report_owl_aws.rename(forest_short).to_csv(
                    self.statistic_monetary_OWL_AWS.format(year=year))
                monetary_real_report_rmov_fnaws.rename(forest_short).to_csv(
                    self.statistic_monetary_rmov_fnaws.format(year=year))
                monetary_real_report_rmov_other.to_csv(
                    self.statistic_monetary_rmov_other.format(year=year))

                monetary_nominal_report_faws.rename(forest_short).to_csv(
                    self.statistic_monetary_nominal_faws.format(year=year))
                monetary_nominal_report_owl_aws.rename(forest_short).to_csv(
                    self.statistic_monetary_nominal_OWL_AWS.format(year=year))
                monetary_nominal_report_rmov_fnaws.rename(forest_short).to_csv(
                    self.statistic_monetary_nominal_rmov_fnaws.format(year=year))
                monetary_nominal_report_owl_aws.to_csv(
                    self.statistic_monetary_nominal_rmov_other.format(year=year))
                logger.debug('EU aggregate [million EUR]:\n%s', supply_table_monetary_faws.sum(axis=0))
                logger.debug('EU aggregate [million EUR]:\n%s', supply_table_monetary_owl_aws.sum(axis=0))

    def load_for_vol_efa(self):
        """Compile supply of wood in physical terms out various tables from EuroStat dataset for_vol_efa.

        Rules:

        * If available, use FAWS-NAI and FNAWS-removals.
        * If FAWS-NAI not available for (country, year), use the value from FOREST-NAI (=data for total forest).
          Remaining gaps: fill by average of surrounding available data.
        * If FNAWS-removals not available, use 0.
        """
        for_vol_efa = estat.read_tsv(self.config[self.service_name]['for_vol_efa'])

        # Net annual increment values
        nai_faws = for_vol_efa.loc[(NAI, FAWS, THS_M3)]
        nai_owl_aws = for_vol_efa.loc[(NAI, OWL_AWS, THS_M3)]
        nai_for = for_vol_efa.loc[(NAI, FOR, THS_M3)]

        # removal values
        rmov_other = for_vol_efa.loc[(RMOV, OLWTC_AWS, THS_M3)]

        # Reindex so we have at least EU-2020 members + everything in the existing indexes:
        index = estat.eu_members(2020).union(nai_faws.index).union(nai_for.index)
        nai_faws = nai_faws.reindex(index)
        nai_owl_aws = nai_owl_aws.reindex(index)
        nai_for = nai_for.reindex(index)
        rmov_other = rmov_other.reindex(index)

        # for removals from FNAWS, set missing values to 0.
        self.rmov_fnaws = for_vol_efa.loc[(RMOV, FNAWS, THS_M3)].reindex(index).fillna(0)

        # Replace missing NAI FAWS values by corresponding NAI FOR values
        for year in nai_faws.columns:
            missing = nai_faws[year].isna()
            nai_faws.loc[missing, year] = nai_for.loc[missing, year]
        # Values that are still missing: fill in gaps by average of values from surrounding years for the same country
        for geo in nai_faws.index[(nai_faws.isna().any(axis=1))]:
            logger.debug('Fill NAI_FAWS data gaps for %s', geo)
            if nai_faws.loc[geo].isna().all():  # If no values are available for this region, we can't do anything.
                logger.error('No valid NAI of FAWS values for %s.', geo)
                continue
            last_value = None  # last valid value
            missing_years = []
            new_vals = nai_faws.loc[geo].copy()
            for year, value in nai_faws.loc[geo].items():
                if not pd.isna(value):
                    if missing_years:
                        # we see a value after finding missing values -> we are at end of a range of missing values and
                        # should update those previous missing values
                        new_vals[missing_years] = 0.5 * (last_value + value) if last_value else value
                        missing_years = []  # reset list of missing years
                    last_value = value
                else:
                    missing_years.append(year)
            if missing_years:  # series ends with a range of missing values
                new_vals[missing_years] = last_value
            nai_faws.loc[geo] = new_vals

        # Values that are still missing: fill in gaps by average of values from surrounding years for the same country
        # but now for other wooded land
        for geo in nai_owl_aws.index[(nai_owl_aws.isna().any(axis=1))]:
            logger.debug('Fill NAI_FAWS data gaps for %s', geo)
            if nai_owl_aws.loc[geo].isna().all():  # If no values are available for this region, we can't do anything.
                logger.error('No valid NAI of FAWS values for %s.', geo)
                continue
            last_value = None  # last valid value
            missing_years = []
            new_vals = nai_owl_aws.loc[geo].copy()
            for year, value in nai_owl_aws.loc[geo].items():
                if not pd.isna(value):
                    if missing_years:
                        # we see a value after finding missing values -> we are at end of a range of missing values and
                        # should update those previous missing values
                        new_vals[missing_years] = 0.5 * (last_value + value) if last_value else value
                        missing_years = []  # reset list of missing years
                    last_value = value
                else:
                    missing_years.append(year)
            if missing_years:  # series ends with a range of missing values
                new_vals[missing_years] = last_value
            nai_owl_aws.loc[geo] = new_vals

        # Values that are still missing: fill in gaps by average of values from surrounding years for the same country
        # but now for other wooded land
        for geo in rmov_other.index[(rmov_other.isna().any(axis=1))]:
            logger.debug('Fill NAI_FAWS data gaps for %s', geo)
            if rmov_other.loc[geo].isna().all():  # If no values are available for this region, we can't do anything.
                logger.error('No valid NAI of FAWS values for %s.', geo)
                continue
            last_value = None  # last valid value
            missing_years = []
            new_vals = rmov_other.loc[geo].copy()
            for year, value in rmov_other.loc[geo].items():
                if not pd.isna(value):
                    if missing_years:
                        # we see a value after finding missing values -> we are at end of a range of missing values and
                        # should update those previous missing values
                        new_vals[missing_years] = 0.5 * (last_value + value) if last_value else value
                        missing_years = []  # reset list of missing years
                    last_value = value
                else:
                    missing_years.append(year)
            if missing_years:  # series ends with a range of missing values
                new_vals[missing_years] = last_value
            rmov_other.loc[geo] = new_vals

        self.nai_faws = nai_faws
        self.nai_owl_aws = nai_owl_aws
        self.rmov_other = rmov_other


def gap_fill_unit_value_exports(unit_value):
    """Gap fill unit value of exports using member state average or EU average."""
    result = unit_value.copy()
    # unit value of exports eu average: first average per country, then over all countries:
    averages = unit_value[range(2000, 2022)].mean(axis=1)
    eu_average = estat.filter_eu27(averages).mean()
    logger.debug('Unit value exports mean:\n%s', averages)
    logger.debug('Unit value exports eu average:\n%s', eu_average)
    for ms in result.index:
        missing = result.loc[ms].isna()
        ms_average = averages[ms]
        if not pd.isna(ms_average):
            result.loc[ms, missing] = ms_average
        else:
            result.loc[ms, missing] = eu_average
    return result


def gap_fill_ratio_export_nai(export, nai):
    """Gap fill ratio of unit value of export / NAI using member state or EU average."""
    ratio = (export / nai).replace(np.inf, np.nan).copy()
    logger.debug('Ratio export/nai:\n%s', ratio.loc[:, range(2011, 2022)])
    result = ratio.copy()
    averages = ratio[range(2000, 2022)].mean(axis=1)
    # ratio export/nai eu average: first average over all member states per year, then over all years, for years from
    # 2012 onwards:
    eu_average = estat.filter_eu27(ratio[range(2012, 2022)]).mean().mean()
    logger.debug('ratio export/nai averages:\n%s\neu average: %s', averages, eu_average)
    # Replace missing values by member state's average value, or by eu average if no data is available for that MS.
    for ms in result.index:
        missing = result.loc[ms].isna()
        ms_average = averages[ms]
        if not pd.isna(ms_average):
            result.loc[ms, missing] = ms_average
        else:
            result.loc[ms, missing] = eu_average
    return result
