"""
This is the prototype for allocating the yield of the croptype-specific SDA to the different nesting areas in the SPA

currently we take two different pollination groups into account: short and long flying pollinators

INPUT:
 - SPA for the different pollinator groups (normally prepared by INCA tool - for this test prepared by hand:
              SPA resampled to 100m, warped to EPSG:3035 and cut to extent in folder 'reference_data' which was
              produced by INCA tool :) )
 - ecosystem types L1 --> normally prepared by INCA tool --> create from CORINE ACC layers from 2018
 - reporting shape and statistics raster for BE (NUTS-0 and NUTS-2) prepared by INCA-tool for year 2018
 - yield maps for pollinator dependant crops (currently only CAPRI classes) for year 2018 in t/ha (IMPORTANT)
 - LUT for the crop-specific pollination dependency on the yield (here for CAPRI classes)

Note: this prototype is not using any block processing. in final script we need block processing so make sure to
       load bufferes etc when running delination or yield assignment via moving window

"""

import os
import numpy as np
import rasterio
from rasterio.windows import Window
from scipy.ndimage.morphology import binary_dilation
from inca.common.ecosystem import ECO_ID, CROP, types_l1
from inca.common.geoprocessing import block_window_generator, number_blocks


def footprint_disc(radius):
    """ round dilineation kernel
    could be replaced with scikit-image if we can use it in INCA-tool
    """
    kernel = np.zeros((2 * radius + 1, 2 * radius + 1))
    y, x = np.ogrid[-radius:radius + 1, -radius:radius + 1]
    mask = x ** 2 + y ** 2 <= radius ** 2
    kernel[mask] = 1
    return kernel


def delinate_Pollination_Areas(group, path_SPA, crop_potential_map, flying_pollinators_map, path_eco_L1, croptypemap, path_extent, df_LUT, pYear, radius, accord):
    #making sure raduis is an int
    '''if os.path.exists(flying_pollinators_map):
        return
    '''
    radius = int(radius)
    block_shape = (4096,4096)
    if croptypemap is None or croptypemap == '':
        croptypemap = path_eco_L1
    with rasterio.open(path_SPA) as src, rasterio.open(croptypemap) as lc, rasterio.open(path_extent) as extent:
        cpm_profile = lc.profile
        cpm_profile.update(tiled=True,
                           compress='LZW',
                           blockysize=256,
                           blockxsize=256)
        dst_profile = cpm_profile
        dst_profile.update(dtype=rasterio.float32, nodata=np.nan)
        nbr_blocks = number_blocks(src.profile, block_shape)
        with rasterio.open(flying_pollinators_map, 'w', **dst_profile) as dst, rasterio.open(crop_potential_map,'w',**cpm_profile) as CPM:
            accord.metadata.update_dataset_tags(dst, f'areas the {group} flying pollinators can reach in the SDA.',
                                                'bool', path_SPA, path_eco_L1, path_extent)
            for _, window in block_window_generator(block_shape, src.height, src.width):

                # calc amout of padding:
                window = Window.from_slices(rows=window[0], cols=window[1])
                # add padding
                PaddedWindow = Window(window.col_off - radius, window.row_off - radius,
                                      window.width + radius*2, window.height + radius*2)

                #read in data
                aData = src.read(1,window=PaddedWindow, boundless=True, masked=True)
                aLC = lc.read(1,window=PaddedWindow, boundless=True, masked=True).astype(dst_profile["dtype"])
                aExtent = extent.read(1,window=PaddedWindow, boundless=True)

                # create binary array for fast dilination
                aSPA = np.zeros_like(aData, dtype=bool)
                aSPA[aData == 1] = True

                # buffer
                aBuffer = binary_dilation(aSPA, footprint_disc(radius)).astype(np.float32)



                if croptypemap == path_eco_L1 :
                    # now we limit to areas which can supply (here only agriculture areas) in statistical areas
                    mask = np.isin(aLC, types_l1[types_l1.name.isin([CROP])][ECO_ID].tolist())
                else:
                    mask = np.isin(aLC, df_LUT[df_LUT['dependency_factor'] >0]['lc_class'].tolist())

                mask[aExtent == 0] = False
                #write out CPM
                aLC[~mask] = CPM.nodata
                CPM.write(aLC[radius:window.height+radius,radius:window.width+radius], 1, window=window)

                aBuffer[~mask] = 0

                #write to disk
                dst.write(aBuffer[radius:window.height+radius,radius:window.width+radius], 1, window=window)


def usable_crop_production(crop_type, path_in, crops_total_yield, croptypemap, df_LUT, path_out, accord, ratio, pYear, path_eco_L1, path_extent):
    """ converts the yield in t/ha into absolute production per pixel and apply the pollination ratio
    Note: we also mask out non statistical areas and non-agriculture areas - extent is base
    """
    # maps are in t/ha --> need pixel_size in ha
    px_factor = accord.pixel_area_m2() / 10000.

    if croptypemap is None or croptypemap == '':
        croptypemap = path_eco_L1

    if path_in is not None and path_in != '':
        yield_file = path_in
    else:
        yield_file = crops_total_yield
    block_shape = (2048,2048)
    with rasterio.open(yield_file) as src, rasterio.open(croptypemap) as lc, rasterio.open(path_extent) as extent:
        dst_profile = accord.ref_profile.copy()
        dst_profile.update(dtype=src.profile['dtype'], nodata=src.nodata)
        nbr_blocks = number_blocks(accord.ref_profile, block_shape)

        with rasterio.open(path_out, 'w', **dst_profile) as dst:
            accord.metadata.update_dataset_tags(dst, f'{crop_type} harvest share in tonnes per pixel which is dependent on '
                                                     f'pollination for the year {pYear}. Dependency factor of {ratio}.',
                                                'tonnes', yield_file)

            for _, window in block_window_generator(block_shape, src.height, src.width):


                #read in data
                aData = src.read(1,window=window, masked=True)
                aLC = lc.read(1,window=window, masked=True)
                aExtent = extent.read(1,window =window)

                # convert from t/ha to absolute and apply pollination dependency factor --> harvest which is due to pollination
                aData = aData * px_factor * ratio

                if croptypemap == path_eco_L1 :
                    # now we limit to areas which can supply (here only agriculture areas) in statistical areas
                    mask = np.isin(aLC, types_l1[types_l1.name.isin([CROP])][ECO_ID].tolist())
                else:
                    mask = np.isin(aLC, df_LUT.loc[crop_type,'lc_class'].tolist())

                mask[aExtent == 0] = False
                aData[~mask] = dst_profile['nodata']
                dst.write(aData.filled(dst_profile['nodata']), 1,window=window)


def Allocate_2_nesting_site(group, crop_type, dPollination_masks, SPA, crop_harvest, pYear, path_out_root, accord, flying_radius):
    """ allocating pollination harvest share back from agriculture area to the nesting sites"""
    # create mask which specify the harvest the pollinator can reach
    path_out = os.path.join(path_out_root, f'crop-pollination_supply_{group}-flying-pollinators_{crop_type}_{pYear}.tif')
    path_out2 = os.path.join(path_out_root, f'crop-pollination_use_{group}-flying-pollinators_{crop_type}_{pYear}.tif')
    path_out3 = os.path.join(path_out_root, f'crop-pollination_unmet_{group}-flying-pollinators_{crop_type}_{pYear}.tif')
    path_out4 = os.path.join(path_out_root, f'crop-pollination_demand_{group}-flying-pollinators_{crop_type}_{pYear}.tif')
    #return path_out, path_out2, path_out3, path_out4
    dst_profile = accord.ref_profile.copy()
    dst_profile.update(dtype=rasterio.float64, nodata=np.nan)
    Done =0
    block_shape = (2048,2048)

    with rasterio.open(dPollination_masks['short'][pYear]) as src_short, rasterio.open(crop_harvest) as src_crop, \
            rasterio.open(SPA) as src_SPA, rasterio.open(path_out, 'w', **dst_profile) as dst_supply, \
            rasterio.open(path_out2, 'w', **dst_profile) as dst_use , \
            rasterio.open(path_out3, 'w', **dst_profile) as dst_unmet, \
            rasterio.open(path_out4, 'w', **dst_profile) as dst_demand :

        if group == 'short':
            accord.metadata.update_dataset_tags(dst_supply, f'Pollination dependent harvest of {crop_type} allocated to the '
                                                     f'nesting sites of {group} flying pollinators for the year {pYear}.',
                                                'tonnes', SPA, crop_harvest, dPollination_masks['short'][pYear])
            accord.metadata.update_dataset_tags(dst_use, f'Harvest of {crop_type} allocated to {group} flying pollinators '
                                                     f'for the year {pYear}.',
                                                'tonnes', crop_harvest, dPollination_masks['short'][pYear])
            accord.metadata.update_dataset_tags(dst_unmet, f'Unmet demand of {crop_type} allocated to {group} flying pollinators '
                                                         f'for the year {pYear}.',
                                                'hectares', crop_harvest, dPollination_masks['short'][pYear])
            accord.metadata.update_dataset_tags(dst_demand, f'demand of {crop_type} allocated to {group} flying pollinators '
                                                           f'for the year {pYear}.',
                                                'hectares', crop_harvest, dPollination_masks['short'][pYear])
        else:
            accord.metadata.update_dataset_tags(dst_supply, f'Pollination dependent harvest of {crop_type} allocated to the '
                                                     f'nesting sites of {group} flying pollinators for the year {pYear}.',
                                                'tonnes', SPA, crop_harvest, dPollination_masks['short'][pYear],
                                                dPollination_masks['long'][pYear])
            accord.metadata.update_dataset_tags(dst_use, f'Harvest of {crop_type} allocated to {group} flying pollinators '
                                                     f'for the year {pYear}.',
                                                'tonnes', crop_harvest, dPollination_masks['short'][pYear],
                                                dPollination_masks['long'][pYear])
            accord.metadata.update_dataset_tags(dst_unmet, f'Unmet demand of {crop_type} allocated to {group} flying pollinators '
                                                         f'for the year {pYear}.',
                                                'hectares', crop_harvest, dPollination_masks['short'][pYear],
                                                dPollination_masks['long'][pYear])
            accord.metadata.update_dataset_tags(dst_demand, f'demand of {crop_type} allocated to {group} flying pollinators '
                                                            f'for the year {pYear}.',
                                                'hectares', crop_harvest, dPollination_masks['short'][pYear],
                                                dPollination_masks['long'][pYear])
        if group == 'long':
            src_long =  rasterio.open(dPollination_masks['long'][pYear])

        for _, window in block_window_generator(block_shape, src_short.height, src_short.width):

            # calc amout of padding:
            window = Window.from_slices(rows=window[0], cols=window[1])
            # add padding
            PaddedWindow = Window(window.col_off - flying_radius, window.row_off - flying_radius,
                                  window.width + flying_radius*2, window.height + flying_radius*2)
            DoublePaddedWindow = Window(window.col_off - 2*flying_radius, window.row_off - 2*flying_radius,
                                        window.width + flying_radius*4, window.height + flying_radius*4)

            if group == 'short':
                aReachable = src_short.read(1, window = PaddedWindow, boundless=True, fill_value = 0) == 1
            elif group == 'long':
                aPart1 = src_short.read(1, window = PaddedWindow, boundless=True, fill_value = 0)
                aReachable = src_long.read(1, window = PaddedWindow, boundless=True, fill_value = 0) == 1
                # since we running ecological niche approach - the areas of the short flying pollinators has to be subtracted
                # from the long flying ones
                aReachable[aPart1 == 1] = False
                aPart1 = None
            else:
                raise ValueError(f'this pollinator group ({group}) is currently not defined.')

            # get the crop_harvest and reduce to area which brings back to nesting sites
            aHarvest = src_crop.read(1, window = PaddedWindow, boundless=True, masked=True).astype(np.float64)

            aHarvest = aHarvest.filled(0)
            aUnmet = (aHarvest !=0).astype(int)*accord.pixel_area_m2() / (100.**2)
            aDemand = (aHarvest !=0).astype(int)*accord.pixel_area_m2() / (100.**2)

            aHarvest[~aReachable] = 0
            aUnmet[aReachable] = 0
            ToDo = aHarvest[flying_radius:-flying_radius, flying_radius:-flying_radius].sum()
            final_mask = src_SPA.read(1, window=PaddedWindow, boundless=True, masked=True).mask
            # free
            aReachable = None

            if ToDo != 0:
                aAlloPattern = footprint_disc(flying_radius).astype(bool)
                # reset the center pixel since that is the harvest we have to allocate and can not be a nesting site
                # ToDO: check if given pixel can be a nesting site --> remove next line
                #aAlloPattern[flying_radius, flying_radius] = False #--> this can cause problems ie supply =! use

                # get the SPA area in which to look for nesting area
                aNesting_padded = src_SPA.read(1, window =  DoublePaddedWindow, boundless=True, fill_value = 0 ) == 1
                # now comes the allocation process
                aResult = np.zeros_like(aNesting_padded, dtype=rasterio.float64)
                # loop over harvest array
                #TODO this can be replaced by similar non loop function from AF
                print(f'*** we have {ToDo} tonnes to allocate for {crop_type}')
                for y in range(aHarvest.shape[0]):
                    for x in range(aHarvest.shape[1]):
                        # check if we have a harvest to allocate
                        if aHarvest[y, x] == 0:
                            continue
                        # get window in nesting array
                        aAllocate = aNesting_padded[y:y+(2*flying_radius)+1, x:x+(2*flying_radius)+1]

                        # check how many valid nesting sites we have in flying distance
                        numNesting = aAllocate[aAlloPattern].sum()

                        # calculate harvest share
                        share = aHarvest[y, x] / float(numNesting)

                        # allocate to active nesting sites
                        iValid = (aAllocate == 1) & (aAlloPattern == 1)
                        aResult[y:y + (2 * flying_radius) + 1, x:x + (2 * flying_radius) + 1][iValid] += share

                # remove padding from result
                aResult = aResult[flying_radius:-flying_radius, flying_radius:-flying_radius]
                Done = aResult[flying_radius:-flying_radius, flying_radius:-flying_radius].sum()
                print(f'*** we have allocate {Done} tonnes of {crop_type} to nesting sites')
            else:
                aResult = np.zeros_like(final_mask, dtype=rasterio.float64)
            # write out
            aResult[final_mask] = np.nan
            aHarvest[final_mask] = np.nan
            aUnmet[final_mask] = np.nan
            #write to disk
            # first SUPPLY
            dst_supply.write(aResult[flying_radius:-flying_radius, flying_radius:-flying_radius].astype(np.float64), 1,window=window)

            # now USE
            dst_use.write(aHarvest[flying_radius:-flying_radius, flying_radius:-flying_radius].astype(np.float64), 1,window=window)

            # now unmet : unreachable pixels
            dst_unmet.write(aUnmet[flying_radius:-flying_radius, flying_radius:-flying_radius].astype(np.float32), 1,window=window)

            # now demand : unreachable pixels
            dst_demand.write(aDemand[flying_radius:-flying_radius, flying_radius:-flying_radius].astype(np.float32), 1,window=window)

        #fprint(f'*** we have allocate {Done} tonnes of {crop_type} to nesting sites')

    return path_out, path_out2, path_out3, path_out4
