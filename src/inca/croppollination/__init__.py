"""Crop Pollination supply / use calculation."""
import logging
import os
from contextlib import ExitStack
from itertools import zip_longest
import pandas as pd
import rasterio
from rasterio.windows import Window
import numpy as np
from scipy.ndimage import convolve

import inca
import inca.common.ecosystem as ecosystem
import inca.common.suts as suts
from inca.croppollination.processing_flights import (delinate_Pollination_Areas, usable_crop_production,
                                                     Allocate_2_nesting_site)
from inca.common.config_check import ConfigRasterDir, ConfigItem, check_csv, check_tsv, ConfigRaster, YEARLY
from inca.common.errors import ConfigError
from inca.common.classification import reclassification, CSV_2_dict
from inca.common.geoprocessing import statistics_byArea_byET, block_window_generator, RasterType, SUM
from inca.common.ecosystem import SHAPE_ID
from inca.common.nuts import NUTS_ID
import inca.data

logger = logging.getLogger(__name__)

_CP = 'Crop Pollination'
_RADIATION = 'monthly_radiation'
_DAYLENGTH = 'monthly_daylength'
_TEMPERATURE = 'monthly_mean_temperature'
_SUMMER = 'summer_months'
_THEMATIC_SCALER = 'osm_mask'
block_shape =(2048, 2048)

_CROPS = ['APPL','CITR','OFRU','OOIL','PULS','RAPE','SOYA','SUNF','TEXT','TOMA']

POLLINATOR_CROPS = {'short': ['APPL', 'CITR', 'OFRU', 'OOIL', 'PULS', 'RAPE', 'SOYA', 'SUNF', 'TEXT', 'TOMA'],
                    'long': ['APPL', 'CITR', 'OFRU', 'OOIL', 'PULS', 'RAPE', 'SOYA', 'SUNF', 'TEXT', 'TOMA']}

def set_cmdline_args(parser):
    """Additional command line arguments for the Air Filtration.

    :param parser: subparser object for Air Filtration.
    """
    pass

class PollinationRun(inca.Run):
    service_name = _CP

    def __init__(self, config):
        super().__init__(config)
        self.config_template.update({
            self.service_name: {'lc_table' : ConfigItem(check_csv, required_columns=['Value','Forest','FA','NS','Edge_NS','Edge_FA']),
                                'meteo_data' : {_RADIATION : {YEARLY : ConfigRasterDir(raster_type=RasterType.RELATIVE)},
                                               _DAYLENGTH : {YEARLY : ConfigRasterDir(raster_type=RasterType.ABSOLUTE_POINT)},
                                               _TEMPERATURE : {YEARLY : ConfigRasterDir(raster_type=RasterType.ABSOLUTE_POINT)}
                                               },
                                _THEMATIC_SCALER: {YEARLY : ConfigRaster(optional=True, raster_type=RasterType.CATEGORICAL)},
                                'SGSI' : {'short' : {YEARLY :ConfigRaster(optional=False, raster_type=RasterType.ABSOLUTE_POINT)},
                                               'long' : {YEARLY : ConfigRaster(optional=True, raster_type=RasterType.ABSOLUTE_POINT)}},
                                'path_LUT_contribution' : ConfigItem(check_csv, required_columns=['CAPRI_code','dependency_factor']),
                                'croptypemap' : {YEARLY : ConfigRaster(optional=True, raster_type=RasterType.CATEGORICAL)},
                                'total_yield' : {YEARLY : ConfigRaster(optional=True, raster_type=RasterType.RELATIVE)},
                                'parameters': {'long': {'SGSI_weight' : ConfigItem(optional=True, default=0.5),
                                                        'SPA_threshold': ConfigItem(optional=True, default=0.2),
                                                        'distance':  ConfigItem(optional=True, default=1000),
                                                        'edge_distance':  ConfigItem(optional=True, default=100),
                                                        _SUMMER: ConfigItem(optional=True, default= [4,5,6,7,8,9])},
                                             'short': {'SGSI_weight' : ConfigItem(optional=True, default=0.5),
                                                       'SPA_threshold': ConfigItem(optional=True, default=0.2),
                                                       'distance': ConfigItem(optional=True, default=200),
                                                       'edge_distance':  ConfigItem(optional=True, default=100),
                                                       _SUMMER: ConfigItem(optional=True, default= [4,5,6,7,8,9])}
                                               },
                                'path_yields' : {YEARLY : {crop : ConfigRaster(optional=True, raster_type=RasterType.RELATIVE) for crop in  _CROPS }}

                                }})
        if self.monetary:  # Inputs used only for monetary evaluation:
            logger.debug('Add config_template for monetary')
            self.config_template[self.service_name].update({
                'AACT_UV': ConfigItem(check_tsv),
                'LUT_CAPRI_ESTAT_codes': ConfigItem(check_csv, required_columns=['CAPRI_code'])})


    def _start(self):
        self.make_output_filenames()
        """Generate the croppollination account"""
        aggregate_maps = self.collect_input_maps()
        df_LUT = pd.read_csv(self.config[_CP]['path_LUT_contribution'], comment='#').set_index('CAPRI_code')
        if self.monetary:
            self.prep_monetary()
        for year in self.years:

            logger.info(f'* run processing for year: {year}')

            #2.1  calculate forest edges
            for guild in self.config[_CP]['SGSI']:
                logger.info(f'** run processing for {guild} flying pollinators')
                if self.config[_CP]['SGSI'][guild][year]:

                    logger.info(f'** run edges processing')
                    self.calculate_edges(year, guild)

                    #2.4 calc FAI
                    logger.info(f'** calculating FAI')
                    self.calculate_FAI(aggregate_maps, year, guild)

                    #2.2 + 2.3 +2.5
                    logger.info(f'** calculating EBMI')
                    self.calculate_EBMI(year, guild)
                    
                    #2.6 is in preprocessing maxent SGSI input
                    self.calculate_SPA(guild, year)

                    # 3.1 + 4.1b create a mask for flying distance from SPA per pollinator group
                    # calculate kernel size in pixel from distance --> currently only works for square pixels and projected coordinate systems
                    radius = np.round(self.config[_CP]["parameters"][guild]["distance"]/ np.sqrt(self.accord.pixel_area_m2()), 0)

                    delinate_Pollination_Areas(guild,
                                                self.path_out_SPA[guild][year],
                                                self.crop_potential_map[guild][year],
                                                self.flying_pollinators[guild][year],
                                                self.get_ecosystem_raster(year),
                                                self.config[_CP]['croptypemap'][year],
                                                self.statistics_raster,
                                                df_LUT,
                                                year,
                                                radius,
                                                self.accord)


            # int_processing (or 3). convert yield maps from t/ha into absolute yield maps (t per pixel) and multiply with crop specific
            # pollination dependancy --> that is the absolute yield the pollinators can bring back to their nesting sites
            
            for element in _CROPS:
                logger.info(f'* create absolute crop production usable by pollinators: {element}')
                usable_crop_production(element,
                                        self.config[_CP]['path_yields'][year].get(element,None),
                                        self.config[_CP]['total_yield'][year],
                                        self.config[_CP]['croptypemap'][year],
                                        df_LUT,
                                        self.pollinated_harvest_share[year][element],
                                        self.accord,
                                        df_LUT.loc[element, 'dependency_factor'],
                                        year,
                                        self.get_ecosystem_raster(year),
                                        self.statistics_raster)


            # 4.2. assign for each SPA pixel the yield from a reachable SDA pixel (SBA areas) of the different pollinator groups

            for guild in self.config[_CP]['SGSI']:
                if self.config[_CP]['SGSI'][guild][year]:
                    print(f'* allocating harvest contribution to SPA for {guild} flying pollinators')
                    radius = int(np.round(self.config[_CP]["parameters"][guild]["distance"] / np.sqrt(self.accord.pixel_area_m2()), 0))
                    for crop_type in POLLINATOR_CROPS[guild]:
                        print(f'** running crop type: {crop_type}')

                        self.pollination_flow[year][crop_type][guild]['supply'], \
                        self.pollination_flow[year][crop_type][guild]['use'], \
                        self.pollination_flow[year][crop_type][guild]['unmet'], \
                        self.pollination_flow[year][crop_type][guild]['demand']   = Allocate_2_nesting_site(guild,
                                                                                             crop_type,
                                                                                             self.flying_pollinators,
                                                                                             self.path_out_SPA[guild][year],
                                                                                             self.pollinated_harvest_share[year][crop_type],
                                                                                             year,
                                                                                             self.temp_dir,
                                                                                             self.accord,
                                                                                             radius)

                        #calculate from biophysical values monetery values
                        if self.monetary:
                            self.monetary_evalution(year, crop_type, guild)


            #create master files suplly and use (unmet is not so straight forward)
            for output in ['supply','use','unmet', 'demand']:
                with rasterio.open(self.pollination_flow[year][POLLINATOR_CROPS['short'][0]]['short'][output]) as src:
                    dst_profile = src.profile
                with ExitStack() as stack, \
                        rasterio.open(self.master[year][output], 'w', **dst_profile) as ds_out:
                    if output in ['unmet','demand'] and self.config[_CP]['SGSI']['long'][year]:
                        long_dss = [stack.enter_context(rasterio.open(self.pollination_flow[year][crop_type]['long'][output])) \
                                    for crop_type in POLLINATOR_CROPS['long'] ]
                        short_dss = [stack.enter_context(rasterio.open(self.pollination_flow[year][crop_type]['short'][output])) \
                                    for crop_type in POLLINATOR_CROPS['short'] ]
                        total_len = len(list(set(POLLINATOR_CROPS['long'] +  POLLINATOR_CROPS['short'])))
                        for _, window in block_window_generator(block_shape, long_dss[0].height, long_dss[0].width):
                            data = np.ma.zeros((total_len, window[0][1] - window[0][0], window[1][1] - window[1][0]))


                            for idx,crop_type_long in enumerate(POLLINATOR_CROPS['long']):
                                if crop_type_long not in POLLINATOR_CROPS['short']:
                                    data[idx] = long_dss[idx].read(1,window =window, masked=True)
                                    continue
                                for idy ,crop_type_short in enumerate(POLLINATOR_CROPS['short']):
                                    if crop_type_short == crop_type_long:
                                        aLong =long_dss[idx].read(1,window =window, masked=True)
                                        aShort = short_dss[idy].read(1,window=window, masked=True)
                                        if output == 'unmet':
                                            data[idx] = np.minimum(aLong,aShort)
                                        elif output == 'demand':
                                            data[idx] = np.maximum(aLong,aShort)
                            extra_crop = 0
                            for idy ,crop_type_short in enumerate(POLLINATOR_CROPS['short']):
                                if crop_type_short not in POLLINATOR_CROPS['long']:
                                    extra_crop += 1
                                    data[-extra_crop] = short_dss[idy].read(1,window=window)


                            data_sum = np.ma.sum(data, axis=0)

                            ds_out.write(data_sum, 1, window = window)

                    else:                       
                        dss = [stack.enter_context(rasterio.open(self.pollination_flow[year][crop_type][guild][output])) \
                                   for guild in self.config[_CP]['SGSI']  for crop_type in POLLINATOR_CROPS[guild] \
                                    if self.config[_CP]['SGSI'][guild][year]]
                        for _, window in block_window_generator(block_shape, dss[0].height, dss[0].width):
                            data = np.zeros((len(dss), window[0][1] - window[0][0], window[1][1] - window[1][0]))
                            for idx, ds in enumerate(dss):
                                data[idx] = ds.read(1, window=window, masked=True)
                            data_sum = np.ma.sum(data, axis=0)
                            ds_out.write(data_sum, 1, window = window)

                        if self.monetary:
                            for product  in ['real','current']:
                                with stack.enter_context(rasterio.open(self.master[year][f'{output}-{product}'], 'w', **dst_profile)) as ds_out2:
                                    dss = [stack.enter_context(rasterio.open(self.pollination_monetary[year][crop_type][guild][f'{output}-{product}'])) \
                                           for guild in self.config[_CP]['SGSI']  for crop_type in POLLINATOR_CROPS[guild] \
                                           if self.config[_CP]['SGSI'][guild][year]]
                                    for _, window in block_window_generator(block_shape, dss[0].height, dss[0].width):
                                        data = np.zeros((len(dss), window[0][1] - window[0][0], window[1][1] - window[1][0]))
                                        for idx, ds in enumerate(dss):
                                            data[idx] = ds.read(1, window=window, masked=True)
                                        data_sum = np.ma.sum(data, axis=0)
                                        ds_out2.write(data_sum, 1, window = window)

            # 4.3.  and 5.1 supply extract the use for each ecosystem type (should be the use side, since the supply comes from cropland)
            dStatistics = {}
            for guild in self.config[_CP]['SGSI']:
                if self.config[_CP]['SGSI'][guild][year]:
                    print(f'* extracting statistics for {guild} flying pollinators')
                    dStatistics[guild] = {}
                    for crop_type in POLLINATOR_CROPS[guild]:
                        print(f'** running crop type: {crop_type}')
                        dStatistics[guild][crop_type] = {}

                        for product  in ['supply','use','unmet'] : #,'demand']:
                            df = statistics_byArea_byET(self.pollination_flow[year][crop_type][guild][product],
                                                        self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                        self.get_ecosystem_raster(year),
                                                        ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                                        block_shape=(4096, 4096))
                            dStatistics[guild][crop_type][product] = df[[SUM]].unstack(1)[SUM]


                        if self.monetary:
                            for product  in ['supply-real','use-real','supply-current','use-current']:
                                df = statistics_byArea_byET(self.pollination_monetary[year][crop_type][guild][product],
                                                            self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                            self.get_ecosystem_raster(year),
                                                            ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                                            block_shape=(4096, 4096))
                                dStatistics[guild][crop_type][product] = df[[SUM]].unstack(1)[SUM]

                        dic_short = ecosystem.types_l1.set_index('name')['short_name'].to_dict()
                        for product  in ['supply','use','unmet']:#,'demand']:
                            dStatistics[guild][crop_type][product].rename(columns=dic_short).to_csv(self.csvs[year][crop_type][guild][product],
                                                                                                    columns=ecosystem.types_l1['short_name'].tolist(),
                                                                                                    index_label=NUTS_ID, float_format='%.3f')

                        if self.monetary:
                            for product  in ['supply-real','use-real','supply-current','use-current']:
                                dStatistics[guild][crop_type][product].rename(columns=dic_short).to_csv(self.csvs[year][crop_type][guild][product],
                                                                                                         columns=ecosystem.types_l1['short_name'].tolist(),
                                                                                                         index_label=NUTS_ID, float_format='%.3f')


            # 6. create the SUT
            """
            Note: due to the distance model the values per area for supply and use do not have to be the same!!!
            How to do it: take the statistics of the supply by ET and write to the supply table
                          take the statistics of the use (only agriculture ET group) and allocate it to INDUSTRY on the use site
                          --> the TOTAL should match overall processed areas (in this example over Belgium) which is not the case.
            """
            #convert statisctics
            total_use = {}
            total_use['use'] = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'])
            if self.monetary:
                total_use['use-current'] = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'])
                total_use['use-real'] = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'])
            for key in total_use.keys():
                for guild in self.config[_CP]['SGSI']:
                    if self.config[_CP]['SGSI'][guild][year]:
                        use = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'])
                        print(f'* extracting statistics for {guild} flying pollinators')
                        for crop_type in POLLINATOR_CROPS[guild]:
                            df = dStatistics[guild][crop_type][key].copy()
                            df[crop_type] = df.sum(axis =1)
                            use = use.join(df[crop_type])
                        total_use[key] = use.add(total_use[key], fill_value = 0)

            total_supply = {}
            total_supply['supply'] = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'], columns =ecosystem.types_l1['name'])
            if self.monetary:
                total_supply['supply-current'] = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'], columns =ecosystem.types_l1['name'])
                total_supply['supply-real'] = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'], columns =ecosystem.types_l1['name'])
            for key in total_supply.keys():
                for guild in self.config[_CP]['SGSI']:
                    if self.config[_CP]['SGSI'][guild][year]:
                        supply = pd.DataFrame(index=self.reporting_shape['CNTR_CODE'], columns =ecosystem.types_l1['name'])
                        print(f'* extracting statistics for {guild} flying pollinators')
                        for crop_type in POLLINATOR_CROPS[guild]:
                            df = dStatistics[guild][crop_type][key].copy()
                            supply = supply.add(df, fill_value=0)
                        total_supply[key] = supply.add(total_supply[key], fill_value = 0)


            suts.write_fuzzy_sut(self.SUT_physical_path.format(year=year), total_supply['supply'],
                                 total_use['use'] , self.service_name,
                           year=year, unit=f'tonnes')
            if self.monetary:
                suts.write_fuzzy_sut(self.SUT_montary_real_path.format(year=year), total_supply['supply-real']/1000000.,
                                     total_use['use-real']/1000000. , self.service_name,
                                     year=year, unit=f'Million EUR (2000 value)')
                suts.write_fuzzy_sut(self.SUT_montary_current_path.format(year=year), total_supply['supply-current']/1000000.,
                                     total_use['use-current']/1000000. , self.service_name,
                                     year=year, unit=f'Million EUR (nominal)')

            #convert statiscitcs to MFA
            cropprov_df = pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(inca.data.__file__)),'cropprovision_lut.csv'),usecols=['code_CAPRI','label_mfa'])
            total_use_MFA = {}
            for key in total_use.keys():
                columns1 = list(total_use[key].columns)
                columns2 = [list(cropprov_df[cropprov_df['code_CAPRI']==column]['label_mfa'])[0] for column in columns1]
                tuples = list(zip(columns1,columns2))
                total_use[key].columns = pd.MultiIndex.from_tuples(tuples,names = ['capri','mfa'])
                total_use_MFA[key] = total_use[key].T.groupby(level='mfa').sum().T

            suts.write_fuzzy_sut(self.SUT_physical_path_MFA.format(year=year), total_supply['supply'],
                                 total_use_MFA['use'] , self.service_name,
                                 year=year, unit=f'tonnes')
            if self.monetary:
                suts.write_fuzzy_sut(self.SUT_montary_real_path_MFA.format(year=year), total_supply['supply-real']/1000000.,
                                     total_use_MFA['use-real']/1000000. , self.service_name,
                                     year=year, unit=f'Million EUR (2000 value)')
                suts.write_fuzzy_sut(self.SUT_montary_current_path_MFA.format(year=year), total_supply['supply-current']/1000000.,
                                     total_use_MFA['use-current']/1000000. , self.service_name,
                                     year=year, unit=f'Million EUR (nominal)')


            '''
            # temporary PATCH, write a single multi-crop table per NUTS_ID
            logger.info(' * Write SUT xlsx per NUTS_ID with MFA rows')
            for NUTS in total_supply['supply'].index.get_level_values(0).unique().to_list():
                logger.info('   - SUT Excel with crop types for region {}'.format(NUTS))
                df2 = total_supply['supply'].xs(NUTS)
                df3 = total_use['use'].xs(NUTS)
                for idx, row in types_l1.iterrows():
                    if not row.ecosystem_id in df2.columns:
                        df2[[row['ecosystem_id']]] = 0
                    df2.rename(columns={row['ecosystem_id']:row['name']},inplace=True)

                suts.write_sut(os.path.splitext(self.SUT_physical_path[self.year])[0]+'_'+NUTS+'.xlsx',
                               df2,
                               df2.sum(axis=1).rename(suts.IND),
                               self.service_name, self.year, '1000 tonnes', region=NUTS)
            '''


    def calculate_edges(self, year, guild):
        #might be interesint to chunk
        radius = int(np.round(self.config[_CP]["parameters"][guild]["edge_distance"]/ np.sqrt(self.accord.pixel_area_m2()), 0))
        with rasterio.open(self.config["land_cover"][year]) as src :
            profile = src.profile
            with rasterio.open(self.forest_edges[guild][year], 'w', **profile) as dst:
                for _, window in block_window_generator(block_shape, profile["height"], profile["width"]):
                    # calc amout of padding:
                    window = Window.from_slices(rows=window[0], cols=window[1])
                    # add padding
                    PaddedWindow = Window(window.col_off - radius, window.row_off - radius,
                                          window.width + radius*2, window.height + radius*2)
                    data = src.read(1, window = PaddedWindow)  # Reading a single band (assuming it's a single-band raster)



                    # Reclassify input Land Cover to extract forest types.
                    # Importing table with LC codes and scores
                    dict_classes = CSV_2_dict(self.config[_CP]['lc_table'],'Value','Forest')

                    # Reclassify raster
                    reclass, dict_classes = reclassification(data, dict_classes, np.nan, 0, outputtype=np.uint8 )
                    data = None

                    # Detecting boundaries (edges) in the forest class 8
                    #bit strange that center pixels is not taken into account, very rare case. but 1-1 comparison with R script


                    kernel = np.ones((radius*2+1, radius*2+1))
                    kernel[radius, radius] = 0
                    # Assuming you have already set a mask if needed
                    edges = convolve(reclass, kernel, mode= 'constant')
                    reclass= None
                    edges[edges == np.sum(kernel)] =  0
                    edges[edges != 0] = 1
                    # Save the edges raster as a GeoTIF
                    dst.write(edges[radius:window.height+radius,radius:window.width+radius], 1, window=window)

    def calculate_FAI(self, input_maps, year, guild):
        # run blockwise processing
        summer = self.config[_CP]['parameters'][guild][_SUMMER]
        with ExitStack() as stack, \
                rasterio.open(self.FAI[guild][year], 'w',
                              **dict(self.accord.ref_profile.copy(), dtype=rasterio.float32, nodata=np.nan,
                                     tiled=True, compress='DEFLATE')) as ds_out:
            temp_dss = [stack.enter_context(rasterio.open(path)) for path in input_maps[_TEMPERATURE][year] if int(os.path.basename(path).split('_')[0]) in  summer ]
            day_dss = [stack.enter_context(rasterio.open(path)) for path in input_maps[_DAYLENGTH][year] if int(os.path.basename(path).split('_')[0]) in  summer ]
            rad_dss = [stack.enter_context(rasterio.open(path)) for path in input_maps[_RADIATION][year] if int(os.path.basename(path).split('_')[0]) in  summer ]
            extent_ds = stack.enter_context(rasterio.open(self.statistics_raster))
            for _, window in block_window_generator(block_shape, temp_dss[0].height, temp_dss[0].width):
                temp_data = np.ma.zeros((len(temp_dss), window[0][1] - window[0][0], window[1][1] - window[1][0]))
                day_data = temp_data.copy()
                rad_data = temp_data.copy()

                for idx, (ds_temp, ds_day, ds_rad) in enumerate(zip_longest(temp_dss, day_dss, rad_dss)):
                    masked_temp = np.ma.masked_values(ds_temp.read(1, window=window, fill_value=ds_temp.nodata),
                                                     ds_temp.nodata)
                    temp_data[idx] = masked_temp * ds_temp.scales + ds_temp.offsets

                    masked_day = np.ma.masked_values(ds_day.read(1, window=window, fill_value=ds_day.nodata),
                                                      ds_day.nodata)
                    day_data[idx] = masked_day * ds_day.scales + ds_day.offsets

                    masked_rad = np.ma.masked_values(ds_rad.read(1, window=window, fill_value=ds_rad.nodata),
                                                     ds_rad.nodata)
                    rad_data[idx] = masked_rad * ds_rad.scales + ds_rad.offsets

                Solar_irr = rad_data/ masked_day*10/36 # (kJ / m2 / day) / (hours_light / day) -> transpose to W:  kJ/H -> 1000/3600 [W]
                BlackGlobe = -0.62 + 1.027 * temp_data + 0.006*Solar_irr
                AI = -39.3 + 4.01*BlackGlobe
                Aggregate = AI.mean(axis=0)

                extent_data = extent_ds.read(1, window=window)
                mask = extent_data == 0
                Aggregate[mask] = ds_out.nodata

                ds_out.write(Aggregate.filled(ds_out.nodata).astype(ds_out.profile['dtype']), window=window, indexes=1)

    def calculate_EBMI(self,year, guild):
        profile_dst = self.accord.ref_profile
        nodata = self.accord.ref_profile['nodata']
        profile_dst['nodata'] = -1.0
        profile_dst['dtype'] = np.float32
        profile_dst['BIGTIFF'] = 'YES'
        scores_FA = CSV_2_dict(self.config[_CP]['lc_table'],'Value','FA')
        scores_NS = CSV_2_dict(self.config[_CP]['lc_table'],'Value','NS')
        scores_Edge_FA = CSV_2_dict(self.config[_CP]['lc_table'],'Value','Edge_FA')
        scores_Edge_NS = CSV_2_dict(self.config[_CP]['lc_table'],'Value','Edge_NS')


        with rasterio.open(self.config['land_cover'][year]) as src, \
                rasterio.open(self.config[_CP][_THEMATIC_SCALER][year]) as scaler_src, \
                rasterio.open(self.FAI[guild][year]) as ai_src, \
                rasterio.open(self.statistics_raster) as extent, \
                rasterio.open(self.forest_edges[guild][year]) as edge_src, \
                rasterio.open(self.EBMI[guild][year], 'w', **profile_dst) as dst:

                for index, window in block_window_generator(block_shape, profile_dst['height'], profile_dst['width']):
                    grid = src.read(window = window)
                    FA, dClasses = reclassification(grid, scores_FA, src.nodata, src.nodata, outputtype=np.float64)
                    NS, dClasses = reclassification(grid, scores_NS, src.nodata, src.nodata, outputtype=np.float64)
                    edges = edge_src.read(window = window)

                    edge_type = np.zeros_like(grid)
                    edge_type[edges != 0] = grid[edges != 0]
                    FA_edge, dClasses = reclassification(edge_type, scores_Edge_FA, src.nodata, src.nodata, outputtype=np.float64)
                    NS_edge, dClasses = reclassification(edge_type, scores_Edge_NS, src.nodata, src.nodata, outputtype=np.float64)


                    FA_LCedge = FA + FA_edge
                    NS_LCedge = NS + NS_edge

                    scaler = scaler_src.read(window=window, masked=True)
                    scaler = scaler.filled(1)
                    #implement checkes
                    FA_LCedge = FA_LCedge*scaler
                    NS_LCedge = NS_LCedge*scaler

                    FANS = (NS_LCedge + FA_LCedge) / 2.

                    FANS[grid == nodata] = profile_dst['nodata']

                    AI = ai_src.read(window=window)

                    ebmi = AI*FANS/100.
                    ebmi[AI<0] = 0.

                    #mask extent
                    aExtent = extent.read(window=window)
                    ebmi[aExtent == 0] = dst.nodata

                    dst.write(ebmi.astype(np.float32),window=window)

    def calculate_SPA(self, guild, year):
        ''' function to calculate the annual Ecosystem Service Potential in crop pollination'''
        # ini dict for annual output files
        dPotential = {}
        dSPA = {}

        #get the annual input files
        path_EBM = self.EBMI[guild][year]
        path_SDM = self.config[_CP]['SGSI'][guild][year]
        #get annual output file
        path_out_potential = self.path_out_potential[guild][year]
        path_out_SPA = self.path_out_SPA[guild][year]

        # block processing
        logger.info('** generation of Ecosystem Service potential files')
        try:
            logger.debug(f'*** usage of EBM file: {os.path.basename(path_EBM)}')
            logger.debug(f'*** usage of SDM file: {os.path.basename(path_SDM)}')

            # set up dst profiles and metadata
            profile_potential = self.accord.ref_profile.copy()
            profile_potential.update(tiled=True,
                                     compress='LZW',
                                     blockysize=256,
                                     blockxsize=256,
                                     count=1,
                                     dtype=rasterio.float32,
                                     nodata=np.nan)

            profile_3 = profile_potential.copy()
            profile_3.update(dtype=rasterio.ubyte, nodata=255)

            # create tags for potential
            self.accord.metadata.read_raster_tags([path_EBM, path_SDM])
            tags_potential = self.accord.metadata.prepare_raster_tags('Generation of Ecosystem Service Potential for year '
                                                                  f'{year} by averaging the EBM and SDM datasets plus masking.', 'ratio')

            tags_SPA = self.accord.metadata.prepare_raster_tags(f'Generation of Service Providing Area for year {year} '
                                                            f'by evaluating the Ecosystem Service Potential with a threshold of {self.config[_CP]["parameters"][guild]["SPA_threshold"]}.', 'binary')

            # run block processing
            with rasterio.open(path_EBM) as src_ebm, \
                    rasterio.open(path_SDM) as src_sdm, \
                    rasterio.open(self.statistics_raster) as extent, \
                    rasterio.open(path_out_potential, 'w', **profile_potential) as dst_pot, \
                    rasterio.open(path_out_SPA, 'w', **profile_3) as dst_spa:
                    # set tags
                    dst_pot.update_tags(**tags_potential)
                    dst_spa.update_tags(**tags_SPA)

                    # now iterate over the blocks of src files and process
                    for index, window in block_window_generator(block_shape, profile_potential['height'],
                                                                              profile_potential['width']):
                        # read data as masked array
                        aEBM = src_ebm.read(1, window=window, masked=True)
                        aSDM = src_sdm.read(1, window=window, masked=True)
                        aExtent = extent.read(1, window=window)

                        # calculate final potential
                        weight = self.config[_CP]['parameters'][guild]['SGSI_weight']
                        old_warn = np.seterr(all='ignore')  # suppress floating point errors from masked values
                        aPotential = (aEBM * (1-weight) + aSDM * weight)
                        np.seterr(**old_warn)

                        aPotential[aExtent == 0] = dst_pot.nodata
                        # write to disk
                        dst_pot.write(aPotential.filled(profile_potential['nodata']).astype(profile_potential['dtype']),
                                      1, window=window)

                        #finally we generate the SPA
                        aSPA = np.full_like(aPotential, 0, dtype=rasterio.ubyte)
                        aSPA[(aEBM >= self.config[_CP]['parameters'][guild]['SPA_threshold']*(1-weight))   & \
                             (aSDM >= self.config[_CP]['parameters'][guild]['SPA_threshold']*(weight)) & \
                             (aPotential >= self.config[_CP]['parameters'][guild]['SPA_threshold']) & \
                             (aPotential <= 1)] = 1

                        aSPA[aExtent == 0] = dst_spa.nodata
                        # write block out
                        dst_spa.write(aSPA.astype(profile_3['dtype']), 1, window=window)

                        # free data
                        aEBM = None
                        aSDM = None
                        aPotential = None
                        aSPA = None


            logger.debug('done.')
        except Exception:
            # here we delete file to rerun after error
            if os.path.exists(path_out_potential):
                os.remove(path_out_potential)
            if os.path.exists(path_out_SPA):
                os.remove(path_out_SPA)
            raise IOError(f'generation of potential file for year {year} was not successful')

    def prep_monetary(self):
        #this will convert the input stat data to rasters. So we get for evey country a price per ton



        # get universal input together
        path_ESTAT = self.config[_CP]["AACT_UV"]
        df_ESTAT_codes = pd.read_csv(self.config[_CP]['LUT_CAPRI_ESTAT_codes'], comment='#', dtype=str)
        #now we can run of the years
        for year in self.years:
            logger.info('* run monetary preparation for year: {}'.format(year))
            ##get need annual input data together

            #generate out of these CAPRI crop codes the relation of needed ESTAT codes
            lESTAT_code = df_ESTAT_codes[df_ESTAT_codes.CAPRI_code.isin(_CROPS)].ESTAT_code.unique().tolist()

            ##prepare the ESTAT data
            logger.info('** preparing the ESTAT TSV data')
            df_pivot = self.prep_ESTAT_data(lESTAT_code, year)

            ####next step - calculate the average prices for the CAPRI crop_types from the ESTAT crop_types
            #NOTE: get ugly since we have not 1:1 relation ships
            logger.info('** convert from ESTAT to CAPRI crop prices')
            #ToDo: this JRC conversion is also questionable
            #make sure all zeros become a NA since JRC let zeros out in the average calculation for each CAPRI crop type
            df_pivot.replace(0, pd.NA, inplace=True)

            #due to the n:M relationsship between the CAPRI and ESTAT we have to run a loop
            for element in _CROPS:
                lUse = df_ESTAT_codes[df_ESTAT_codes.CAPRI_code == element].ESTAT_code.unique().tolist()
                df_pivot[element] = df_pivot[lUse].mean(axis=1)

            logger.info('** generate current price tables')
            df_prices_current = df_pivot[_CROPS].copy()
            #### NOTE: we have also to check if we have all countries in the prices or ESTAT misses one
            lMissing = [x for x in self.statistics_shape.index.tolist() if x not in df_prices_current.index.tolist()]
            df_prices_current = df_prices_current.reindex(df_prices_current.index.values.tolist() + lMissing)
            #fill up missing prices with the EU average
            ##TODO: that is really tricky since JRC handle some cases specially - need universal way
            #first: JRC calculates the EU average prices ignoring ZEROS in the statistics (therefore they get a higher average as when using reported 0 prices)
            #second: every country with a reported crop production for a crop type needs a prices (if not existing then it is the EU average)
            df_prices_current.fillna(df_pivot.mean(), inplace=True)
            df_prices_real = df_prices_current.mul(self.deflator[2000] /self.deflator[year], axis = 'index')

            #merge the current price shapefile and rasterize
            gdf_prices_current = self.statistics_shape.merge(df_prices_current, how='left', left_index=True, right_index=True)
            gdf_prices_real = self.statistics_shape.merge(df_prices_real, how='left', left_index=True, right_index=True)
            for crop_type in _CROPS:
                self.accord.rasterize(gdf_prices_real, crop_type,self.monetary_temp[year][crop_type]['real'])
                self.accord.rasterize(gdf_prices_current, crop_type,self.monetary_temp[year][crop_type]['current'])

    def prep_ESTAT_data(self, lESTAT_code, year):
        from inca.common import estat
        '''
        helper function to prepare the ESTAT TSV datasets to be usable in crop pollination
        '''
        # first read in tsv file into dataframe
        df = estat.read_tsv(self.config[_CP]["AACT_UV"])

        #### filter by countries, years, crop codes
        # filter to right strucpro
        df = df.loc[(slice(None),'EUR_T',slice(None)),slice(None)]


        # filter to needed crop_types by code
        # that is more tricky - we have to filter on the ESTAT codes which are need to generate the CAPRI groups
        df = df[df.index.isin(lESTAT_code, level=0)]

        # now we filter to the needed years
        lYears = [int(year) - 1, int(year), int(year) + 1]
        lNeeded = [x for x in lYears if x in df.columns.tolist()]
        # small check
        if len(lNeeded) == 0:
            logger.info(
                'no annual data in the production statistics available. Taking closest year available')
            diff = np.abs(np.array(df.columns.tolist()) - int(year))
            year = df.columns[np.where(diff == np.min(diff))].array[0]
            lYears = [int(year) - 1, int(year), int(year) + 1]
            lNeeded = [x for x in lYears if x in df.columns.tolist()]

        df = df[lNeeded].copy()

        # now we have to convert column data into numbers and if not valid into NULL
        # we do that after the filtering to reduce the workload!

        # to right countries - in this case always EU27 or EU28 to garantuee the EU average
        # get the country code extracted
        #df['country_code'] = df.index.get_level_values(2)
        df = estat.filter_eu27(df)


        for element in [x for x in lYears if x in df.columns.tolist()]:
            df[element] = pd.to_numeric(df[element], errors='coerce', downcast='float')

        # calculating the three year average
        df['three_year_avg'] = df[[x for x in lYears if x in df.columns.tolist()]].mean(axis=1)

        # pivot the dataframe and bring to needed core information
        df.index = df.index.droplevel(level=1)
        df_pivot = df['three_year_avg'].unstack(0)

        return df_pivot

    def monetary_evalution(self, year, crop_type, guild):
        path_out = self.pollination_monetary[year][crop_type][guild]['use-current']
        path_out1 = self.pollination_monetary[year][crop_type][guild]['use-real']
        path_out2 = self.pollination_monetary[year][crop_type][guild]['supply-current']
        path_out3 = self.pollination_monetary[year][crop_type][guild]['supply-real']
        with rasterio.open(self.pollination_flow[year][crop_type][guild]['use']) as src_use:
            dst_profile = src_use.profile

        with rasterio.open(self.monetary_temp[year][crop_type]['current']) as src_euro_current, \
                rasterio.open(self.monetary_temp[year][crop_type]['real']) as src_euro_real, \
                rasterio.open(self.pollination_flow[year][crop_type][guild]['use']) as src_use, \
                rasterio.open(self.pollination_flow[year][crop_type][guild]['supply']) as src_supply, \
                rasterio.open(path_out, 'w', **dst_profile) as dst_use_current, \
                rasterio.open(path_out1,'w',**dst_profile) as dst_use_real, \
                rasterio.open(path_out2,'w',**dst_profile) as dst_supply_current, \
                rasterio.open(path_out3,'w',**dst_profile) as dst_supply_real :

                self.accord.metadata.update_dataset_tags(dst_use_current, f'monetary evaluation of use expressed in current euro.',
                                                    'euros', self.monetary_temp[year][crop_type]['current'], \
                                                    self.pollination_flow[year][crop_type][guild]['use'])
                self.accord.metadata.update_dataset_tags(dst_use_real, f'monetary evaluation of use expressed in real euro.',
                                                    'euros', self.monetary_temp[year][crop_type]['real'], \
                                                    self.pollination_flow[year][crop_type][guild]['use'])
                self.accord.metadata.update_dataset_tags(dst_supply_current, f'monetary evaluation of supply expressed in current euro.',
                                                    'euros', self.monetary_temp[year][crop_type]['current'], \
                                                    self.pollination_flow[year][crop_type][guild]['supply'])
                self.accord.metadata.update_dataset_tags(dst_supply_real, f'monetary evaluation of supply expressed in real euro.',
                                                'euros', self.monetary_temp[year][crop_type]['real'], \
                                                self.pollination_flow[year][crop_type][guild]['supply'])

                for _, window in block_window_generator(block_shape, src_use.height, src_use.width):
                    #read in data
                    current_prices = src_euro_current.read(1,window=window)
                    real_prices = src_euro_real.read(1,window=window)
                    use_data = src_use.read(1,window=window)
                    supply_data = src_supply.read(1,window=window)
                    if (np.all(use_data == src_use.nodata) or np.all(np.isnan(use_data))) and \
                            (np.all(supply_data == src_supply.nodata) or np.all(np.isnan(supply_data))):
                        continue
                    use_current = np.multiply(use_data, current_prices).astype(np.float64)
                    use_real = np.multiply(use_data, real_prices).astype(np.float64)
                    supply_current = np.multiply(supply_data, current_prices).astype(np.float64)
                    supply_real = np.multiply(supply_data, real_prices).astype(np.float64)

                    dst_use_current.write(use_current, indexes = 1 , window=window)
                    dst_use_real.write(use_real, indexes = 1 , window=window)
                    dst_supply_current.write(supply_current, indexes = 1 , window=window)
                    dst_supply_real.write(supply_real, indexes = 1 , window=window)
        logger.info(f"finished monetary evaluation {crop_type}, {guild}")



    def make_output_filenames(self):
        """Generates the filenames for all final output files."""
        self.forest_edges = {}
        self.FAI = {}
        self.EBMI = {}
        self.path_out_potential = {}
        self.path_out_potcats = {}
        self.path_out_SPA = {}
        self.path_mastermask = {}
        self.crop_potential_map = {}
        self.flying_pollinators = {}
        self.pollinated_harvest_share = {}
        self.pollination_flow = {}
        self.csvs = {}
        self.master = {}
        crop_pollination = _CP.lower().replace(' ', '-')
        self.SUT_physical_path = os.path.join(self.suts, f'{crop_pollination}_report_SUT-physical_tonnes_'
                                                         '{year}.xlsx')
        self.SUT_physical_path_MFA = os.path.join(self.suts, f'{crop_pollination}-MFA_report_SUT-physical_tonnes_'
                                                         '{year}.xlsx')
        self.SUT_montary_real_path = os.path.join(self.suts, f'{crop_pollination}_report_SUT-monetary_M-EURO-real_'
                                                         '{year}.xlsx')
        self.SUT_montary_real_path_MFA = os.path.join(self.suts, f'{crop_pollination}-MFA_report_SUT-monetary_M-EURO-real_'
                                                         '{year}.xlsx')
        self.SUT_montary_current_path = os.path.join(self.suts, f'{crop_pollination}_report_SUT-monetary_M-EURO-current_'
                                                         '{year}.xlsx')
        self.SUT_montary_current_path_MFA = os.path.join(self.suts, f'{crop_pollination}-MFA_report_SUT-monetary_M-EURO-current_'
                                                         '{year}.xlsx')


        self.monetary_temp = {}
        self.pollination_monetary = {}

        for year in self.years :
            # create output file paths
            self.pollination_flow[year] = {}
            self.pollinated_harvest_share[year] = {}
            self.csvs[year] = {}
            self.master[year] = {}
            self.monetary_temp[year] = {}
            self.pollination_monetary[year] = {}

            for crop_type in _CROPS:
                self.pollinated_harvest_share[year][crop_type] = os.path.join(self.temp_dir, f'harvest-share-pollinated_{crop_type}_tonnes_{year}.tif')
                self.monetary_temp[year][crop_type] = {}
                self.pollination_monetary[year][crop_type] = {}
                self.monetary_temp[year][crop_type]['current'] = os.path.join(self.temp_dir, f'price_{crop_type}_euro_tonnes_current_{year}.tif')
                self.monetary_temp[year][crop_type]['real'] = os.path.join(self.temp_dir, f'price_{crop_type}_euro_tonnes_real_{year}.tif')
                self.pollination_flow[year][crop_type]  = {}
                self.csvs[year][crop_type]= {}

                for guild in self.config[_CP]['SGSI']:
                    self.pollination_flow[year][crop_type][guild] = {}
                    self.pollination_monetary[year][crop_type][guild] = {}
                    self.csvs[year][crop_type][guild] = {}
                    for output in ['use','supply'] :
                        self.csvs[year][crop_type][guild][output] = os.path.join(self.statistic_dir, f'crop-pollination_statistics_{output}_{guild}-flying-pollinators_{crop_type}_{year}.csv')
                        for monetary in ['real','current'] :
                            self.csvs[year][crop_type][guild][f'{output}-{monetary}'] = os.path.join(self.statistic_dir, f'crop-pollination_statistics_{output}_{guild}-flying-pollinators_{crop_type}_EURO-{monetary}_{year}.csv')
                            self.pollination_monetary[year][crop_type][guild][f'{output}-{monetary}'] = os.path.join(self.temp_dir, f'crop-pollination_{output}_{guild}-flying-pollinators_{crop_type}_EURO-{monetary}_{year}.tif')
                    for output in ['unmet','demand']:
                        self.csvs[year][crop_type][guild][output] = os.path.join(self.statistic_dir, f'crop-pollination_statistics_{output}_{guild}-flying-pollinators_{crop_type}_{year}.csv')


            for output in ['use','supply'] :
                self.master[year][output] = os.path.join(self.maps, f'crop-pollination_map_{output}_tonnes_{year}.tif')
                for monetary in ['real','current'] :
                    self.master[year][f'{output}-{monetary}'] = os.path.join(self.maps, f'crop-pollination_map_{output}_EURO-{monetary}_{year}.tif')



            for output in ['unmet','demand']:
                self.master[year][output] = os.path.join(self.maps, f'crop-pollination_map_{output}_hectares_{year}.tif')

        for guild in self.config[_CP]['SGSI']:
            self.path_out_potential[guild] = {}
            self.path_out_SPA[guild] = {}
            self.path_mastermask[guild] = {}
            self.EBMI[guild] = {}
            self.FAI[guild] = {}
            self.forest_edges[guild] = {}
            self.crop_potential_map[guild] = {}
            self.flying_pollinators[guild] = {}
            for year in self.years :
                    self.FAI[guild][year] = os.path.join(self.temp_dir, f'ai_{guild}_{str(year)}.tif')
                    self.forest_edges[guild][year] = os.path.join(self.temp_dir, f'edges-{guild}_{year}.tif')
                    self.EBMI[guild][year] = os.path.join(self.maps, f'EBMI_crop-pollination-{guild}_{str(year)}.tif')
                    self.path_out_potential[guild][year] = os.path.join(self.maps, f'crop-pollination-{guild}_map_potential_ratio_{year}.tif')
                    self.path_out_SPA[guild][year] = os.path.join(self.maps, f'crop-pollination-{guild}_map_potential-SPA_binary_{year}.tif')
                    self.path_mastermask[guild][year] = os.path.join(self.temp_dir, f'crop-pollination-{guild}_agriculture-mask_{year}.tif')
                    self.crop_potential_map[guild][year] = os.path.join(self.temp_dir, f'crop-pollination-{guild}_map_potential-crop_{year}.tif')
                    self.flying_pollinators[guild][year] = os.path.join(self.temp_dir, f'SBA_pollination-area_{guild}-flying-pollinators_{year}.tif')

    def collect_input_maps(self):
        """Check all required input maps are there, and sort them in the right order."""
        # check if we have all data  available
        summers = list()
        for guild in self.config[_CP]['SGSI']:
            if self.config[_CP]['SGSI'][guild][self.years[0]]:
                summers = summers + self.config[_CP]['parameters'][guild][_SUMMER]
        aggregate_maps = {_RADIATION: dict(), _DAYLENGTH: dict(), _TEMPERATURE: dict()}
        summers = list(set(summers))
        for year in self.years:
            for map_type, maps in aggregate_maps.items():

                # If we have more than 1 map, map files should be numbered, with the format '<number>_*.tif', so
                # we know which maps belong together.  We sort maps according to that number, and check if we have
                # exactly the numbers we expect.
                map_error = ConfigError(f'The {map_type} directory must '
                                        f'contain raster files numbered {summers}.',
                                        [_CP, map_type])
                try:
                    # Extract leading number from filenames, and use that to sort files:
                    sorted_maps = sorted([(int(os.path.basename(f).split('_')[0]), f)
                                          for f in self.config[_CP]['meteo_data'][map_type][year] if int(os.path.basename(f).split('_')[0]) in summers],
                                         key=lambda x: x[0])
                    # All is good:
                    maps[year] = [file for _, file in sorted_maps]
                except ValueError:
                    raise map_error

        return aggregate_maps