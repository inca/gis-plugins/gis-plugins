# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""The `inca` module provides the basic framework for all ecosystem service account calculations.

Every ecosystem account calculation is started using an :obj:`inca.Run` object.  Each ecosystem service module contains
a subclass of :obj:`inca.Run` which performs the specific calculations for that service.  For example, we can run a wood
provision calculation from an :obj:`inca.woodprovision.WoodRun` object as follows::

    import inca.woodprovision

    config = {'year': 2000,
              'land_cover': {2000: '/path/to/land_cover_2000.tif'},
              'output_dir': '/path/to/output',
              'run_name': 'wood_example'
              # ... all other required settings for a wood provision calculation
              }
    run = inca.woodprovision.WoodRun(config)
    run.start()

The :obj:`inca.Run` base class takes care of common tasks such as

* creating output directories,
* setting up the log file,
* validating the configuration and
* determining the required extent for input and output data,
* warping / clipping geographic input data as needed and
* mapping land cover data to ecosystem types.

In order for this to work, an ecosystem run class should do the following:

#. Inherit from :obj:`inca.Run`.
#. Extend :attr:`inca.Run.config_template` with the configuration required for the service.
#. Override the :meth:`inca.Run._start` method as the starting point of the actual calculation.

A minimal example looks as follows::

  class MyRun(inca.Run):

      def __init__(self, config):
          super().__init__(config, 'My Ecosystem Service name')

          self.config_template.update({'my_config_key': ConfigItem(),
                                       # ... add ConfigItems describing expected config structure ...
                                       })

      def _start(self):
          logger.debug('Hello from My Ecosystem Service.')
          # ... run actual calculation here ...

"""

import logging
import math
import os
import sys
import shutil
import tempfile
from collections import defaultdict
from enum import Enum

import geopandas as gpd
import numpy as np
import osgeo
import pandas as pd
import pyproj
import rasterio
import yaml

import inca.common.ecosystem
import inca.common.estat
from inca.common.classification import reclassification
from inca.common.config_check import ConfigCheck, ConfigItem, ConfigRaster, check_csv, YEARLY
from inca.common.ecosystem import ECO_ID, ECOTYPE, ECOTYPE_L1, ECOTYPE_L2, LANDCOVER_ID, SHAPE_ID, \
    ecotype_l1_cat, ecotype_l2_cat, types_l1, types_l2
from inca.common.errors import Error, ConfigError
from inca.common.geoprocessing import GeoProcessing, POLY_MIN_SIZE, INCA_EPSG, INCA_MINIMUM_RESOLUTION, COUNT, \
    statistics_byArea_byET, block_window_generator, number_blocks, RasterType
from inca.common.nuts import NUTS_ID, get_nuts_shape
from inca.common.fileprocessing import createDataFrame

if sys.version_info[:2] >= (3, 8):
    # TODO: Import directly (no need for conditional) when `python_requires = >= 3.8`
    from importlib.metadata import PackageNotFoundError, version  # pragma: no cover
else:
    from importlib_metadata import PackageNotFoundError, version  # pragma: no cover

try:
    dist_name = 'inca-tool'
    __version__ = version(dist_name)
except PackageNotFoundError:  # pragma: no cover
    __version__ = "unknown"
finally:
    del version, PackageNotFoundError

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
_log_format = logging.Formatter('%(asctime)s %(name)s [%(levelname)s] - %(message)s')
_logfile_handler = None
_logfile = None

logging.captureWarnings(True)
warnlogger = logging.getLogger('py.warnings')

_STATISTICS_SHAPE = 'statistics_shape'
_REPORTING_SHAPE = 'reporting_shape'
_DEFLATOR = 'deflator'


def set_up_console_logging(verbose=False):
    """Install a log handler that prints to the terminal.

    :param verbose: boolean if verbose logging should be printed
    """
    ch = logging.StreamHandler()
    ch.setFormatter(_log_format)
    ch.setLevel(logging.DEBUG if verbose else logging.INFO)
    logger.addHandler(ch)
    warnlogger.addHandler(ch)


def set_up_logfile(log_dir, verbose=False, filename=f'{__name__}.log'):
    """Install a log handler that prints to a file in the provided directory.

    :param log_dir: absolute folder name in which the log file is generated
    :param verbose: boolean if verbose logging is entered in the log file
    :param filename: base name for the logfile
    """
    global _logfile_handler
    global _logfile
    _logfile = os.path.join(log_dir, filename)
    fh = logging.FileHandler(_logfile, encoding='utf-8')
    fh.setLevel(logging.DEBUG if verbose else logging.INFO)
    fh.setFormatter(_log_format)

    logger.addHandler(fh)
    warnlogger.addHandler(fh)
    _logfile_handler = fh


def remove_logfile_handler():
    """Remove the log file handler.

    Required to clean up when we reload the inca plugin.  Otherwise, an extra log handler starts writing to the same
    file every time we reload the plugin.
    """
    global _logfile_handler
    if _logfile_handler is not None:
        logger.removeHandler(_logfile_handler)


def get_logfile():
    """Return the filename of the current log file.

    :return: absolute file name of the log file
    """
    return _logfile


class ShapeType(Enum):
    """Enum for statistics or reporting shapes."""

    STATISTICS = 0
    REPORTING = 1


class Cancelled(Exception):
    """Custom Exception to signal cancellation of a Run.

    This Exception not raised by the inca package itself.  Rather, it is "injected" into the thread of a running inca
    calculation by the QGIS plugin when the user clicks the cancel button.
    """

    pass


class Run:
    """Common skeleton for all inca runs, takes care of logging, output directories, config validation, ...

    :param config: dictionary containing all configuration for the current run.
    """

    service_name = None  # To be set in each subclass

    def __init__(self, config):
        logger.debug('Run.__init__')

        # If running from command line, config contains a 'func' attribute used to select the run type (side effect of
        # our use of argparse subparsers).  This attribute must not be written to the final config file.
        config.pop('func', None)

        self.monetary = config.get('monetary', False)  #: This boolean flag controls if we run monetary evaluation.

        self.config_template = {
            'land_cover': {YEARLY: ConfigRaster(check_projected=True, check_unit=True,
                                                raster_type=RasterType.CATEGORICAL)},
            'land_classification': ConfigItem(check_csv, optional=True,
                                              required_columns=[LANDCOVER_ID,
                                                                (ECOTYPE_L1, ECOTYPE_L2)],
                                              unique_columns=[LANDCOVER_ID],
                                              dtypes={ECOTYPE_L1: ecotype_l1_cat, ECOTYPE_L2: ecotype_l2_cat}),
        }  #: Dictionary of :obj:`inca.common.config_check.ConfigItem` describing the required configuration for this run.

        if self.monetary:
            self.config_template[_DEFLATOR] = ConfigItem(check_function=check_csv, required_columns=[NUTS_ID])

        self.years = config.get('years')

        # Check self.years is a non-empty list of integers
        if not self.years or not isinstance(self.years, list) or any(not isinstance(y, int) for y in self.years):
            raise ConfigError('Please provide a list of years for which to calculate accounts.', ['years'])
        self.config = config
        if not self.config.get('run_name'):
            raise ConfigError('Please provide a run name.', ['run_name'])
        if not self.config.get('output_dir'):
            raise ConfigError('Please provide an output directory.', ['output_dir'])
        self.run_dir = os.path.abspath(os.path.join(self.config['output_dir'], self.config['run_name']))
        self.maps = os.path.join(self.run_dir, 'maps')
        self.suts = os.path.join(self.run_dir, 'SUTs')
        self.additional_results = os.path.join(self.run_dir, 'additional_results')
        self.statistic_dir = os.path.join(self.run_dir, 'statistics')
        self.temp_dir = os.path.join(self.run_dir, 'TEMP')
        self._progress = 0.
        self._progress_callback = None  #: Callback function to report progress to QGIS.
        self._progress_weight_run = 0.85  #: default contribution of run itself to progress bar, remaining part is for raster check.
        self._ecosystem_rasters = {}
        self.deflator = None

    def start(self, progress_callback=None):
        """Call this method to start the actual calculation.

        Wraps :meth:`inca.common.config_check.ConfigCheck.validate` and :meth:`inca.Run._start` with exception handlers.

        :param progress_callback: progressbar for the QGIS plug-in
        """
        logger.info(f'*** Welcome to the INCA-tool for generation of {self.service_name} account ***')

        self._progress_callback = progress_callback
        assert (0. <= self._progress_weight_run <= 1.0)
        self.add_progress(0.)

        self._create_dirs()
        global _logfile_handler
        if _logfile_handler is None:
            set_up_logfile(self.run_dir, self.config.get('verbose', False))

        # dump config as provided by user, so we can see exactly what the input was when we want to debug
        self._dump_config()

        # small trick to fill "selected regions" when just given EU27
        if (self.config.get('selected_regions') == ['EU27']):
            self.config['selected_regions'] = inca.common.estat.eu_members(2020).tolist()

        logger.info('INCA version %s using '
                    'GDAL (osgeo) %s, rasterio %s, geopandas %s, numpy %s, pandas %s, pyproj %s, PROJ %s',
                    __version__, osgeo.__version__, rasterio.__version__,
                    gpd.__version__, np.__version__, pd.__version__,
                    pyproj.__version__, pyproj.proj_version_str)

        try:
            logger.info('Run a check of the provided vector files and landcover raster file')
            self._StudyScopeCheck()
            logger.info('Run validation of given config file')
            config_check = ConfigCheck(self)
            config_check.validate()
            logger.info('Adjust raster input to reference raster if needed')
            config_check.adjust_rasters()
            logger.info('load existing files')
            self._load_deflator_table()
            self._load_ecosystem_raster()
            self._progress = 100. * (1. - self._progress_weight_run)
            logger.info('>>> start service account generation')
            self._start()
        except Cancelled:
            logger.exception('INCA run canceled.')
            raise
        except BaseException:
            logger.exception('INCA error:')
            raise
        logger.info('INCA run complete.')

    def _start(self):
        """This method should be implemented in each subclass.  It is the starting point of the actual calculation."""
        raise NotImplementedError

    def _create_dirs(self):
        """Create output directory for this run, or check if it already exists."""
        try:
            os.makedirs(self.config['output_dir'], exist_ok=True)  # top output directory
        except OSError as e:
            raise RuntimeError(f'Failed to create output directory "{self.config["output_dir"]}": {e}')

        # We want a single subdir "run_name", 1 level below the output dir.  Therefore, "run_name" should not contain
        # directory separators.
        if os.sep in self.config['run_name'] or (os.altsep is not None and os.altsep in self.config['run_name']):
            raise Error(f'Run name "{self.config["run_name"]}" contains directory separator.')

        logger.debug('Create run_dir %s', self.run_dir)
        try:
            os.mkdir(self.run_dir)
        except FileExistsError:
            if self.config.get('continue'):
                logger.debug('Continuing work in existing run directory %s', self.run_dir)
            else:
                raise Error(f'Run directory {self.run_dir} already exists.  '
                            'Use option "continue" to resume a previous run.')

        os.makedirs(self.suts, exist_ok=True)
        os.makedirs(self.maps, exist_ok=True)
        os.makedirs(self.statistic_dir, exist_ok=True)
        os.makedirs(self.temp_dir, exist_ok=True)

    def _dump_config(self):
        """Write a YAML dump of the current config in our run directory."""
        with open(os.path.join(self.run_dir, f'{self.service_name.lower().replace(" ", "_")}-config.yaml'), 'w') as f:
            f.write(yaml.dump(self.config))

    def _add_progress_prerun(self, p):
        """Update progress bar outside of the main run phase.

        This method should be used to update the progress bar for calculations in during initialization and raster
        checks, outside of the specific ecosystem service run itself.

        :param p: progress percentage of a pre-run processing step (value between 1 - 100)
        """
        self._progress += p * (1 - self._progress_weight_run)
        if self._progress_callback:
            self._progress_callback(self._progress)

    def add_progress(self, p):
        """Add an amount `p` to the total progress made and update the progress bar if it exists.

        The sum of all values `p` over the course of a run should equal 100.

        :param p: progress percentage of processing step in service (value between 1 - 100)
        """
        self._progress += p * self._progress_weight_run
        if self._progress_callback:
            self._progress_callback(self._progress)

    def _load_deflator_table(self):
        """Read the deflator table CSV file, and check we have row for every data area."""
        logger.debug('* load deflator table')
        if not self.monetary:
            # If we don't run a monetary evaluation, we do not require a deflator table
            return
        self.deflator = pd.read_csv(self.config[_DEFLATOR], comment='#').set_index(NUTS_ID)
        try:
            self.deflator.columns = [int(year) for year in self.deflator.columns]  # transform strings to ints
        except ValueError:
            raise ConfigError('could not convert column to year. Check that only years and NUTS_ID are in CSV file.',
                              [_DEFLATOR])
        missing_regions = ~self.statistics_shape.index.isin(self.deflator.index)
        if missing_regions.any():
            raise ConfigError('Deflator table does not contain all data areas: missing ' +
                              ', '.join(self.statistics_shape.index[missing_regions]) + '.', [_DEFLATOR])
        missing_years = [year for year in self.config['years'] if year not in self.deflator]
        if missing_years:
            raise ConfigError('Deflator table does not contain data for year(-s) ' +
                              ', '.join(missing_years) + '.', [_DEFLATOR])

        if 2000 not in self.deflator.columns:
            raise ConfigError('Deflator table does not contain data for the base year 2000.', [_DEFLATOR])

        self.deflator = self.deflator.loc[self.statistics_shape.index]

    def _load_region_shape(self, kind: ShapeType):
        """Load the statistics or reporting shapes into a :class:`geopandas.GeoDataFrame`.

        If the config parameter for the shape is a literal string 'NUTS-0', 'NUTS-1' or 'NUTS-2', we use the default
        (NUTS 2021) shape for that level.  Otherwise, the parameter should be the filename of a user-provided shp or
        gpkg, and we load that file.

        The resulting :class:`geopandas.GeoDataFrame` is indexed by NUTS_ID, and has an additional column SHAPE_ID,
        which contains an integer identifier for each shape.  This column can be used when rasterizing shapefiles.

        :param kind: identifier of type ShapeType (currently: STATISTICS or REPORTING)
        """

        keys = {ShapeType.STATISTICS: _STATISTICS_SHAPE,
                ShapeType.REPORTING: _REPORTING_SHAPE}
        labels = {ShapeType.STATISTICS: 'input statistics',
                  ShapeType.REPORTING: 'reporting'}
        config_key = keys[kind]
        label = labels[kind]
        nuts_strings = {'NUTS-0': 0,
                        'NUTS-1': 1,
                        'NUTS-2': 2}
        config_val = self.config.get(config_key)
        if not config_val:  # None or empty string
            raise ConfigError(f'No shapefile for {label} provided.', [config_key])

        nuts_level = nuts_strings.get(config_val)
        if nuts_level is not None:  # config val was literal string NUTS-0 / 1 / 2
            df = get_nuts_shape(nuts_level)
        else:
            # No nuts_level -> config_val should be a filename
            try:
                df = gpd.read_file(config_val).set_index(NUTS_ID).sort_index()
            except KeyError:
                raise ConfigError(f'The provided shapefile for {label} does not have a column {NUTS_ID}.',
                                  [config_key])
            except BaseException as e:
                raise Error(f'Failed to read {label} shape: {e}.')

        # SHAPE_ID: integer number, counting starts from 1.
        df[SHAPE_ID] = range(1, 1 + df.shape[0])
        return df

    def get_ecosystem_mapping(self, level=1) -> pd.DataFrame:
        """Return a dataframe relating landcover categories to the standard ecosystem types.

        If the configuration does not contain a user-provided mapping, return the default mapping for CORINE land cover
        data.

        :param level: hierarchical level in the requested ecosystem typology
        :return: A DataFrame with columns `ecosystem_type_{L1 or L2}`, `landcover_id`, `ecosystem_id` and `name`.
        """
        if not self.config['land_classification']:
            # Default: assume land cover map is CORINE and use standard CORINE mapping
            # convert L2 ecosystem type to L1 -> just convert float to int
            logger.debug('Using default CORINE ecosystem type mapping')
            return inca.common.ecosystem.df_corine_mapping(level=level)
        else:
            logger.debug('Using ecosystem type mapping from %s', self.config['land_classification'])
            return inca.common.ecosystem.read_mapping(self.config['land_classification'], level)

    def get_ecosystem_mapping_metadata(self, level):
        """Return a string representation of the landcover_id -> ecosystem_id mapping.

        :param level: hierarchical level in the requested ecosystem typology
        :return: string representation of the landcover_id -> ecosystem_id mapping
        """
        # group all input landcover id's per output Maes type:
        mapping = defaultdict(list)

        for x in self.get_ecosystem_mapping(level).itertuples():
            mapping[getattr(x, ECOTYPE)].append(getattr(x, LANDCOVER_ID))

        return ', '.join(f'{mapping[ecotype]} -> {ecotype}' for ecotype in sorted(mapping))

    def _load_ecosystem_raster(self):
        """Load existing ecosystem rasters in the TEMP folder into the _ecosystem_rasters dict."""
        # get all raster files in TEMP folder
        try:
            df = createDataFrame(self.temp_dir, '.tif')
        except BaseException:
            logger.debug('no previous ecosystem maps exists to load')
            return
        # filter to files which are ecosystem rasters based on file_name_pattern
        df = df[df.basename.str.startswith('ecosystem_types_L')]
        if df.shape[0] != 0:
            logger.debug('* previous ecosystem maps exists to load from TEMP folder')
            # extract the year and ecosystem type level
            df = df.join((df['basename'].str.split('.', n=1).str[0]).str.split("_", expand=True)
                         .rename(columns={0: 'x1', 1: 'x2',  2: 'level',  3: 'year'}))
            # split out level number
            df['level'] = df['level'].str.extract(r'(\d+)')
            # set existing raster to dic
            for row in df.itertuples():
                self._ecosystem_rasters[int(row.year), int(row.level)] = os.path.normpath(row.path)
        else:
            logger.debug('* no previous ecosystem maps exists to load')

    def get_ecosystem_raster(self, year, level=1, add_progress=None, block_shape=(4096, 4096)):
        """Ecosystem raster generation for specified year and typology level

        Either new reclassification and raster generation is run or if file already exists the file name is returned.

        :param year: year for which the ecosystem raster has to be generated
        :param level: hierarchical level in the requested ecosystem typology
        :param add_progress: progressbar object
        :param block_shape: block size for raster chunk processing
        :return: filename of the reclassified ecosystem type map for the given year
        """
        """Get the filename of the reclassified ecosystem type map for the given year."""
        if (year, level) in self._ecosystem_rasters:  # previously calculated
            return self._ecosystem_rasters[year, level]

        out_file = os.path.join(self.temp_dir, f'ecosystem_types_L{level}_{year}.tif')

        logger.debug('Start level %d ecosystem type reclassification.', level)
        ecosystem_mapping = dict(list(self.get_ecosystem_mapping(level=level)[[LANDCOVER_ID,
                                                                               ECO_ID]].values))
        logger.debug('ecosystem raster: got mapping %s', ecosystem_mapping)

        self.accord.metadata.read_raster_tags([self.config['land_cover'][year]])

        if level == 1:
            dtype = np.uint8
        else:
            dtype = np.uint16
        nodata = np.iinfo(dtype).max
        assert max(ecosystem_mapping.values()) < nodata, f'Ecosystem id {max(ecosystem_mapping.values())} ' \
                                                         f'does not fit in dtype {dtype}'

        with rasterio.open(self.config['land_cover'][year], 'r') as ds_lc, \
                rasterio.open(out_file, 'w', **dict(self.accord.ref_profile,
                                                    dtype=dtype,
                                                    nodata=nodata)) as ds_out:
            nblocks = number_blocks(self.accord.ref_profile, block_shape)
            for _, window in block_window_generator(block_shape, ds_lc.height, ds_lc.width):
                ds_out.write(reclassification(ds_lc.read(1, window=window), dict_classes=ecosystem_mapping,
                                              nodata_in=ds_lc.nodata, nodata_out=ds_out.nodata,
                                              outputtype=dtype)[0],
                             window=window, indexes=1)
                if add_progress:
                    add_progress(100.0 / nblocks)

            ds_out.update_tags(**self.accord.metadata.prepare_raster_tags('Ecosystem type mapping', 'ecosystem types'),
                               ecosystem_mapping=self.get_ecosystem_mapping_metadata(level=level))

        self._ecosystem_rasters[year, level] = out_file
        logger.debug('Ecosystem reclassfication done.')
        return out_file

    def get_ecosystem_map_area_statistics(self, year, level=1, region='statistic', block_shape=(4096, 4096),
                                          add_progress=lambda p: None):
        """Area [ha.] of each ETclass for specified ecosystem map level by NUTS_ID of the statistics/reporting regions.

        :param level: level of ecosystem map for which the area statistics have to be generated
        :param region: statistical or reporting regions
        :param year: year of the ecosystem map for which the statistics have to be generated
        :param block_shape: size of the blocks for processing
        :param add_progress: optional progress bar object

        :return: pandas DataFrame containing the areas in hectare per ETclass and NUTS_ID
        """
        logger.debug(f'extracting ecosystem type area statistics for level {level} and {region} region of year {year}')

        # get correct ecosystem_type clear name to raster value dic
        if level == 1:
            eco_dic = types_l1.set_index('name')[ECO_ID].to_dict()
        elif level == 2:
            eco_dic = types_l2.set_index('name')[ECO_ID].to_dict()
        else:
            raise ValueError('The requested ecosystem level does not exist currently.')

        if region == 'statistic':
            region_path = self.statistics_raster
            region_dic = self.statistics_shape[SHAPE_ID].to_dict()
        elif region == 'reporting':
            region_path = self.reporting_raster
            region_dic = self.reporting_shape[SHAPE_ID].to_dict()
        else:
            raise ValueError('The requested region case does not exist currently.')

        # run statistics
        df = statistics_byArea_byET(self.get_ecosystem_raster(year, level=level),
                                    region_path,
                                    region_dic,
                                    self.get_ecosystem_raster(year, level=level),
                                    eco_dic,
                                    block_shape=block_shape,
                                    add_progress=add_progress)
        # convert to ha
        df['area_ha'] = df[COUNT] * self.accord.pixel_area_m2() / 10000.
        logger.debug('area statistics extraction done.')

        return df

    def _StudyScopeCheck(self):
        """Set up statistical and reporting region vector files, project extent, resolution, raster metadata.

        - Establish the list of statistics regions needed to cover the chosen reporting regions.

        - Initialize the metadata object to handle raster tags and standard output profiles for rasterio.  The landcover
           map provided for the first reference year is used as a master.

        - Generate raster masks of the reporting_regions and statistics_regions, which can be used for block-processing.
        """
        self.statistics_shape = self._load_region_shape(ShapeType.STATISTICS)
        self.reporting_shape = self._load_region_shape(ShapeType.REPORTING)
        if self.service_name == 'Local Climate Regulation':
            minimum_resolution = 150.
        else :
            minimum_resolution = INCA_MINIMUM_RESOLUTION

        ### 1. check if reporting and statistic vectors are in corrct EPSG
        logger.debug('* check if provided vector files have correct EPSG')
        # reporting vector file
        try:
            check_epsg = self.reporting_shape.crs.to_epsg()
        except BaseException:
            raise ConfigError('Please provide a reporting shapefile with a valid EPSG projection.', [_REPORTING_SHAPE])
        if check_epsg != INCA_EPSG:
            self.reporting_shape.to_crs(epsg=INCA_EPSG, inplace=True)
            logger.debug('** reporting vector file had to be warped')

        # statistics vector file
        try:
            check_epsg = self.statistics_shape.crs.to_epsg()
        except BaseException:
            raise ConfigError('Please provide a statistics shapefile with a valid EPSG projection.',
                              [_STATISTICS_SHAPE])
        if check_epsg != INCA_EPSG:
            self.statistics_shape.to_crs(epsg=INCA_EPSG, inplace=True)
            logger.debug('** statistics vector file had to be warped')

        ### 2. now we generate the "statistics_regions" out of the "reporting_regions"
        logger.debug('* get the reporting regions')
        # first we check the given "selected_regions" from  config all exist in reporting_shape
        # mainly needed when tool is run in command line mode
        logger.debug('** check if even all selected_regions (reporting_regions) exist in the reporting vector file')
        selected_regions = self.config.get('selected_regions')
        if not isinstance(selected_regions, list) or not len(selected_regions):
            raise ConfigError('Please select one or more reporting regions.', ['selected_regions'])
        lMismatch = [x for x in selected_regions if x not in self.reporting_shape.index]
        if len(lMismatch) != 0:
            raise ConfigError('The following regions are missing from the provided reporting vector file: ' +
                              ', '.join(lMismatch), [_REPORTING_SHAPE])
        self.reporting_shape = self.reporting_shape.reindex(selected_regions).sort_index()

        # second, we clip the statistics vector file by the reporting one to get a list of all names from the statistic
        # regions intersecting the reporting ones --> faster then a gpd.overlay()
        logger.debug('** clip the statistical vector file by selected reporting regions')
        df_check = gpd.clip(self.statistics_shape, self.reporting_shape)
        # we have to remove false areas (boundary of a statistical region is identical to boundary of reporting one)
        df_check = df_check[~df_check.is_empty]
        df_check = df_check[df_check.area > POLY_MIN_SIZE]
        if df_check.empty:
            raise ConfigError('No areas in the statistics regions file overlap with the selected reporting regions.',
                              [_STATISTICS_SHAPE])
        # check if all reporting polygons are completely covered by a statistical ones (minimum overlap)
        area_delta = self.reporting_shape.area.sum() - df_check.area.sum()
        if abs(area_delta) > POLY_MIN_SIZE:
            raise ConfigError('The statistics regions file does not completely cover all selected reporting regions.',
                              [_STATISTICS_SHAPE])
        # extract the identifier to get the reporting_regions
        self.statistics_shape = self.statistics_shape.reindex(df_check.index.unique()).sort_index()

        ### 3. now we generate the bounds for the statistical AOI
        logger.debug('* calculate the raster statistical AOI')
        # first get total bounds for selected regions in statistical vector file
        # format: minx, miny, maxx, maxy
        bbox = self.statistics_shape.total_bounds
        # allign to min_resolution increment to support better merges
        bbox = bbox / minimum_resolution
        # named tuple - BoundingBox(left, bottom, right, top)
        AOI_bbox = rasterio.coords.BoundingBox(left=math.floor(bbox[0]) * minimum_resolution,
                                               bottom=math.floor(bbox[1]) * minimum_resolution,
                                               right=math.ceil(bbox[2]) * minimum_resolution,
                                               top=math.ceil(bbox[3]) * minimum_resolution)

        ### 4. now we set up the accord object with the needed extent
        logger.debug('* initialize the global raster AccoRD object')
        # Note: since we do not give a reference raster file to GeoProcessing object we have to fill some info manual
        self.accord = GeoProcessing("INCA Tool by VITO (https://ecosystem-accounts.jrc.ec.europa.eu/)",
                                    self.service_name, self.temp_dir,
                                    GDAL_verbose=self.config.get('verbose', False))
        # set the extent for the raster files using the statistical domain
        self.accord.ref_extent = AOI_bbox

        ### 5. check if input land cover map MASTER to get resolution for set up of metadata object
        logger.debug('** pre-check the provided MASTER land cover raster file if all statistical regions are covered')
        land_cover_year0 = self.config.get('land_cover', {}).get(self.years[0])
        if not land_cover_year0:
            raise ConfigError(f'Please provide a land cover file for year {self.years[0]}.',
                              ['land_cover', self.years[0]])
        try:
            aoi_ok = self.accord.vector_in_raster_extent_check(land_cover_year0, self.statistics_shape,
                                                               check_projected=True, check_unit=True, stand_alone=True)
        except Error as e:
            # Catch inca.Error exceptions and re-raise as ConfigError linked to the config['landcover'][self.years[0]]:
            raise ConfigError(e.message, ['land_cover', self.years[0]])
        if not aoi_ok:
            raise ConfigError(
                'Not all needed statistical_regions specified by the reporting_regions are included in the ' +
                'provided land cover map ({}). '.format(land_cover_year0) +
                'Please provide a land cover map with a minimum extent of {} in EPSG:{}.'.format(AOI_bbox, INCA_EPSG),
                ['land_cover', self.years[0]])

        ### 6. we still have to fill the reference profile in the AccoRD object for GeoProcessing
        logger.debug('** give statistic raster info as reference file to the AccoRD object (profile, extent)')
        # (we use information of the master land cover map for that)
        # we need a test to check if we fullfill the INCA minimum resolution
        with rasterio.open(land_cover_year0) as src:
            src_profile = src.profile
            src_res = src.res

        # a fix for the Local Climate Model which has to run always in 150m spatial resolution
        if self.service_name == 'Local Climate Regulation':
            src_res = (minimum_resolution, minimum_resolution)

        if src_res[0] > minimum_resolution:
            logger.warning(f'The provided landcover map has a resolution of {src_res[0]}m, which is coarser than the '
                           f'proposed MINIMUM RESOLUTION of {minimum_resolution}m.  The land cover map will be '
                           'resampled. Please consider using a higher resolution land cover map if the resampling '
                           'produces non-desired results.')
            src_res = (minimum_resolution, minimum_resolution)

        # we set up the standard raster profile
        self.accord.ref_profile = {'driver': 'GTiff',
                                   'dtype': src_profile['dtype'],
                                   'nodata': src_profile['nodata'],
                                   'width': int((AOI_bbox.right - AOI_bbox.left) / src_res[0]),
                                   'height': int((AOI_bbox.top - AOI_bbox.bottom) / src_res[1]),
                                   'count': 1,
                                   'crs': rasterio.crs.CRS.from_epsg(INCA_EPSG),
                                   'transform': rasterio.transform.from_origin(AOI_bbox.left, AOI_bbox.top,
                                                                               src_res[0], src_res[1]),
                                   'blockxsize': 256,
                                   'blockysize': 256,
                                   'tiled': True,
                                   'compress': 'deflate',
                                   'interleave': 'band',
                                   'bigtiff': 'if_saver'}
        # create rasterio profile for the reporting area also for further processing
        logger.debug('** set up the raster profile and extent for the reporting regions')
        self._create_reporting_profile()

        ### 7. also generate a raster version of stats and reporting vector file for future blockprocessing tasks
        logger.debug('* create raster versions of statistic and reporting vectors for block processing tasks')
        # first, reporting vector file
        # output file name
        self.reporting_raster = os.path.join(self.temp_dir, 'reporting_shape_rasterized.tif')
        # run rasterization
        self.accord.rasterize(self.reporting_shape, SHAPE_ID, self.reporting_raster,
                              guess_dtype=True, mode='statistical')

        # second, statistical vector file
        # output file name
        self.statistics_raster = os.path.join(self.temp_dir, 'statistics_shape_rasterized.tif')
        # run rasterization
        self.accord.rasterize(self.statistics_shape, SHAPE_ID, self.statistics_raster,
                              guess_dtype=True, mode='statistical')

    def _create_reporting_profile(self):
        """Generate the reporting regions' rasterio profile for from the statistical regions' rasterio profile."""
        # first get total bounds for selected regions in reporting vector file
        # format: minx, miny, maxx, maxy
        bbox = self.reporting_shape.total_bounds
        if self.service_name=='Local Climate Regulation':
            minimum_resolution = 150.
        else:
            minimum_resolution = INCA_MINIMUM_RESOLUTION

        # allign to min_resolution increment to support better merges
        bbox = bbox / minimum_resolution
        # named tuple - BoundingBox(left, bottom, right, top)
        AOI_bbox = rasterio.coords.BoundingBox(left=math.floor(bbox[0]) * minimum_resolution,
                                               bottom=math.floor(bbox[1]) * minimum_resolution,
                                               right=math.ceil(bbox[2]) * minimum_resolution,
                                               top=math.ceil(bbox[3]) * minimum_resolution)
        self.accord.reporting_extent = AOI_bbox
        # adapt the rasterio profile of statistical_regions
        self.accord.reporting_profile = self.accord.ref_profile.copy()
        self.accord.reporting_profile.update(width=int((AOI_bbox.right - AOI_bbox.left) /
                                                       self.accord.ref_profile['transform'].a),
                                             height=int((AOI_bbox.top - AOI_bbox.bottom) /
                                                        abs(self.accord.ref_profile['transform'].e)),
                                             transform=rasterio.transform.from_origin(AOI_bbox.left,
                                                                                      AOI_bbox.top,
                                                                                      self.accord.
                                                                                      ref_profile['transform'].a,
                                                                                      abs(self.accord.
                                                                                          ref_profile['transform'].e)))

    def cut_to_reporting_regions(self, file, add_progress=lambda p: None):
        """Cut a raster file to the minimum extent for the reporting regions, and set to nodata outside reporting \
        regions.

        The file is modified in-place (overwritten).

        :param file: absolute file name of raster file to be cut
        :param add_progress: optional progressbar object
        """
        logger.info('Cut map %s to reporting region', file)
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpfile = os.path.join(tmpdir, os.path.basename(file))
            shutil.copy2(file, tmpfile)
            self.accord.crop_2_reporting_AOI(tmpfile, file, self.reporting_raster, add_progress=add_progress,
                                             block_shape=(4096, 4096))
