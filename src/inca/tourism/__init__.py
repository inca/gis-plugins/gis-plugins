# Copyright (c) 2022 European Union.
#
# The tool was developed with the contribution of the Joint Research Centre of the European Commission.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the European Union Public
# Licence, either version 1.2 of the License, or (at your option) any later version.
# You may not use this work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on
# an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#
# See the Licence for the specific language governing permissions and limitations under the Licence.

"""Nature-based tourism accounting."""

import sys

if sys.version_info[:2] >= (3, 9):
    import importlib.resources as importlib_resources
else:
    import importlib_resources

import logging
import os
from contextlib import ExitStack

import numpy as np
import pandas as pd
import rasterio

import inca
import inca.common.classification as classification
import inca.common.ecosystem as ecosystem
from inca.common.config_check import ConfigItem, ConfigRaster, check_csv, YEARLY
from inca.common.errors import Error, ConfigError
from inca.common.nuts import NUTS_ID
from inca.common.geoprocessing import statistics_byArea, statistics_byArea_byET, SUM, COUNT, \
    block_window_generator, number_blocks, RasterType
from inca.common.suts import write_sut, HSH, EXP, df_sectors
from inca.common.ecosystem import SHAPE_ID

logger = logging.getLogger(__name__)

OVERNIGHT_STAYS = 'OvernightStays'
ECO_OVERNIGHT_STAYS = 'EcoOvernightStays'
ECO_CONTRI = 'eco_contri'
DATA_AREA_CODE = 'DataAreaCode'
TYPE_OF_AREA = 'TypeOfArea'
YEAR = 'Year'
WEIGHT = 'Weight'
CODE = 'Code'
CONTRIBUTION_PERCENTAGE = "ContributionPercentage"
COUNTRY_OF_RESIDENCE = 'CountryOfResidence'
REPORTING_COUNTRY = 'Reporting country'
FOREIGN_COUNTRY = 'Foreign country'

DATA_AREA_CAT_MAP = 'data_area_cat_map'
DATA_AREA_TABLE = 'data_area_table'

ACCESSIBILITY_TABLE = 'accessibility_table'
ACCESSIBILITY_CAT_MAP = 'accessibility_cat_map'
ACCESSIBILITY_WEIGHT_MAP = 'accessibility_weight_map'
FACILITIES_TABLE = 'facilities_table'
FACILITIES_CAT_MAP = 'facilities_cat_map'
FACILITIES_WEIGHT_MAP = 'facilities_weight_map'
ATTRACTIVENESS_TABLE = 'attractiveness_table'
ATTRACTIVENESS_CAT_MAP = 'attractiveness_cat_map'
ATTRACTIVENESS_WEIGHT_MAP = 'attractiveness_weight_map'

country_of_residence_dtype = pd.CategoricalDtype([FOREIGN_COUNTRY, REPORTING_COUNTRY])

_WEIGHT_NODATA = -1


class TourismRun(inca.Run):
    """Run class for Nature-based Tourism ecosystem accounts."""

    service_name = 'Nature-based Tourism'

    def __init__(self, config):
        """Construct Tourism run from config."""
        super().__init__(config)

        self._progress_weight_run = 0.8  # ~20% of time spent in initial raster checks, ~80% in actual run

        self.config_template.update({
            self.service_name: {
                'base_visits_data': ConfigItem(check_function=check_csv,
                                               required_columns=[DATA_AREA_CODE, YEAR, TYPE_OF_AREA,
                                                                 COUNTRY_OF_RESIDENCE, OVERNIGHT_STAYS],
                                               dtypes={COUNTRY_OF_RESIDENCE: country_of_residence_dtype}),
                'typeAreaContributionTable':
                    ConfigItem(check_function=check_csv, optional=True,
                               required_columns=[TYPE_OF_AREA, COUNTRY_OF_RESIDENCE, CONTRIBUTION_PERCENTAGE],
                               dtypes={COUNTRY_OF_RESIDENCE: country_of_residence_dtype}),
                'step3_ecosystemTypeAttractiveness_table':
                    ConfigItem(check_function=check_csv, optional=True, required_columns=[CODE, WEIGHT],
                               allow_missing=False),
                DATA_AREA_CAT_MAP: ConfigRaster(optional=True, raster_type=RasterType.CATEGORICAL),
                DATA_AREA_TABLE: ConfigItem(check_csv, required_columns=[CODE, WEIGHT], optional=True,
                                            allow_missing=False),
                ACCESSIBILITY_TABLE:
                    ConfigItem(check_function=check_csv, required_columns=[CODE, WEIGHT], optional=True,
                               allow_missing=False),
                FACILITIES_TABLE:
                    ConfigItem(check_function=check_csv, required_columns=[CODE, WEIGHT], optional=True,
                               allow_missing=False),
                ATTRACTIVENESS_TABLE:
                    ConfigItem(check_function=check_csv, required_columns=[CODE, WEIGHT], optional=True,
                               allow_missing=False),
                # For accessbility / facilities / attractiveness, user may either provide a categorical map +
                # reclassification table, or a weight map.
                ACCESSIBILITY_CAT_MAP: {YEARLY: ConfigRaster(optional=True,
                                                             raster_type=RasterType.CATEGORICAL)},
                ACCESSIBILITY_WEIGHT_MAP: {YEARLY: ConfigRaster(optional=True,
                                                                raster_type=RasterType.RELATIVE)},
                FACILITIES_CAT_MAP: {YEARLY: ConfigRaster(optional=True,
                                                          raster_type=RasterType.CATEGORICAL)},
                FACILITIES_WEIGHT_MAP: {YEARLY: ConfigRaster(optional=True,
                                                             raster_type=RasterType.RELATIVE)},
                ATTRACTIVENESS_CAT_MAP: {YEARLY: ConfigRaster(optional=True,
                                                              raster_type=RasterType.CATEGORICAL)},
                ATTRACTIVENESS_WEIGHT_MAP: {YEARLY: ConfigRaster(optional=True,
                                                                 raster_type=RasterType.RELATIVE)}
            }
        })

        self.make_output_filenames()

    def check_data_area_config(self):
        """Check the configuration for data area weights tis right.

        data_area_cat_map and data_area_weights should both be empty, or both should be specified"""
        config = self.config[self.service_name]
        if config[DATA_AREA_CAT_MAP] and not config[DATA_AREA_TABLE]:
            raise ConfigError('To use a data area contribution map, provide a table of weights as well.',
                              [self.service_name, DATA_AREA_TABLE])
        if config[DATA_AREA_TABLE] and not config[DATA_AREA_CAT_MAP]:
            raise ConfigError('To use data area contribution weights, specify a data area contribution map as well.',
                              [self.service_name, DATA_AREA_CAT_MAP])

    def check_proxy_weight_config(self, cat_map, table, weight_map):
        """Check that either `config[cat_map]` and `config[table]` are set, or `config[weight_map]` is set."""
        config = self.config[self.service_name]
        if config[weight_map][self.year]:
            if config[cat_map][self.year] or config[table]:
                raise ConfigError('Specify either a categorical map and a reclassification table, or a weight map.',
                                  [self.service_name, weight_map, self.year])
        else:
            if config[cat_map][self.year] and not config[table]:
                raise ConfigError('Proxy configuration contains a categorical map with no corresponding weight table.',
                                  [self.service_name, table])
            if config[table] and not config[cat_map][self.year]:
                raise ConfigError('Proxy configuration contains a weight table, but no corresponding categorical map.',
                                  [self.service_name, cat_map, self.year])

    def check_spatial_disaggregation_config(self):
        """Check the input configuration for spatial disaggregation is consistent.

        We create a spatial proxy for ecosystem contribution to tourism based on criteria
        - accessibility
        - facilities
        - attractiveness

        For each of these criteria, the user may provide a *either* a categorical map with a reclassification table,
        *or* a weight map.

        """
        self.check_proxy_weight_config(ACCESSIBILITY_CAT_MAP, ACCESSIBILITY_TABLE, ACCESSIBILITY_WEIGHT_MAP)
        self.check_proxy_weight_config(FACILITIES_CAT_MAP, FACILITIES_TABLE, FACILITIES_WEIGHT_MAP)
        self.check_proxy_weight_config(ACCESSIBILITY_CAT_MAP, ACCESSIBILITY_TABLE, ACCESSIBILITY_WEIGHT_MAP)

    def make_output_filenames(self):
        """Generate the names for the output files, taking into account location of the temporary folder."""
        self.ecotype_reclass_weights = os.path.join(self.temp_dir, 'tourism_map_ecotype-reclass-weights_{year}.tif')
        self.attract_reclass_weights = os.path.join(self.temp_dir, 'tourism_map_attract-reclass-weights_{year}.tif')
        self.access_reclass_weights = os.path.join(self.temp_dir, 'tourism_map_access-reclass-weights_{year}.tif')
        self.facilities_reclass_weights = os.path.join(self.temp_dir,
                                                       'tourism_map_facilities-reclass-weights_{year}.tif')
        self.final_weight_map = os.path.join(self.temp_dir, 'tourism_map_weights_{year}.tif')
        self.weight_proportional = os.path.join(self.temp_dir, 'tourism_map_weight-proportional_{year}.tif')
        self.supply_map_national = os.path.join(self.maps,
                                                'tourism_map_supply_amountOvernightStays-national_{year}.tif')
        self.supply_map_foreign = os.path.join(self.maps,
                                               'tourism_map_supply_amountOvernightStays-foreign_{year}.tif')
        self.use = os.path.join(self.statistic_dir, 'tourism_statistics_use_amountOvernightStays_{year}.csv')
        self.supply = os.path.join(self.statistic_dir, 'tourism_statistics_supply_amountOvernightStays_{year}.csv')
        self.sut = os.path.join(self.suts, 'tourism_report_SUT_amountOvernightStays_{year}.xlsx')
        self.supply_map = os.path.join(self.maps, 'tourism_map_supply_amountOvernightStays_{year}.tif')

    def _start(self):
        """Start the Tourism calculation."""
        # set year for the current run
        self.year = self.years[0]
        if len(self.years) > 1:
            logger.warning('Nature-based Tourism ecosystem account can only run for a single year at a time.  '
                           'Calculating results for year %d', self.year)

        self.check_data_area_config()
        self.check_spatial_disaggregation_config()

        tourist_data = self.step1_MAIN_read_tourist_visit()

        data_area_contribution_table = None
        progress_remaining = 1.
        if self.config[self.service_name][DATA_AREA_CAT_MAP]:
            data_area_contribution_table = self.make_data_area_contribution_table(
                add_progress=lambda p: self.add_progress(0.15 * p))
            progress_remaining = 0.85
            data_area_contribution_table.to_csv(os.path.join(self.temp_dir, 'data_area_contribution_table.csv'))
        tourist_data = self.step2_MAIN_allocate_nature_visits(tourist_data, data_area_contribution_table)

        # Get overnight stays indexed by NUTS_ID, CountryOfResidence and Ecosystem type:
        stats = self.step3_MAIN_allocate_eco_type_contribution(
            tourist_data, add_progress=lambda p: self.add_progress(progress_remaining * p))
        # comment
        stats = stats.round().astype(int) # round here, so that use and supply result into the same totals (fix rounding issue INCA-1370)
        use_table = stats.groupby([NUTS_ID, COUNTRY_OF_RESIDENCE]).sum().round().astype(int)
        use_table = use_table.unstack(level=COUNTRY_OF_RESIDENCE)
        use_table.columns = use_table.columns.values  # Replace CategoricalIndex resulting from unstack by plain columns
        use_table = use_table.rename(columns={FOREIGN_COUNTRY: EXP, REPORTING_COUNTRY: HSH})
        logger.debug('Use table:\n%s', use_table)
        use_table.rename(columns=dict(df_sectors[['Sector', 'short_name']].values)).to_csv(
            self.use.format(year=self.year))

        supply_table = stats.groupby([NUTS_ID, ecosystem.ECOTYPE]).sum().round().astype(int)
        supply_table = supply_table.unstack(level=ecosystem.ECOTYPE)
        supply_table.columns = supply_table.columns.values
        logger.debug('Supply table:\n%s', supply_table)
        supply_table.rename(
            columns=dict(ecosystem.types_l1[['name', 'short_name']].values)).to_csv(self.supply.format(year=self.year))

        write_sut(self.sut.format(year=self.year), supply_table, use_table,
                  self.service_name, self.year, 'number of overnight stays')

        for map in (self.supply_map.format(year=self.year),
                    self.supply_map_foreign.format(year=self.year), self.supply_map_national.format(year=self.year)):
            self.cut_to_reporting_regions(map)

    def step1_MAIN_read_tourist_visit(self):
        """Read overnight stays data.

        Overnight statys csv should contain these columns:
        - Year (fe. 2018)
        - CountryOfResidence (fe. Foreign country)
        - TypeOfArea (fe. Cities)
        - DataAreaCode (fe. NL11)

        :return tourist_data: dataframe with visit data per county, data area,...
        """
        logger.debug("Start Step1: read tourist visit data")

        # read in the base visit data table
        base_data = pd.read_csv(self.config[self.service_name]["base_visits_data"],
                                dtype={'Year': pd.Int16Dtype(),
                                       COUNTRY_OF_RESIDENCE: country_of_residence_dtype,
                                       TYPE_OF_AREA: 'category',
                                       DATA_AREA_CODE: 'category'}, index_col=0)
        base_data = base_data.loc[~base_data['Year'].isna() & (base_data['Year'] == self.year)]
        if base_data.empty:
            raise Error(f'file {self.config[self.service_name]["base_visits_data"]} has no data for year {self.year}.')
        tourist_data = base_data[base_data[DATA_AREA_CODE].isin(self.statistics_shape.index)].reset_index(drop=True)
        tourist_data.dropna(subset=[OVERNIGHT_STAYS], inplace=True)
        logger.debug('Tourist data:\n%s', tourist_data)
        self.check_NUTStable_shape(tourist_data)

        return tourist_data

    def check_NUTStable_shape(self, tourist_data):
        """Check if the DataAreaCodes in the table provided match the ones from the shapefile.

        :param tourist_data: dataframe with visit data per country, dataArea,...
        """
        # compare lists
        missing = ~self.statistics_shape.index.isin(tourist_data[DATA_AREA_CODE])

        if missing.any():
            raise Error("These statistics shapefile id's are missing from the Overnight stays data: "
                        f"{', '.join(self.statistics_shape.index[missing])}.")

    def step2_MAIN_allocate_nature_visits(self, tourist_data, data_area_contribution_table=None):
        """Attribute overnight stays to ecosystems, based on typeArea and dataArea contribution tables.

        :param tourist_data: the dataframe containing the visit data. See step1_MAIN_read_tourist_visit.

        :return tourist_data: tourist_data dataframe is enhanced with NUTS_IDs from self.shapefile_areas
        """
        logger.debug("Step2: allocate amount of nature visits from overnight stays")

        # detect number of different "TypeOfArea"
        # todo make a check if these are also present in TypeOfArea table
        # uniqueTypeOfArea = tourist_data["TypeOfArea"].unique()

        tourist_data = self.step2_typeArea(tourist_data)

        # If user has configured data area map and table:
        if data_area_contribution_table is not None:
            tourist_data = self.step2_dataArea(tourist_data, data_area_contribution_table)

        return tourist_data

    def step2_typeArea(self, visits_data):
        """Read in the default table specifying which Areas get which contribution to ecosystem related visits.

        :param visits_data: input tourist_data dataframe

        :return visits_data: tourist_data datafrane enhanced with default contributions
        """
        config_filename = self.config[self.service_name]["typeAreaContributionTable"]
        ref = importlib_resources.files(inca.data).joinpath('tourism_typeAreaContributionTable.csv')
        with open(ref, 'rb') as default:
            file = config_filename if config_filename else default
            typeOfArea_contribution_table = pd.read_csv(file, dtype={TYPE_OF_AREA: 'category',
                                                                     COUNTRY_OF_RESIDENCE: country_of_residence_dtype})

        # give each TypeOfArea a weight
        visits_data = visits_data.merge(typeOfArea_contribution_table[["TypeOfArea",
                                                                       COUNTRY_OF_RESIDENCE,
                                                                       CONTRIBUTION_PERCENTAGE]],
                                        left_on=[COUNTRY_OF_RESIDENCE, TYPE_OF_AREA],
                                        right_on=[COUNTRY_OF_RESIDENCE, TYPE_OF_AREA],
                                        how="left")

        # go over each Area, multiply each visit with value found in above table
        visits_data["eco_contri"] = visits_data[OVERNIGHT_STAYS] * visits_data[CONTRIBUTION_PERCENTAGE]

        return visits_data

    def step2_dataArea(self, visits_data, data_area_contribution_table):
        """Multiply visit data for each dataArea with a weight from data_area_contribution_table, if provided.

        :param visits_data: input tourist_data dataframe
        :return visits_data: tourist_data dataframe enhanced with ecosystem contribution data
        """
        # give each DataArea a weight
        visits_data = visits_data.merge(data_area_contribution_table[CONTRIBUTION_PERCENTAGE],
                                        left_on=[DATA_AREA_CODE],
                                        right_index=True,
                                        how="left")

        # go over each Area, multiply each visit with value found in above table
        visits_data["eco_contri_update"] = visits_data[OVERNIGHT_STAYS] * visits_data["ContributionPercentage_y"]
        visits_data.loc[visits_data["eco_contri_update"].isna(), "eco_contri_update"] = visits_data["eco_contri"]
        visits_data["eco_contri"] = visits_data["eco_contri_update"]

        return visits_data

    def step3_MAIN_allocate_eco_type_contribution(self, tourist_data, add_progress=lambda p: None):
        """Allocate the total ecosystem contribution to overnight stays to specific ecosystems.

        :param tourist_data: input tourist_data dataframe
        :param: add_progress: progress bar update callback

        :return supply: dataframe with visits allocated to ecosystems
        """
        logger.debug("Start Step 3: allocate ecosystemtype contribution")

        # select the input maps and multiply them to obtain the final weight map.
        self.step3_ecosystemWeights(add_progress=lambda p: self.add_progress(0.5 * p))
        logger.debug('progress after weights: %s', self._progress)

        # Allocate visits to ecosystems
        # disaggregate tourist_data based on weights:
        eco_sum = tourist_data.groupby([DATA_AREA_CODE,
                                        COUNTRY_OF_RESIDENCE])[ECO_CONTRI].sum()
        final_weight_map = self.final_weight_map.format(year=self.year)
        supply_map_national = self.supply_map_national.format(year=self.year)
        supply_map_foreign = self.supply_map_foreign.format(year=self.year)
        supply_map = self.supply_map.format(year=self.year)

        proxy_sums = self.accord.spatial_disaggregation_byArea(final_weight_map,
                                                               eco_sum.xs(REPORTING_COUNTRY,
                                                                          level=COUNTRY_OF_RESIDENCE),
                                                               self.statistics_raster, self.statistics_shape[SHAPE_ID],
                                                               supply_map_national,
                                                               lambda p: add_progress(0.1 * p),
                                                               unit_info='Number of overnight stays.',
                                                               processing_info='Spatial disaggregation of overnight '
                                                               'stays by visitors from reporting country.')
        logger.debug('progress after disaggregation reporting cntr: %s', self._progress)
        self.accord.spatial_disaggregation_byArea(final_weight_map,
                                                  eco_sum.xs(FOREIGN_COUNTRY, level=COUNTRY_OF_RESIDENCE),
                                                  self.statistics_raster, self.statistics_shape[SHAPE_ID],
                                                  supply_map_foreign,
                                                  lambda p: add_progress(0.1 * p), proxy_sums=proxy_sums,
                                                  unit_info='Number of overnight stays.',
                                                  processing_info='Spatial disaggregation of overnight stays '
                                                  'by visitors from foreign countries.')
        logger.debug('progress after disaggregation foreign: %s', self._progress)
        with rasterio.open(supply_map, 'w', **dict(self.accord.ref_profile.copy(),
                                                   dtype=np.float32)) as ds_out:
            sum_rasters(ds_out, supply_map_national, supply_map_foreign, lambda p: add_progress(0.1 * p))
            self.accord.metadata.update_dataset_tags(ds_out,
                                                     f'Nature-based Tourism supply for year {self.year}',
                                                     'Number of overnight stays',
                                                     supply_map_national, supply_map_foreign)
        logger.debug('progress after sum_rasters: %s', self._progress)

        # Calculate stats for supply_national and supply_foreign separately:
        stats_national = statistics_byArea_byET(supply_map_national,
                                                self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                                self.get_ecosystem_raster(self.year, level=1),
                                                ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                                lambda p: add_progress(0.1 * p))
        logger.debug('progress after supply stats national %s:', self._progress)
        stats_foreign = statistics_byArea_byET(supply_map_foreign,
                                               self.reporting_raster, self.reporting_shape[SHAPE_ID],
                                               self.get_ecosystem_raster(self.year, level=1),
                                               ecosystem.types_l1.set_index('name')[ecosystem.ECO_ID],
                                               lambda p: add_progress(0.1 * p))
        logger.debug('progress after supply stats foreign %s:', self._progress)

        # Assemble all into a single dataframe, indexed by (NUTS_ID, ECOTYPE, COUNTRY_OF_RESIDENCE)
        stats_national[COUNTRY_OF_RESIDENCE] = REPORTING_COUNTRY
        stats_foreign[COUNTRY_OF_RESIDENCE] = FOREIGN_COUNTRY
        result = pd.concat([stats_national, stats_foreign]).astype(
            {COUNTRY_OF_RESIDENCE: country_of_residence_dtype}).set_index(COUNTRY_OF_RESIDENCE, append=True)
        # Return only the 'sum' column
        return result['sum']

    def step3_ecosystemWeights(self, add_progress):
        """Load the seperate weight maps and multiply them to build the final weight map.

        - weights based in ecosystem type
        - weights based on accessibility
        - weights based on facilities
        - weights based on Landscape attractiveness
        """
        logger.debug("Step 3: Calculating Landcover Weights")
        config = self.config[self.service_name]
        ecosystem_attractiveness_table = config["step3_ecosystemTypeAttractiveness_table"]
        if not ecosystem_attractiveness_table:  # None or empty -> use default
            ref = importlib_resources.files(inca.data).joinpath('tourism_ecosystemTypeAttractivenessTable.csv')
            with ref.open('r') as file:
                df_table = pd.read_csv(file, usecols=[CODE, WEIGHT], dtype={WEIGHT: np.float32})
        else:
            df_table = read_weight_table(ecosystem_attractiveness_table)

        # Accessibility / Facilities / Attractiveness wieghts:
        # Load categorical or weight maps depending on whether weight reclassification tables are provided or not:
        accessibility_weights = config[ACCESSIBILITY_TABLE]
        if accessibility_weights:
            accessibility_map = config[ACCESSIBILITY_CAT_MAP][self.year]
        else:
            accessibility_map = config[ACCESSIBILITY_WEIGHT_MAP][self.year]

        facilities_weights = config[FACILITIES_TABLE]
        if facilities_weights:
            facilities_map = config[FACILITIES_CAT_MAP][self.year]
        else:
            facilities_map = config[FACILITIES_WEIGHT_MAP][self.year]

        attractiveness_weights = config[ATTRACTIVENESS_TABLE]
        if attractiveness_weights:
            attractiveness_map = config[ATTRACTIVENESS_CAT_MAP][self.year]
        else:
            attractiveness_map = config[ATTRACTIVENESS_WEIGHT_MAP][self.year]

        # Weigh progress bar contribution from reclass and multiplications:
        # number of operations can change depending on configuration (number of provided maps and reclass tables)
        # reclass operations: ecosystem classification + 1 for every weight table provided:
        num_reclass = 2 + sum(1 for table in [accessibility_weights, facilities_weights, attractiveness_weights]
                              if table)
        # multiplications: 1 for every map provided
        num_multiplications = 1 + sum(1 for map in [accessibility_map, facilities_map, attractiveness_map] if map)

        progress_per_reclass = 1. / (num_reclass + num_multiplications)
        progress_multiplications = 1. * num_multiplications / (num_reclass + num_multiplications)

        def add_progress_reclass(p):
            add_progress(progress_per_reclass * p)

        def add_progress_multiplication(p):
            add_progress(progress_multiplications * p)

        # ecosystem type raster already computed earlier, so no progress bar contribution from this call
        ecotype_raster = self.get_ecosystem_raster(self.year, add_progress=add_progress_reclass)
        # weight_map step 1: weights based on ecosystem type
        ecotype_weight_map = self.ecotype_reclass_weights.format(year=self.year)
        self.reclassify_weight_raster(ecotype_raster, df_table, ecotype_weight_map, add_progress_reclass)

        # weight map step 2: Accessibility
        logger.debug("Step 3: Calculating accessibility Weights")
        if accessibility_weights:
            accessibility_weight_map = self.access_reclass_weights.format(year=self.year)
            self.reclassify_weight_raster(accessibility_map,
                                          read_weight_table(accessibility_weights),
                                          accessibility_weight_map, add_progress_reclass)
            accessibility_map = accessibility_weight_map

        # weight map step 3: Facilities
        logger.debug("Step 3: Calculating facilities Weights")
        if facilities_weights:
            facilities_weight_map = self.facilities_reclass_weights.format(year=self.year)
            self.reclassify_weight_raster(facilities_map,
                                          read_weight_table(facilities_weights),
                                          facilities_weight_map, add_progress_reclass)
            facilities_map = facilities_weight_map

        # weight map step 4: Landscape attractiveness
        logger.debug("Step 3: Calculating attractiveness Weights")
        if attractiveness_weights:
            attractiveness_weight_map = self.attract_reclass_weights.format(year=self.year)
            self.reclassify_weight_raster(attractiveness_map,
                                          read_weight_table(attractiveness_weights),
                                          attractiveness_weight_map, add_progress_reclass)
            attractiveness_map = attractiveness_weight_map

        # Multiply all provided weight maps in to a final map
        self.multiply_weight_rasters([map for map in (ecotype_weight_map, accessibility_map,
                                                      facilities_map, attractiveness_map) if map],
                                     add_progress_multiplication)

    def make_data_area_contribution_table(self, add_progress):
        """Use the provided data area category map and weight table to create a data area contribution table."""
        # Reclassify data area contribution category using the weight table:
        file_weight_map = os.path.join(self.temp_dir, 'data_area_weight_map.tif')
        self.reclassify_weight_raster(self.config[self.service_name][DATA_AREA_CAT_MAP],
                                      pd.read_csv(self.config[self.service_name][DATA_AREA_TABLE],
                                                  usecols=[CODE, WEIGHT]),
                                      file_weight_map, add_progress=add_progress)
        # Calculate average weight per data area:
        stats = statistics_byArea(file_weight_map, self.statistics_raster, self.statistics_shape[SHAPE_ID],
                                  add_progress)
        return pd.DataFrame({CONTRIBUTION_PERCENTAGE: stats[SUM] / stats[COUNT]})

    def reclassify_weight_raster(self, raster, table, filename, add_progress, block_shape=(2048, 2048)):
        """Reclassify a categorical raster to a weight raster."""
        reclass_dict = dict(zip(table[CODE], table[WEIGHT]))
        logger.debug('input raster: %s\noutput raster: %s\nreclass_dict:\n%s', raster, filename, reclass_dict)
        self.accord.metadata.read_raster_tags([raster])
        with rasterio.open(raster) as ds, \
            rasterio.open(filename, 'w', **dict(ds.profile,
                                                dtype=np.float32,
                                                compress='DEFLATE',
                                                nodata=_WEIGHT_NODATA)) as ds_out:
            progress_block = 100. / number_blocks(ds_out.profile, block_shape)
            for _, window in block_window_generator(block_shape, ds.profile['height'], ds.profile['width']):
                input = ds.read(1, window=window)
                reclass, _ = classification.reclassification(raster_in=input,
                                                             dict_classes=reclass_dict,
                                                             nodata_in=ds.nodata,
                                                             nodata_out=ds_out.nodata,
                                                             outputtype=np.float32)
                ds_out.write(reclass, window=window, indexes=1)
                add_progress(progress_block)
            self.accord.metadata.update_dataset_tags(ds_out, 'Weight raster for Nature-based Tourism', 'dimensionless', raster)

    def multiply_weight_rasters(self, weight_rasters, add_progress, block_shape=(2048, 2048)):
        """Multiply a list of weight rasters to obtain the final weight raster.

        A pixel in the resulting raster shall have value NODATA as soon as one or more of the input raster values for
        that pixel is is NODATA.
        """
        with ExitStack() as stack:
            datasets = [stack.enter_context(rasterio.open(filename)) for filename in weight_rasters]
            with rasterio.open(self.final_weight_map.format(year=self.year), 'w', **datasets[0].profile) as ds_out:
                progress_step = 100. / (number_blocks(ds_out.profile, block_shape) * len(datasets))
                for _, window in block_window_generator(block_shape, ds_out.profile['height'], ds_out.profile['width']):
                    (row_start, row_end), (col_start, col_end) = window
                    combined_weights = np.ones((row_end - row_start, col_end - col_start), dtype=np.float32)
                    combined_nodata = False
                    for ds in datasets:
                        weights = ds.read(1, window=window)
                        # TODO Check user didn't provide weight raster or reclass table with neg values *at run start*.
                        nodata = (weights == ds.nodata) | (weights < 0)
                        combined_weights[nodata] = _WEIGHT_NODATA  # set new nodata values
                        combined_nodata |= nodata
                        combined_weights[~combined_nodata] *= weights[~combined_nodata]
                        add_progress(progress_step)
                    ds_out.write(combined_weights, window=window, indexes=1)
                self.accord.metadata.update_dataset_tags(ds_out,
                                                         'Final weight raster for Nature-based Tourism spatial disaggregation.',
                                                         'dimenstionless', *weight_rasters)


def sum_rasters(ds_out, raster1, raster2, add_progress, block_shape=(2048, 2048)):
    """Add two rasters using block processing.  Propagate nodata pixels from both."""
    with rasterio.open(raster1) as ds1, rasterio.open(raster2) as ds2:
        progress_block = 100. / number_blocks(ds_out.profile, block_shape)
        for _, window in block_window_generator(block_shape, ds_out.profile['height'], ds_out.profile['width']):
            data1 = ds1.read(1, window=window)
            data2 = ds2.read(1, window=window)
            result = np.empty_like(data1)
            have_data = (data1 != ds1.nodata) & (data2 != ds2.nodata)
            result[~have_data] = ds_out.nodata
            result[have_data] = data1[have_data] + data2[have_data]
            ds_out.write(result, window=window, indexes=1)
            add_progress(progress_block)


def read_weight_table(filename):
    """Read a CSV file containing weights for a reclassification."""
    return pd.read_csv(filename, usecols=[CODE, WEIGHT], dtype={WEIGHT: np.float32})
